/**
 * @file    uGtFinorPreviewProc.cc
 * @author  Gregor Aradi, Bernhard Arnold
 * @brief   Finor preview board processor implementation
 * @date    November 2016
 */
#include "ugt/swatch/uGtFinorPreviewProc.h"
#include "ugt/swatch/uGtFinorCommands.h"

// SWATCH header
#include "swatch/core/Factory.hpp"
#include "swatch/action/Command.hpp"
#include "swatch/action/CommandSequence.hpp"
#include "swatch/action/StateMachine.hpp"

#include "ugt/swatch/uGtFinorPreviewAlgo.h"

SWATCH_REGISTER_CLASS(ugt::uGtFinorPreviewProc);

namespace amc502{
  class AMC502Processor;
}

namespace ugt
{

uGtFinorPreviewProc::uGtFinorPreviewProc(const ::swatch::core::AbstractStub& aStub) :
 amc502::AMC502Processor(aStub)
{
  // Add algo interface
  registerInterface(new uGtFinorPreviewAlgo(this->driver()));

  typedef ::swatch::action::CommandSequence tComSeq;
  typedef ::swatch::action::Command tCmd;

  // Add commands
  tCmd& cmdInputMask = registerCommand<uGtFinorInputMaskCommand>("uGtFinorInputMask");

  // Setup sequence
  tComSeq& seqSetup = registerSequence("uGtFinorPreviewSeqSetup", CmdIds::kReset)
    .then(CmdIds::kPayloadReset);

  // Configure sequence
  tComSeq& seqConfigure = registerSequence("uGtFinorPreviewConfigure", cmdInputMask);

  // add commands/sequences to the state machine transitions
  swatch::processor::RunControlFSM& lFSM = getRunControlFSM();
  lFSM.setup.add(seqSetup);
  lFSM.configure.add(seqConfigure);
  lFSM.start.add(getCommand(CmdIds::kResetCounter));
}


uGtFinorPreviewProc::~uGtFinorPreviewProc()
{
}

} // namespace ugt
