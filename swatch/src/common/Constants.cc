#include "ugt/swatch/Constants.h"



namespace ugt
{

const size_t kOrbitLength = 3564;
const size_t kMaxAlgorithms = 512;
const size_t kMaxPrescaleSets = 256;
const size_t kPrescalePrecision = 2;
const size_t kMaxUgtModules = 6;

const double kLhcClockFrequency = 40.07897e6;
const unsigned int kOrbitsPerLuminositySegment = pow(2, 18);
const unsigned int kMaximumCounts = kOrbitLength * kOrbitsPerLuminositySegment;
const double kLuminositySegmentInSeconds = kMaximumCounts / kLhcClockFrequency; // 23.31 seconds
const double kToRate = 1. / kLuminositySegmentInSeconds;


} // namespace ugt
