#include "ugt/swatch/uGtAlgo.h"

#include "ugt/swatch/Constants.h"
#include "ugt/swatch/uGtProcessor.h"

#include "swatch/action/MonitoringSetting.hpp"
#include "swatch/core/MetricConditions.hpp"

#include "mp7/MP7Controller.hpp"
#include "uhal/HwInterface.hpp"

#include "log4cplus/loggingmacros.h"

#include <stdexcept>
#include <limits>


namespace ugt
{

const std::string ALGO_COUNT = "ALGO";
const std::string ALGO_RATE = "RATE";
const std::string ALGO_PREVIEW_COUNT = "PREVIEW_CNT";
const std::string ALGO_PREVIEW_RATE = "PREVIEW_RATE";
const std::string ALGO_COUNT_BEFORE_PRESCALE = "BPALGO";
const std::string ALGO_RATE_BEFORE_PRESCALE = "BPRATE";
const std::string POST_DEADTIME_ALGO_COUNT = "PDALGO";
const std::string POST_DEADTIME_ALGO_RATE = "PDRATE";
const std::string PRESCALE = "PRESCALE";
const std::string SEPARATOR = "_";

const std::string FINOR_COUNT = "FINOR_CNT";
const std::string FINOR_RATE = "FINOR_RATE";


uGtAlgo::uGtAlgo(mp7::MP7Controller& driver)
  : driver_(driver)
  , metricPsIndex_(registerMetric<uint32_t>("prescaleColumnIndex"))
  , metricPsIndexInUse_(registerMetric<uint32_t>("prescaleColumnIndexInUse"))
  , metricPreviewPsIndex_(registerMetric<uint32_t>("prescColIndexPreview"))
  , metricPreviewPsIndexInUse_(registerMetric<uint32_t>("prescColIndexInUsePreview"))
  , metricBxNumberMax_(registerMetric<uint32_t>("bunchCrossingNumberMax"))
  , metricLuminositySegmentNumber_(registerMetric<uint32_t>("luminositySegmentNumber"))
  , metricOrbitCounter_(registerMetric<uint64_t>("orbitCounter"))
  , metricFinOrCounter_(registerMetric<uint32_t>("LocalFinOR"))
  , metricFinOrRate_(registerMetric<double>("RATE_LocalFinOR"))
  , metricVetoCounter_(registerMetric<uint32_t>("LocalVeto"))
  , metricVetoRate_(registerMetric<double>("RATE_LocalVeto"))
  , metricL1aCounter_(registerMetric<uint32_t>("L1a"))
  , metricL1aRate_(registerMetric<double>("RATE_L1a"))
  , metricPreviewFinOrCounter_(registerMetric<uint32_t>("LocalFinORPreview"))
  , metricPreviewFinOrRate_(registerMetric<double>("RATE_LocalFinORPreview"))
  , metricOtf_(registerMetric<uint32_t>("prescOtf"))
  , metricOtfInUse_(registerMetric<uint32_t>("prescOtfInUse"))
  , metricPreviewOtf_(registerMetric<uint32_t>("prescOtfPreview"))
  , metricPreviewOtfInUse_(registerMetric<uint32_t>("prescOtfInUsePreview"))
  , metricRateCounters_()
  , metricRates_()
  , metricPreviewRateCounters_()
  , metricPreviewRates_()
  , metricRateCountersBeforePrescale_()
  , metricRatesBeforePrescale_()
  , metricPostDeadtimeRateCounters_()
  , metricPostDeadtimeRates_()
  , metricPrescales_()
  , slotId_(-1)
  , triggerMenu_()
  , finorMask_()
  , prescales_()
{
  for (size_t ii = 0; ii < kMaxAlgorithms; ii++)
  {
    std::stringstream ss;
    ss << ALGO_COUNT << SEPARATOR << std::setfill('0') << std::setw(3) << ii;
    metricRateCounters_.push_back(&registerMetric<uint32_t>(ss.str(), ::swatch::core::GreaterThanCondition<uint32_t>(kMaximumCounts)));

    ss.clear();
    ss.str(std::string());
    ss << ALGO_RATE << SEPARATOR << std::setfill('0') << std::setw(3) << ii;
    metricRates_.push_back(&registerMetric<double>(ss.str(), ::swatch::core::GreaterThanCondition<double>(kLhcClockFrequency)));

    ss.clear();
    ss.str(std::string());
    ss << ALGO_COUNT_BEFORE_PRESCALE << SEPARATOR << std::setfill('0') << std::setw(3) << ii;
    metricRateCountersBeforePrescale_.push_back(&registerMetric<uint32_t>(ss.str(), ::swatch::core::GreaterThanCondition<uint32_t>(kMaximumCounts)));

    ss.clear();
    ss.str(std::string());
    ss << ALGO_RATE_BEFORE_PRESCALE << SEPARATOR << std::setfill('0') << std::setw(3) << ii;
    metricRatesBeforePrescale_.push_back(&registerMetric<double>(ss.str(), ::swatch::core::GreaterThanCondition<double>(kLhcClockFrequency)));

    ss.clear();
    ss.str(std::string());
    ss << POST_DEADTIME_ALGO_COUNT << SEPARATOR << std::setfill('0') << std::setw(3) << ii;
    metricPostDeadtimeRateCounters_.push_back(&registerMetric<uint32_t>(ss.str(), ::swatch::core::GreaterThanCondition<uint32_t>(kMaximumCounts)));

    ss.clear();
    ss.str(std::string());
    ss << POST_DEADTIME_ALGO_RATE << SEPARATOR << std::setfill('0') << std::setw(3) << ii;
    metricPostDeadtimeRates_.push_back(&registerMetric<double>(ss.str(), ::swatch::core::GreaterThanCondition<double>(kLhcClockFrequency)));

    ss.clear();
    ss.str(std::string());
    ss << PRESCALE << SEPARATOR << std::setfill('0') << std::setw(3) << ii;
    metricPrescales_.push_back(&registerMetric<double>(ss.str()));


    ss.clear();
    ss.str(std::string());
    ss << ALGO_PREVIEW_COUNT << SEPARATOR << std::setfill('0') << std::setw(3) << ii;
    metricPreviewRateCounters_.push_back(&registerMetric<uint32_t>(ss.str(), ::swatch::core::GreaterThanCondition<uint32_t>(kMaximumCounts)));

    ss.clear();
    ss.str(std::string());
    ss << ALGO_PREVIEW_RATE << SEPARATOR << std::setfill('0') << std::setw(3) << ii;
    metricPreviewRates_.push_back(&registerMetric<double>(ss.str(), ::swatch::core::GreaterThanCondition<double>(kLhcClockFrequency)));

  }
}


uGtAlgo::~uGtAlgo()
{
}


void
uGtAlgo::setSlotId(const uint32_t id)
{
  std::cout << "setSlotId = " << id << std::endl;
  slotId_ = id;
}

uint32_t
uGtAlgo::getModuleId() const
{
  return (slotId_-1);
}

uGtTriggerMenu&
uGtAlgo::getTriggerMenu()
{
  return triggerMenu_;
}

void
uGtAlgo::setFinorMask(const xdata::Table* p)
{
  finorMask_ = p;
  triggerMenu_.setFinorMaskContainer(p);
}

void
uGtAlgo::setPrescales(const xdata::Table* p, const xdata::Table* pMask)
{
  prescales_ = p;
  triggerMenu_.setPrescalesContainer(p, pMask);
}

void
uGtAlgo::setAlgoBxMask(const xdata::Table* p)
{
  algoBxMask_ = p;
  triggerMenu_.setAlgoBxMaskContainer(p);
}

void
uGtAlgo::setVetoMask(const xdata::Table* p)
{
  vetoMask_ = p;
  triggerMenu_.setVetoMaskContainer(p);
}

uint32_t
uGtAlgo::getBxNumberMax() const
{
  ::uhal::HwInterface& hw = driver_.hw();

  const ::uhal::Node& node = hw.getNode("gt_mp7_frame.rb.tcm_status.bx_nr_max");
  ::uhal::ValWord<uint32_t> value = node.read();

  hw.getClient().dispatch();

  return value;
}


uint64_t
uGtAlgo::getOrbitNumber() const
{
  ::uhal::HwInterface& hw = driver_.hw();

  const ::uhal::Node& node_l = hw.getNode("gt_mp7_frame.rb.tcm_status.orbit_nr_l");
  ::uhal::ValWord<uint32_t> value_l = node_l.read();

  const ::uhal::Node& node_h = hw.getNode("gt_mp7_frame.rb.tcm_status.orbit_nr_h");
  ::uhal::ValWord<uint32_t> value_h = node_h.read();

  hw.getClient().dispatch();

  return static_cast<uint64_t>(value_h) << 32 | value_l;
}


uint32_t
uGtAlgo::getLuminositySection() const
{
  ::uhal::HwInterface& hw = driver_.hw();

  const ::uhal::Node& node_luminosity_seg_nr = hw.getNode("gt_mp7_frame.rb.tcm_status.luminosity_seg_nr");
  ::uhal::ValWord<uint32_t> data_luminosity_seg_nr = node_luminosity_seg_nr.read();

  hw.getClient().dispatch();

  return data_luminosity_seg_nr;
}


double
uGtAlgo::getSecondsBeforeLuminositySegmentUpdate(log4cplus::Logger* logger) const
{
  uint64_t orbit_nr = getOrbitNumber();

  double position = kLuminositySegmentInSeconds * (1. - (orbit_nr % kOrbitsPerLuminositySegment) / double(kOrbitsPerLuminositySegment));

  if (logger)
  {
    std::stringstream ss;
    ss << "uGtAlgo::getSecondsBeforeLuminositySegmentUpdate: " << position;
    LOG4CPLUS_INFO(*logger, ss.str());
  }
  else
  {
    std::cout << "uGtAlgo::getSecondsBeforeLuminositySegmentUpdate: " << position << "\n";
  }

  return position;
}


void
uGtAlgo::retrieveMetricValues()
{
  ::uhal::HwInterface& hw = driver_.hw();

  //
  // check trigger scalers
  //
  uint32_t data_finor_counter = std::numeric_limits<unsigned int>::max();
  uint32_t data_veto_counter = std::numeric_limits<unsigned int>::max();
  uint32_t data_l1a_counter = std::numeric_limits<unsigned int>::max();
  uint32_t data_ps_idx = std::numeric_limits<unsigned int>::max();
  uint32_t data_ps_idx_in_use = std::numeric_limits<unsigned int>::max();
  uint32_t data_ps_idx_preview = std::numeric_limits<unsigned int>::max();
  uint32_t data_ps_idx_in_use_preview = std::numeric_limits<unsigned int>::max();
  uint32_t data_lumi_seg_nr = std::numeric_limits<unsigned int>::max();
  uint32_t data_finor_counter_preview = std::numeric_limits<unsigned int>::max();

  uint32_t data_otf = std::numeric_limits<unsigned int>::max();
  uint32_t data_otf_preview = std::numeric_limits<unsigned int>::max();
  uint32_t data_otf_in_use = std::numeric_limits<unsigned int>::max();
  uint32_t data_otf_in_use_preview = std::numeric_limits<unsigned int>::max();

  std::vector<uint32_t> data_scaler_before_prescale;
  std::vector<uint32_t> data_scaler;
  std::vector<uint32_t> data_scaler_preview;
  std::vector<uint32_t> data_scaler_post_dead_time;
  std::vector<double> data_prescales;

  // Avoid luminosity segment change durring
  unsigned int loop = 0;
  while (true)
  {
    if (loop) std::cout << "uGtAlgo::retrieveMetricValues(): loop = " << loop++ << "\n";

    // check luminosity segment number
    const ::uhal::Node& node_lumi_seg_nr = hw.getNode("gt_mp7_frame.rb.tcm_status.luminosity_seg_nr");
    ::uhal::ValWord<uint32_t> data_lumi_seg_nr_before = node_lumi_seg_nr.read();

    hw.getClient().dispatch();

    const ::uhal::Node& node_finor_counter = hw.getNode("gt_mp7_gtlfdl.rate_cnt_finor");
    ::uhal::ValWord<uint32_t> _data_finor_counter = node_finor_counter.read();

    const ::uhal::Node& node_veto_counter = hw.getNode("gt_mp7_gtlfdl.rate_cnt_veto");
    ::uhal::ValWord<uint32_t> _data_veto_counter = node_veto_counter.read();

    const ::uhal::Node& node_l1a_counter = hw.getNode("gt_mp7_gtlfdl.rate_cnt_l1a");
    ::uhal::ValWord<uint32_t> _data_l1a_counter = node_l1a_counter.read();

    const ::uhal::Node& node_ps_idx = hw.getNode("gt_mp7_gtlfdl.prescale_factor_set_index");
    ::uhal::ValWord<uint32_t> _data_ps_idx = node_ps_idx.read();

    const ::uhal::Node& node_ps_idx_in_use = hw.getNode("gt_mp7_gtlfdl.previous_prescale_set_index");
    ::uhal::ValWord<uint32_t> _data_ps_idx_in_use = node_ps_idx_in_use.read();

    ::uhal::ValWord<uint32_t> _data_ps_idx_preview = hw.getNode("gt_mp7_gtlfdl.preview.prescale_factor_set_index").read();

    ::uhal::ValWord<uint32_t> _data_ps_idx_in_use_preview = hw.getNode("gt_mp7_gtlfdl.preview.previous_prescale_set_index").read();

    // otf flags
    ::uhal::ValWord<uint32_t> _data_otf = hw.getNode("gt_mp7_gtlfdl.prescale_otf_flags.production_flag").read();
    ::uhal::ValWord<uint32_t> _data_otf_preview = hw.getNode("gt_mp7_gtlfdl.prescale_otf_flags.preview_flag").read();

    // otf previous applied flags
    ::uhal::ValWord<uint32_t> _data_otf_in_use = hw.getNode("gt_mp7_gtlfdl.prescale_otf_applied.previous_prescale_otf").read();
    ::uhal::ValWord<uint32_t> _data_otf_in_use_preview = hw.getNode("gt_mp7_gtlfdl.prescale_preview_otf_applied.previous_prescale_preview_otf").read();

    // prepare read blocks
    const ::uhal::Node& node_scaler_before_prescale = hw.getNode("gt_mp7_gtlfdl.rate_cnt_before_prescaler");
    ::uhal::ValVector<uint32_t> _data_scaler_before_prescale = node_scaler_before_prescale.readBlock(node_scaler_before_prescale.getSize());

    const ::uhal::Node& node_scaler = hw.getNode("gt_mp7_gtlfdl.rate_cnt_after_prescaler");
    ::uhal::ValVector<uint32_t> _data_scaler = node_scaler.readBlock(node_scaler.getSize());

    const ::uhal::Node& node_scaler_preview = hw.getNode("gt_mp7_gtlfdl.preview.rate_cnt_after_prescaler");
    ::uhal::ValVector<uint32_t> _data_scaler_preview = node_scaler_preview.readBlock(node_scaler_preview.getSize());

    const ::uhal::Node& node_scaler_post_dead_time = hw.getNode("gt_mp7_gtlfdl.rate_cnt_post_dead_time");
    ::uhal::ValVector<uint32_t> _data_scaler_post_dead_time = node_scaler_post_dead_time.readBlock(node_scaler_post_dead_time.getSize());

    const ::uhal::Node& node_prescales = hw.getNode("gt_mp7_gtlfdl.prescale_factor");
    ::uhal::ValVector<uint32_t> _data_prescales = node_prescales.readBlock(node_prescales.getSize());

    ::uhal::ValWord<uint32_t> _data_finor_counter_preview = hw.getNode("gt_mp7_gtlfdl.preview.rate_cnt_finor").read();

    hw.getClient().dispatch();


    // check luminosity segment number
    ::uhal::ValWord<uint32_t> data_lumi_seg_nr_after = node_lumi_seg_nr.read();

    hw.getClient().dispatch();

    if (data_lumi_seg_nr_before.value() != data_lumi_seg_nr_after.value())
    {
      std::cout << "uGtAlgo::retrieveMetricValues(): LS jump: "
                << data_lumi_seg_nr_before.value() << " != "
                << data_lumi_seg_nr_after.value() << std::endl;
      continue;
    }
    // no updates of luminosity segement number during the reads

    data_finor_counter = _data_finor_counter.value();
    data_veto_counter = _data_veto_counter.value();
    data_l1a_counter = _data_l1a_counter.value();
    data_ps_idx = _data_ps_idx.value();
    data_ps_idx_in_use = _data_ps_idx_in_use.value();
    data_ps_idx_preview = _data_ps_idx_preview.value();
    data_ps_idx_in_use_preview = _data_ps_idx_in_use_preview.value();
    data_lumi_seg_nr = data_lumi_seg_nr_after.value();
    data_finor_counter_preview = _data_finor_counter_preview.value();
    data_otf = _data_otf.value();
    data_otf_in_use = _data_otf_in_use.value();
    data_otf_preview = _data_otf_preview.value();
    data_otf_in_use_preview = _data_otf_in_use_preview.value();

    data_scaler_before_prescale = _data_scaler_before_prescale.value();
    data_scaler = _data_scaler.value();
    data_scaler_preview = _data_scaler_preview.value();
    data_scaler_post_dead_time = _data_scaler_post_dead_time.value();

    for (const auto& value : _data_prescales.value())
    {
      // convert prescale to double representation
      const double scale = std::pow(10, kPrescalePrecision);
      data_prescales.push_back(static_cast<double>(value) / scale);
    }

    break;
  }

  setMetricValue<uint32_t>(metricFinOrCounter_, data_finor_counter);
  setMetricValue<double>(metricFinOrRate_, data_finor_counter*kToRate);
  setMetricValue<uint32_t>(metricVetoCounter_, data_veto_counter);
  setMetricValue<double>(metricVetoRate_, data_veto_counter*kToRate);
  setMetricValue<uint32_t>(metricL1aCounter_, data_l1a_counter);
  setMetricValue<double>(metricL1aRate_, data_l1a_counter*kToRate);
  setMetricValue<uint32_t>(metricPsIndex_, data_ps_idx);
  setMetricValue<uint32_t>(metricPsIndexInUse_, data_ps_idx_in_use);
  setMetricValue<uint32_t>(metricPreviewPsIndex_, data_ps_idx_preview);
  setMetricValue<uint32_t>(metricPreviewPsIndexInUse_, data_ps_idx_in_use_preview);
  setMetricValue<uint32_t>(metricLuminositySegmentNumber_, data_lumi_seg_nr);
  setMetricValue<uint32_t>(metricPreviewFinOrCounter_, data_finor_counter_preview);
  setMetricValue<double>(metricPreviewFinOrRate_, data_finor_counter_preview*kToRate);
  setMetricValue<uint32_t>(metricOtf_, data_otf);
  setMetricValue<uint32_t>(metricOtfInUse_, data_otf_in_use);
  setMetricValue<uint32_t>(metricPreviewOtf_, data_otf_preview);
  setMetricValue<uint32_t>(metricPreviewOtfInUse_, data_otf_in_use_preview);

  if (kMaxAlgorithms != data_scaler_before_prescale.size()) throw std::runtime_error("uGtAlgo::retrieveMetricValues: size mismatch");
  if (kMaxAlgorithms != data_scaler.size()) throw std::runtime_error("uGtAlgo::retrieveMetricValues: size mismatch");
  if (kMaxAlgorithms != data_scaler_post_dead_time.size()) throw std::runtime_error("uGtAlgo::retrieveMetricValues: size mismatch");
  if (kMaxAlgorithms != data_prescales.size()) throw std::runtime_error("uGtAlgo::retrieveMetricValues: size mismatch");
  if (kMaxAlgorithms != data_scaler_preview.size()) throw std::runtime_error("uGtAlgo::retrieveMetricValues: size mismatch");

  for (size_t ii = 0; ii < kMaxAlgorithms; ii++)
  {
    const Algorithm* pAlgo = triggerMenu_.getAlgorithm(ii);
    if (pAlgo)
    {
      // std::map<unsigned int, unsigned int>::const_iterator cit = algo->getPrescales().find(data_ps_idx);
      // auto cit = pAlgo->getPrescales().find(data_ps_idx);
      auto cit = pAlgo->getPrescalesForPsIndex(data_ps_idx);
      if (cit == pAlgo->getPrescales().end())
      {
        std::cout << "could not find the prescale for algo = " << ii << " ps idx = " << data_ps_idx << "\n";
      }
      else
      {
        const double merged = pAlgo->getMergedPrescaleMask(data_ps_idx);
        setWarningCondition((*metricPrescales_.at(ii)), ::swatch::core::NotEqualCondition<double>(merged));
        // uint32_t prescaleValue = cit->second;
        // uint32_t maskValue = algo->getMask();
        // setWarningCondition((*metricPrescales_.at(ii)), ::swatch::core::NotEqualCondition<uint32_t>(prescaleValue*maskValue));
      }
    }
    setMetricValue<double>(*(metricPrescales_.at(ii)), data_prescales.at(ii));

    if (pAlgo and pAlgo->getModuleId() == (slotId_ - 1)) // module_id is zero based
    {
      setErrorCondition((*metricRateCounters_.at(ii)), ::swatch::core::GreaterThanCondition<uint32_t>(kMaximumCounts));
      setErrorCondition((*metricPreviewRateCounters_.at(ii)), ::swatch::core::GreaterThanCondition<uint32_t>(kMaximumCounts));
      setErrorCondition((*metricRateCountersBeforePrescale_.at(ii)), ::swatch::core::GreaterThanCondition<uint32_t>(kMaximumCounts));
      setErrorCondition((*metricPostDeadtimeRateCounters_.at(ii)), ::swatch::core::GreaterThanCondition<uint32_t>(kMaximumCounts));
      setErrorCondition((*metricRates_.at(ii)), ::swatch::core::GreaterThanCondition<double>(kLhcClockFrequency));
      setErrorCondition((*metricPreviewRates_.at(ii)), ::swatch::core::GreaterThanCondition<double>(kLhcClockFrequency));
      setErrorCondition((*metricRatesBeforePrescale_.at(ii)), ::swatch::core::GreaterThanCondition<double>(kLhcClockFrequency));
      setErrorCondition((*metricPostDeadtimeRates_.at(ii)), ::swatch::core::GreaterThanCondition<double>(kLhcClockFrequency));

      metricRateCounters_.at(ii)->setMonitoringStatus(::swatch::core::monitoring::kEnabled);
      metricRates_.at(ii)->setMonitoringStatus(::swatch::core::monitoring::kEnabled);
      metricPreviewRateCounters_.at(ii)->setMonitoringStatus(::swatch::core::monitoring::kEnabled);
      metricPreviewRates_.at(ii)->setMonitoringStatus(::swatch::core::monitoring::kEnabled);
      metricRateCountersBeforePrescale_.at(ii)->setMonitoringStatus(::swatch::core::monitoring::kEnabled);
      metricRatesBeforePrescale_.at(ii)->setMonitoringStatus(::swatch::core::monitoring::kEnabled);
      metricPostDeadtimeRateCounters_.at(ii)->setMonitoringStatus(::swatch::core::monitoring::kEnabled);
      metricPostDeadtimeRates_.at(ii)->setMonitoringStatus(::swatch::core::monitoring::kEnabled);

      setMetricValue<uint32_t>(*(metricRateCountersBeforePrescale_.at(ii)), data_scaler_before_prescale.at(ii));
      setMetricValue<double>(*(metricRatesBeforePrescale_.at(ii)), data_scaler_before_prescale.at(ii)*kToRate);
      setMetricValue<uint32_t>(*(metricRateCounters_.at(ii)), data_scaler.at(ii));
      setMetricValue<double>(*(metricRates_.at(ii)), data_scaler.at(ii)*kToRate);
      setMetricValue<uint32_t>(*(metricPreviewRateCounters_.at(ii)), data_scaler_preview.at(ii));
      setMetricValue<double>(*(metricPreviewRates_.at(ii)), data_scaler_preview.at(ii)*kToRate);
      setMetricValue<uint32_t>(*(metricPostDeadtimeRateCounters_.at(ii)), data_scaler_post_dead_time.at(ii));
      setMetricValue<double>(*(metricPostDeadtimeRates_.at(ii)), data_scaler_post_dead_time.at(ii)*kToRate);
    }
    else
    {
      //std::cout << "uGtAlgo::retrieveMetricValues(): disabling id = " << ii << " algo = " << (algo ? "on" : "off")
      //          << " slotId = " << slotId_ << " module_id = " << (algo ? algo->module_id : -1) << "\n";

      metricRateCounters_.at(ii)->setMonitoringStatus(::swatch::core::monitoring::kDisabled);
      metricRates_.at(ii)->setMonitoringStatus(::swatch::core::monitoring::kDisabled);
      metricPreviewRateCounters_.at(ii)->setMonitoringStatus(::swatch::core::monitoring::kDisabled);
      metricPreviewRates_.at(ii)->setMonitoringStatus(::swatch::core::monitoring::kDisabled);
      metricRateCountersBeforePrescale_.at(ii)->setMonitoringStatus(::swatch::core::monitoring::kDisabled);
      metricRatesBeforePrescale_.at(ii)->setMonitoringStatus(::swatch::core::monitoring::kDisabled);
      metricPostDeadtimeRateCounters_.at(ii)->setMonitoringStatus(::swatch::core::monitoring::kDisabled);
      metricPostDeadtimeRates_.at(ii)->setMonitoringStatus(::swatch::core::monitoring::kDisabled);

      if (data_scaler.at(ii))
      {
        metricRateCounters_.at(ii)->setMonitoringStatus(::swatch::core::monitoring::kNonCritical);
        setErrorCondition((*metricRateCounters_.at(ii)), ::swatch::core::NotEqualCondition<uint32_t>(0x0));
        setMetricValue<uint32_t>(*(metricRateCounters_.at(ii)), data_scaler.at(ii));
      }

      if (data_scaler_preview.at(ii))
      {
        metricPreviewRateCounters_.at(ii)->setMonitoringStatus(::swatch::core::monitoring::kNonCritical);
        setErrorCondition((*metricPreviewRateCounters_.at(ii)), ::swatch::core::NotEqualCondition<uint32_t>(0x0));
        setMetricValue<uint32_t>(*(metricPreviewRateCounters_.at(ii)), data_scaler_preview.at(ii));
      }

      if (data_scaler_before_prescale.at(ii))
      {
        metricRateCountersBeforePrescale_.at(ii)->setMonitoringStatus(::swatch::core::monitoring::kNonCritical);
        setErrorCondition((*metricRateCountersBeforePrescale_.at(ii)), ::swatch::core::NotEqualCondition<uint32_t>(0x0));
        setMetricValue<uint32_t>(*(metricRateCountersBeforePrescale_.at(ii)), data_scaler.at(ii));
      }

      if (data_scaler_post_dead_time.at(ii))
      {
        metricPostDeadtimeRateCounters_.at(ii)->setMonitoringStatus(::swatch::core::monitoring::kNonCritical);
        setErrorCondition((*metricPostDeadtimeRateCounters_.at(ii)), ::swatch::core::NotEqualCondition<uint32_t>(0x0));
        setMetricValue<uint32_t>(*(metricPostDeadtimeRateCounters_.at(ii)), data_scaler.at(ii));
      }
    }
  }


  //
  // check other registers
  //
  setMetricValue<uint32_t>(metricBxNumberMax_, getBxNumberMax());
  setWarningCondition(metricBxNumberMax_, ::swatch::core::NotEqualCondition<uint32_t>(0xdeb));



  setMetricValue<uint64_t>(metricOrbitCounter_, getOrbitNumber());

}



} // namespace ugt
/* eof */
