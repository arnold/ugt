

#include "ugt/swatch/uGtFinorCommands.h"

#include "ugt/swatch/Constants.h"
#include "ugt/swatch/AMC502Processor.h"
#include "ugt/swatch/AMC502Controller.h"

#include "uhal/HwInterface.hpp"

#include "xdata/String.h"
#include "xdata/Table.h"

#include <boost/lexical_cast.hpp>

#include <vector>

namespace ugt
{


static const std::string kL1MenuParam = "l1menu";
static const std::string kModuleIndex = "ModuleIndex";

uGtFinorInputMaskCommand::uGtFinorInputMaskCommand(const std::string& aId,
  ::swatch::action::ActionableObject& aActionable) :
  ::swatch::action::Command(aId, aActionable, xdata::UnsignedInteger())
  // result is UnsignedInteger
{
  registerParameter(kL1MenuParam, xdata::Table());
}

::swatch::action::Command::State
uGtFinorInputMaskCommand::code(const ::swatch::core::XParameterSet& params)
{
  setProgress(0., "uGtFinorInputMaskCommand ...");

  // Default all inputs are masked (false).
  std::vector<bool> masks(kMaxUgtModules, false);

  // Get menu from gatekeeper
  const xdata::Table& table = params.get<xdata::Table>(kL1MenuParam);

  // Enable masks for used modules by iterating all algorithms.
  const std::vector<std::string> cols = table.getColumns();
  for (size_t i = 0; i < table.getRowCount(); i++)
  {
    unsigned int module_id = boost::lexical_cast<unsigned int>(table.getValueAt(i, kModuleIndex)->toString());
    if (module_id >= kMaxUgtModules)
    {
      std::stringstream ss;
      ss << "uGtFinorInputMaskCommand: module_id exceeds maximum (1..." << (kMaxUgtModules - 1) << "): '" << module_id << "'.";
      setStatusMsg(ss.str());
      return State::kError;
    }
    // Enable used module mask.
    masks.at(module_id) = true;
  }

  // Get hardware interface
  amc502::AMC502Processor &p = getActionable<amc502::AMC502Processor>();
  amc502::AMC502Controller& driver = p.driver();
  ::uhal::HwInterface& hw = driver.hw();

  // Mask modules
  for (size_t i = 0; i < masks.size(); ++i)
  {
      std::ostringstream id;
      id << "payload.input_masks.module_" << i;
      const uhal::Node& node = hw.getNode(id.str());
      node.write(masks.at(i));
  }

  hw.getClient().dispatch();

  // Diagnostic output
  std::stringstream ss;
  for (size_t i = 0; i < masks.size(); ++i)
    ss << "input mask for module[" << i << "] = " << masks.at(i) << std::endl;
  std::cout << ss.str() << std::endl;

  setProgress(1., "uGtFinorInputMaskCommand ... done");
  return State::kDone;
}


} // namespace ugt
