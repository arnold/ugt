#include "ugt/swatch/uGtSystem.h"
#include "swatch/core/Factory.hpp"
#include "swatch/action/SystemStateMachine.hpp"
#include "swatch/processor/Processor.hpp"
#include "swatch/dtm/DaqTTCManager.hpp"


SWATCH_REGISTER_CLASS(ugt::uGtSystem);

namespace ugt {

uGtSystem::uGtSystem(const swatch::core::AbstractStub& aStub) :
	swatch::system::System(aStub)
{
    // system run control state machine
    typedef swatch::processor::RunControlFSM tProcFSM;
    typedef swatch::dtm::RunControlFSM tDaqTTCFSM;

    swatch::system::RunControlFSM& fsm = getRunControlFSM();

    fsm.coldReset
                 .add(getDaqTTCs(), tDaqTTCFSM::kStateInitial, tDaqTTCFSM::kTrColdReset)
                 .add(getProcessors(), tProcFSM::kStateInitial, tProcFSM::kTrColdReset);

    fsm.setup
             .add(getDaqTTCs(), tDaqTTCFSM::kStateInitial, tDaqTTCFSM::kTrClockSetup)
             .add(getProcessors(), tProcFSM::kStateInitial, tProcFSM::kTrSetup)
             .add(getDaqTTCs(), tDaqTTCFSM::kStateClockOK, tDaqTTCFSM::kTrCfgDaq);

    fsm.configure.add(getProcessors(), tProcFSM::kStateSync, tProcFSM::kTrConfigure);

    fsm.align.add(getProcessors(), tProcFSM::kStateConfigured, tProcFSM::kTrAlign);	

    fsm.start.add(getProcessors(), tProcFSM::kStateAligned, tProcFSM::kTrStart)
             .add(getDaqTTCs(), tDaqTTCFSM::kStateConfigured, tDaqTTCFSM::kTrStart);

    fsm.pause.add(getDaqTTCs(), tDaqTTCFSM::kStateRunning, tDaqTTCFSM::kTrPause);

    fsm.resume.add(getDaqTTCs(), tDaqTTCFSM::kStatePaused, tDaqTTCFSM::kTrResume);

    fsm.stopFromPaused.add(getDaqTTCs(), tDaqTTCFSM::kStatePaused, tDaqTTCFSM::kTrStop)
                      .add(getProcessors(), tProcFSM::kStateRunning, tProcFSM::kTrStop);

    fsm.stopFromRunning.add(getDaqTTCs(), tDaqTTCFSM::kStateRunning, tDaqTTCFSM::kTrStop)
                       .add(getProcessors(), tProcFSM::kStateRunning, tProcFSM::kTrStop);

    fsm.stopFromAligned.add(getProcessors(), tProcFSM::kStateAligned, tProcFSM::kTrStop);
}

uGtSystem::~uGtSystem() {

}

} // namespace ugt

