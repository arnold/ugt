#include "ugt/swatch/Algorithm.h"

#include <sstream>
#include <iomanip>
#include <iostream>


namespace ugt
{

Algorithm::Algorithm(const std::string& name, const uint32_t index,
             const uint32_t module_id, const uint32_t local_id)
  : mName(name)
  , mIndex(index)
  , mModuleId(module_id)
  , mLocalId(local_id)
  , mVeto(0)
  , mMask(0)
  , mPrescales()
  , mAlgoBxMask()
  { }

Algorithm::~Algorithm()
{
}


const std::string&
Algorithm::getName() const
{
  return mName;
}

const uint32_t
Algorithm::getIndex() const
{
  return mIndex;
}

const uint32_t
Algorithm::getModuleId() const
{
  return mModuleId;
}

const uint32_t
Algorithm::getLocalId() const
{
  return mLocalId;
}

const uint32_t
Algorithm::getVeto() const
{
  return mVeto;
}

const uint32_t
Algorithm::getMask() const
{
  return mMask;
}

const double
Algorithm::getPrescale(uint32_t psIndex) const
{
  for (auto cit = mPrescales.begin(); cit != mPrescales.end(); ++cit)
  {
    const toolbox::PrescaleColumnItem& column = cit->first;
    if (column.index == psIndex)
      return cit->second;
  }

  std::stringstream os;
  os << "Algorithm getPrescale: did not find prescale for psIndex=" << psIndex;
  throw AlgorithmException(os.str());
  return 0;
}

const double
Algorithm::getMergedPrescaleMask(uint32_t psIndex) const
{
  for (auto cit = mPrescales.begin(); cit != mPrescales.end(); ++cit)
  {
    const toolbox::PrescaleColumnItem& column = cit->first;
    if (column.index == psIndex)
      return cit->second * mMask;
  }

  std::stringstream os;
  os << "Algorithm getMergedPrescaleMask: did not find prescale for psIndex=" << psIndex;
  throw AlgorithmException(os.str());

  return 0;
}


const std::map<toolbox::PrescaleColumnItem, double>&
Algorithm::getPrescales() const
{
  return mPrescales;
}

std::map<toolbox::PrescaleColumnItem, double>::const_iterator
Algorithm::getPrescalesForPsIndex(uint32_t psIndex) const
{
  for(auto it = mPrescales.begin(); it != mPrescales.end(); ++it)
  {
    const toolbox::PrescaleColumnItem& column = it->first;
    if (column.index == psIndex)
      return it;
  }
  return mPrescales.end();
}

const std::string&
Algorithm::getFormattedLabelForPsIndex(uint32_t psIndex) const
{
  return getPrescaleColumnItemForPsIndex(psIndex).getFormattedLabel();
}

const toolbox::PrescaleColumnItem&
Algorithm::getPrescaleColumnItemForPsIndex(uint32_t psIndex) const
{
  auto it = getPrescalesForPsIndex(psIndex);
  if (it == mPrescales.end())
  {
    // should never happen
    std::ostringstream oss;
    oss << "Algorithm getFormattedLabelForPsIndex: did not find prescales for psIndex=" << psIndex;
    throw AlgorithmException(oss.str());
  }
  return (it->first);
}

const std::map<uint32_t, uint32_t>&
Algorithm::getAlgoBxMask() const
{
  return mAlgoBxMask;
}



void
Algorithm::setVeto(uint32_t veto)
{
  mVeto = veto;
}

void
Algorithm::setMask(uint32_t mask)
{
  mMask = mask;
}

void
Algorithm::setPrescales(const std::map<toolbox::PrescaleColumnItem, double>& prescales)
{
  mPrescales = prescales;
}

void
Algorithm::setAlgoBxMask(std::map<uint32_t, uint32_t>& algoBxMask)
{
  mAlgoBxMask = algoBxMask;
}

void
Algorithm::show() const
{
  std::cout
    << std::setw(3) << mIndex
    << " " << mModuleId
    << " " << std::setw(3) << mLocalId
    << " " << mName
    << " " << mMask
    << " " << mVeto
    << std::endl;
}

} // namespace ugt
