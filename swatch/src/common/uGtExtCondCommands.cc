

#include "ugt/swatch/uGtExtCondCommands.h"
#include "ugt/swatch/AMC502Processor.h"
#include "ugt/swatch/AMC502Controller.h"

#include "uhal/HwInterface.hpp"


namespace ugt
{

static const size_t EXT_COND_CHANNELS = 64;

uGtExtCondDelayCommand::uGtExtCondDelayCommand(const std::string& aId,
  ::swatch::action::ActionableObject& aActionable) :
  ::swatch::action::Command(aId, aActionable, xdata::UnsignedInteger())
  // result is UnsignedInteger
{
  for (size_t i = 0; i < EXT_COND_CHANNELS; ++i)
  {
    std::stringstream name;
    name << "delay_" << std::setfill('0') << std::setw(2) << i;
    registerParameter(name.str(), xdata::UnsignedInteger(0));
  }
}

::swatch::action::Command::State
uGtExtCondDelayCommand::code(const ::swatch::core::XParameterSet& params)
{
  setProgress(0., "uGtExtCondDelayCommand ...");

  std::vector<uint32_t> delays;

  for (size_t i = 0; i < EXT_COND_CHANNELS; ++i)
  {
    std::stringstream name;
    name << "delay_" << std::setfill('0') << std::setw(2) << i;
    delays.push_back(params.get<xdata::UnsignedInteger>(name.str()).value_);
  }

  amc502::AMC502Processor &p = getActionable<amc502::AMC502Processor>();
  amc502::AMC502Controller& driver = p.driver();
  ::uhal::HwInterface& hw = driver.hw();

  for (size_t i = 0; i < EXT_COND_CHANNELS; ++i)
  {
    std::stringstream name;
    name << "payload.delay_regs.delay_" << std::setfill('0') << std::setw(2) << i;
    const ::uhal::Node& node = hw.getNode(name.str());
    node.write(delays.at(i));
  }

  hw.getClient().dispatch();

  setProgress(1., "uGtExtCondDelayCommand ... done");
  return State::kDone;
}


uGtExtCondPhaseShiftCommand::uGtExtCondPhaseShiftCommand(const std::string& aId,
  ::swatch::action::ActionableObject& aActionable) :
  ::swatch::action::Command(aId, aActionable, xdata::UnsignedInteger())
{
  for (size_t i = 0; i < EXT_COND_CHANNELS; ++i)
  {
    std::stringstream name;
    name << "phase_shift_" << std::setfill('0') << std::setw(2) << i;
    registerParameter(name.str(), xdata::UnsignedInteger(1));
  }
}

::swatch::action::Command::State
uGtExtCondPhaseShiftCommand::code(const ::swatch::core::XParameterSet& params)
{
  setProgress(0., "uGtExtCondPhaseShiftCommand ...");

  std::vector<uint32_t> phaseshifts;

  for (size_t i = 0; i < EXT_COND_CHANNELS; ++i)
  {
    std::stringstream name;
    name << "phase_shift_" << std::setfill('0') << std::setw(2) << i;
    phaseshifts.push_back(params.get<xdata::UnsignedInteger>(name.str()).value_);
  }

  amc502::AMC502Processor &p = getActionable<amc502::AMC502Processor>();
  amc502::AMC502Controller& driver = p.driver();
  ::uhal::HwInterface& hw = driver.hw();

  for (size_t i = 0; i < EXT_COND_CHANNELS; ++i)
  {
    std::stringstream name;
    name << "payload.phase_shift_regs.phase_shift_" << std::setfill('0') << std::setw(2) << i;
    const ::uhal::Node& node = hw.getNode(name.str());
    node.write(phaseshifts.at(i));
  }

  hw.getClient().dispatch();

  setProgress(1., "uGtExtCondPhaseShiftCommand ... done");
  return State::kDone;
}


} // namespace ugt
