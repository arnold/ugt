#include "ugt/swatch/uGtTriggerMenu.h"

#include "ugt/swatch/toolbox/PrescaleTable.h"
#include "ugt/swatch/toolbox/PrescaleSet.h"
#include "ugt/swatch/toolbox/MaskTable.h"
#include "ugt/swatch/toolbox/AlgoBXMaskTable.h"

#include "swatch/logger/Logger.hpp"
#include "mp7/Utilities.hpp"

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

#include <iomanip>


namespace ugt
{

uGtTriggerMenu::uGtTriggerMenu() :
  mTriggerMenu()
{
}


uGtTriggerMenu::~uGtTriggerMenu()
{
  clear();
}


void
uGtTriggerMenu::clear()
{
  for (TriggerMenu::const_iterator cit = mTriggerMenu.begin();
       cit != mTriggerMenu.end(); cit++)
  {
    delete cit->second;
  }
  mTriggerMenu.clear();

}


void
uGtTriggerMenu::setTriggerMenu(const xdata::Table& table)
{
  // check for duplicated algo names
  const std::vector<std::string> cols = table.getColumns();

  for (size_t row = 0; row < table.getRowCount(); ++row)
  {
    std::string name;
    unsigned int index = std::numeric_limits<unsigned int>::max();
    unsigned int module_id = std::numeric_limits<unsigned int>::max();
    unsigned int local_id = std::numeric_limits<unsigned int>::max();

    for (const auto& col: cols)
    {
      auto value = table.getValueAt(row, col)->toString();
      boost::trim(value);

      if (col == "AlgoName")
      {
        name = value;
      }
      else if (col == "GlobalIndex")
      {
        index = boost::lexical_cast<unsigned int>(value);
      }
      else if (col == "ModuleIndex")
      {
        module_id = boost::lexical_cast<unsigned int>(value);
      }
      else if (col == "LocalIndex")
      {
        local_id = boost::lexical_cast<unsigned int>(value);
      }
    }

    Algorithm* pAlgo = new Algorithm(name, index, module_id, local_id);
    mTriggerMenu.insert(std::make_pair(pAlgo->getIndex(), pAlgo));
  }
}


const Algorithm*
uGtTriggerMenu::getAlgorithm(const unsigned int index) const
{
  auto cit = mTriggerMenu.find(index);
  if (cit == mTriggerMenu.end()) {
    return NULL;
  }
  return cit->second;
}

const Algorithm*
uGtTriggerMenu::getAlgorithmByName(const std::string name) const
{
  for (auto cit = mTriggerMenu.begin(); cit != mTriggerMenu.end(); cit++)
  {
    const Algorithm* pAlgo = cit->second;
    if (pAlgo->getName() == name)
    {
      return pAlgo;
    }
  }

  return NULL;
}

void uGtTriggerMenu::setFinorMaskContainer(const xdata::Table* table)
{
  toolbox::FinorMaskTable finorMaskTable;
  // MaskTableException will be catched in uGtTriggerMenuCommand(...)
  finorMaskTable.load(*table, *this);
  for (auto cit = mTriggerMenu.begin(); cit != mTriggerMenu.end(); cit++)
  {
    Algorithm* pAlgo = cit->second;
    pAlgo->setMask(finorMaskTable.getMask(pAlgo->getName()));
  }
}


void uGtTriggerMenu::setVetoMaskContainer(const xdata::Table* table)
{
  toolbox::VetoMaskTable vetoMaskTable;
  // MaskTableException will be catched in uGtTriggerMenuCommand(...)
  vetoMaskTable.load(*table, *this);
  for (auto cit = mTriggerMenu.begin(); cit != mTriggerMenu.end(); cit++)
  {
    Algorithm* pAlgo = cit->second;
    pAlgo->setMask(vetoMaskTable.getMask(pAlgo->getName()));
  }
}


void uGtTriggerMenu::setAlgoBxMaskContainer(const xdata::Table* table)
{
  toolbox::AlgoBXMaskTable algobxMaskTable;
  // AlgoBXMaskTableException will be catched in uGtTriggerMenuCommand(...)
  algobxMaskTable.load(*table, *this);

  for (auto cit = mTriggerMenu.begin(); cit != mTriggerMenu.end(); cit++)
  {
    Algorithm* pAlgo = cit->second;
    if (pAlgo == NULL)
      continue;

    std::map<uint32_t, uint32_t> vec = algobxMaskTable.getAlgoBXMask(pAlgo->getName());
    pAlgo->setAlgoBxMask(vec);
  }
}


void uGtTriggerMenu::setPrescalesContainer(const xdata::Table* table, const xdata::Table* tableMask)
{
  toolbox::PrescaleTable prescaleTable;
  // PrescaleTableException will be catched in uGtTriggerMenuCommand(...)
  prescaleTable.load(*table, *tableMask, *this);

  for (auto cit = mTriggerMenu.begin(); cit != mTriggerMenu.end(); cit++)
  {
    Algorithm* pAlgo = cit->second;

    std::map<toolbox::PrescaleColumnItem, double> map =
        prescaleTable.getPrescalesForAlgo(pAlgo->getName());

    pAlgo->setPrescales(map);
  }
}


void
uGtTriggerMenu::show() const
{
  std::cout << "uGtTriggerMenu::show:\n";
  for (auto cit = mTriggerMenu.begin();
       cit != mTriggerMenu.end(); cit++)
  {
    const ugt::Algorithm* pAlgo = cit->second;
    pAlgo->show();
  }
}


bool
uGtTriggerMenu::algoExists(std::string algoname) const
{
  bool exists = false;
  if (getAlgorithmByName(algoname) != NULL)
    exists = true;

  return exists;
}


size_t
uGtTriggerMenu::size() const
{
  return mTriggerMenu.size();
}

uGtTriggerMenu::iterator
uGtTriggerMenu::begin() const
{
  return mTriggerMenu.begin();
}

uGtTriggerMenu::iterator
uGtTriggerMenu::end() const
{
  return mTriggerMenu.end();
}

} // namespace ugt
/* eof */
