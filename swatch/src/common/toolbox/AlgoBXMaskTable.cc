#include "ugt/swatch/toolbox/AlgoBXMaskTable.h"

#include "ugt/swatch/Constants.h"
#include "swatch/core/toolbox/IntListParser.hpp"

#include <boost/lexical_cast.hpp>
#include <boost/tokenizer.hpp>

#include <sstream>

namespace ugt {
namespace toolbox {

const std::string kTableType = "AlgoBxMaskTable";

const std::string kAlgoName = "algo";
const std::string kRange = "range";
const std::string kMask = "mask";


AlgoBXMaskTable::AlgoBXMaskTable()
: AbstractTable(kTableType), mMaskList(), mDefaultMask(0)
{
}

AlgoBXMaskTable::AlgoBXMaskTable(const xdata::Table& table, const uGtTriggerMenu& menu)
: AbstractTable(kTableType), mMaskList(), mDefaultMask(0)
{
  load(table, menu);
}

AlgoBXMaskTable::~AlgoBXMaskTable()
{
}

// throws AlgoBXMaskTableException
void
AlgoBXMaskTable::verify(const xdata::Table& table)
{
  std::stringstream msg;
  const std::vector<std::string> columns = table.getColumns();
  const size_t rowCount = table.getRowCount();

  try
  {
    // check whether 3 columns exist
    std::vector<std::string> colExpected;
    colExpected.push_back(kAlgoName);
    colExpected.push_back(kRange);
    colExpected.push_back(kMask);
    checkColumnsValid(columns, colExpected);

    // check that minimum 1 row exist in the form
    // <row>ALL, ALL, 1</row>
    checkRowsSizeValid(rowCount);

  }
  catch (const toolbox::AbstractTableException& e)
  {
    throw AlgoBXMaskTableException(e.what());
  }

  std::set<std::string> algoset;            // algo name set
  std::pair<std::set<std::string>::iterator, bool> rc;
  std::vector<std::string> maskOutOfRange;      // mask/veto is not 0 or 1
  std::vector<uint32_t> rangeOutOfRange;      // range values that are out of range or invalid
  bool bFoundDefaultAlgo = false;

  // Check for default algo and ranges in algo row
  for (size_t row = 0; row < rowCount; ++row)
  {
    std::string algoName = table.getValueAt(row, kAlgoName)->toString();
    std::string range = table.getValueAt(row, kRange)->toString();
    uint32_t mask = boost::lexical_cast<uint32_t>(table.getValueAt(row, kMask)->toString());

    // Search for ranges which are out of range and
    // whether the lower border is higher than the upper border
    if (not isDefaultRange(range))
    {
      typedef boost::tokenizer<boost::char_separator<char>> tokenizer;
      boost::char_separator<char> sep{"-"};
      tokenizer tok{range, sep};

      uint32_t lastValue = 0;
      for (auto cit = tok.begin(); cit != tok.end(); ++cit)
      {
        try
        {
          // collect errors that are out of range and when first value is greater than second
          uint32_t rangePart =  boost::lexical_cast<uint32_t>(*cit);
          if (rangePart < 1 || rangePart > 3564 || lastValue > rangePart)
          {
            rangeOutOfRange.push_back(row);
            break;
          }

          lastValue = rangePart;
        }
        catch (const boost::bad_lexical_cast &e)
        {
          // collect errors that are NaN
          rangeOutOfRange.push_back(row);
          break;
        }

      } // end loop tok
    }

    // Search for mask values out of range (0 || 1)
    if (mask != 0 && mask != 1)
      maskOutOfRange.push_back(algoName);


    // Is default algo available on in first row
    if (isDefaultAlgoRowAvailable(algoName, range))
    {
      if (row != 0)
      {
        // Default algo is missing
        throw AlgoBXMaskTableException(buildMinDefaultRowMessage());
      }

      bFoundDefaultAlgo = true;
    }

  }

  // Default algo is missing
  if (not bFoundDefaultAlgo)
  {
    throw AlgoBXMaskTableException(buildMinDefaultRowMessage());
  }

  try
  {
    // Found mask values ot ouf range
    if (maskOutOfRange.size())
    {
      // check whether mask value is 0/1 other wise raise error
      std::stringstream singleMsg, multiMsg;
      singleMsg << "contains invalid mask value (mask must be '1' or '0') for algo";
      multiMsg << singleMsg.rdbuf() << "s";
      genericCheckErrors(maskOutOfRange, singleMsg.str(), multiMsg.str());
    }

    // Found bx range is out of range
    if (rangeOutOfRange.size())
    {
      std::stringstream singleMsg, multiMsg;
      singleMsg << "contains invalid BX range (must be between 1-3564) in row";
      multiMsg << singleMsg.rdbuf() << "s";
      genericCheckErrors(rangeOutOfRange, singleMsg.str(), multiMsg.str());
    }
  }
  catch (const toolbox::AbstractTableException& e)
  {
    throw AlgoBXMaskTableException(e.what());
  }

  // everything is fine - table is ok
}

// throws AlgoBXMaskTableException
void
AlgoBXMaskTable::verify(const xdata::Table& table, const uGtTriggerMenu& menu)
{
  const size_t rowCount = table.getRowCount();

  // verify table w/o menu relation
  verify(table);

  try
  {
    // check that menu contains entries
    checkMenuSizeValid(menu.size());

    std::pair<std::map<std::string, uint32_t>::iterator,bool> rc;
    std::set<std::string> unknownAlgos;      // algo is not known in menu

    for (size_t row = 0; row < rowCount; ++row)
    {
      std::string algoName = table.getValueAt(row, kAlgoName)->toString();

      // check whether algo is available in menu and algoName may no be 'ALL'
      if (not menu.algoExists(algoName) && not isDefaultAlgo(algoName))
      {
        // algo in table is not available in menu
        unknownAlgos.insert(algoName);
      }
    } // end loop row


    // unknownAlgo
    std::stringstream singleMsg, multiMsg;
    singleMsg << "contains unknown algo";
    multiMsg << multiMsg.rdbuf() << "s";
    genericCheckErrors(unknownAlgos, singleMsg.str(), multiMsg.str());
  }
  catch (const toolbox::AbstractTableException& e)
  {
    throw AlgoBXMaskTableException(e.what());
  }


}


void
AlgoBXMaskTable::load(const xdata::Table& table, const uGtTriggerMenu& menu)
{
  // verify table w/ menu
  verify(table, menu);

  const size_t rowCount = table.getRowCount();
  for (size_t row = 0; row < rowCount; ++row)
  {
    std::string algoName = table.getValueAt(row, kAlgoName)->toString();
    std::string range = table.getValueAt(row, kRange)->toString();
    uint32_t mask = boost::lexical_cast<uint32_t>(table.getValueAt(row, kMask)->toString());

    if (isDefaultAlgo(algoName) && isDefaultRange(range))
    {
      mDefaultMask = mask;
      continue;
    }

    const std::vector<uint32_t> vecParsed = getParsedRangeList(range);
    if (vecParsed.size() > 0)
    {
      // set up algo bx mask vector
      AlgoBXMaskItem item = AlgoBXMaskItem(algoName, range, mask);
      mMaskList.push_back(item);
    }
  } // end loop row

}



size_t
AlgoBXMaskTable::size() const
{
  return mMaskList.size();
}


std::map<uint32_t, uint32_t>
AlgoBXMaskTable::getAlgoBXMask(const std::string& algo)
{
  std::map<uint32_t, uint32_t>algobxMap;
  for (uint32_t i = 1; i <= kOrbitLength; ++i)
  {
    algobxMap.insert(std::make_pair(i, mDefaultMask));
  }

  for (auto cit = mMaskList.begin(); cit != mMaskList.end(); ++cit)
  {
    const AlgoBXMaskItem item = *cit;
    if (isDefaultAlgo(item.algo) || algo == item.algo)
    {
      std::vector<uint32_t> vecParsed = getParsedRangeList(item.range);
      for (auto citBx = vecParsed.begin(); citBx != vecParsed.end(); ++citBx)
      {
        algobxMap[*citBx] = item.mask;
      }
    }
  }

  return algobxMap;
}


const std::vector<AlgoBXMaskItem>&
AlgoBXMaskTable::getMaskList() const
{
  return mMaskList;
}



AlgoBXMaskTable::iterator
AlgoBXMaskTable::begin() const
{
  return mMaskList.begin();
}


AlgoBXMaskTable::iterator
AlgoBXMaskTable::end() const
{
  return mMaskList.end();
}

uint32_t
AlgoBXMaskTable::getDefaultMask() const
{
  return mDefaultMask;
}

bool
AlgoBXMaskTable::isDefaultRange(const std::string& range)
{
  return isDefaultAlgo(range);
}


std::vector<uint32_t>
AlgoBXMaskTable::getParsedRangeList(const std::string& range)
{
  std::string sRange = range;
  if (isDefaultRange(range))
  {
    sRange = kLhcBxRange;
  }

  std::vector<uint32_t> vecParsed = ::swatch::core::toolbox::UIntListParser::parse(sRange);
  return vecParsed;
}


void
AlgoBXMaskTable::print()
{
  std::stringstream ss;
  ss << "================ Algo BX Mask =================" << std::endl;
  ss << "Default mask: " << mDefaultMask << std::endl;
  for (auto cit = mMaskList.begin(); cit != mMaskList.end(); ++cit)
  {
    const AlgoBXMaskItem& item = *cit;
    ss << item.algo << "," << item.range << "," << item.mask << std::endl;
  }
  ss << "===============================================" << std::endl;
  std::cout << ss.str();
}


void
AlgoBXMaskTable::checkRowsSizeValid(size_t rowsSize)
{
  if (rowsSize < 1)
  {
    std::stringstream msg;
    msg << mTableType << " ";
    msg << "must contain at least a default row ";
    msg << "containing '<row>'" << toolbox::kDefaultAlgo
        << "," << toolbox::kDefaultAlgo
        << ",n</row>' with n = [0,1]";
    throw AlgoBXMaskTableException(msg.str());
  }
}

std::string
AlgoBXMaskTable::buildMinDefaultRowMessage() const
{
  std::ostringstream msg;
  msg << mTableType << " ";
  msg << "must contain default row at first position '"
      << "<row>" << toolbox::kDefaultAlgo << "," << toolbox::kDefaultAlgo
      << ",n</row>' with n = [0,1]";
  return msg.str();
}

bool
AlgoBXMaskTable::isDefaultAlgoRowAvailable(const std::string& algo, const std::string& range)
{
  return isDefaultAlgo(algo) && isDefaultRange(range);
}


} // namespace toolbox
} // namespace ugt
