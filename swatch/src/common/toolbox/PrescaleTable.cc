#include "ugt/swatch/toolbox/PrescaleTable.h"
#include "ugt/swatch/toolbox/MaskTable.h"

#include "ugt/swatch/Constants.h"

#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>

#include <cstdlib>
#include <stdint.h>
#include <stdexcept>
#include <sstream>

namespace ugt {
namespace toolbox {

const std::string kTableType = "PrescaleTable";
const std::string kAlgoNamePSIndex = "algo/prescale-index";

PrescaleTable::PrescaleTable()
  : AbstractTable(kTableType), mPrescaleSets()
{
}

PrescaleTable::PrescaleTable(const xdata::Table& tablePrescales,
  const xdata::Table& tableMask, const uGtTriggerMenu& menu)
  : AbstractTable(kTableType), mPrescaleSets()
{
  load(tablePrescales, tableMask, menu);
}


// throws PrescaleTableException
void
PrescaleTable::verify(const xdata::Table& table)
{
  const std::vector<std::string> columns = table.getColumns();
  const size_t colCount = columns.size();
  const size_t rowCount = table.getRowCount();

  try
  {
    // check for correct columns
    auto citColumn = std::find(columns.begin(), columns.end(), kAlgoNamePSIndex);
    if (citColumn == columns.end())
    {
      std::ostringstream msg;
      msg << mTableType << " must contain '" << kAlgoNamePSIndex
          << "' as first column'.";
      throw PrescaleTableException(msg.str());
    }

    // check whether columns exceed maximum
    // (+1) is the name column in the table
    if (columns.size()-1 > kMaxPrescaleSets)
    {
      std::ostringstream msg;
      msg << mTableType << " contains "
        << columns.size() << " columns and exeeds the maximum (one algo column and maximum " << kMaxPrescaleSets << " prescale columns).";
      throw PrescaleTableException(msg.str());
    }
  }
  catch (const toolbox::AbstractTableException& e)
  {
    throw PrescaleTableException(e.what());
  }

  std::set<std::string> algoset;            // algo name set
  std::pair<std::set<std::string>::iterator, bool> rc;
  std::vector<std::string> duplicatedAlgos;     // duplicated algo name in table
  std::vector<std::string> invalidColumns;      // invalid columns (ps index > 255)

  for (size_t col = 0; col < colCount; ++col)
  {
    const std::string& column = columns[col];

    // Check whether the column index is in valid range between 0...255
    if (column != kAlgoNamePSIndex)
    {
      try
      {
        uint32_t index = toolbox::PrescaleSet::getIndexFromColumn(column);
        if (index < 0 || index >= kMaxPrescaleSets)
          invalidColumns.push_back(column);
      }
      catch (const toolbox::PrescaleSetException& e)
      {
        invalidColumns.push_back(column);
      }
    }

    // Check for duplicated algorithm names and non finite prescale values
    for (size_t row = 0; row < rowCount; ++row)
    {
      std::string algoName = table.getValueAt(row, kAlgoNamePSIndex)->toString();
      boost::trim(algoName);

      if (column == kAlgoNamePSIndex)
      {
        rc = algoset.insert(algoName);
        if (not rc.second)
          duplicatedAlgos.push_back(algoName);

        continue;
      }

    } // end loop row
  } // end loop col

  try
  {
    // Found duplicated algos names
    if (duplicatedAlgos.size())
    {
      std::stringstream singleMsg, multiMsg;
      singleMsg << "contains duplicated algo name";
      multiMsg << singleMsg.rdbuf() << "s";
      genericCheckErrors(duplicatedAlgos, singleMsg.str(), multiMsg.str());
    }

    // Found invalid columns
    if (invalidColumns.size())
    {
      std::stringstream singleMsg, multiMsg;
      singleMsg << "contains invalid table column (index must be between 0-255)";
      multiMsg << "contains invalid table columns (index must be between 0-255)";
      genericCheckErrors(invalidColumns, singleMsg.str(), multiMsg.str());
    }
  }
  catch (const toolbox::AbstractTableException& e)
  {
    throw PrescaleTableException(e.what());
  }

  // everything is fine - table is ok
}

// throws PrescaleTableException
void
PrescaleTable::verify(const xdata::Table& table, const uGtTriggerMenu& menu)
{
  // verify table w/o menu relation
  verify(table);

  const size_t rowCount = table.getRowCount();

  try
  {
    // check that menu contains entries
    checkMenuSizeValid(menu.size());

    std::vector<std::string> missingAlgos;    // algo is missing in table but required by menu
    std::set<std::string> algoset;
    std::pair<std::set<std::string>::iterator, bool> rc;

    for (size_t row = 0; row < rowCount; ++row)
    {
      std::string algoName = table.getValueAt(row, kAlgoNamePSIndex)->toString();
      boost::trim(algoName);

      // prepare a set of algoNames for later checks
      rc = algoset.insert(algoName);
      if (not rc.second)
      {
        // this may never happen
        std::ostringstream msg;
        msg << "Duplicated Algo found after verify()! Algo: " << algoName;
        throw AbstractTableException(msg.str());
      }
    } // end loop row

    // check whether each algo in menu is available in prescale table
    for (auto cit = menu.begin(); cit != menu.end(); ++cit)
    {
      const Algorithm* pAlgo = cit->second;
      if (algoset.find(pAlgo->getName()) == algoset.end())
      {
        // algo is available in prescale table
        missingAlgos.push_back(pAlgo->getName());
      }
    } // end loop menu

    // missing algos in table
    if (missingAlgos.size())
    {
      std::stringstream singleMsg, multiMsg;
      singleMsg << "is missing an algo that is required by the trigger menu";
      multiMsg << "is missing algos that are required by the trigger menu";
      genericCheckErrors(missingAlgos, singleMsg.str(), multiMsg.str());
    }
  }
  catch (const toolbox::AbstractTableException& e)
  {
    throw PrescaleTableException(e.what());
  }
}

void
PrescaleTable::load(const xdata::Table& tablePrescales,
  const xdata::Table& tableMask, const uGtTriggerMenu& menu)
{
  load(tablePrescales, tableMask, menu, true);
}

// throws PrescaleTableException
void
PrescaleTable::load(const xdata::Table& tablePrescales,
  const xdata::Table& tableMask, const uGtTriggerMenu& menu, bool doChecks)
{
  // check tableMask for correctness
  toolbox::FinorMaskTable maskTable;
  try
  {
    maskTable.load(tableMask, menu, doChecks);
  }
  catch(toolbox::MaskTableException &e)
  {
    throw PrescaleTableException(e.what());
  }

  if (doChecks)
  {
    // verify table w/ menu
    verify(tablePrescales, menu);
  }

  const size_t rowCount = tablePrescales.getRowCount();
  const std::vector<std::string> columns = tablePrescales.getColumns();

  for (size_t col = 0; col < columns.size(); ++col)
  {
    const std::string& column = columns.at(col);

    // skip algo name column
    if (column == kAlgoNamePSIndex)
      continue;

    std::map<std::string, double> prescales;
    for (size_t row = 0; row < rowCount; ++row)
    {
      std::string algo = tablePrescales.getValueAt(row, kAlgoNamePSIndex)->toString();
      boost::trim(algo);
      // ignore algos in table that are not existing in menu
      if (not menu.algoExists(algo))
        continue;

      // get prescale value string representation
      const std::string s = tablePrescales.getValueAt(row, column)->toString();

      try
      {
        // convert to double using custom precision
        const double prescale = toDouble(s, kPrescalePrecision);
        prescales.insert(std::make_pair(algo, prescale));
      }
      catch (std::invalid_argument& e)
      {
        std::ostringstream msg;
        msg << "Invalid prescale value (column: " << column << ", row: " << row << ")";
        msg << ": '" << s << "'";
        throw PrescaleTableException(msg.str());
      }
      catch (std::out_of_range& e)
      {
        std::ostringstream msg;
        msg << "Prescale value out of range (column: " << column << ", row: " << row << ")";
        msg << ": '" << s << "'";
        throw PrescaleTableException(msg.str());
      }
    }

    PrescaleSet set(column, prescales);
    mPrescaleSets[set.getIndex()] = set;

  } // end loop row
}



size_t
PrescaleTable::getNextPrescaleIndexAvailable(size_t index, size_t offset)
{
  auto it = mPrescaleSets.find(index);
  for (size_t i = 0; i < offset; ++i)
  {
    ++it;
    if (it == mPrescaleSets.end())
    {
      return index;
    }
  }
  return it->first;
}


const PrescaleSet&
PrescaleTable::getPrescaleSet(size_t index) const
{
  if (not mPrescaleSets.count(index))
  {
    std::ostringstream message;
    message << "no such prescale set index \"" << index << "\" in prescale table";
    throw PrescaleTableException(message.str());
  }
  return mPrescaleSets.at(index);
}

const PrescaleSet&
PrescaleTable::getNextPrescaleSet(size_t index, size_t offset)
{
  size_t nextIndex = getNextPrescaleIndexAvailable(index, offset);
  return getPrescaleSet(nextIndex);
}


size_t
PrescaleTable::size() const
{
  return mPrescaleSets.size();
}


PrescaleTable::iterator
PrescaleTable::begin() const
{
  return mPrescaleSets.begin();
}


PrescaleTable::iterator
PrescaleTable::end() const
{
  return mPrescaleSets.end();
}

void
PrescaleTable::clear()
{
  mPrescaleSets.clear();
}


std::map<toolbox::PrescaleColumnItem, double>
PrescaleTable::getPrescalesForAlgo(const std::string& algo)
{
  std::map<toolbox::PrescaleColumnItem, double> prescales;
  for(auto cit = mPrescaleSets.begin(); cit != mPrescaleSets.end(); ++cit)
  {
    toolbox::PrescaleSet& prescaleSet = cit->second;
    toolbox::PrescaleColumnItem column(prescaleSet.getIndex(), prescaleSet.getLabel());
    const double prescale = prescaleSet.getPrescale(algo);
    prescales.insert(std::make_pair(column, prescale));
  }

  return prescales;
}

void
PrescaleTable::print()
{
  std::stringstream msg;

  for (auto cit = mPrescaleSets.begin(); cit != mPrescaleSets.end(); ++cit)
  {
    const toolbox::PrescaleSet& prescaleSet = cit->second;

    msg << "psindex=" << cit->first << "size=" << prescaleSet.size() <<
        " prescaleset=" << std::endl;

    for(auto it = prescaleSet.begin(); it != prescaleSet.end(); ++it)
    {
      msg << "... name=" << it->first
          << " prescale=" << it->second
          << std::endl;
    }

    msg << std::endl;
  }
  std::cout << msg.str();
}

// throws std::invalid_argument
// throws std::std::out_of_range
double
PrescaleTable::toDouble(const std::string& s, size_t precision) const
{
  const double scale = std::pow(10, precision);
  return std::round(std::stod(s) * scale) / scale;
}

} // namespace toolbox
} // namespace ugt
