#include "ugt/swatch/toolbox/AbstractTable.h"

#include <sstream>
#include <algorithm>


namespace ugt {
namespace toolbox {

const std::string kLhcBxRange = "1-3564";
const std::string kDefaultAlgo = "ALL";


AbstractTable::AbstractTable(std::string tableType)
  : mTableType(tableType)
{
}


bool
AbstractTable::isDefaultAlgo(std::string algo)
{
  bool bDefaultAlgo = false;
  if (algo == toolbox::kDefaultAlgo)
    bDefaultAlgo = true;

  return bDefaultAlgo;
}


template <typename T>
void
AbstractTable::genericCheckErrors(const std::vector<T>& vec, const std::string& singleMsg, const std::string& multiMsg)
{
  if (vec.size())
  {
    std::ostringstream message;
    message << mTableType << " ";

    if (vec.size() > 1)
      message << multiMsg << ": [";
    else
      message << singleMsg << ": [";
    for (auto it = vec.begin(); it != vec.end(); ++it)
    {
      if (it != vec.begin())
        message << ", ";
      message << *it;
    }
    message << "]";
    throw AbstractTableException(message.str());
  }
}

template <typename T>
void
AbstractTable::genericCheckErrors(const std::set<T>& set, const std::string& singleMsg, const std::string& multiMsg)
{
  if (set.size())
  {
    std::ostringstream message;
    message << mTableType << " ";

    if (set.size() > 1)
      message << multiMsg << ": [";
    else
      message << singleMsg << ": [";
    for (auto it = set.begin(); it != set.end(); ++it)
    {
      if (it != set.begin())
        message << ", ";
      message << *it;
    }
    message << "]";
    throw AbstractTableException(message.str());
  }
}

void
AbstractTable::checkMenuSizeValid(size_t menuSize)
{
  if (menuSize < 1)
  {
    std::stringstream msg;
    msg << "uGtTriggerMenu is empty. A valid trigger menu needs to be loaded first.";
    msg << " Wait for the Run Control bringing the uGt Cell into 'Configured' state or";
    msg << " use the SWATCH Command 'uGtTriggerMenuCommand'";
    throw AbstractTableException(msg.str());
  }
}

void
AbstractTable::checkColumnsValid(const std::vector<std::string>& columns, const std::vector<std::string>& columnsExpected)
{
  std::ostringstream msg;
  msg << mTableType << " ";

  if (columns.size() != columnsExpected.size())
  {
    // mask or bxmask table
    msg << "contains " << columns.size();
    msg << " columns instead of " << columnsExpected.size();
    msg << ". Check table for line '<columns>";
    for (auto cit = columnsExpected.begin(); cit != columnsExpected.end(); ++cit)
    {
      if (cit == columnsExpected.begin())
      {
        msg << *cit;
        continue;
      }
      msg << "," << *cit;
    }
    msg << "</columns>.'";
    msg << " There may be the wrong table loaded.";
    throw AbstractTableException(msg.str());
  }
  else
  {
    bool bMissing = false;
    msg << "is missing column ";
    for (auto cit = columnsExpected.begin(); cit != columnsExpected.end(); ++cit)
    {
      auto itFound = std::find(columns.begin(), columns.end(), *cit);
      if (itFound == columns.end())
      {
        bMissing = true;
        msg << "'" << *cit << "',";
      }
    }
    msg << ".";
    if (bMissing)
    {
      throw AbstractTableException(msg.str());
    }
  }
}

void
AbstractTable::checkRowsSizeValid(size_t rowsSize)
{
  if (rowsSize < 1)
  {
    std::stringstream msg;
    msg << mTableType << " ";
    msg << "must contain at least a default row ";
    msg << "containing '<row>'" << toolbox::kDefaultAlgo
        << ",n</row>' with n = [0,1]";
    throw AbstractTableException(msg.str());
  }
}

bool
AbstractTable::isDefaultTable(const size_t cols, const size_t rows)
{
  return (cols <= 0 && rows <= 0);
}

/* template instanciations */
template void AbstractTable::genericCheckErrors(const std::vector<uint32_t>& vec, const std::string& singleMsg, const std::string& multiMsg);
template void AbstractTable::genericCheckErrors(const std::vector<std::string>& vec, const std::string& singleMsg, const std::string& multiMsg);
template void AbstractTable::genericCheckErrors(const std::set<std::string>& set, const std::string& singleMsg, const std::string& multiMsg);

} // namespace toolbox
} // namespace ugt
