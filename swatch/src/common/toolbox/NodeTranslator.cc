#include "ugt/swatch/toolbox/NodeTranslator.h"

#include "uhal/uhal.hpp"

#include "boost/date_time/posix_time/posix_time.hpp"
#include "boost/uuid/uuid.hpp"
#include "boost/uuid/uuid_io.hpp"

#include <algorithm>
#include <ctime>
#include <iomanip>
#include <map>
#include <sstream>

namespace ugt {
namespace toolbox {


/* public methods */


NodeTranslator::NodeTranslator() = default;


NodeTranslator::~NodeTranslator() = default;


template <typename T>
std::string
NodeTranslator::tr(const ::uhal::Node& node, const ::uhal::ValWord<T>& value)
{
  const std::string type = getType(node);
  if (type == "version")
    return tr_version(value.value());
  if (type == "timestamp")
    return tr_timestamp(value.value());
  if (type == "signed")
    return tr_signed(value.value());
  if (type == "boolean")
    return tr_boolean(value.value());
  if (type == "hex")
    return tr_hex(value.value());
  return tr_default(value.value());
}


template <typename T>
std::string
NodeTranslator::tr(const ::uhal::Node& node, const ::uhal::ValVector<T>& values)
{
  const std::string type = getType(node);
  if (type == "string")
    return tr_string(values.value());
  if (type == "uuid")
    return tr_uuid(values.value());
  return tr_default(values.value());
}


template <typename T>
std::string
NodeTranslator::operator() (const ::uhal::Node& node, const ::uhal::ValWord<T>& value)
{
  return tr(node, value);
}


template <typename T>
std::string
NodeTranslator::operator() (const ::uhal::Node& node, const ::uhal::ValVector<T>& values)
{
  return tr(node, values);
}


/* protected methods */


template <typename T>
std::string
NodeTranslator::tr_default(const T& value)
{
  return "NodeTranslator::unknown_value_type";
}


template <typename T>
std::string
NodeTranslator::tr_default(const std::vector<T>& values)
{
  return "NodeTranslator::unknown_vector_type";
}


template <typename T>
std::string
NodeTranslator::tr_string(const std::vector<T>& values)
{
  if (values.empty())
    return tr_default(values);
  std::vector<uint8_t> bytes = serialize(values);
  std::string s(bytes.size(), 0);
  std::copy(bytes.begin(), bytes.end(), s.begin());
  return s;
}


template <typename T>
std::string
NodeTranslator::tr_uuid(const std::vector<T>& values)
{
  if (values.empty())
    return tr_default(values);
  std::vector<uint8_t> bytes = serialize(values);
  std::reverse(bytes.begin(), bytes.end()); // Workaround for VHDL writer enc.
  boost::uuids::uuid uuid;
  if (bytes.size() == uuid.size())
    std::copy(bytes.begin(), bytes.end(), uuid.begin());
  std::ostringstream os;
  os << uuid;
  return os.str();
}


template <typename T>
std::string
NodeTranslator::tr_version(const T& value)
{
  std::ostringstream os;
  std::vector<uint8_t> bytes = serialize(value);
  os << static_cast<signed>(bytes.at(2))<< ".";
  os << static_cast<signed>(bytes.at(1)) << ".";
  os << static_cast<signed>(bytes.at(0));
  return os.str();
}


template <typename T>
std::string
NodeTranslator::tr_timestamp(const T& value)
{
  boost::posix_time::ptime t;
  t = boost::posix_time::from_time_t(static_cast<time_t>(value));
  return boost::posix_time::to_iso_extended_string(t);
}


template <typename T>
std::string
NodeTranslator::tr_signed(const T& value)
{
  std::ostringstream os;
  os << static_cast<int64_t>(value);
  return os.str();
}


template <typename T>
std::string
NodeTranslator::tr_boolean(const T& value)
{
  std::ostringstream os;
  os << static_cast<bool>(value);
  return os.str();
}


template <typename T>
std::string
NodeTranslator::tr_hex(const T& value)
{
  std::ostringstream os;
  os << "0x" << std::hex << std::setfill('0') << std::setw(8) << value;
  return os.str();
}


/* private methods */


std::string
NodeTranslator::getType(const ::uhal::Node& node)
{
  const boost::unordered_map<std::string, std::string>& params = node.getParameters();
  boost::unordered_map<std::string, std::string>::const_iterator result = params.find("type");
  return (result != params.end()) ? result->second : "";
}


template <typename T>
std::vector<uint8_t>
NodeTranslator::serialize(const T& value)
{
  std::vector<uint8_t> bytes(sizeof(T));
  const uint8_t* p = reinterpret_cast<const uint8_t*>(&value);
  for (size_t i = 0; i < sizeof(T); ++i)
  {
    bytes[i] = p[i];
  }
  return bytes;
}


template <typename T>
std::vector<uint8_t>
NodeTranslator::serialize(const std::vector<T>& values)
{
  std::vector<uint8_t> bytes;
  for (const auto& value: values)
  {
    std::vector<uint8_t> slice = serialize(value);
    bytes.insert(bytes.end(), slice.begin(), slice.end());
  }
  return bytes;
}


/* template instantiations */


template std::string NodeTranslator::tr(const ::uhal::Node&, const ::uhal::ValWord<uint32_t>&);
template std::string NodeTranslator::tr(const ::uhal::Node&, const ::uhal::ValVector<uint32_t>&);
template std::string NodeTranslator::operator() (const ::uhal::Node&, const ::uhal::ValWord<uint32_t>&);
template std::string NodeTranslator::operator() (const ::uhal::Node&, const ::uhal::ValVector<uint32_t>&);
template std::vector<uint8_t> NodeTranslator::serialize(const uint32_t& value);
template std::vector<uint8_t> NodeTranslator::serialize(const std::vector<uint32_t>& values);

} // namespace toolbox
} // namespace ugt
