#include "ugt/swatch/toolbox/MemoryImage.h"

#include <cstdlib>
#include <stdint.h>
#include <iomanip>
#include <sstream>

namespace ugt {
namespace toolbox {


template<class T>
MemoryImage<T>::MemoryImage()
 : values_(), cols_(0), rows_(0)
{
}


template<class T>
MemoryImage<T>::MemoryImage(size_t cols, size_t rows, const T& value)
 : values_(cols * rows, value), cols_(cols), rows_(rows)
{
}


template<class T>
MemoryImage<T>::~MemoryImage() = default;


template<class T>
size_t
MemoryImage<T>::cols() const
{
  return cols_;
}


template<class T>
size_t
MemoryImage<T>::rows() const
{
  return rows_;
}


template<class T>
size_t
MemoryImage<T>::size() const
{
  return values_.size();
}


template<class T>
const std::vector<T>&
MemoryImage<T>::values() const
{
  return values_;
}


template<class T>
void
MemoryImage<T>::setValues(const std::vector<T>& values)
{
  clear(); // Clear values before assigning a probably shorter list of values.
  typename std::vector<T>::const_iterator src = values.begin();
  typename std::vector<T>::iterator dest = values_.begin();
  while (src != values.end() and dest != values_.end())
    *dest++ = *src++;
}


template<class T>
const T&
MemoryImage<T>::value(size_t index) const
{
  return values_.at(index); // Throws exception on index violation.
}


template<class T>
void
MemoryImage<T>::setValue(size_t index, const T& value)
{
  values_.at(index) = value; // Throws exception on index violation.
}


template<class T>
const T&
MemoryImage<T>::value(size_t col, size_t row) const
{
  return values_.at(col * rows() + row); // Throws exception on index violation.
}


template<class T>
void
MemoryImage<T>::setValue(size_t col, size_t row, const T& value)
{
  values_.at(col * rows() + row) = value; // Throws exception on index violation.
}


template<class T>
bool
MemoryImage<T>::test(size_t n, size_t row) const
{
  const size_t col = n / (sizeof(T) * 8);
  return value(col, row) & (1 << (n - (col * sizeof(T) * 8)));
}


template<class T>
void
MemoryImage<T>::set(size_t n, size_t row, bool value)
{
  const size_t bitwidth = sizeof(T) * 8;
  const size_t col = n / bitwidth;
  const size_t offset = col * bitwidth;
  T buffer = this->value(col, row);
  if (value)
    buffer |= (1 << (n - offset));
  else
    buffer &= ~(1 << (n - offset));
  setValue(col, row, buffer);
}


template<class T>
void
MemoryImage<T>::clear(const T& value)
{
  typename std::vector<T>::iterator dest = values_.begin();
  while (dest != values_.end())
    *dest++ = value;
}


template<class T>
void
MemoryImage<T>::fillCounter()
{
  for (size_t i = 0; i < size(); ++i)
    setValue(i, i);
}


template<class T>
void
MemoryImage<T>::fillRandom()
{
  for (size_t i = 0; i < size(); ++i)
    setValue(i, rand());
}


template<class T>
std::ostream&
operator<<(std::ostream& o, const MemoryImage<T>& image)
{
  const size_t charcount = sizeof(T) * 2; // hexchars required to display value of type T
  for (size_t row = 0; row < image.rows(); ++row)
  {
    if (row)
      o << std::endl;
    std::stringstream ss;
    ss << std::hex << std::setfill('0');
    for (size_t col = image.cols(); col > 0; --col) // take care: reverse order of words for feeding the stream.
    {
      ss << std::setw(charcount) << image.value((col - 1) * image.rows() + row);
    }
    o << ss.str() << std::flush;
  }
  return o;
}

/* template instantiations */

template class MemoryImage<uint32_t>;
template ::std::ostream& operator<<(::std::ostream&, const MemoryImage<uint32_t>&);


} // namespace toolbox
} // namespace ugt
