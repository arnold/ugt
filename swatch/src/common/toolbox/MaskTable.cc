#include "ugt/swatch/toolbox/MaskTable.h"

#include <boost/lexical_cast.hpp>

#include <cstdlib>
#include <stdexcept>
#include <sstream>

namespace ugt {
namespace toolbox {

const std::string kAlgoName = "algo";
const std::string kFinorMask = "mask";
const std::string kVetoMask = "veto";


MaskTable::MaskTable(const std::string& type)
: AbstractTable(), mType(type), mDefaultMask(0)
{
  if (type == kFinorMask)
    mTableType = "FinorMaskTable";
  else if (type == kVetoMask)
    mTableType = "VetoMaskTable";
}

MaskTable::MaskTable(const std::string& type,
    const xdata::Table& table, const uGtTriggerMenu& menu)
: AbstractTable(), mType(type), mDefaultMask(0)
{
  load(table, menu);
}


// throws MaskTableException
void
MaskTable::verify(const xdata::Table& xtable)
{
  std::stringstream msg;
  const std::vector<std::string> columns = xtable.getColumns();
  const size_t rowCount = xtable.getRowCount();

  try
  {
    // check whether 2 columns exist
    std::vector<std::string> colExpected;
    colExpected.push_back(kAlgoName);
    colExpected.push_back(mType);
    checkColumnsValid(columns, colExpected);

    // check that minimum 1 row exist
    checkRowsSizeValid(rowCount);
  }
  catch (const toolbox::AbstractTableException &e)
  {
    throw MaskTableException(e.what());
  }


  std::set<std::string> algoset;            // algo name set
  std::pair<std::set<std::string>::iterator, bool> rc;
  bool bFoundDefaultAlgo = false;
  std::vector<std::string> duplicatedAlgos;     // duplicated algo name in table
  std::vector<std::string> maskOutOfRange;      // mask/veto is not 0 or 1

  // Check for duplicates in algo row
  for (size_t row = 0; row < rowCount; ++row)
  {
    std::string algoName = xtable.getValueAt(row, kAlgoName)->toString();
    uint32_t mask = boost::lexical_cast<uint32_t>(xtable.getValueAt(row, mType)->toString());

    // Search for duplicated algo names
    rc = algoset.insert(algoName);
    if (not rc.second)
      duplicatedAlgos.push_back(algoName);

    // Is default algo available
    if (isDefaultAlgo(algoName))
    {
      bFoundDefaultAlgo = true;
    }

    // Search for mask values out of range (0 || 1)
    if (mask != 0 && mask != 1)
      maskOutOfRange.push_back(algoName);
  }

  try
  {
    // Default algo is missing
    if (not bFoundDefaultAlgo)
    {
      // Default algo is missing
      checkRowsSizeValid(0);
    }

    // Found duplicated algos names
    if (duplicatedAlgos.size())
    {
      // check for duplicated algo names in mask table
      std::stringstream singleMsg, multiMsg;
      singleMsg << "contains duplicated algo name";
      multiMsg << singleMsg.rdbuf() << "s";
      genericCheckErrors(duplicatedAlgos, singleMsg.str(), multiMsg.str());
    }

    // Found mask values ot ouf range
    if (maskOutOfRange.size())
    {
      // check whether mask value is 0/1 other wise raise error
      std::stringstream singleMsg, multiMsg;
      singleMsg << "contains invalid mask value (mask must be '1' or '0') for algo";
      multiMsg << singleMsg.rdbuf() << "s";
      genericCheckErrors(maskOutOfRange, singleMsg.str(), multiMsg.str());
    }
  }
  catch (const toolbox::AbstractTableException& e)
  {
    throw MaskTableException(e.what());
  }

  // everything is fine - table is ok
}

// throws MaskTableException
void
MaskTable::verify(const xdata::Table& xtable, const uGtTriggerMenu& menu)
{
  const size_t rowCount = xtable.getRowCount();

  // verify table w/o menu relation
  verify(xtable);

  try
  {
    // check that menu contains entries
    checkMenuSizeValid(menu.size());

    std::pair<std::map<std::string, uint32_t>::iterator,bool> rc;
    std::set<std::string> unknownAlgos;      // algo is not known in menu

    for (size_t row = 0; row < rowCount; ++row)
    {
      std::string algoName = xtable.getValueAt(row, kAlgoName)->toString();

      // skip default algo 'ALL'
      if (isDefaultAlgo(algoName))
        continue;

      // check whether algo is available in menu
      if (not menu.algoExists(algoName))
      {
        // algo in table is not available in menu
        unknownAlgos.insert(algoName);
      }
    } // end loop row


    // unknownAlgo
    std::stringstream singleMsg, multiMsg;
    singleMsg << "contains unknown algo";
    multiMsg << multiMsg.rdbuf() << "s";
    genericCheckErrors(unknownAlgos, singleMsg.str(), multiMsg.str());

  }
  catch (const toolbox::AbstractTableException& e)
  {
    throw MaskTableException(e.what());
  }

}

void
MaskTable::load(const xdata::Table& table, const uGtTriggerMenu& menu)
{
  load(table, menu, true);
}

// throws MaskTableException
void
MaskTable::load(const xdata::Table& table, const uGtTriggerMenu& menu, bool doChecks)
{
  if (doChecks)
  {
    // verify table w/ menu
    verify(table, menu);
  }

  const size_t rowCount = table.getRowCount();
  for (size_t row = 0; row < rowCount; ++row)
  {
    std::string algoName = table.getValueAt(row, kAlgoName)->toString();
    uint32_t mask = boost::lexical_cast<uint32_t>(table.getValueAt(row, mType)->toString());

    // skip default algo
    if (isDefaultAlgo(algoName))
    {
      mDefaultMask = mask;
    }

    // initialise masks with with default mask
    if (row == 0)
    {
      std::pair<std::map<std::string, uint32_t>::iterator,bool> rc;

      // file mMask with tm algos and fill mask with default mask
      for (auto cit = menu.begin(); cit != menu.end(); ++cit)
      {
        const Algorithm* a = cit->second;

        // fill mask with default value
        rc = mMask.insert(std::make_pair(a->getName(), mDefaultMask));
        if (not rc.second)
        {
          // this may never happen, menu algo name is duplicated
          std::ostringstream msg;
          msg << mTableType << " duplicated algo found in menu.";
          throw MaskTableException(msg.str());
        }
      } // end loop menu
    }

    auto itAlgo = mMask.find(algoName);
    if (itAlgo != mMask.end())
    {
      mMask[algoName] = mask;
    }
  } // end loop row

}


size_t
MaskTable::size() const
{
  return mMask.size();
}

uint32_t
MaskTable::getMask(const std::string& algo) const
{
  auto it = mMask.find(algo);
  if (it == mMask.end())
  {
    std::ostringstream msg;
    msg << mTableType << ": No mask found for algo name = " << algo;
    throw MaskTableException(msg.str());
  }
  return it->second;
}

uint32_t
MaskTable::getDefaultMask() const
{
  return mDefaultMask;
}


const std::map<std::string, uint32_t>&
MaskTable::getMaskMap() const
{
  return mMask;
}


MaskTable::iterator
MaskTable::begin() const
{
  return mMask.begin();
}

MaskTable::iterator
MaskTable::end() const
{
  return mMask.end();
}

void
MaskTable::clear()
{
  mDefaultMask = 0;
  mMask.clear();
}

void
MaskTable::add(const std::string& algoName, uint32_t mask)
{
  mMask.insert(std::make_pair(algoName, mask));
}


void
MaskTable::print()
{
  std::stringstream ss;
  ss << "================ " << mTableType << " =================" << std::endl;
  ss << "Default mask: " << mDefaultMask << std::endl;
  for (auto cit = begin(); cit != end(); ++cit)
  {
    ss << cit->first << "," << cit->second << std::endl;
  }
  ss << "=============================================" << std::endl;
  std::cout << ss.str();
}



FinorMaskTable::FinorMaskTable()
: MaskTable(kFinorMask)
{ }

FinorMaskTable::FinorMaskTable(const xdata::Table& table, const uGtTriggerMenu& menu)
: MaskTable(kFinorMask, table, menu)
{ }

FinorMaskTable::~FinorMaskTable()
{ }



VetoMaskTable::VetoMaskTable()
: MaskTable(kVetoMask)
{ }

VetoMaskTable::VetoMaskTable(const xdata::Table& table, const uGtTriggerMenu& menu)
: MaskTable(kVetoMask, table, menu)
{ }

VetoMaskTable::~VetoMaskTable()
{ }


} // namespace toolbox
} // namespace ugt
