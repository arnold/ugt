#include "ugt/swatch/toolbox/PrescaleSet.h"

#include "ugt/swatch/toolbox/MaskTable.h"

#include <boost/algorithm/string.hpp>

#include <cstdlib>
#include <stdint.h>
#include <stdexcept>
#include <sstream>

namespace ugt {
namespace toolbox {


PrescaleSet::PrescaleSet()
: mColumn(), mIndex(0), mLabel(), mPrescales()
{
}


PrescaleSet::PrescaleSet(const std::string& column,
                         const std::map<std::string, double> prescales)
: mColumn(column), mPrescales(prescales)
{
  mIndex = toolbox::PrescaleSet::getIndexFromColumn(mColumn);
  mLabel = toolbox::PrescaleSet::getLabelFromColumn(mColumn);
}

PrescaleSet::~PrescaleSet()
{
}


const std::string&
PrescaleSet::getColumn() const
{
  return mColumn;
}

size_t
PrescaleSet::getIndex() const
{
  return mIndex;
}


const std::string&
PrescaleSet::getLabel() const
{
  return mLabel;
}

const std::string
PrescaleSet::getFormattedLabel() const
{
  PrescaleColumnItem item = PrescaleColumnItem(mIndex, mLabel);
  return item.getFormattedLabel();
}

size_t
PrescaleSet::size() const
{
  return mPrescales.size();
}

bool
PrescaleSet::isEmpty() const
{
  return mPrescales.size()==0;
}

void
PrescaleSet::clear()
{
  mPrescales.clear();
  mIndex = 0;
  mColumn.clear();
  mLabel.clear();
}

void
PrescaleSet::updateSet(const std::string& column, const std::map<std::string, double>& prescales)
{
  clear();

  mColumn = column;
  mIndex = toolbox::PrescaleSet::getIndexFromColumn(mColumn);
  mLabel = toolbox::PrescaleSet::getLabelFromColumn(mColumn);

  for (auto cit = prescales.begin(); cit != prescales.end(); ++cit)
  {
    mPrescales.insert(std::make_pair(cit->first, cit->second));
  }
}


double
PrescaleSet::getPrescale(const std::string& algo) const
{
  auto it = mPrescales.find(algo);
  if (it == mPrescales.end())
  {
    std::ostringstream msg;
    msg << "No prescale found for algo=" << algo;
    throw PrescaleSetException(msg.str());
  }

  return it->second;
}


const
std::map<std::string, double>&
PrescaleSet::getPrescales() const
{
  return mPrescales;
}


PrescaleSet::iterator
PrescaleSet::findPrescale(const std::string& algo) const
{
  return mPrescales.find(algo);
}

PrescaleSet::iterator
PrescaleSet::begin() const
{
  return mPrescales.begin();
}

PrescaleSet::iterator
PrescaleSet::end() const
{
  return mPrescales.end();
}

// throws PrescaleSetException
size_t
PrescaleSet::getIndexFromColumn(const std::string &column)
{
  size_t index = 0;
  std::vector<std::string> tokens;
  boost::split(tokens, column, boost::is_any_of(":"));

  std::string strToken = tokens.at(0);
  boost::trim(strToken);
  try
  {
    index = boost::lexical_cast<size_t>(strToken);
  }
  catch (boost::bad_lexical_cast &e)
  {
    std::ostringstream message;
    message << "Column '" << column <<
        "' does not contain or is no number for '" << strToken << "'";
    throw PrescaleSetException(message.str());
  }
  return index;
}

std::string
PrescaleSet::getLabelFromColumn(const std::string& column)
{
  std::ostringstream label;
  std::vector<std::string> tokens;
  boost::split(tokens, column, boost::is_any_of(":"));

  if (tokens.size() == 1)
  {
    // only index listed (e.g.: '5')
    std::string sIndex = tokens.at(0);
    boost::trim(sIndex);
    label << sIndex;
  }
  else if (tokens.size() == 2)
  {
    // index and luminosity label (e.g.: '5:1.15E34')
    std::string sLabel = tokens.at(1);
    boost::trim(sLabel);
    label << sLabel;
  }
  else
  {
    // too many delimiter ":"
    label << column;
    std::cout <<"PrescaleSet: column syntax not correct for '"
        << column << "'. Use syntax: <index>:<label>" << std::endl;
  }
  return label.str();
}

} // namespace toolbox
} // namespace ugt
