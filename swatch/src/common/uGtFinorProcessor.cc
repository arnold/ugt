/**
 * @file    uGtFinorProcessor.cc
 * @author  Gregor Aradi, Bernhard Arnold
 * @brief   Finor board processor implementation
 * @date    March 2016
 */
#include "ugt/swatch/uGtFinorProcessor.h"
#include "ugt/swatch/uGtFinorCommands.h"

// SWATCH header
#include "swatch/core/Factory.hpp"
#include "swatch/action/Command.hpp"
#include "swatch/action/CommandSequence.hpp"
#include "swatch/action/StateMachine.hpp"

#include "ugt/swatch/uGtFinorAlgo.h"

SWATCH_REGISTER_CLASS(ugt::uGtFinorProcessor);

namespace amc502{
  class AMC502Processor;
}

namespace ugt
{

uGtFinorProcessor::uGtFinorProcessor(const ::swatch::core::AbstractStub& aStub) :
 amc502::AMC502Processor(aStub)
{
  // Add algo interface
  registerInterface(new uGtFinorAlgo(this->driver()));

  typedef ::swatch::action::CommandSequence tComSeq;
  typedef ::swatch::action::Command tCmd;

  // Add commands
  tCmd& cmdInputMask = registerCommand<uGtFinorInputMaskCommand>("uGtFinorInputMask");

  // Setup sequence
  tComSeq& seqSetup = registerSequence("uGtFinorSeqSetup", CmdIds::kReset)
    .then(CmdIds::kPayloadReset);

  // Configure sequence
  tComSeq& seqConfigure = registerSequence("uGtFinorConfigure", cmdInputMask);

  // add commands/sequences to the state machine transitions
  swatch::processor::RunControlFSM& lFSM = getRunControlFSM();
  lFSM.setup.add(seqSetup);
  lFSM.configure.add(seqConfigure);
  lFSM.start.add(getCommand(CmdIds::kResetCounter));
}


uGtFinorProcessor::~uGtFinorProcessor()
{
}

} // namespace ugt
