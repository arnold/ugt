/**
 * @file    uGtFinorPreviewAlgo.cc
 * @author  Gregor Aradi, Bernhard Arnold
 * @brief   Finor preview board algo implementation
 * @date    2016-11-28
 */
#include "ugt/swatch/uGtFinorPreviewAlgo.h"

#include "ugt/swatch/Constants.h"
#include "ugt/swatch/AMC502Controller.h"

#include "swatch/action/MonitoringSetting.hpp"
#include "swatch/core/MetricConditions.hpp"

#include "uhal/HwInterface.hpp"

#include "log4cplus/loggingmacros.h"

#include <limits>
#include <stdexcept>


namespace ugt
{


uGtFinorPreviewAlgo::uGtFinorPreviewAlgo(amc502::AMC502Controller& driver)
  : driver_(driver)
  , metricBxNumberMax_(registerMetric<uint32_t>("bunchCrossingNumberMax"))
  , metricLuminositySegmentNumber_(registerMetric<uint32_t>("luminositySegmentNumber"))
  , metricOrbitCounter_(registerMetric<uint64_t>("orbitCounter"))
  , metricRateCntFinorPreviewCounter_(registerMetric<uint32_t>("FinorPreviewCounter"))
  , metricRateCntFinorPreviewRate_(registerMetric<double>("FinorPreviewRate"))
  , vecMetricRateCntLocFinorCounter_()
  , vecMetricRateCntLocFinorRate_()
{
  for (uint32_t i = 0; i < kMaxUgtModules; i++)
  {
    std::stringstream ss;
    ss << "LocalFinorCounter" << std::setfill('0') << std::setw(1) << i;
    vecMetricRateCntLocFinorCounter_.push_back(&registerMetric<uint32_t>(ss.str()));

    ss.clear(); ss.str("");
    ss << "LocalFinorRate" << std::setfill('0') << std::setw(1) << i;
    vecMetricRateCntLocFinorRate_.push_back(&registerMetric<double>(ss.str()));
  }
}

uGtFinorPreviewAlgo::~uGtFinorPreviewAlgo()
{
}

void
uGtFinorPreviewAlgo::retrieveMetricValues()
{
  ::uhal::HwInterface& hw = driver_.hw();


  for (uint32_t retry = 0; retry < 2; retry++)
  {
    const uint32_t luminositySegmentBefore = getLuminositySection();

    // Prepare nodes for hw access
    const ::uhal::Node& node_rate_cnt_finor_tcds = hw.getNode("payload.rate_cnt_finor_tcds");
    ::uhal::ValWord<uint32_t> data_rate_cnt_finor_tcds = node_rate_cnt_finor_tcds.read();

    const ::uhal::Node& node_rate_cnt_loc_finor = hw.getNode("payload.rate_cnt_loc_finor");
    ::uhal::ValVector<uint32_t> data_rate_cnt_loc_finor = node_rate_cnt_loc_finor.readBlock(node_rate_cnt_loc_finor.getSize());

    hw.getClient().dispatch();

    const uint32_t bxNumberMax = getBxNumberMax();
    const uint32_t orbitNumber = getOrbitNumber();

    const uint32_t luminositySegmentAfter = getLuminositySection();

    if (luminositySegmentBefore == luminositySegmentAfter)
    {
      // Read from hardware was in same luminosity section
      // Thus, store retrieved data to matrics

      std::vector<uint32_t> vec_rate_cnt_loc_finor;
      vec_rate_cnt_loc_finor = data_rate_cnt_loc_finor.value();


      // Set metrics with latest data values


      setMetricValue<uint32_t>(metricRateCntFinorPreviewCounter_, data_rate_cnt_finor_tcds.value());
      setMetricValue<double>(metricRateCntFinorPreviewRate_, data_rate_cnt_finor_tcds.value()*ugt::kToRate);

      for (uint32_t i = 0; i < vec_rate_cnt_loc_finor.size(); i++)
      {
        setErrorCondition((*vecMetricRateCntLocFinorCounter_.at(i)), ::swatch::core::GreaterThanCondition<uint32_t>(kMaximumCounts));
        setMetricValue<uint32_t>(*(vecMetricRateCntLocFinorCounter_.at(i)), vec_rate_cnt_loc_finor.at(i));

        setErrorCondition((*vecMetricRateCntLocFinorRate_.at(i)), ::swatch::core::GreaterThanCondition<double>(kLhcClockFrequency));
        setMetricValue<double>(*(vecMetricRateCntLocFinorRate_.at(i)), vec_rate_cnt_loc_finor.at(i)*ugt::kToRate);
      }

      setMetricValue<uint32_t>(metricLuminositySegmentNumber_, luminositySegmentAfter);
      setMetricValue<uint64_t>(metricOrbitCounter_, orbitNumber);

      setMetricValue<uint32_t>(metricBxNumberMax_, bxNumberMax);
      setWarningCondition(metricBxNumberMax_, ::swatch::core::NotEqualCondition<uint32_t>(0xdeb));
      // Leave retry loop,
      break;
    }
  }
}


uint32_t
uGtFinorPreviewAlgo::getBxNumberMax() const
{
  ::uhal::HwInterface& hw = driver_.hw();

  const ::uhal::Node& node = hw.getNode("payload.tcm_regs.bx_nr_max");
  ::uhal::ValWord<uint32_t> value = node.read();

  hw.getClient().dispatch();

  return value;
}


uint64_t
uGtFinorPreviewAlgo::getOrbitNumber() const
{
  ::uhal::HwInterface& hw = driver_.hw();

  const ::uhal::Node& node_l = hw.getNode("payload.tcm_regs.orbit_nr_l");
  ::uhal::ValWord<uint32_t> value_l = node_l.read();

  const ::uhal::Node& node_h = hw.getNode("payload.tcm_regs.orbit_nr_h");
  ::uhal::ValWord<uint32_t> value_h = node_h.read();

  hw.getClient().dispatch();

  return static_cast<uint64_t>(value_h) << 32 | value_l;
}


uint32_t
uGtFinorPreviewAlgo::getLuminositySection() const
{
  ::uhal::HwInterface& hw = driver_.hw();

  const ::uhal::Node& node_luminosity_seg_nr = hw.getNode("payload.tcm_regs.luminosity_seg_nr");
  ::uhal::ValWord<uint32_t> data_luminosity_seg_nr = node_luminosity_seg_nr.read();

  hw.getClient().dispatch();

  return data_luminosity_seg_nr;
}


} /* end namespace ugt */
