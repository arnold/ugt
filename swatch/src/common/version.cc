#include "config/version.h"
#include "ugt/swatch/version.h"


GETPACKAGEINFO(ugtswatch)


void 
ugtswatch::checkPackageDependencies() throw (config::PackageInfo::VersionException)
{
  CHECKDEPENDENCY(config);
}

std::set<std::string, std::less<std::string> > 
ugtswatch::getPackageDependencies()
{
  std::set<std::string, std::less<std::string> > dependencies;
  ADDDEPENDENCY(dependencies,config);
  return dependencies;
}
/* eof */
