/**
 * @file    uGtExtCondProcessor.cc
 * @author  Gregor Aradi, Bernhard Arnold
 * @brief   Finor board processor implementation
 * @date    2016-02-29
 */
#include "ugt/swatch/uGtExtCondProcessor.h"
#include "ugt/swatch/uGtExtCondCommands.h"

// SWATCH header
#include "swatch/core/Factory.hpp"
#include "swatch/action/Command.hpp"
#include "swatch/action/CommandSequence.hpp"
#include "swatch/action/StateMachine.hpp"

#include "ugt/swatch/uGtExtCondAlgo.h"

SWATCH_REGISTER_CLASS(ugt::uGtExtCondProcessor);

namespace amc502{
  class AMC502Processor;
}


namespace ugt
{

uGtExtCondProcessor::uGtExtCondProcessor(const ::swatch::core::AbstractStub& aStub) :
 amc502::AMC502Processor(aStub)
{
  // Add algo interface
  registerInterface(new uGtExtCondAlgo(this->driver()));

  typedef ::swatch::action::CommandSequence tComSeq;
  typedef ::swatch::action::Command tCmd;

  // Add commands
  tCmd& cmdDelay = registerCommand<uGtExtCondDelayCommand>("uGtExtCondDelay");
  tCmd& cmdPhaseShift = registerCommand<uGtExtCondPhaseShiftCommand>("uGtExtCondPhaseShift");

  // Setup sequence
  tComSeq& seqSetup = registerSequence("uGtExtCondSeqSetup", CmdIds::kReset)
    .then(CmdIds::kPayloadReset);

  // Configure sequence
  tComSeq& seqConfigure = registerSequence("uGtExtCondConfigure", CmdIds::kCfgTxBuffers)
    .then(CmdIds::kCfgDataValidFmt)
    .then(cmdDelay)
    .then(cmdPhaseShift)
    ;

  // add commands/sequences to the state machine transitions
  swatch::processor::RunControlFSM& lFSM = getRunControlFSM();
  lFSM.setup.add(seqSetup);
  lFSM.configure.add(seqConfigure);
  lFSM.align.add(getCommand(CmdIds::kCfgTxMGTs));
  lFSM.start.add(getCommand(CmdIds::kResetCounter));
}


uGtExtCondProcessor::~uGtExtCondProcessor()
{

}

} // namespace ugt
