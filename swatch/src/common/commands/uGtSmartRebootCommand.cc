#include "ugt/swatch/commands/uGtSmartRebootCommand.h"

#include "ugt/swatch/uGtProcessor.h"
#include "ugt/swatch/toolbox/NodeTranslator.h"

#include "mp7/MP7Controller.hpp"
#include "mp7/MmcController.hpp"
#include "uhal/HwInterface.hpp"
#include "uhal/log/exception.hpp"

#include "xdata/Boolean.h"
#include "xdata/String.h"
#include "xdata/UnsignedInteger.h"

#include <cstdlib>


namespace ugt
{


uGtSmartRebootCommand::uGtSmartRebootCommand(const std::string& aId,
                                             ::swatch::action::ActionableObject& aActionable) :
  ::swatch::mp7::cmds::RebootFPGA(aId, aActionable)
  // result is UnsignedInteger
{
    //registerParameter("sdfile", xdata::String("")); // registered in parent
    registerParameter("uuid_fw", xdata::String(""));
    registerParameter("FWBuildVersion", xdata::String(""));
    registerParameter("autoloadfw", xdata::Boolean(false));
}


::swatch::action::Command::State
uGtSmartRebootCommand::code(const ::swatch::core::XParameterSet& params)
{
  setProgress(0., "uGtSmartRebootCommand ...");

  const std::string param_sdfile = params.get<xdata::String>("sdfile").value_;
  const std::string param_uuid_fw = params.get<xdata::String>("uuid_fw").value_;
  const bool param_autoloadfw = params.get<xdata::Boolean>("autoloadfw").value_;
  const std::string param_fwbuildversion = params.get<xdata::String>("FWBuildVersion").value_;

  bool requiresReboot = false;

  std::ostringstream msg;
  msg << "uGtSmartRebootCommand ... done";

  // Automatic fw reboot flag is disabled
  if (not param_autoloadfw)
  {
    msg << " (automatic firmware loading is disabled)";
    setStatusMsg(msg.str());
    setProgress(1., msg.str());
    return State::kDone;
  }

  setStatusMsg("uGtSmartRebootCommand ... automatic firmware loading enabled");


  // Check whether the sdfile contains the FW build version
  // gt_mp7_xe_vXYZA_module_0.bin contains XYZA
  size_t found = param_sdfile.find(param_fwbuildversion);
  if (found == std::string::npos)
  {
    msg.str(""); msg.clear();
    msg << "uGtSmartRebootCommand ... sdfile '" << param_sdfile
        << "' (L1CE) does not contain the FW build version (L1CE) '"
        << param_fwbuildversion << "'";
    setStatusMsg(msg.str());
    setProgress(1., msg.str());
    return State::kError;
  }


  uGtProcessor &p = getActionable<uGtProcessor>();
  ::mp7::MP7Controller& driver = p.driver();
  ::uhal::HwInterface& hw = driver.hw();

  // Check that an MP7 w/ at least an GoldenImage is available
  const ::uhal::Node& node_algorev = hw.getNode("ctrl.id.algorev");
  ::uhal::ValWord<uint32_t> data_algorev = node_algorev.read();

  // Check uGT MP7 FW uuid
  const ::uhal::Node& node_uuid_fw = hw.getNode("gt_mp7_gtlfdl.read_versions.l1tm_fw_uuid");
  ::uhal::ValVector<uint32_t> data_uuid_fw = node_uuid_fw.readBlock(node_uuid_fw.getSize());

  // Check uGT FW Build id
  const ::uhal::Node& node_fw_buildid = hw.getNode("gt_mp7_frame.module_info.build_version");
  ::uhal::ValWord<uint32_t> data_fw_buildid = node_fw_buildid.read();


  toolbox::NodeTranslator tr;
  try
  {
    hw.getClient().dispatch();


    const std::string uuid_fw = tr(node_uuid_fw, data_uuid_fw);
    setStatusMsg("uGtSmartRebootCommand ... retrieved firmware UUID: " + uuid_fw);

    uint32_t fwBuildVersion = std::stoul(param_fwbuildversion, nullptr, 16);

    // Reboot FPGA when FW UUID is not equal
    // or the uGT FW build version differs
    if (uuid_fw != param_uuid_fw || data_fw_buildid != fwBuildVersion)
    {
      requiresReboot = true;
    }
  }
  catch(uhal::exception::exception& e)
  {
    requiresReboot = true;
  }

  // FW is up-to-date
  if (not requiresReboot)
  {
    msg << " (firmware '" << param_sdfile << "' is up-to-date)";
    setProgress(1., msg.str());
    return State::kDone;
  }


  // Read available images from SD card
  ::mp7::MmcPipeInterface mmcNode(driver.hw().getNode< ::mp7::MmcPipeInterface>("uc"));
  std::vector<std::string> fileNames = mmcNode.ListFilesOnSD();

  auto itsdfile = std::find(fileNames.begin(), fileNames.end(), param_sdfile);
  if (itsdfile == fileNames.end())
  {
    // Image is not available on SD card
    std::ostringstream msg;
    msg << "uGtSmartRebootCommand ... firmware file '";
    msg << param_sdfile << "' is missing on SD card'. ";
    msg << "Check filename or upload it first onto the SD card.";
    setProgress(1., msg.str());
    return State::kError;
  }

  // Execute reboot
  setProgress(.2, "uGtSmartRebootCommand ... requiring reboot of image: " + param_sdfile);
  ::swatch::mp7::cmds::RebootFPGA::code(params);

  // Sanity check
  const ::uhal::Node& node_uuid_fw_sanity = hw.getNode("gt_mp7_gtlfdl.read_versions.l1tm_fw_uuid");
  ::uhal::ValVector<uint32_t> data_uuid_fw_sanity = node_uuid_fw_sanity.readBlock(node_uuid_fw_sanity.getSize());
  hw.getClient().dispatch();

  const std::string uuid_fw_sanity = tr(node_uuid_fw_sanity, data_uuid_fw_sanity);
  if (uuid_fw_sanity != param_uuid_fw)
  {
    std::ostringstream msg;
    msg << "uGtSmartRebootCommand ... rebooting firmware failed. ";
    msg << "Mismatch firware UUID (L1CE) " << param_uuid_fw << " != ";
    msg << "firmware UUID (HW) " << uuid_fw_sanity;
    setStatusMsg(msg.str());
    setProgress(1., msg.str());
    return State::kError;
  }

  msg << " (firmware loading of " << param_sdfile << " finished successfully)";
  setProgress(1., msg.str());
  return State::kDone;
}

} // namespace ugt
/* eof */
