//
// uGtPrescaleCommand has been splitted to
//   a) uGtPreparePrescaleCommand
//   b) uGtApplyPrescaleCommand
// for multi-board operations
//
#include "ugt/swatch/commands/uGtApplyPrescaleCommand.h"

#include "ugt/swatch/uGtProcessor.h"

#include "mp7/MP7Controller.hpp"
#include "uhal/HwInterface.hpp"

#include "xdata/UnsignedInteger.h"

namespace ugt
{

uGtApplyPrescaleCommand::uGtApplyPrescaleCommand(const std::string& aId,
                                                 ::swatch::action::ActionableObject& aActionable) :
  ::swatch::action::Command(aId, aActionable, xdata::UnsignedInteger())
  // result is UnsignedInteger
{
}


::swatch::action::Command::State
uGtApplyPrescaleCommand::code(const ::swatch::core::XParameterSet& params)
{
  setProgress(0., "uGtApplyPrescaleCommand ...");

  uGtProcessor &proc = getActionable<uGtProcessor>();
  ::mp7::MP7Controller& driver = proc.driver();
  ::uhal::HwInterface& hw = driver.hw();

  hw.getNode("gt_mp7_gtlfdl.command_pulses.request_update_factor_pulse").write(1);
  hw.getClient().dispatch();

  setResult(xdata::UnsignedInteger(0));
  setStatusMsg("uGtApplyPrescaleCommand: done");

  setProgress(1., "uGtApplyPrescaleCommand ... done");

  return State::kDone;
}

} // namespace ugt
/* eof */
