#include "ugt/swatch/commands/uGtResetCounterCommand.h"

#include "ugt/swatch/uGtProcessor.h"

#include "mp7/MP7Controller.hpp"
#include "uhal/HwInterface.hpp"

#include "xdata/UnsignedInteger.h"

namespace ugt
{

uGtResetCounterCommand::uGtResetCounterCommand(const std::string& aId,
                                 ::swatch::action::ActionableObject& aActionable) :
  ::swatch::action::Command(aId, aActionable, xdata::UnsignedInteger())
  // result is UnsignedInteger
{
}


::swatch::action::Command::State
uGtResetCounterCommand::code(const ::swatch::core::XParameterSet& params)
{
  setProgress(0., "uGtResetCounterCommand ...");

  uGtProcessor &p = getActionable<uGtProcessor>();
  ::mp7::MP7Controller& driver = p.driver();
  ::uhal::HwInterface& hw = driver.hw();

  // Reset uGT specific counter.
  const ::uhal::Node& node = hw.getNode("gt_mp7_frame.counter_reset");
  node.write(0x1);
  hw.getClient().dispatch();

  setResult(xdata::UnsignedInteger(0));
  setStatusMsg("uGtResetCounterCommand: " + node.getId());

  setProgress(1., "uGtResetCounterCommand ... done");

  return State::kDone;
}

} // namespace ugt
/* eof */
