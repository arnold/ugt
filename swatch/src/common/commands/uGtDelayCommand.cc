#include "ugt/swatch/commands/uGtDelayCommand.h"

#include "ugt/swatch/uGtProcessor.h"

#include "mp7/MP7Controller.hpp"
#include "uhal/HwInterface.hpp"

#include "xdata/UnsignedInteger.h"

namespace ugt
{

uGtDelayCommand::uGtDelayCommand(const std::string& aId,
                                 ::swatch::action::ActionableObject& aActionable) :
  ::swatch::action::Command(aId, aActionable, xdata::UnsignedInteger())
{
  registerParameter("l1a_latency", xdata::UnsignedInteger(0));
}


::swatch::action::Command::State
uGtDelayCommand::code(const ::swatch::core::XParameterSet& params)
{
  setProgress(0., "uGtDelayCommand ...");

  unsigned int delay_l1a_latency = params.get<xdata::UnsignedInteger>("l1a_latency").value_;

  uGtProcessor &p = getActionable<uGtProcessor>();
  ::mp7::MP7Controller& driver = p.driver();
  ::uhal::HwInterface& hw = driver.hw();

  const uhal::Node& l1a_latency = hw.getNode("gt_mp7_gtlfdl.l1a_latency_delay");

  l1a_latency.write(delay_l1a_latency);

  hw.getClient().dispatch();

  std::cout << "l1a_latency = " << delay_l1a_latency << std::endl;

  setResult(xdata::UnsignedInteger(0));
  setStatusMsg("uGtDelayCommand: done");

  setProgress(1., "uGtDelayCommand ... done");

  return State::kDone;
}

} // namespace ugt
/* eof */
