#include "ugt/swatch/commands/uGtResetCommand.h"

#include "ugt/swatch/uGtProcessor.h"

#include "mp7/MP7Controller.hpp"
#include "uhal/HwInterface.hpp"

#include "xdata/UnsignedInteger.h"

namespace ugt
{

uGtResetCommand::uGtResetCommand(const std::string& aId,
                                 ::swatch::action::ActionableObject& aActionable) :
  ::swatch::action::Command(aId, aActionable, xdata::UnsignedInteger())
  // result is UnsignedInteger
{
}


::swatch::action::Command::State
uGtResetCommand::code(const ::swatch::core::XParameterSet& params)
{
  setProgress(0., "uGtResetCommand ...");

  uGtProcessor &p = getActionable<uGtProcessor>();
  ::mp7::MP7Controller& driver = p.driver();
  ::uhal::HwInterface& hw = driver.hw();

  // Reset uGT specific logic.
  const ::uhal::Node& node = hw.getNode("gt_mp7_frame.pulse_reset");
  node.write(0x1);
  hw.getClient().dispatch();

  setResult(xdata::UnsignedInteger(0));
  setStatusMsg("uGtResetCommand: " + node.getId());
  std::cout << "uGtResetCommand: " << node.getId() << std::endl;

  setProgress(1., "uGtResetCommand ... done");

  return State::kDone;
}

} // namespace ugt
/* eof */
