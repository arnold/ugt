#include "ugt/swatch/commands/uGtCalTrigSuppressionCommand.h"

#include "ugt/swatch/uGtProcessor.h"

#include "mp7/MP7Controller.hpp"
#include "uhal/HwInterface.hpp"

#include "xdata/UnsignedInteger.h"

namespace ugt
{

uGtCalTrigSuppressionCommand::uGtCalTrigSuppressionCommand(const std::string& aId,
                                                           ::swatch::action::ActionableObject& aActionable) :
  ::swatch::action::Command(aId, aActionable, xdata::UnsignedInteger())
  // result is UnsignedInteger
{
  registerParameter("begin", xdata::UnsignedInteger(0));
  registerParameter("end", xdata::UnsignedInteger(0));
}


::swatch::action::Command::State
uGtCalTrigSuppressionCommand::code(const ::swatch::core::XParameterSet& params)
{
  setProgress(0., "uGtCalTrigSuppressionCommand ...");

  unsigned int begin = params.get<xdata::UnsignedInteger>("begin").value_;
  unsigned int end = params.get<xdata::UnsignedInteger>("end").value_;

  uGtProcessor &p = getActionable<uGtProcessor>();
  ::mp7::MP7Controller& driver = p.driver();
  ::uhal::HwInterface& hw = driver.hw();

  const uhal::Node& beginNode = hw.getNode("gt_mp7_gtlfdl.calibration_trigger_gap.begin");
  const uhal::Node& endNode = hw.getNode("gt_mp7_gtlfdl.calibration_trigger_gap.end");

  beginNode.write(begin);
  endNode.write(end);

  hw.getClient().dispatch();

  std::stringstream ss;
  ss << "calibration trigger suppression:" << std::endl;
  ss << " begin BX = " << begin << std::endl;
  ss << " end BX = " << end << std::endl;
  std::cout << ss.str() << std::endl;

  setResult(xdata::UnsignedInteger(0));
  setStatusMsg("uGtCalTrigSuppressionCommand: done");

  setProgress(1., "uGtCalTrigSuppressionCommand ... done");

  return State::kDone;
}

} // namespace ugt
/* eof */
