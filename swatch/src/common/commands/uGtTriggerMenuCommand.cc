#include "ugt/swatch/commands/uGtTriggerMenuCommand.h"

#include "ugt/swatch/toolbox/PrescaleTable.h"
#include "ugt/swatch/toolbox/MaskTable.h"
#include "ugt/swatch/toolbox/AlgoBXMaskTable.h"

#include "ugt/swatch/uGtProcessor.h"
#include "ugt/swatch/uGtAlgo.h"
#include "ugt/swatch/uGtTriggerMenu.h"

#include "utm/tmEventSetup/tmEventSetup.hh"

#include "mp7/MP7Controller.hpp"
#include "uhal/HwInterface.hpp"

#include "xdata/Table.h"
#include "xdata/String.h"
#include "xdata/UnsignedInteger.h"

#include "log4cplus/loggingmacros.h"
#include "pugixml.hpp"

namespace ugt
{

uGtTriggerMenuCommand::uGtTriggerMenuCommand(const std::string& aId,
                                             ::swatch::action::ActionableObject& aActionable) :
  ::swatch::action::Command(aId, aActionable, xdata::UnsignedInteger())
  // result is UnsignedInteger
{
  registerParameter("l1menu", xdata::Table());
  registerParameter("Xml file path", xdata::String(""));
  registerParameter("finorMask", xdata::Table());
  registerParameter("prescales", xdata::Table());
  registerParameter("algorithmBxMask", xdata::Table());
  registerParameter("vetoMask", xdata::Table());
}


::swatch::action::Command::State
uGtTriggerMenuCommand::code(const ::swatch::core::XParameterSet& params)
{
  setProgress(0., "uGtTriggerMenuCommand ... ");

  uGtProcessor &p = getActionable<uGtProcessor>();
  uGtAlgo& algo = dynamic_cast<uGtAlgo&>(p.getAlgo());
  LOG4CPLUS_INFO(getActionable().getLogger(), "uGtTriggerMenuCommand: slot = " << p.getSlot());
  algo.setSlotId(p.getSlot());

  // get menu from gatekeeper
  const xdata::Table& menuTable = params.get<xdata::Table>("l1menu");
  uGtTriggerMenu& ugt_menu = algo.getTriggerMenu();
  ugt_menu.setTriggerMenu(menuTable);

  // get prescales/masks from gatekeeper
  const xdata::Table& finorMask = params.get<xdata::Table>("finorMask");
  const xdata::Table& prescales = params.get<xdata::Table>("prescales");
  const xdata::Table& algorithmBxMask = params.get<xdata::Table>("algorithmBxMask");
  const xdata::Table& vetoMask = params.get<xdata::Table>("vetoMask");

  try
  {
    algo.setFinorMask(&finorMask);
    algo.setPrescales(&prescales, &finorMask);
    algo.setVetoMask(&vetoMask);
    algo.setAlgoBxMask(&algorithmBxMask);
  }
  catch (toolbox::MaskTableException &e)
  {
    std::ostringstream msg;
    msg << "uGtTriggerMenuCommand: " << e.what();
    setStatusMsg(msg.str());
    return State::kError;
  }
  catch (toolbox::PrescaleTableException &e)
  {
    std::ostringstream msg;
    msg << "uGtTriggerMenuCommand: " << e.what();
    setStatusMsg(msg.str());
    return State::kError;
  }
  catch (toolbox::AlgoBXMaskTableException &e)
  {
    std::ostringstream msg;
    msg << "uGtTriggerMenuCommand: " << e.what();
    setStatusMsg(msg.str());
    return State::kError;
  }


  ugt_menu.show();

  // parse menu file and create pugi xml document ...
  std::string path = params.get<xdata::String>("Xml file path").value_;
  if (path.size())
  {
    // Create & fill the result
    boost::shared_ptr<pugi::xml_document> lSwatchXmlDoc(new pugi::xml_document());

    pugi::xml_node lInfraNode = lSwatchXmlDoc->append_child("infra");
    lInfraNode.append_attribute("id") = "uGT";

    pugi::xml_node lContextNode =  lInfraNode.append_child("context");
    lContextNode.append_attribute("id") = "uGtProcessor";

    pugi::xml_node lParamNode = lContextNode.append_child("param");
    lParamNode.append_attribute("id") = "l1menu";
    lParamNode.append_attribute("type") = "table";

    pugi::xml_node node =  lParamNode.append_child("columns").append_child(pugi::node_pcdata);
    node.set_value("AlgoName, GlobalIndex, ModuleIndex, LocalIndex");

    node = lParamNode.append_child("types").append_child(pugi::node_pcdata);
    node.set_value("string, uint, uint, uint");

    pugi::xml_node rows = lParamNode.append_child("rows");

    const tmeventsetup::esTriggerMenu* menu = tmeventsetup::getTriggerMenu(path);
    const std::map<std::string, tmeventsetup::esAlgorithm>& algoMap = menu->getAlgorithmMap();

    for (std::map<std::string, tmeventsetup::esAlgorithm>::const_iterator cit = algoMap.begin();
         cit != algoMap.end(); cit++)
    {
      const tmeventsetup::esAlgorithm& algo = cit->second;
      node = rows.append_child("row").append_child(pugi::node_pcdata);
      std::stringstream ss;
      ss << "\"" << algo.getName() << "\", " << algo.getIndex() << ", "
         << algo.getModuleId() << ", " << algo.getModuleIndex();
      node.set_value(ss.str().c_str());
    }

    pugi::xml_node uuid_node = lContextNode.append_child("param");
    uuid_node.append_attribute("id") = "uuid_fw";
    uuid_node.append_attribute("type") = "string";
    pugi::xml_node data =  uuid_node.append_child(pugi::node_pcdata);
    std::string text = menu->getFirmwareUuid();
    data.set_value(text.c_str());

    pugi::xml_node name_node = lContextNode.append_child("param");
    name_node.append_attribute("id") = "menu_name";
    name_node.append_attribute("type") = "string";
    pugi::xml_node name =  name_node.append_child(pugi::node_pcdata);
    text = menu->getName();
    name.set_value(text.c_str());

    lSwatchXmlDoc->save(std::cout);
  }

  setResult(xdata::UnsignedInteger(0));
  setStatusMsg("uGtTriggerMenuCommand: done");
  setProgress(1., "uGtTriggerMenuCommand ... done");

  return State::kDone;
}

} // namespace ugt
/* eof */
