//
//   a) uGtPreparePreviewPrescaleCommand
//   b) uGtApplyPreviewPrescaleCommand
// for multi-board operations
//
// PARAMETER: initPreview
// true...initialize preview prescales by using index+1
// false...set preview prescales with index

#include "ugt/swatch/commands/uGtPreparePreviewPrescaleCommand.h"

#include "ugt/swatch/rules/PrescaleRule.h"
#include "ugt/swatch/rules/FinorMaskRule.h"
#include "ugt/swatch/toolbox/PrescaleTable.h"
#include "ugt/swatch/toolbox/MaskTable.h"
#include "ugt/swatch/toolbox/MemoryImage.h"

#include "ugt/swatch/Constants.h"
#include "ugt/swatch/uGtProcessor.h"
#include "ugt/swatch/uGtAlgo.h"
#include "ugt/swatch/uGtTriggerMenu.h"

#include "mp7/MP7Controller.hpp"
#include "uhal/HwInterface.hpp"

#include "xdata/Boolean.h"
#include "xdata/Table.h"
#include "xdata/UnsignedInteger.h"

#include "log4cplus/loggingmacros.h"

namespace ugt
{

uGtPreparePreviewPrescaleCommand::uGtPreparePreviewPrescaleCommand(const std::string& aId,
                                       ::swatch::action::ActionableObject& aActionable) :
  ::swatch::action::Command(aId, aActionable, xdata::UnsignedInteger())
  // result is UnsignedInteger
{
  registerParameter("prescales", xdata::Table(), rules::PrescaleRule());
  registerParameter("index", xdata::UnsignedInteger(0));
  registerParameter("initPreview", xdata::Boolean(false));
  registerParameter("finorMask", xdata::Table(), rules::FinorMaskRule());
  registerParameter("otf", xdata::UnsignedInteger(0));
}


::swatch::action::Command::State
uGtPreparePreviewPrescaleCommand::code(const ::swatch::core::XParameterSet& params)
{
  setProgress(0., "uGtPreparePreviewPrescaleCommand ...");

  // Get prescale table and index
  const xdata::Table& table = params.get<xdata::Table>("prescales");
  unsigned int index = params.get<xdata::UnsignedInteger>("index").value_;
  bool bInitPreview = params.get<xdata::Boolean>("initPreview").value_;
  uint32_t otf = params.get<xdata::UnsignedInteger>("otf").value_;
  // get finor Table
  const xdata::Table& tabMask = params.get<xdata::Table>("finorMask");

  if (index > kMaxPrescaleSets || (bInitPreview && (index+1 > kMaxPrescaleSets)))
  {
    std::stringstream ss;
    ss << "uGtPreparePreviewPrescaleCommand: prescale-index exceeds maximum (" << kMaxPrescaleSets << "): '" << index << "'.";
    setStatusMsg(ss.str());
    return State::kError;
  }

  if (otf < 0 || otf > 1)
  {
    std::stringstream ss;
    ss << "uGtPreparePreviewPrescaleCommand: otf flag is invalid (allowed 0/1): '" << otf << "'.";
    setStatusMsg(ss.str());
    return State::kError;
  }


  toolbox::MemoryImage<uint32_t> image(1, kMaxAlgorithms, 0x0); // 1x<n_algos> DWORDs, default = 0x0

  uGtProcessor &proc = getActionable<uGtProcessor>();
  uGtAlgo& algo = dynamic_cast<uGtAlgo&>(proc.getAlgo());
  const uGtTriggerMenu& ugtMenu = algo.getTriggerMenu();

  toolbox::PrescaleTable prescaleTable;
  toolbox::FinorMaskTable maskTable;

  // consistency check of table and menu
  try
  {
    prescaleTable.load(table, tabMask, ugtMenu);
    maskTable.load(tabMask, ugtMenu);
  }
  catch (toolbox::PrescaleTableException &e)
  {
    LOG4CPLUS_ERROR(getActionable().getLogger(), e.what());
    setStatusMsg(e.what());
    return State::kError;
  }
  catch(toolbox::MaskTableException &e)
  {
    LOG4CPLUS_ERROR(getActionable().getLogger(), e.what());
    setStatusMsg(e.what());
    return State::kError;
  }


  // prepare registers
  ::mp7::MP7Controller& driver = proc.driver();
  ::uhal::HwInterface& hw = driver.hw();

  const std::string reg = "gt_mp7_gtlfdl.preview.prescale_factor";
  const uhal::Node& prescales = hw.getNode(reg);


  if (bInitPreview)
  {
    // During configuration, get next prescale index available
    // or get default prescale column index
    index = (prescaleTable.getNextPrescaleIndexAvailable(index));
  }

  // update prescales and mask
  for (auto cit = ugtMenu.begin(); cit != ugtMenu.end(); cit++)
  {
    Algorithm* pAlgo = cit->second;
    const std::string algoName = pAlgo->getName();
    const uint32_t algoIndex = pAlgo->getIndex();

    pAlgo->setPrescales(prescaleTable.getPrescalesForAlgo(algoName));
    pAlgo->setMask(maskTable.getMask(algoName));

    // Fractional prescales are stored as integer representations.
    const double scale = std::pow(10, kPrescalePrecision);
    const uint32_t mergedPrescale = static_cast<uint32_t>(std::round(pAlgo->getMergedPrescaleMask(index) * scale));
    image.setValue(algoIndex, mergedPrescale);

    // LOG4CPLUS_INFO(getActionable().getLogger(), "uGtPreparePreviewPrescaleCommand: index = " << algoIndex
    // << " value = " << mergedPrescale
    // << " prescale/mask = " << presc << "/" << mask);
  }



  // write prescale image to hw
  prescales.writeBlock(image.values());
  hw.getNode("gt_mp7_gtlfdl.preview.prescale_factor_set_index").write(index);
  hw.getNode("gt_mp7_gtlfdl.prescale_otf_flags.preview_flag").write(otf);
  hw.getClient().dispatch();

  // sanity checks
  setStatusMsg("uGtPreparePreviewPrescaleCommand: performing read-back test...");
  ::uhal::ValWord<uint32_t> data_index = hw.getNode("gt_mp7_gtlfdl.preview.prescale_factor_set_index").read();
  hw.getClient().dispatch();
  if (index != data_index)
  {
    std::stringstream ss;
    ss << "uGtPreparePreviewPrescaleCommand: prescale column index mismatch: write = " << index << " read = " << data_index;
    setStatusMsg(ss.str());
    return State::kError;
  }

  ::uhal::ValWord<uint32_t> data_otf = hw.getNode("gt_mp7_gtlfdl.prescale_otf_flags.preview_flag").read();
  hw.getClient().dispatch();
  if (otf != data_otf)
  {
    std::stringstream ss;
    ss << "uGtPreparePreviewPrescaleCommand: otf flag mismatch: write = " << otf << " read = " << data_otf;
    setStatusMsg(ss.str());
    return State::kError;
  }

  ::uhal::ValVector<uint32_t> data_prescale = prescales.readBlock(prescales.getSize());
  hw.getClient().dispatch();
  for (size_t i = 0; i < kMaxAlgorithms; i++)
  {
    if (data_prescale.at(i) != image.value(i))
    {
      std::stringstream ss;
      ss << "uGtPreparePreviewPrescaleCommand: prescale mismatch: index = " << i
         << "write = " << image.value(i) << " read = " << data_prescale.at(i);
      setStatusMsg(ss.str());
      return State::kError;
    }
  }

  setStatusMsg("uGtPreparePreviewPrescaleCommand: performing read-back test...done");
  setResult(xdata::UnsignedInteger(0));
  setStatusMsg("uGtPreparePreviewPrescaleCommand: done");

  setProgress(1., "uGtPreparePreviewPrescaleCommand ... done");

  return State::kDone;
}

} // namespace ugt
/* eof */
