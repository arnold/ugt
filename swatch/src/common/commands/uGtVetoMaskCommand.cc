#include "ugt/swatch/commands/uGtVetoMaskCommand.h"

#include "ugt/swatch/rules/VetoMaskRule.h"
#include "ugt/swatch/toolbox/MaskTable.h"
#include "ugt/swatch/toolbox/MemoryImage.h"

#include "ugt/swatch/Constants.h"
#include "ugt/swatch/uGtProcessor.h"
#include "ugt/swatch/uGtAlgo.h"
#include "ugt/swatch/uGtTriggerMenu.h"

#include "mp7/MP7Controller.hpp"
#include "uhal/HwInterface.hpp"

#include "xdata/Table.h"
#include "xdata/UnsignedInteger.h"

#include "log4cplus/loggingmacros.h"

namespace ugt
{

uGtVetoMaskCommand::uGtVetoMaskCommand(const std::string& aId,
                                       ::swatch::action::ActionableObject& aActionable) :
  ::swatch::action::Command(aId, aActionable, xdata::UnsignedInteger())
  // result is UnsignedInteger
{
  registerParameter("vetoMask", xdata::Table(), rules::VetoMaskRule());
}


::swatch::action::Command::State
uGtVetoMaskCommand::code(const ::swatch::core::XParameterSet& params)
{
  setProgress(0., "uGtVetoMaskCommand ...");

  // get table
  const xdata::Table& table = params.get<xdata::Table>("vetoMask");
  const std::vector<std::string> columns = table.getColumns();

  uGtProcessor &p = getActionable<uGtProcessor>();
  uGtAlgo& algo = dynamic_cast<uGtAlgo&>(p.getAlgo());
  const uGtTriggerMenu& ugtMenu = algo.getTriggerMenu();

  toolbox::VetoMaskTable vetoTable;
  try
  {
    vetoTable.load(table, ugtMenu);
  }
  catch(toolbox::MaskTableException &e)
  {
    LOG4CPLUS_ERROR(getActionable().getLogger(), e.what());
    setStatusMsg(e.what());
    return State::kError;
  }

  toolbox::MemoryImage<uint32_t> image(1, kMaxAlgorithms, vetoTable.getDefaultMask()); // 1x<n_algos> DWORDs, initialized with default Veto

  ::mp7::MP7Controller& driver = p.driver();
  ::uhal::HwInterface& hw = driver.hw();

  const std::string reg = "gt_mp7_gtlfdl.masks";
  const uhal::Node& masks = hw.getNode(reg);

  ::uhal::ValVector<uint32_t> values;
  values = masks.readBlock(masks.getSize());
  hw.getClient().dispatch();
  image.setValues(values.value());

  // update veto mask
  for (auto cit = ugtMenu.begin(); cit != ugtMenu.end(); cit++)
  {
    Algorithm* pAlgo = cit->second;
    const uint32_t algoIndex = pAlgo->getIndex();
    const uint32_t veto = vetoTable.getMask(pAlgo->getName());

    pAlgo->setVeto(veto);

    image.set(1, algoIndex, veto); // set bit 1

  }
  masks.writeBlock(image.values());
  hw.getClient().dispatch();

  // sanity check
  ::uhal::ValVector<uint32_t> data_masks = masks.readBlock(masks.getSize());
  hw.getClient().dispatch();
  for (size_t i = 0; i < kMaxAlgorithms; i++)
  {
    if (data_masks.at(i) != image.value(i))
    {
      std::stringstream ss;
      ss << "uGtVetoMaskCommand: veto-mask mismatch: id = " << i
         << "write = " << image.value(i) << " read = " << data_masks.at(i);
      setStatusMsg(ss.str());
      return State::kError;
    }
  }

  setResult(xdata::UnsignedInteger(0));
  setStatusMsg("uGtVetoMaskCommand: done");

  setProgress(1., "uGtVetoMaskCommand ... done");

  return State::kDone;
}

} // namespace ugt
/* eof */
