#include "ugt/swatch/commands/uGtVersionCommand.h"

#include "ugt/swatch/uGtProcessor.h"
#include "ugt/swatch/toolbox/NodeTranslator.h"

#include "mp7/MP7Controller.hpp"
#include "uhal/HwInterface.hpp"

#include "utm/tmEventSetup/tmEventSetup.hh"

#include "xdata/String.h"
#include "xdata/UnsignedInteger.h"

#include "log4cplus/loggingmacros.h"

namespace ugt
{

uGtVersionCommand::uGtVersionCommand(const std::string& aId,
                                     ::swatch::action::ActionableObject& aActionable) :
  ::swatch::action::Command(aId, aActionable, xdata::UnsignedInteger())
  // result is UnsignedInteger
{
  registerParameter("uuid", xdata::String("00000000-0000-0000-0000-000000000000"));
  registerParameter("menu_name", xdata::String(""));
  registerParameter("uuid_fw", xdata::String(""));
  registerParameter("module_id", xdata::UnsignedInteger(0));
}


::swatch::action::Command::State
uGtVersionCommand::code(const ::swatch::core::XParameterSet& params)
{
  setProgress(0., "uGtVersionCommand ...");

  const std::string uuid_menu = params.get<xdata::String>("uuid").value_;
  const std::string menu_name = params.get<xdata::String>("menu_name").value_;
  const std::string uuid_fw = params.get<xdata::String>("uuid_fw").value_;
  const unsigned int module_id = params.get<xdata::UnsignedInteger>("module_id").value_;

  uGtProcessor &p = getActionable<uGtProcessor>();
  ::mp7::MP7Controller& driver = p.driver();
  ::uhal::HwInterface& hw = driver.hw();

  // menu name
  const ::uhal::Node& node_name = hw.getNode("gt_mp7_gtlfdl.read_versions.l1tm_name");
  ::uhal::ValVector<uint32_t> data_name = node_name.readBlock(node_name.getSize());

  // menu uuid
  const ::uhal::Node& node_uuid_menu = hw.getNode("gt_mp7_gtlfdl.read_versions.l1tm_uuid");
  ::uhal::ValVector<uint32_t> data_uuid_menu = node_uuid_menu.readBlock(node_uuid_menu.getSize());

  // fw uuid
  const ::uhal::Node& node_uuid_fw = hw.getNode("gt_mp7_gtlfdl.read_versions.l1tm_fw_uuid");
  ::uhal::ValVector<uint32_t> data_uuid_fw = node_uuid_fw.readBlock(node_uuid_fw.getSize());

  // module_id
  const ::uhal::Node& node_module_id = hw.getNode("gt_mp7_gtlfdl.read_versions.module_id");
  ::uhal::ValWord<uint32_t> data_module_id = node_module_id.read();

  // menu name hash
  const ::uhal::Node& node_menu_hash = hw.getNode("gt_mp7_gtlfdl.read_versions.l1tm_uuid_hash");
  ::uhal::ValWord<uint32_t> data_menu_hash = node_menu_hash.read();

  // fw uuid hash
  const ::uhal::Node& node_uuid_fw_hash = hw.getNode("gt_mp7_gtlfdl.read_versions.l1tm_fw_uuid_hash");
  ::uhal::ValWord<uint32_t> data_uuid_fw_hash = node_uuid_fw_hash.read();

  //
  // auxiliary information
  const ::uhal::Node& node_info_timestamp = hw.getNode("gt_mp7_frame.module_info.timestamp");
  ::uhal::ValWord<uint32_t> data_info_timestamp = node_info_timestamp.read();

  const ::uhal::Node& node_info_username = hw.getNode("gt_mp7_frame.module_info.username");
  ::uhal::ValVector<uint32_t> data_info_username = node_info_username.readBlock(node_info_username.getSize());

  const ::uhal::Node& node_info_hostname = hw.getNode("gt_mp7_frame.module_info.hostname");
  ::uhal::ValVector<uint32_t> data_info_hostname = node_info_hostname.readBlock(node_info_hostname.getSize());

  const ::uhal::Node& node_info_frame_version = hw.getNode("gt_mp7_frame.module_info.frame_version");
  ::uhal::ValWord<uint32_t> data_info_frame_version = node_info_frame_version.read();

  const ::uhal::Node& node_compiler_version = hw.getNode("gt_mp7_gtlfdl.read_versions.l1tm_compiler_version");
  ::uhal::ValWord<uint32_t> data_compiler_version = node_compiler_version.read();

  const ::uhal::Node& node_gtl_version = hw.getNode("gt_mp7_gtlfdl.read_versions.gtl_fw_version");
  ::uhal::ValWord<uint32_t> data_gtl_version = node_gtl_version.read();

  const ::uhal::Node& node_fdl_version = hw.getNode("gt_mp7_gtlfdl.read_versions.fdl_fw_version");
  ::uhal::ValWord<uint32_t> data_fdl_version = node_fdl_version.read();

  hw.getClient().dispatch();

  // Create a uHAL node translator.
  toolbox::NodeTranslator tr;


  // auxiliary information
  std::stringstream ss;

  ss << "module_info.timestamp = " << tr(node_info_timestamp, data_info_timestamp) << "\n";
  ss << "module_info.username = " << tr(node_info_username, data_info_username) << "\n";
  ss << "module_info.hostname = " << tr(node_info_hostname, data_info_hostname) << "\n";

  ss << "module_info.frame_version = " << tr(node_info_frame_version, data_info_frame_version) << "\n";

  std::string name = tr(node_name, data_name);

  ss << "Menu name = " << name << "\n";
  ss << "Menu name UUID = " << tr(node_uuid_menu, data_uuid_menu) << "\n";
  ss << "FW UUID = " << tr(node_uuid_fw, data_uuid_fw) << "\n";
  ss << "Menu name hash = " << data_menu_hash << "\n";
  ss << "FW UUID hash = " << data_uuid_fw_hash << "\n";
  ss << "Module ID = " << data_module_id << "\n";

  ss << "VHDLProducer version = " << tr(node_compiler_version, data_compiler_version) << "\n";
  ss << "GTL version = " << tr(node_gtl_version, data_gtl_version) << "\n";
  ss << "FDL version = " << tr(node_fdl_version, data_fdl_version) << "\n";

  std::cout << ss.str() << std::endl;


  // consistency checks
  if (tr(node_uuid_menu, data_uuid_menu) != uuid_menu)
  {
    std::stringstream msg;
    msg << "uGtVersionCommand: menu uuid mismatch: ";
    msg << "'" << uuid_menu << "' != '" << tr(node_uuid_menu, data_uuid_menu) << "'";
    setStatusMsg(msg.str());
    return State::kError;
  }

  if (tr(node_uuid_fw, data_uuid_fw) != uuid_fw)
  {
    std::stringstream msg;
    msg << "uGtVersionCommand: fw uuid mismatch: ";
    msg << "'" << uuid_fw << "' != '" << tr(node_uuid_fw, data_uuid_fw) << "'";
    setStatusMsg(msg.str());
    return State::kError;
  }

  if (data_menu_hash != tmeventsetup::getMmHashN(menu_name))
  {
    setStatusMsg("uGtVersionCommand: menu name hash mismatch");
    return State::kError;
  }

  if (data_uuid_fw_hash != tmeventsetup::getMmHashN(uuid_fw))
  {
    setStatusMsg("uGtVersionCommand: fw uuid hash mismatch");
    return State::kError;
  }

  if (data_module_id != module_id)
  {
    std::stringstream msg;
    msg << "uGtVersionCommand: module_id hash mismatch: ";
    msg << "'" << module_id << "' != '" << data_module_id << "'";
    setStatusMsg(msg.str());
    return State::kError;
  }

  setResult(xdata::UnsignedInteger(0));
  setStatusMsg("uGtVersionCommand: " + ss.str());

  setProgress(1., "uGtVersionCommand ... done");

  return State::kDone;
}

} // namespace ugt
/* eof */
