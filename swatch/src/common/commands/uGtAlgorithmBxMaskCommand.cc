#include "ugt/swatch/commands/uGtAlgorithmBxMaskCommand.h"

#include "ugt/swatch/rules/AlgoBxMaskRule.h"
#include "ugt/swatch/toolbox/AlgoBXMaskTable.h"
#include "ugt/swatch/toolbox/MemoryImage.h"
#include "ugt/swatch/toolbox/NodeTranslator.h"

#include "ugt/swatch/uGtProcessor.h"
#include "ugt/swatch/uGtAlgo.h"
#include "ugt/swatch/uGtTriggerMenu.h"

#include "mp7/MP7Controller.hpp"
#include "uhal/HwInterface.hpp"

#include "xdata/Table.h"
#include "xdata/UnsignedInteger.h"

namespace ugt
{

uGtAlgorithmBxMaskCommand::uGtAlgorithmBxMaskCommand(const std::string& aId,
                                                     ::swatch::action::ActionableObject& aActionable) :
  ::swatch::action::Command(aId, aActionable, xdata::UnsignedInteger())
  // result is UnsignedInteger
{
  registerParameter("algorithmBxMask", xdata::Table(), rules::AlgoBxMaskRule());
}


::swatch::action::Command::State
uGtAlgorithmBxMaskCommand::code(const ::swatch::core::XParameterSet& params)
{
  setProgress(0., "uGtAlgorithmBxMaskCommand ...");

  // Get table
  const xdata::Table& xtable = params.get<xdata::Table>("algorithmBxMask");
  const std::vector<std::string> columns = xtable.getColumns();

  uGtProcessor &p = getActionable<uGtProcessor>();
  uGtAlgo& algo = dynamic_cast<uGtAlgo&>(p.getAlgo());
  const uGtTriggerMenu& ugtMenu = algo.getTriggerMenu();

  toolbox::AlgoBXMaskTable algobxMaskTable;
  try
  {
    algobxMaskTable.load(xtable, ugtMenu);
  }
  catch (toolbox::AlgoBXMaskTableException &e)
  {
    std::ostringstream msg;
    msg << "uGtAlgorithmBxMaskCommand: " << e.what();
    setStatusMsg(msg.str());
    return State::kError;
  }

  // The algorithm BX mask image is 16x4096 DWORDs in size.
  // Every 4096 DWORD slice represents 32 algorithms and their
  // corresponding 3564 BX masks.
  //
  //        <------------- algorithms ------------>
  //        <0...31> <32..63> <64..95> ... <...511>
  //    0 ^ 00000000 00001000 00002000 ... 0000f000
  //      | 00000001 00001001 00002001 ... 0000f001
  //      | 00000002 00001002 00002002 ... 0000f002
  //     bx 00000003 00001003 00002003 ... 0000f003
  //      | 00000004 00001004 00002004 ... 0000f004
  //      | ...      ...      ...      ... ...
  // 3563 v 00000deb 00001deb 00002deb ... 0000fdeb
  //        00000dec 00001dec 00002dec ... 0000fdec
  //        ...      ...      ...      ... ...
  // (addr) 00000fff 00001fff 00002fff ... 0000ffff
  //
  toolbox::MemoryImage<uint32_t> image(16, 4096, algobxMaskTable.getDefaultMask()); // 16x4096 DWORDs and init with default bx mask


  uint32_t pos = 0;
  for (auto cit = ugtMenu.begin(); cit != ugtMenu.end(); cit++, pos++)
  {
    Algorithm* pAlgo = cit->second;

    // update algo bx mask
    std::map<uint32_t, uint32_t> algobxMap = algobxMaskTable.getAlgoBXMask(pAlgo->getName());
    pAlgo->setAlgoBxMask(algobxMap);

    const uint32_t algoIndex = pAlgo->getIndex();
    const std::map<uint32_t, uint32_t>& algoBxMask = pAlgo->getAlgoBxMask();
    for (auto citBx = algoBxMask.begin(); citBx != algoBxMask.end(); citBx++)
    {
      // reduce the bx by one to setup the algoBxMask memory image correctly
      uint32_t bx = citBx->first - 1;
      uint32_t mask = citBx->second;

      // BX mapping of LHC domain to hardware
      //    LHC    HW
      //     1      0
      //     2      1
      //    ...    ...
      //  3564   3563

      image.set(algoIndex, bx, mask); // enable/disable algo mask bit
      // if (not mask) LOG4CPLUS_INFO(getActionable().getLogger(),
      //           "uGtAlgorithmBxMaskCommand: masked index = " << algoIndex
      //           << " in bx " << bx << " maskValue = " << mask);
    }
    // setProgress(.5 / algoBxMask.size() * pos, "uGtAlgorithmBxMaskCommand ... mapping masks ...");
  }
  setProgress(.8, "uGtAlgorithmBxMaskCommand ... mapping masks ...");

  ::mp7::MP7Controller& driver = p.driver();
  ::uhal::HwInterface& hw = driver.hw();

  const std::string reg = "gt_mp7_gtlfdl.algo_bx_mem";
  const uhal::Node& algo_bx_mem = hw.getNode(reg);

  algo_bx_mem.writeBlock(image.values());
  hw.getClient().dispatch();

  setResult(xdata::UnsignedInteger(0));
  setStatusMsg("uGtAlgorithmBxMaskCommand: done");

  setProgress(1., "uGtAlgorithmBxMaskCommand ... done");

  return State::kDone;
}

} // namespace ugt
/* eof */
