#include "ugt/swatch/rules/PrescaleRule.h"
#include "ugt/swatch/toolbox/PrescaleTable.h"

#include "swatch/core/rules/NoEmptyCells.hpp"
#include "swatch/core/XMatch.hpp"

namespace ugt {
namespace rules {

void
PrescaleRule::describe(std::ostream& aStream) const
{
  aStream << "check(PrescaleTable)";
}

swatch::core::XMatch
PrescaleRule::verify (const xdata::Table& aValue) const
{
  const size_t colCount = aValue.getColumns().size();
  const size_t rowCount = aValue.getRowCount();

  // Skip verification when table is default table
  if (toolbox::PrescaleTable::isDefaultTable(colCount, rowCount))
    return swatch::core::XMatch(true);

  try
  {
    toolbox::PrescaleTable container;

    // Check for empty cells
    swatch::core::rules::NoEmptyCells noEmptyCellsRule;
    swatch::core::XMatch match = noEmptyCellsRule.verify(aValue);
    if (not match.ok)
    {
      std::stringstream msg;
      msg << container.getTableType() << " contains " << match.details;
      throw toolbox::PrescaleTableException(msg.str());
    }

    container.verify(aValue);
  }
  catch (const toolbox::PrescaleTableException& e)
  {
    return swatch::core::XMatch(false, e.what());
  }

  return swatch::core::XMatch(true);
}

} // namespace rules
} // namespace ugt
