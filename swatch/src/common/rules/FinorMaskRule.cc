#include "ugt/swatch/rules/FinorMaskRule.h"
#include "ugt/swatch/toolbox/MaskTable.h"

#include "swatch/core/rules/NoEmptyCells.hpp"
#include "swatch/core/XMatch.hpp"

#include <iostream>
#include <string>

namespace ugt {
namespace rules {

void
FinorMaskRule::describe(std::ostream& aStream) const
{
  aStream << "check(FinorTable)";
}

swatch::core::XMatch
FinorMaskRule::verify (const xdata::Table& aValue) const
{
  const size_t colCount = aValue.getColumns().size();
  const size_t rowCount = aValue.getRowCount();

  // Skip verification when table is default table
  if (toolbox::MaskTable::isDefaultTable(colCount, rowCount))
    return swatch::core::XMatch(true);

  try
  {
    toolbox::FinorMaskTable container;

    // Check for empty cells
    swatch::core::rules::NoEmptyCells noEmptyCellsRule;
    swatch::core::XMatch match = noEmptyCellsRule.verify(aValue);
    if (not match.ok)
    {
      std::stringstream msg;
      msg << container.getTableType() << " contains " << match.details;
      throw toolbox::MaskTableException(msg.str());
    }

    container.verify(aValue);
  }
  catch (const toolbox::MaskTableException& e)
  {
    return swatch::core::XMatch(false, e.what());
  }

  return swatch::core::XMatch(true);
}

} // namespace rules
} // namespace ugt
