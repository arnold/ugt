#include "ugt/swatch/rules/AlgoBxMaskRule.h"
#include "ugt/swatch/toolbox/AlgoBXMaskTable.h"

#include "swatch/core/rules/NoEmptyCells.hpp"
#include "swatch/core/XMatch.hpp"

namespace ugt {
namespace rules {

void
AlgoBxMaskRule::describe(std::ostream& aStream) const
{
  aStream << "check(AlgoBxMaskTable)";
}

swatch::core::XMatch
AlgoBxMaskRule::verify (const xdata::Table& aValue) const
{
  const size_t colCount = aValue.getColumns().size();
  const size_t rowCount = aValue.getRowCount();

  // Skip verification when table is default table
  if (toolbox::AlgoBXMaskTable::isDefaultTable(colCount, rowCount))
    return swatch::core::XMatch(true);

  try
  {
    toolbox::AlgoBXMaskTable container;

    // Check for empty cells
    swatch::core::rules::NoEmptyCells noEmptyCellsRule;
    swatch::core::XMatch match = noEmptyCellsRule.verify(aValue);
    if (not match.ok)
    {
      std::stringstream msg;
      msg << container.getTableType() << " contains " << match.details;
      throw toolbox::AlgoBXMaskTableException(msg.str());
    }

    container.verify(aValue);
  }
  catch (toolbox::AlgoBXMaskTableException& e)
  {
    return swatch::core::XMatch(false, e.what());
  }

  return swatch::core::XMatch(true);
}

} // namespace rules
} // namespace ugt
