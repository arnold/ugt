#include "ugt/swatch/uGtExtCondAlgo.h"

#include "ugt/swatch/AMC502Controller.h"

#include "swatch/action/MonitoringSetting.hpp"
#include "swatch/core/MetricConditions.hpp"

#include "uhal/HwInterface.hpp"

#include "log4cplus/loggingmacros.h"

#include <limits>
#include <stdexcept>


namespace ugt
{

uGtExtCondAlgo::uGtExtCondAlgo(amc502::AMC502Controller& driver)
  : driver_(driver)
  , metricBxNumberMax_(registerMetric<uint32_t>("bunchCrossingNumberMax"))
  , metricLuminositySegmentNumber_(registerMetric<uint32_t>("luminositySegmentNumber"))
  , metricOrbitCounter_(registerMetric<uint64_t>("orbitCounter"))
{

}

uGtExtCondAlgo::~uGtExtCondAlgo()
{
}

void
uGtExtCondAlgo::retrieveMetricValues()
{

  for (uint32_t retry = 0; retry < 2; retry++)
  {
    const uint32_t luminositySegmentBefore = getLuminositySection();


    const uint32_t bxNumberMax = getBxNumberMax();
    const uint32_t orbitNumber = getOrbitNumber();

    const uint32_t luminositySegmentAfter = getLuminositySection();

    if (luminositySegmentBefore == luminositySegmentAfter)
    {
      // Read from hardware was in same luminosity section
      // Thus, store retrieved data to matrics

      // Set metrics with latest data values
      setMetricValue<uint32_t>(metricLuminositySegmentNumber_, luminositySegmentAfter);
      setMetricValue<uint64_t>(metricOrbitCounter_, orbitNumber);

      setMetricValue<uint32_t>(metricBxNumberMax_, bxNumberMax);
      setWarningCondition(metricBxNumberMax_, ::swatch::core::NotEqualCondition<uint32_t>(0xdeb));
      // Leave retry loop
      break;
    }
  } // end retry loop
}


uint32_t
uGtExtCondAlgo::getBxNumberMax() const
{
  ::uhal::HwInterface& hw = driver_.hw();

  const ::uhal::Node& node = hw.getNode("payload.tcm_regs.bx_nr_max");
  ::uhal::ValWord<uint32_t> value = node.read();

  hw.getClient().dispatch();

  return value;
}


uint64_t
uGtExtCondAlgo::getOrbitNumber() const
{
  ::uhal::HwInterface& hw = driver_.hw();

  const ::uhal::Node& node_l = hw.getNode("payload.tcm_regs.orbit_nr_l");
  ::uhal::ValWord<uint32_t> value_l = node_l.read();

  const ::uhal::Node& node_h = hw.getNode("payload.tcm_regs.orbit_nr_h");
  ::uhal::ValWord<uint32_t> value_h = node_h.read();

  hw.getClient().dispatch();

  return static_cast<uint64_t>(value_h) << 32 | value_l;
}


uint32_t
uGtExtCondAlgo::getLuminositySection() const
{
  ::uhal::HwInterface& hw = driver_.hw();

  const ::uhal::Node& node_luminosity_seg_nr = hw.getNode("payload.tcm_regs.luminosity_seg_nr");
  ::uhal::ValWord<uint32_t> data_luminosity_seg_nr = node_luminosity_seg_nr.read();

  hw.getClient().dispatch();

  return data_luminosity_seg_nr;
}


} /* end namespace ugt */
