/*
 * @file:   AMC502Commands.cc
 * @author: Gregor Aradi, Bernhard Arnold
 *
 */

#include "ugt/swatch/AMC502Commands.h"
#include "ugt/swatch/AMC502Controller.h"
#include "ugt/swatch/AMC502Processor.h"

// MP7 Headers
#include "mp7/CtrlNode.hpp"
#include "mp7/ReadoutCtrlNode.hpp"
#include "mp7/ReadoutNode.hpp"
#include "mp7/TTCNode.hpp"

// XDAQ Headers
#include "xdata/Boolean.h"
#include "xdata/Integer.h"
#include "xdata/String.h"
#include "xdata/UnsignedInteger.h"

// uHAL headers
#include "uhal/HwInterface.hpp"

// SWATCH headers
#include "swatch/core/utilities.hpp"
#include "swatch/logger/Logger.hpp"
#include "swatch/processor/AlgoInterface.hpp"


namespace amc502 {


AMC502ResetCommand::AMC502ResetCommand(const std::string& aId, swatch::action::ActionableObject& aActionable):
  Command(aId, aActionable, xdata::String())
{
  registerParameter("clockSource", xdata::String("external"));
}

::swatch::action::Command::State AMC502ResetCommand::code(const ::swatch::core::XParameterSet& params)
{

  AMC502Processor& p = getActionable<AMC502Processor>();
  AMC502Controller& driver = p.driver();
  setProgress(0.,"[AMC502] Resetting clocks");

  std::string mode = params.get<xdata::String>("clockSource").value_;

  if ( mode.compare("internal") && mode.compare("external") ) {
      setStatusMsg("[AMC502] Clock can be set only to 'internal' or 'external'");
      return State::kError;
  }

  try {
    driver.reset(mode);
  } catch ( ::mp7::exception &e ) {

    setStatusMsg("[AMC502] Reset failed: "+e.description());
    return State::kError;
  }

  setProgress(1.,"[AMC502] Reset completed");

  return State::kDone;
}


AMC502PayloadResetCommand::AMC502PayloadResetCommand(const std::string& aId, swatch::action::ActionableObject& aActionable):
  Command(aId, aActionable, xdata::String())
{
}


::swatch::action::Command::State AMC502PayloadResetCommand::code(const ::swatch::core::XParameterSet& params)
{

  AMC502Processor& p = getActionable<AMC502Processor>();
  AMC502Controller& driver = p.driver();
  ::uhal::HwInterface& hw = driver.hw();
  setProgress(0.,"[AMC502] Payload reset starting");

  const ::uhal::Node& node = hw.getNode("payload.reset");
  node.write(0x1);
  hw.getClient().dispatch();

  setProgress(1.,"[AMC502] Payload reset completed");

  return State::kDone;
}


AMC502ResetCounterCommand::AMC502ResetCounterCommand(const std::string& aId, swatch::action::ActionableObject& aActionable):
  Command(aId, aActionable, xdata::String())
{
}

::swatch::action::Command::State AMC502ResetCounterCommand::code(const ::swatch::core::XParameterSet& params)
{
  AMC502Processor& p = getActionable<AMC502Processor>();
  AMC502Controller& driver = p.driver();
  ::uhal::HwInterface& hw = driver.hw();
  setProgress(0.,"[AMC502] Reset counter starting");

  const ::uhal::Node& node = hw.getNode("payload.counter_reset");
  node.write(0x1);
  hw.getClient().dispatch();

  setProgress(1.,"[AMC502] Reset counter completed");

  return State::kDone;
}



} // namespace amc502



/* eof */
