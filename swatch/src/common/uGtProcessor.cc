#include "ugt/swatch/uGtProcessor.h"
#include "ugt/swatch/uGtAlgo.h"

#include "ugt/swatch/commands/uGtAlgorithmBxMaskCommand.h"
#include "ugt/swatch/commands/uGtApplyPrescaleCommand.h"
#include "ugt/swatch/commands/uGtApplyPreviewPrescaleCommand.h"
#include "ugt/swatch/commands/uGtCalTrigSuppressionCommand.h"
#include "ugt/swatch/commands/uGtDelayCommand.h"
#include "ugt/swatch/commands/uGtPreparePrescaleCommand.h"
#include "ugt/swatch/commands/uGtPreparePreviewPrescaleCommand.h"
#include "ugt/swatch/commands/uGtResetCommand.h"
#include "ugt/swatch/commands/uGtResetCounterCommand.h"
#include "ugt/swatch/commands/uGtSmartRebootCommand.h"
#include "ugt/swatch/commands/uGtTriggerMenuCommand.h"
#include "ugt/swatch/commands/uGtVersionCommand.h"
#include "ugt/swatch/commands/uGtVetoMaskCommand.h"

#include "swatch/core/Factory.hpp"
#include "swatch/action/CommandSequence.hpp"
#include "swatch/action/StateMachine.hpp"


SWATCH_REGISTER_CLASS(ugt::uGtProcessor);


namespace ugt
{

uGtProcessor::uGtProcessor(const ::swatch::core::AbstractStub& aStub) :
  ::swatch::mp7::MP7Processor(aStub)
{
  typedef ::swatch::action::CommandSequence tComSeq;

  // Add algo interface
  registerInterface(new uGtAlgo(this->driver()));

  // Add commands
  ::swatch::action::Command& resetCommand = registerCommand<uGtResetCommand>("uGtReset");
  ::swatch::action::Command& versionCommand = registerCommand<uGtVersionCommand>("uGtVersion");
  ::swatch::action::Command& delayCommand = registerCommand<uGtDelayCommand>("uGtDelay");
  ::swatch::action::Command& calTrigSuppressionCommand = registerCommand<uGtCalTrigSuppressionCommand>("uGtCalTrigSuppression");
  ::swatch::action::Command& algoBxMemCommand = registerCommand<uGtAlgorithmBxMaskCommand>("uGtAlgorithmBxMask");
  ::swatch::action::Command& vetoMaskCommand = registerCommand<uGtVetoMaskCommand>("uGtVetoMask");
  ::swatch::action::Command& readTriggerMenuCommand = registerCommand<uGtTriggerMenuCommand>("uGtTriggerMenuCommand");
  ::swatch::action::Command& preparePrescaleCommand = registerCommand<uGtPreparePrescaleCommand>("uGtPreparePrescale");
  ::swatch::action::Command& applyPrescaleCommand = registerCommand<uGtApplyPrescaleCommand>("uGtApplyPrescale");
  ::swatch::action::Command& preparePreviewPrescaleCommand = registerCommand<uGtPreparePreviewPrescaleCommand>("uGtPreparePreviewPrescale");
  ::swatch::action::Command& applyPreviewPrescaleCommand = registerCommand<uGtApplyPreviewPrescaleCommand>("uGtApplyPreviewPrescale");
  ::swatch::action::Command& resetCounterCommand = registerCommand<uGtResetCounterCommand>("uGtResetCounter");
  ::swatch::action::Command& smartRebootCommand = registerCommand<uGtSmartRebootCommand>("uGtSmartReboot");


  // Setup sequence
  tComSeq& seqSetup = registerSequence("uGtSeqSetup", smartRebootCommand)
    .then(versionCommand)
    .then(CmdIds::kReset)
    .then(resetCommand)
    .then(delayCommand);

  // Configure sequence
  tComSeq& seqConfigure = registerSequence("uGtSeqConfigure", CmdIds::kCfgRxBuffers, "cfgRxBuffers_Muon")
    .then(CmdIds::kCfgRxBuffers, "cfgRxBuffers_Calo")
    .then(CmdIds::kCfgRxBuffers, "cfgRxBuffers_ExtCond")
    .then(CmdIds::kCfgEasyRxLatency)
    .then(CmdIds::kCfgEasyTxLatency)
    .then(CmdIds::kSetupReadout)
    .then(CmdIds::kLoadReadoutMenu)
    .then(readTriggerMenuCommand)
    .then(algoBxMemCommand)
    .then(preparePrescaleCommand)
    .then(applyPrescaleCommand)
    .then(preparePreviewPrescaleCommand)
    .then(applyPreviewPrescaleCommand)
    .then(calTrigSuppressionCommand)
    .then(vetoMaskCommand);

  // align sequence
  tComSeq& seqAlign = registerSequence("uGtSeqAligned", CmdIds::kCfgRxMGTs, "cfgRxMGTs_Muon")
    .then(CmdIds::kCfgRxMGTs, "cfgRxMGTs_Calo")
    .then(CmdIds::kCfgRxMGTs, "cfgRxMGTs_ExtCond")
    .then(CmdIds::kAlignMGTs, "alignMGTs_Muon")
    .then(CmdIds::kAlignMGTs, "alignMGTs_Calo")
    .then(CmdIds::kAlignMGTs, "alignMGTs_ExtCond");

  // Start sequence
  tComSeq& seqStart = registerSequence("uGtSeqStart", CmdIds::kClearCounters)
    .then(resetCounterCommand);

  // add commands/sequences to the state machine transitions
  swatch::processor::RunControlFSM& lFSM = getRunControlFSM();
  lFSM.coldReset.add(getCommand(CmdIds::kReboot));
  lFSM.setup.add(seqSetup);
  lFSM.configure.add(seqConfigure);
  lFSM.align.add(seqAlign);
  lFSM.start.add(seqStart);

}


uGtProcessor::~uGtProcessor()
{
}


} // namespace ugt
/* eof */
