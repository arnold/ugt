
// C++ Headers
#include <cstdlib>
#include <sstream>
#include <algorithm>
#include <ext/slist>

// Boost Headers
#include <boost/assign.hpp>
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/range/adaptor/map.hpp>
#include <boost/range/algorithm/copy.hpp>
#include <boost/unordered/unordered_map.hpp>


#include "ugt/swatch/AMC502Controller.h"

// MP7 Headers
#include "mp7/AlignMonNode.hpp"
#include "mp7/BoardData.hpp"
#include "mp7/ChanBufferNode.hpp"
#include "mp7/ClockingNode.hpp"
#include "mp7/ClockingR1Node.hpp"
#include "mp7/ClockingXENode.hpp"
#include "mp7/CtrlNode.hpp"
#include "mp7/DatapathNode.hpp"
#include "mp7/exception.hpp"
#include "mp7/FormatterNode.hpp"
#include "mp7/Logger.hpp"
#include "mp7/Measurement.hpp"
#include "mp7/MGTRegionNode.hpp"
#include "mp7/MmcPipeInterface.hpp"
#include "mp7/operators.hpp"
#include "mp7/Orbit.hpp"
#include "mp7/PathConfigurator.hpp"
#include "mp7/ReadoutNode.hpp"
#include "mp7/SI5326Node.hpp"
#include "mp7/SI570Node.hpp"
#include "mp7/TTCNode.hpp"
#include "mp7/Utilities.hpp"


// Namespace declaration
namespace l7 = mp7::logger;


namespace amc502 {


AMC502Controller::AMC502Controller(const uhal::HwInterface& aHw) :
  MP7MiniController(aHw)
{
  MP7_LOG(l7::kInfo) << "Building AMC502 controller " << id();
}

AMC502Controller::~AMC502Controller() {
}

void AMC502Controller::reset(const std::string& aClkSrc) {
  MP7_LOG(l7::kNotice) << "Resetting AMC502 board '" << id() << "'";

  std::string aRefClkCfg = aClkSrc;
  std::string aTTCCfg = aClkSrc;

  bool externalClock(false);
  if (aClkSrc == "external")
    externalClock = true;
  else if (aClkSrc == "internal")
    externalClock = false;
  else
    throw mp7::ArgumentError("AMC502 Clock source can be either internal or external");


  mCtrl.softReset();

  //std::string kindStr = boost::lexical_cast<std::string>(mKind);
  std::string kindStr = "r1";

  {
    // Hold clk4 in reset
    mp7::CtrlNode::Clock40Guard guard(mCtrl);

    mCtrl.selectClk40Source(externalClock);
    MP7_LOG(l7::kInfo) << "AMC502 detected";

  } // guard goes out of scope here, clk40 is released
  mCtrl.waitClk40Lock();

  mp7::TTCConfigurator ttcCfgtor(aTTCCfg + ".xml", kindStr, mp7::MP7MiniController::sharePath);

  // Ensure that the master clock choice is compatible with the ttc configuration
  if (ttcCfgtor.getConfig().clkSrc != aClkSrc) {
    MP7_LOG(l7::kError) << "AMC502 Clock source mismatch: ttcclk=" << ttcCfgtor.getConfig().clkSrc << ", master= " << aClkSrc;
  }
  ttcCfgtor.configure(mTTC);

}


} // namespace amc502
