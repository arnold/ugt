
#include "ugt/swatch/AMC502Processor.h"

// SWATCH headers
#include "swatch/logger/Logger.hpp"
#include "swatch/core/Factory.hpp"
#include "swatch/processor/PortCollection.hpp"
#include "swatch/processor/ProcessorStub.hpp"

// SWATCH MP7 headers
#include "swatch/mp7/cmds/BufferTraits.hpp"
#include "swatch/mp7/cmds/ZeroInputs.hpp"
#include "swatch/mp7/cmds/SetID.hpp"
#include "swatch/mp7/cmds/ConfigureRxMGTs.hpp"
#include "swatch/mp7/cmds/ConfigureTxMGTs.hpp"
#include "swatch/mp7/cmds/AlignRxsTo.hpp"
#include "swatch/mp7/cmds/AutoAlign.hpp"
#include "swatch/mp7/cmds/ConfigureBuffers.hpp"
#include "swatch/mp7/cmds/CaptureBuffers.hpp"
#include "swatch/mp7/cmds/SaveBuffersToFile.hpp"
#include "swatch/mp7/cmds/LatencyBuffers.hpp"
#include "swatch/mp7/cmds/EasyLatency.hpp"
#include "swatch/mp7/cmds/HeaderFormatter.hpp"
#include "swatch/mp7/cmds/DatavalidFormatter.hpp"

#include "swatch/mp7/MP7Ports.hpp"
#include "swatch/mp7/MP7ReadoutInterface.hpp"
#include "swatch/mp7/MP7TTCInterface.hpp"

#include "ugt/swatch/AMC502Commands.h"
#include "ugt/swatch/AMC502Algo.h"
#include "ugt/swatch/AMC502ReadoutInterface.h"

// uHAL headers
#include "uhal/HwInterface.hpp"
#include "uhal/ConnectionManager.hpp"

// log4cplus headers
#include "log4cplus/loggingmacros.h"

// C++ headers
#include <iomanip>


namespace swpro = swatch::processor;
namespace swmp7 = swatch::mp7::cmds;

SWATCH_REGISTER_CLASS(amc502::AMC502Processor);


namespace amc502 {


// Static Members Initialization
const std::string AMC502Processor::CmdIds::kReset = "resetBoard";
const std::string AMC502Processor::CmdIds::kPayloadReset = "payloadReset";
const std::string AMC502Processor::CmdIds::kZeroInputs = "zeroInputs";
const std::string AMC502Processor::CmdIds::kSetId = "setId";
const std::string AMC502Processor::CmdIds::kCfgRxMGTs = "cfgRxMGTs";
const std::string AMC502Processor::CmdIds::kCfgTxMGTs = "cfgTxMGTs";
const std::string AMC502Processor::CmdIds::kAlignMGTs = "alignMGTs";
const std::string AMC502Processor::CmdIds::kAutoAlignMGTs = "autoAlignMGTs";
const std::string AMC502Processor::CmdIds::kCfgRxBuffers = "cfgRxBuffers";
const std::string AMC502Processor::CmdIds::kCfgTxBuffers = "cfgTxBuffers";
const std::string AMC502Processor::CmdIds::kCaptureBuffers = "capture";
const std::string AMC502Processor::CmdIds::kSaveRxBuffers = "saveRxBuffers";
const std::string AMC502Processor::CmdIds::kSaveTxBuffers = "saveTxBuffers";
const std::string AMC502Processor::CmdIds::kCfgLatencyRxBuffers = "latencyRxBuffers";
const std::string AMC502Processor::CmdIds::kCfgLatencyTxBuffers = "latencyTxBuffers";
const std::string AMC502Processor::CmdIds::kCfgEasyRxLatency = "easyRxLatency";
const std::string AMC502Processor::CmdIds::kCfgEasyTxLatency = "easyTxLatency";
const std::string AMC502Processor::CmdIds::kCfgHdrFormatter = "cfgHdrFormatter";
const std::string AMC502Processor::CmdIds::kCfgDataValidFmt = "cfgDVFormatter";
const std::string AMC502Processor::CmdIds::kResetCounter = "resetCounter";



//---
AMC502Processor::AMC502Processor(const swatch::core::AbstractStub& aStub) :
MP7AbstractProcessor(aStub)
{
  // Extract stub, and create driver
  const swatch::processor::ProcessorStub& stub = getStub();

  mDriver = new AMC502Controller(uhal::ConnectionManager::getDevice(stub.id, stub.uri, stub.addressTable));

  // Build subcomponents
  registerInterface(new swatch::mp7::MP7TTCInterface(*mDriver));
  registerInterface(new AMC502ReadoutInterface(*mDriver));
  registerInterface(new swpro::InputPortCollection());
  registerInterface(new swpro::OutputPortCollection());

  // Register algo interface in sub class.


  buildPorts(stub);

  // Register default MP7 commands and AMC502 reset command
  registerCommand<AMC502ResetCommand>(CmdIds::kReset);
  registerCommand<swmp7::ZeroInputs>(CmdIds::kZeroInputs);
  registerCommand<swmp7::SetID>(CmdIds::kSetId);
  registerCommand<swmp7::ConfigureRxMGTs>(CmdIds::kCfgRxMGTs);
  registerCommand<swmp7::ConfigureTxMGTs>(CmdIds::kCfgTxMGTs);
  registerCommand<swmp7::AlignRxsTo>(CmdIds::kAlignMGTs);
  registerCommand<swmp7::AutoAlign>(CmdIds::kAutoAlignMGTs);
  registerCommand<swmp7::ConfigureRxBuffers>(CmdIds::kCfgRxBuffers);
  registerCommand<swmp7::ConfigureTxBuffers>(CmdIds::kCfgTxBuffers);
  registerCommand<swmp7::CaptureBuffers>(CmdIds::kCaptureBuffers);
  registerCommand<swmp7::SaveRxBuffersToFile>(CmdIds::kSaveRxBuffers);
  registerCommand<swmp7::SaveTxBuffersToFile>(CmdIds::kSaveTxBuffers);
  registerCommand<swmp7::LatencyRxBuffers>(CmdIds::kCfgLatencyRxBuffers);
  registerCommand<swmp7::LatencyTxBuffers>(CmdIds::kCfgLatencyTxBuffers);
  registerCommand<swmp7::EasyRxLatency>(CmdIds::kCfgEasyRxLatency);
  registerCommand<swmp7::EasyTxLatency>(CmdIds::kCfgEasyTxLatency);
  registerCommand<swmp7::HeaderFormatter>(CmdIds::kCfgHdrFormatter);
  registerCommand<swmp7::DatavalidFormatter>(CmdIds::kCfgDataValidFmt);
  registerCommand<AMC502PayloadResetCommand>(CmdIds::kPayloadReset);
  registerCommand<AMC502ResetCounterCommand>(CmdIds::kResetCounter);

  uint64_t lFwVsn = retrieveFirmwareVersion();
  LOG4CPLUS_INFO(getLogger(), "AMC502 processor '" << this->getId() << "' built: firmware 0x" << std::hex << lFwVsn);
}


AMC502Processor::~AMC502Processor()
{
  delete mDriver;
}

void AMC502Processor::buildPorts(const swatch::processor::ProcessorStub& stub)
{

  const ::mp7::DatapathDescriptor lDescriptor = mDriver->channelMgr().getDescriptor();

  // Add input ports
  for (auto it = stub.rxPorts.begin(); it != stub.rxPorts.end(); it++) {

    swatch::mp7::MP7RxPort* rxPort = new swatch::mp7::MP7RxPort(it->id, it->number, *mDriver);
    getInputPorts().addPort(rxPort);
    const ::mp7::RegionInfo& lRegInfo = lDescriptor.getRegionInfoByChannel(it->number);

    mRxDescriptors.insert(
        swatch::mp7::ChannelDescriptor(
        it->id,
        it->number,
        lRegInfo.mgtIn != ::mp7::kNoMGT,
        lRegInfo.bufIn != ::mp7::kNoBuffer,
        lRegInfo.fmt,
        rxPort
        )
    );

  }


  for (auto it = stub.txPorts.begin(); it != stub.txPorts.end(); it++) {

    getOutputPorts().addPort(new swatch::mp7::MP7TxPort(it->id, it->number, *mDriver));

    const ::mp7::RegionInfo& lRegInfo = lDescriptor.getRegionInfoByChannel(it->number);

    mTxDescriptors.insert(
        swatch::mp7::ChannelDescriptor(
        it->id,
        it->number,
        lRegInfo.mgtOut != ::mp7::kNoMGT,
        lRegInfo.bufOut != ::mp7::kNoBuffer,
        lRegInfo.fmt
        )
    );
  }
}


AMC502Controller& AMC502Processor::driver()
{
  return *mDriver;
}


uint64_t
AMC502Processor::retrieveFirmwareVersion() const
{
  uhal::ValWord<uint32_t> v = mDriver->getCtrl().getNode("id.fwrev").read();
  mDriver->hw().dispatch();

  return v.value();
}


std::string
AMC502Processor::firmwareInfo() const
{
  return "";
}


void AMC502Processor::retrieveMetricValues()
{
  setMetricValue<>(mMetricFirmwareVersion, retrieveFirmwareVersion());
}

} // namespace amc502
