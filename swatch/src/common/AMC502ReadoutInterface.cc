
#include "ugt/swatch/AMC502ReadoutInterface.h"

// SWATCH headers
#include "swatch/core/TTSUtils.hpp"

// AMC502 Headers
#include "ugt/swatch/AMC502Controller.h"

namespace swtts = ::swatch::core::tts;

namespace amc502 {

AMC502ReadoutInterface::AMC502ReadoutInterface(::amc502::AMC502Controller& controller) :
mDriver(controller)
{
}


AMC502ReadoutInterface::~AMC502ReadoutInterface()
{
}


void AMC502ReadoutInterface::retrieveMetricValues()
{
  setMetricValue<>(mMetricTTS, static_cast<swtts::State>(swtts::kReady));
  setMetricValue<>(mMetricAMCCoreReady, static_cast<bool>(true));
  setMetricValue<>(mMetricEventCounter, static_cast<uint32_t>(0));
}


} // namespace amc502
