/**
 * @author      Takashi Matsushita
 */

#ifndef ugt_swatch_uGtTriggerMenu_h
#define ugt_swatch_uGtTriggerMenu_h

#include <map>

#include "xdata/Table.h"
#include "ugt/swatch/Algorithm.h"

namespace ugt
{

typedef std::map<unsigned int, Algorithm*> TriggerMenu;


/**
 * Class for the uGt menu
 */
class uGtTriggerMenu
{
  public:
    typedef std::map<unsigned int, Algorithm*>::const_iterator iterator;

    uGtTriggerMenu();
    virtual ~uGtTriggerMenu();

    void setTriggerMenu(const xdata::Table& table);
    const Algorithm* getAlgorithm(const unsigned int index) const;
    const Algorithm* getAlgorithmByName(const std::string name) const;
    const TriggerMenu& get() const { return mTriggerMenu; };
    void show() const;
    void clear();
    void setFinorMaskContainer(const xdata::Table* table);  // throws toolbox::MaskTableException
    void setVetoMaskContainer(const xdata::Table* table);   // throws toolbox::MaskTableException
    void setAlgoBxMaskContainer(const xdata::Table* table); // throws toolbox::AlgoBXMaskTableException
    void setPrescalesContainer(const xdata::Table* table, const xdata::Table* tableMask);  // throws toolbox::PrescaleTableException


    /** Returns true when algoname exists in menu */
    bool algoExists(std::string algoname) const;

    /** Returns number of algorithms in the TriggerMenu map. */
    size_t size() const;

    /** Returns map iterator to beginning. */
    iterator begin() const;

    /** Returns map iterator to end. */
    iterator end() const;

  private:
    TriggerMenu mTriggerMenu;

};

} // namespace ugt

#endif /* ugt_swatch_uGtTriggerMenu_h */
/* eof */
