/**
 * @file    uGtFinorProcessor.h
 * @author  Gregor Aradi, Bernhard Arnold
 * @brief   Finor board processor implementation
 * @date    2016-02-29
 */
#ifndef UGT_UGTFINORPROCESSOR_H
#define UGT_UGTFINORPROCESSOR_H


#include "ugt/swatch/AMC502Processor.h"

#include "swatch/processor/ProcessorStub.hpp"

#include "uhal/HwInterface.hpp"
#include "uhal/ConnectionManager.hpp"


namespace ugt
{

class uGtFinorProcessor : public amc502::AMC502Processor
{
  public:

    uGtFinorProcessor(const ::swatch::core::AbstractStub&);
    virtual ~uGtFinorProcessor();
};

} // namespace ugt

#endif // UGT_UGTFINORPROCESSOR_H
