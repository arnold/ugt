/**
 * @file    AMC502Controller.hpp
 * @author  Gregor Aradi, Bernhard Arnold
 * @brief   AMC502 controller implementation
 * @date
 */

#ifndef AMC502_AMC502CONTROLLER_HPP
#define AMC502_AMC502CONTROLLER_HPP


// uHal Headers
#include "uhal/HwInterface.hpp"

// MP7 Headers
#include "mp7/definitions.hpp"
#include "mp7/MP7MiniController.hpp"
#include "mp7/CommandSequence.hpp"
#include "mp7/CtrlNode.hpp"
#include "mp7/MmcManager.hpp"
#include "mp7/Utilities.hpp"
#include "mp7/Measurement.hpp"
#include "mp7/ChannelManager.hpp"
#include "mp7/DatapathDescriptor.hpp"
#include "mp7/Orbit.hpp"
#include "mp7/ReadoutNode.hpp"


namespace mp7 {
class AlignMonNode;
class BoardData;
class ChanBufferNode;
class ClockingNode;
class ClockingR1Node;
class ClockingXENode;
class FormatterNode;
class MGTRegionNode;
class SI5326Node;
class SI570Node;
class TTCNode;
class DatapathNode;
class ReadoutNode;
}

namespace amc502 {

class AMC502Controller : public ::mp7::MP7MiniController {
public:

    //!
    AMC502Controller(const uhal::HwInterface& aHw);

    //!
    virtual ~AMC502Controller();


    void reset(const std::string& aClkSrc);


  };

} // namespace amc502



#endif /* AMC502_AMC502CONTROLLER_HPP */
