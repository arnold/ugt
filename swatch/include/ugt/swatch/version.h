#ifndef ugt_swatch_version_h
#define ugt_swatch_version_h

#include "config/PackageInfo.h"


#define UGTSWATCH_VERSION_MAJOR 0
#define UGTSWATCH_VERSION_MINOR 24
#define UGTSWATCH_VERSION_PATCH 0

#define UGTSWATCH_VERSION_CODE PACKAGE_VERSION_CODE(UGTSWATCH_VERSION_MAJOR,UGTSWATCH_VERSION_MINOR,UGTSWATCH_VERSION_PATCH)
#ifndef UGTSWATCH_PREVIOUS_VERSIONS
  #define UGTSWATCH_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(UGTSWATCH_VERSION_MAJOR,UGTSWATCH_VERSION_MINOR,UGTSWATCH_VERSION_PATCH)
#else
  #define UGTSWATCH_FULL_VERSION_LIST  UGTSWATCH_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(UGTSWATCH_VERSION_MAJOR,UGTSWATCH_VERSION_MINOR,UGTSWATCH_VERSION_PATCH)
#endif

namespace ugtswatch
{
  const std::string package  = "ugtswatch";
  const std::string versions = UGTSWATCH_FULL_VERSION_LIST;
  const std::string description = "SWATCH plugins for uGT";
  const std::string authors = "Gregor Aradi, Bernhard Arnold, Takashi Matsushita";
  const std::string summary = "SWATCH plugins for uGT";
  const std::string link = "http://cactus.web.cern.ch/";
  config::PackageInfo getPackageInfo();
  void checkPackageDependencies() throw (config::PackageInfo::VersionException);
  std::set<std::string, std::less<std::string> > getPackageDependencies();

} // namespace ugtswatch
#endif /* ugt_swatch_version_h */
/* eof */
