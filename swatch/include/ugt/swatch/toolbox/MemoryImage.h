/**
 * @author  Bernhard Arnold
 */

#ifndef ugt_swatch_MemoryImage_h
#define ugt_swatch_MemoryImage_h

#include <cstdlib>
#include <ostream>
#include <vector>

namespace ugt {
namespace toolbox {

/** Template class for column based uHAL memories as used by algorithm BX mask
 * memory.
 *
 * For storing values exceeding 32 bits the values are split up in multiple
 * memory slices in the following format: (example for 128 bit wide values
 * consuming 4 columns):
 *
 * Example for 8 x 128 bit wide values consuming 4 columns, the listed data
 * represents the DWORD adresses.
 *
 *   <col[0]> <col[1]> <col[2]> <col[3]>
 *   00000000 00000008 00000010 00000018
 *   00000001 00000009 00000011 00000019
 *   00000002 0000000a 00000012 0000001a
 *   00000003 0000000b 00000013 0000001b
 *   00000004 0000000c 00000014 0000001c
 *   00000005 0000000d 00000015 0000001d
 *   00000006 0000000e 00000016 0000001e
 *   00000007 0000000f 00000017 0000001f
 *
 */
template<class T>
class MemoryImage
{
public:
  MemoryImage();
  /** Initializes a memory image by assigning number of columns and rows. The
   * actual size in words is columns * rows. The assigned layout is static and
   * can not be altered. */
  MemoryImage(size_t cols, size_t rows, const T& value = T(0));
  virtual ~MemoryImage();

  /** Returns number of columns assigned to the memory image. */
  size_t cols() const;
  /** Returns number of rows assigned to the memory image. */
  size_t rows() const;
  /** Returns actual number of words consumed by the memory image. */
  size_t size() const;

  /** Returns a flat list of all columns in a sequence. */
  const std::vector<T>& values() const;
  /** Sets the image by assigning a flat list of all columns in a sequence. If
   * the list of values is longer, it will cut of, if shorter the remaining part
   * of the image is cleared to zero. */
  void setValues(const std::vector<T>& values);

  /** Returns a word value by specifiing its overall index. */
  const T& value(size_t index) const;
  /** Set a word value specifiing its overall index. */
  void setValue(size_t index, const T& value);

  /** Returns a word value located by column and row. */
  const T& value(size_t col, size_t row) const;
  /** Set a word value located by column and row. */
  void setValue(size_t col, size_t row, const T& value);

  /** Tests a single bit by its poition *n* (mapped over all columns) and
   * row. For example, an image with four columns of an 32 bit data type
   * represents a valid range of bit positions from 0 up to 127.*/
  bool test(size_t n, size_t row) const;
  /** Set/Clear a single bit by its poition *n* (mapped over all columns) and
   * row. For example, an image with four columns of an 32 bit data type
   * represents a valid range of bit positions from 0 up to 127.*/
  void set(size_t n, size_t row, bool value);

  /** Clears the entire image with zero. Optionally a different value can be
   * assigned. For example to set all bits of the image to true, assign a value
   * of 0xffffffff (using a 32 bit data type). */
  void clear(const T& value = T(0));
  /** Fills the entire image with a counter, representing every value's address offset. */
  void fillCounter();
  /** Fills the entire image with random values. */
  void fillRandom();

protected:
  std::vector<T> values_;
  size_t cols_, rows_;
};

/** Overloaded ostream operator for printing purpose. Provided for convenience. */
template<class T>
::std::ostream& operator<<(::std::ostream& o, const ugt::toolbox::MemoryImage<T>& image);

} // namespace toolbox
} // namespace ugt

#endif
