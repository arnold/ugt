#ifndef ugt_swatch_MaskTable_h
#define ugt_swatch_MaskTable_h

#include "ugt/swatch/toolbox/AbstractTable.h"
#include "swatch/core/exception.hpp"

#include <xdata/Table.h>
#include <xdata/String.h>
#include <stdint.h>

#include <string>
#include <vector>
#include <map>
#include <set>

#include "ugt/swatch/uGtTriggerMenu.h"

namespace ugt {
namespace toolbox {

DEFINE_SWATCH_EXCEPTION(MaskTableException);


class MaskTable: public AbstractTable
{
public:
  /** Iterator type for mask table. */
  typedef std::map<std::string, uint32_t>::const_iterator iterator;


  MaskTable(const std::string& type);
  MaskTable(const std::string& type,
      const xdata::Table& table, const uGtTriggerMenu& menu);
  virtual ~MaskTable() { };


  virtual void verify(const xdata::Table& xtable);
  virtual void verify(const xdata::Table& xtable, const uGtTriggerMenu& menu);

  /** Load (veto/finor) mask from table provided by gatekeeper.
      Verify table when 'doChecks' is true, otherwise skip all checks.
      Throws MaskTableException on errors.*/
  void load(const xdata::Table& table, const uGtTriggerMenu& menu, bool doChecks);

  /** Load (veto/finor) mask from table provided by gatekeeper
      and check with trigger menu for validity.
      Throws MaskTableException on errors.*/
  void load(const xdata::Table& table, const uGtTriggerMenu& menu);

  /** Returns number of mask values/algos in the map. */
  size_t size() const;

  /** Returns the mask from the corresponding algo id */
  uint32_t getMask(const std::string& algo) const;

  /** Returns the default mask defined in the table */
  uint32_t getDefaultMask() const;

  void print();

  /** Returns map iterator to beginning. */
  iterator begin() const;
  /** Returns map iterator to end. */
  iterator end() const;

  void clear();

  void add(const std::string& algoName, uint32_t mask);

protected:
  std::string mType;  // column indentifying the veto or finor mask table (column)


private:
  std::map<std::string, uint32_t> mMask;  // algo name, mask value
  uint32_t mDefaultMask;                  // Default mask by default algo row

  /** Access mask map */
  const std::map<std::string, uint32_t>& getMaskMap() const;
};


class FinorMaskTable: public MaskTable
{
public:
  FinorMaskTable();
  FinorMaskTable(const xdata::Table& table, const uGtTriggerMenu& menu);
  virtual ~FinorMaskTable();

};

class VetoMaskTable: public MaskTable
{
public:
  VetoMaskTable();
  VetoMaskTable(const xdata::Table& table, const uGtTriggerMenu& menu);
  virtual ~VetoMaskTable();

};

} // namespace toolbox
} // namespace ugt

#endif
