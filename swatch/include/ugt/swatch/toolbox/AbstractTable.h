#ifndef ugt_swatch_AbstractTable_h
#define ugt_swatch_AbstractTable_h

#include "swatch/core/exception.hpp"

#include <xdata/Table.h>
#include <stdint.h>

#include <string>
#include <vector>
#include <set>

#include "ugt/swatch/uGtTriggerMenu.h"

namespace ugt {
namespace toolbox {

DEFINE_SWATCH_EXCEPTION(AbstractTableException);

extern const std::string kLhcBxRange;
extern const std::string kDefaultAlgo;

class AbstractTable
{

public:

  AbstractTable() { };
  AbstractTable(std::string tableType);
  virtual ~AbstractTable() { };

  /** Verify whether give table is valid */
  virtual void verify(const xdata::Table& table) = 0;

  /** Verify whether give table is valid and check menu related issues */
  virtual void verify(const xdata::Table& table, const uGtTriggerMenu& menu) = 0;

  /** Returns Table type string **/
  const std::string& getTableType() const {return mTableType;};

  /** Returns number of mask values/algos in the map. */
  size_t size() const { return 0; };


  /** Return true when algo is default algo ('ALL') */
  bool isDefaultAlgo(std::string algo);

  /** Throw an exception and generates corresponding message
      depending on the message strings and content of vector 'vec'
      or set 'set'.
      Throws AbstractTableException */
  template <typename T>
  void genericCheckErrors(const std::vector<T>& vec, const std::string& singleMsg, const std::string& multiMsg);
  template <typename T>
  void genericCheckErrors(const std::set<T>& set, const std::string& singleMsg, const std::string& multiMsg);

  /* Checks the menu size, throws AbstractTableException */
  void checkMenuSizeValid(size_t menuSize);
  /* Checks for correct and missing columns in table, throws AbstractTableException */
  void checkColumnsValid(const std::vector<std::string>& columns, const std::vector<std::string>& columnsExpected);
  /* Checks row size of a table, throws AbstractTableException */
  void checkRowsSizeValid(size_t rowsSize);

  /* Checks whether its default table (empty) or not */
  static bool isDefaultTable(const size_t cols, const size_t rows);

protected:
  std::string mTableType;   // Type of table

};



} // namespace toolbox
} // namespace ugt

#endif
