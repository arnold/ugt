#ifndef ugt_swatch_AlgoBXMaskTable_h
#define ugt_swatch_AlgoBXMaskTable_h

#include "ugt/swatch/toolbox/AbstractTable.h"
#include "swatch/core/exception.hpp"

#include <xdata/Table.h>
#include <xdata/String.h>
#include <stdint.h>

#include <string>
#include <vector>
#include <map>

#include "ugt/swatch/uGtTriggerMenu.h"

namespace ugt {
namespace toolbox {

DEFINE_SWATCH_EXCEPTION(AlgoBXMaskTableException);

/**
 * Structure to hold uGt AlgoBxMaskItem information
 * algo  ... defined algo name or 'ALL'
 * range ... applied range (e.g. 3-9, 9, ALL)
 * mask  ... applied mask value (either 0 or 1)
 */
struct AlgoBXMaskItem
{
  const std::string algo;
  const std::string range;
  const uint32_t mask;

  AlgoBXMaskItem (const std::string& algo,
    const std::string& range, const uint32_t mask)
    : algo(algo)
    , range(range)
    , mask(mask)
    { }
};


/**
 */
class AlgoBXMaskTable: public AbstractTable
{

public:
  /** Iterator type for prescales sets. */
  typedef std::vector<AlgoBXMaskItem>::const_iterator iterator;


  AlgoBXMaskTable();
  AlgoBXMaskTable(const xdata::Table& table, const uGtTriggerMenu& menu);
  virtual ~AlgoBXMaskTable();

  virtual void verify(const xdata::Table& table);
  virtual void verify(const xdata::Table& table, const uGtTriggerMenu& menu);

  /** Load algo BX masks from table provided by gatekeeper
      and check with trigger menu for validity.
      Throws AlgoBXMaskTableException on errors.*/
  void load(const xdata::Table& table, const uGtTriggerMenu& menu);


  /** Returns number of algo bx entries in the vector. */
  size_t size() const;

  /** Returns the algo bx mask map with bx/mask pairs from the corresponding algo name.
   *  the return map contains all mask for the whole orbit (1-3564) */
  std::map<uint32_t, uint32_t> getAlgoBXMask(const std::string& algo);

  /** Access mask list */
  const std::vector<AlgoBXMaskItem>& getMaskList() const;


  /** Returns map iterator to beginning. */
  iterator begin() const;

  /** Returns map iterator to end. */
  iterator end() const;

  /** Returns the default bx mask defined in the table */
  uint32_t getDefaultMask() const;

  /** Return true when range is default range ('ALL') */
  bool isDefaultRange(const std::string& range);

  
  std::vector<uint32_t> getParsedRangeList(const std::string& range);

  void print();

private:
  std::vector<AlgoBXMaskItem> mMaskList;  // (algo name, range and mask value)
  uint32_t mDefaultMask;                  // Default mask by default algo row

  void checkRowsSizeValid(size_t rowsSize);
  std::string buildMinDefaultRowMessage() const;

  /** Return true when algo and range 'ALL' */
  bool isDefaultAlgoRowAvailable(const std::string& algo, const std::string& range);


};

} // namespace toolbox
} // namespace ugt

#endif
