#ifndef ugt_swatch_PrescaleTable_h
#define ugt_swatch_PrescaleTable_h

#include "ugt/swatch/toolbox/AbstractTable.h"
#include "ugt/swatch/toolbox/PrescaleSet.h"

#include "swatch/core/exception.hpp"

#include <xdata/Table.h>
#include <stdint.h>

#include <string>
#include <vector>

#include "ugt/swatch/uGtTriggerMenu.h"

namespace ugt {
namespace toolbox {

DEFINE_SWATCH_EXCEPTION(PrescaleTableException);

class FinorMaskTable;


class PrescaleTable: public AbstractTable
{
public:
  /** Iterator type for prescales sets. */
  typedef std::map<size_t, PrescaleSet>::const_iterator iterator;
  typedef std::map<size_t, PrescaleSet>::iterator edit_iterator;


  PrescaleTable();
  PrescaleTable(const xdata::Table& tablePrescales,
                const xdata::Table& tableMask,
                const uGtTriggerMenu& menu);

  virtual ~PrescaleTable() = default;

  virtual void verify(const xdata::Table& table);
  virtual void verify(const xdata::Table& table,
                      const uGtTriggerMenu& menu);

  /** Load prescales and mask from table provided by gatekeeper.
      Verify table when 'doChecks' is true, otherwise skip all checks.
      Throws PrescaleTableException on errors.*/
  void load(const xdata::Table& tablePrescales,
            const xdata::Table& tableMask,
            const uGtTriggerMenu& menu,
            bool doChecks);

  /** Load prescales and mask from table provided by gatekeeper
      and check with trigger menu for validity.
      Throws PrescaleTableException on errors.*/
  void load(const xdata::Table& tablePrescales,
            const xdata::Table& tableMask,
            const uGtTriggerMenu& menu);


  /** Return the next prescale index available by index (and offset) or
      return just index. */
  size_t getNextPrescaleIndexAvailable(size_t index, size_t offset = 1);

  /** Access prescale set by index. */
  const PrescaleSet& getPrescaleSet(size_t index) const;

  /** Access the next prescale set by index of the current prescale set. */
  const PrescaleSet& getNextPrescaleSet(size_t index, size_t offset = 1);

  /** Returns a prescales map for a specific algo */
  std::map<toolbox::PrescaleColumnItem, double> getPrescalesForAlgo(const std::string& algo);

  /** Returns number of prescales sets in the table. */
  size_t size() const;

  /** Returns map iterator to beginning. */
  iterator begin() const;

  /** Returns map iterator to end. */
  iterator end() const;

  /** Clears all prescale sets */
  void clear();

  void print();

private:
  /** Convert prescale string representation to double representation.
   * Throws std::invalid_argument on conversion errors. */
  double toDouble(const std::string& s, const size_t precision) const;

private:
  std::map<size_t, PrescaleSet> mPrescaleSets; // column index, PrescaleSet
};

} // namespace toolbox
} // namespace ugt

#endif
