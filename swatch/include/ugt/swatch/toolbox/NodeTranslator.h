/**
 * @author  Bernhard Arnold
 */

#ifndef ugt_swatch_NodeTranslator_h
#define ugt_swatch_NodeTranslator_h

#include "uhal/Node.hpp"

#include <iostream>
#include <map>
#include <stdexcept>
#include <string>

#include <stdint.h>


namespace ugt {
namespace toolbox {

class NodeTranslator
{
public:

  NodeTranslator();
  virtual ~NodeTranslator();

  /** Translates a value to a formatted human readable string.
   * Translation types are retrieved from the node parameter dictionary.
   */
  template <typename T>
  std::string tr(const ::uhal::Node& node, const ::uhal::ValWord<T>& value);

  /** Translates a vector of values to a formatted human readable string.
   * Translation types are retrieved from the node parameter dictionary.
   */
  template <typename T>
  std::string tr(const ::uhal::Node& node, const ::uhal::ValVector<T>& values);

  /** Provided for convenience.
   */
  template <typename T>
  std::string operator() (const ::uhal::Node& node, const ::uhal::ValWord<T>& value);

  /** Provided for convenience.
   */
  template <typename T>
  std::string operator() (const ::uhal::Node& node, const ::uhal::ValVector<T>& values);

protected:

  /** Translates to a default formatting. Provided as fallback solution.
   */
  template <typename T>
  std::string tr_default(const T& values);

  /** Translates to a default formatting. Provided as fallback solution.
   */
  template <typename T>
  std::string tr_default(const std::vector<T>& values);

  /** Translates a value vector to string.
   */
  template <typename T>
  std::string tr_string(const std::vector<T>& values);

  /** Translates a value vector to UUID.
   */
  template <typename T>
  std::string tr_uuid(const std::vector<T>& values);

  /** Translates a value vector to a version number.
   */
  template <typename T>
  std::string tr_version(const T& values);

  /** Translates a value to an ISO datetime.
   */
  template <typename T>
  std::string tr_timestamp(const T& values);

  /** Translates a value to a signed integer.
   */
  template <typename T>
  std::string tr_signed(const T& values);

  /** Translates a value to a boolean.
   */
  template <typename T>
  std::string tr_boolean(const T& value);

  /** Translates a value to a hex representation.
   */
  template <typename T>
  std::string tr_hex(const T& value);

private:

  /** Retruns type of node, returns empty string if no type param assigned.
   */
  std::string getType(const ::uhal::Node& node);

  /** Serialize simple data types to byte vector.
   *
   * @param value multibyte value to be serialized.
   * @return Vector containing the bytes.
   */
  template<typename T>
  std::vector<uint8_t> serialize(const T& value);

  /** Serialize vector of simple data types to byte vector.
   *
   * @param values vector of multibyte values to be serialized.
   * @return Vector containing the bytes.
   */
  template<typename T>
  std::vector<uint8_t> serialize(const std::vector<T>& values);

};

} // namespace toolbox
} // namespace ugt

#endif // ugt_swatch_NodeTranslator_h
