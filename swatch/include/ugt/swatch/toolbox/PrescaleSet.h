#ifndef ugt_swatch_PrescaleSet_h
#define ugt_swatch_PrescaleSet_h

#include "swatch/core/exception.hpp"
#include <boost/lexical_cast.hpp>

#include <stdint.h>

#include <string>
#include <vector>

namespace ugt {
namespace toolbox {

DEFINE_SWATCH_EXCEPTION(PrescaleSetException);

class MaskTable;

/**
 * Structure holding a Prescale column
 * containing index and optional label
 */
struct PrescaleColumnItem
{
  const uint32_t index;
  const std::string label;
  const std::string formattedLabel;

  PrescaleColumnItem (const uint32_t index,
             const std::string& label)
    : index(index)
    , label(label)
    , formattedLabel(buildFormattedLabel())
    { }

  // Sort PrescaleColumnItems by index 0,1,2...
  bool operator<(PrescaleColumnItem const& other) const
  {
    return (index < other.index);
  }

  // Returns true when index and label are equal
  // (e.g.: index=5, label="5" -> true
  // index=5, label="1E123" -> false)
  bool isIndexEqualLabel() const
  {
    uint32_t columnlabel = 0;
    bool isEqual = false;
    try
    {
      columnlabel = boost::lexical_cast<size_t>(label);
      if (index == columnlabel)
        isEqual = true;
      else
        isEqual = false;
    }
    catch (boost::bad_lexical_cast &e)
    {
      isEqual = false;
    }
    return isEqual;
  }

  const std::string& getFormattedLabel() const
  {
    return formattedLabel;
  }

  // Returns index and label formatted
  // used for displaying that information to the user
  // (e.g.: index=5, label="5" -> returns: "5"
  // index=5, label="1E123" -> returns: "1E123 (5)")
  std::string buildFormattedLabel() const
  {
    std::stringstream columnlabel;

    if (isIndexEqualLabel())
      columnlabel << index;
    else
      columnlabel << label << " (" << index << ")";

    return columnlabel.str();
  }
};

class PrescaleSet
{
public:
  /** Iterator type for algo/prescales map. */
  typedef std::map<std::string, double>::const_iterator iterator;


  PrescaleSet();
  PrescaleSet(const std::string& column, const std::map<std::string, double> prescales);
  virtual ~PrescaleSet();

  /** Returns prescale set index. */
  size_t getIndex() const;

  /** Returns optinal prescale set label. */
  const std::string& getLabel() const;

  /** Returns a formatted label "mLabel (mIndex)" or only "mIndex" when mLabel contains the index **/
  const std::string getFormattedLabel() const;

  /** Returns prescale column string */
  const std::string& getColumn() const;

  /** Returns number of prescales values/algos in the set. */
  size_t size() const;

  /** Returns true, when algos/prescale map contains no elements. False, when map contains elements */
  bool isEmpty() const;

  /** Returns prescale values indexed by algorithm name. */
  double getPrescale(const std::string& algo) const;


  /** Returns prescale map with <algoname, prescale> */
  const std::map<std::string, double>& getPrescales() const;

  /** Returns the iterator pointing to the prescale value by algorithm name. */
  iterator findPrescale(const std::string& algo) const;

  /** Returns map iterator to beginning. */
  iterator begin() const;

  /** Returns map iterator to end. */
  iterator end() const;


  /** Clears the algorithm/prescales list, prescale label
   *  and resets the index to default value. */
  void clear();

  /** Update a prescale set with a new column and algorithm/prescale map.
   *  The old content will be cleared first*/
  void updateSet(const std::string& column, const std::map<std::string, double>& prescales);

  /** Returns index from a column (e.g.: "1"->1, "1:15E45"->1, "1:1234"->1) **/
  static size_t getIndexFromColumn(const std::string& column);

private:
  std::string mColumn;   // prescale column from table
  size_t mIndex;         // prescale index
  std::string mLabel;    // prescale label

  std::map<std::string, double> mPrescales; // algo, prescale value


  /** Returns label from a column (e.g.: "1"->"1", "1:15E45"->"15E45", "1:1234"->"1234") **/
  static std::string getLabelFromColumn(const std::string& column);


};

} // namespace toolbox
} // namespace ugt

#endif
