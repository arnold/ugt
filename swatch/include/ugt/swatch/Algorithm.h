#ifndef _UGT_SWATCH_ALGORITHM_H_
#define _UGT_SWATCH_ALGORITHM_H_

#include <stdint.h>
#include <map>
#include <string>

#include "swatch/core/exception.hpp"
#include "ugt/swatch/toolbox/PrescaleSet.h"

namespace ugt
{

DEFINE_SWATCH_EXCEPTION(AlgorithmException);

class Algorithm
{
public:

  Algorithm(const std::string& name,
             const uint32_t index,
             const uint32_t module_id,
             const uint32_t local_id);
  virtual ~Algorithm();

  /** Returns the name of algorithm */
  const std::string& getName() const;
  /** Returns the index of algorithm */
  const uint32_t getIndex() const;
  /** Returns the board module id the algorithm is on */
  const uint32_t getModuleId() const;
  /** Returns the local id of algorithm */
  const uint32_t getLocalId() const;
  /** Returns the veto mask of algorithm */
  const uint32_t getVeto() const;
  /** Returns the finor mask of algorithm */
  const uint32_t getMask() const;
  /** Returns the prescale value accessed by prescale column index psIndex */
  const double getPrescale(uint32_t psIndex) const;
  /** Returns the prescale value merged with (finor)mask
      accessed by prescale column index psIndex.
      The return value is the product of prescale*mask */
  const double getMergedPrescaleMask(uint32_t psIndex) const;

  /** Returns the prescale map with <prescale column, prescale value> pairs */
  const std::map<toolbox::PrescaleColumnItem, double>& getPrescales() const;
  /** Returns an iterator to the position of the prescale value accessed by prescale column index psIndex.*/
  std::map<toolbox::PrescaleColumnItem, double>::const_iterator getPrescalesForPsIndex(uint32_t psIndex) const;

  const toolbox::PrescaleColumnItem& getPrescaleColumnItemForPsIndex(uint32_t psIndex) const;
  const std::string& getFormattedLabelForPsIndex(uint32_t psIndex) const;

  /** Returns the algo bx mask map with <bx value, mask value> pairs */
  const std::map<uint32_t, uint32_t>& getAlgoBxMask() const;


  /** Set veto mask value */
  void setVeto(uint32_t veto);
  /** Set (finor)mask value */
  void setMask(uint32_t mask);

  /** Set prescales map */
  void setPrescales(const std::map<toolbox::PrescaleColumnItem, double>& prescales);
  /** Set algo bx mask map */
  void setAlgoBxMask(std::map<uint32_t, uint32_t>& algoBxMask); // bx-id, mask

  /** Prints properties of the algorithm object to stdout */
  void show() const;

private:
  const std::string mName;
  const uint32_t mIndex;
  const uint32_t mModuleId;
  const uint32_t mLocalId;
  uint32_t mVeto;
  uint32_t mMask;
  std::map<toolbox::PrescaleColumnItem, double> mPrescales;  // <prescale column (index, label), prescale value>
  std::map<uint32_t, uint32_t> mAlgoBxMask; // <bx value, mask value>

};

} // namespace ugt
#endif /* _UGT_SWATCH_ALGORITHM_H_ */
