#ifndef ugt_swatch_uGtExtCondAlgo_h
#define ugt_swatch_uGtExtCondAlgo_h

#include "ugt/swatch/AMC502Algo.h"

#include "swatch/core/SimpleMetric.hpp"

namespace amc502{
  class AMC502Controller;
}


namespace ugt
{

/**
 * This class implements AM502Algo
 */
class uGtExtCondAlgo : public amc502::AMC502Algo
{
  public:
    uGtExtCondAlgo(amc502::AMC502Controller& driver);

    virtual ~uGtExtCondAlgo();

    uint32_t getBxNumberMax() const;
    //! Returns 64 bit orbit number.
    uint64_t getOrbitNumber() const;
    //! Returns current luminosity segment number.
    uint32_t getLuminositySection() const;


  protected:
    virtual void retrieveMetricValues();

  private:
    amc502::AMC502Controller& driver_;

    ::swatch::core::SimpleMetric <uint32_t>& metricBxNumberMax_;
    ::swatch::core::SimpleMetric <uint32_t>& metricLuminositySegmentNumber_;
    ::swatch::core::SimpleMetric <uint64_t>& metricOrbitCounter_;

};

} // namespace ugt


#endif /* ugt_swatch_uGtExtCondAlgo_h */
/* eof */
