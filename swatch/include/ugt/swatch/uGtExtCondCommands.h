

#ifndef UGT_SWATCH_UGTEXTCONDCOMMANDS_H
#define UGT_SWATCH_UGTEXTCONDCOMMANDS_H

#include "swatch/action/Command.hpp"

#include "xdata/Boolean.h"
#include "xdata/Integer.h"
#include "xdata/String.h"
#include "xdata/Table.h"
#include "xdata/UnsignedInteger.h"

#include "swatch/logger/Logger.hpp"

namespace ugt
{


class uGtExtCondDelayCommand : public ::swatch::action::Command
{
  public:
    uGtExtCondDelayCommand(const std::string& aId,
                   ::swatch::action::ActionableObject& aActionable);
    virtual ~uGtExtCondDelayCommand() {};
    virtual State code(const ::swatch::core::XParameterSet& params);
};

class uGtExtCondPhaseShiftCommand : public ::swatch::action::Command
{
  public:
    uGtExtCondPhaseShiftCommand(const std::string& aId,
                   ::swatch::action::ActionableObject& aActionable);
    virtual ~uGtExtCondPhaseShiftCommand() {};
    virtual State code(const ::swatch::core::XParameterSet& params);
};


} // namespace ugt

#endif /* UGT_SWATCH_UGTEXTCONDCOMMANDS_H */
/* eof */
