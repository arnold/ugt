#ifndef ugt_system_h
#define ugt_system_h

#include "swatch/system/System.hpp"

namespace ugt {
class uGtSystem : public swatch::system::System {
public:
    uGtSystem(const swatch::core::AbstractStub& aStub);
    virtual ~uGtSystem();
};

} // namespace ugt

#endif /* define ugt_system_h */
