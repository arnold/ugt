/*
 * @file:   AMC502Commands.cc
 * @author: Gregor Aradi, Bernhard Arnold
 *
 */

#ifndef UGT_SWATCH_AMC502COMMANDS_H
#define UGT_SWATCH_AMC502COMMANDS_H

#include "swatch/action/Command.hpp"
#include "swatch/core/utilities.hpp"


namespace amc502 {

/**
 * This class implements AMC502 reset command
 */
class AMC502ResetCommand : public ::swatch::action::Command
{
  public:
    AMC502ResetCommand(const std::string& aId,
                       ::swatch::action::ActionableObject& aActionable);
    virtual ~AMC502ResetCommand() {}
    virtual State code(const ::swatch::core::XParameterSet& params);
};


class AMC502PayloadResetCommand : public ::swatch::action::Command
{
  public:
    AMC502PayloadResetCommand(const std::string& aId,
                       ::swatch::action::ActionableObject& aActionable);
    virtual ~AMC502PayloadResetCommand() {}
    virtual State code(const ::swatch::core::XParameterSet& params);
};


class AMC502ResetCounterCommand : public ::swatch::action::Command
{
  public:
    AMC502ResetCounterCommand(const std::string& aId,
                       ::swatch::action::ActionableObject& aActionable);
    virtual ~AMC502ResetCounterCommand() {}
    virtual State code(const ::swatch::core::XParameterSet& params);
};


} // namespace amc502

#endif /* UGT_SWATCH_AMC502COMMANDS_H */
/* eof */
