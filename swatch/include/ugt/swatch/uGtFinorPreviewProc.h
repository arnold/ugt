/**
 * @file    uGtFinorPreviewProc.h
 * @author  Gregor Aradi, Bernhard Arnold
 * @brief   Finor preview board processor implementation
 * @date    2016-11-28
 */
#ifndef UGT_UGTFINORPREVIEWPROC_H
#define UGT_UGTFINORPREVIEWPROC_H


#include "ugt/swatch/AMC502Processor.h"

#include "swatch/processor/ProcessorStub.hpp"

#include "uhal/HwInterface.hpp"
#include "uhal/ConnectionManager.hpp"


namespace ugt
{

class uGtFinorPreviewProc : public amc502::AMC502Processor
{
  public:

    uGtFinorPreviewProc(const ::swatch::core::AbstractStub&);
    virtual ~uGtFinorPreviewProc();
};

} // namespace ugt

#endif // UGT_UGTFINORPREVIEWPROC_H
