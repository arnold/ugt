#ifndef _UGT_SWATCH_VETOMASKRULE_H_
#define _UGT_SWATCH_VETOMASKRULE_H_

#include "xdata/Table.h"
#include "swatch/core/XRule.hpp"

namespace ugt {
namespace rules {

class VetoMaskRule : public swatch::core::XRule<xdata::Table>
{

public:
  VetoMaskRule() { };
  virtual ~VetoMaskRule() { };

  virtual swatch::core::XMatch verify (const xdata::Table& aValue) const;

private:
  virtual void describe(std::ostream& aStream) const;

};


} // namespace rules
} // namespace ugt
#endif /* _UGT_SWATCH_VETOMASKRULE_H_ */
