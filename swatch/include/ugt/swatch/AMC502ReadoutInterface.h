/*
 * File:   AMC502ReadoutInterface.hpp
 * Author: Bernhard Arnold
 *
 * Created on April 4, 2016, 11:19 AM
 */

#ifndef __SWATCH_MP7_AMC502READOUTINTERFACE_HPP__
#define __SWATCH_MP7_AMC502READOUTINTERFACE_HPP__


// Swatch Headers
#include "swatch/processor/ReadoutInterface.hpp"


namespace amc502 {
class AMC502Controller;
}


namespace amc502 {

class AMC502ReadoutInterface : public swatch::processor::ReadoutInterface {
public:
    AMC502ReadoutInterface( ::amc502::AMC502Controller& );

    virtual ~AMC502ReadoutInterface();

protected:
    virtual void retrieveMetricValues();

private:
    ::amc502::AMC502Controller& mDriver;
};

} // namespace amc502

#endif /* __SWATCH_MP7_AMC502READOUTINTERFACE_HPP__ */
