#ifndef ugt_swatch_commands_uGtResetCounterCommand_h
#define ugt_swatch_commands_uGtResetCounterCommand_h

#include "swatch/action/Command.hpp"


namespace ugt {

/**
 * This class implements uGt reset counter command
 * to reset uGT specific counter
 */
class uGtResetCounterCommand : public ::swatch::action::Command
{
public:
  uGtResetCounterCommand(const std::string& aId,
                  ::swatch::action::ActionableObject& aActionable);
  virtual ~uGtResetCounterCommand() = default;
  virtual State code(const ::swatch::core::XParameterSet& params);
};

} // namespace ugt
#endif /* ugt_swatch_commands_uGtResetCounterCommand_h */
