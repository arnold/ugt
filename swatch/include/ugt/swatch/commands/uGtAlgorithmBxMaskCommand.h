#ifndef ugt_swatch_commands_uGtAlgorithmBxMaskCommand_h
#define ugt_swatch_commands_uGtAlgorithmBxMaskCommand_h

#include "swatch/action/Command.hpp"


namespace ugt {

/**
 * This class implements uGt algoBxMem command
 */
class uGtAlgorithmBxMaskCommand : public ::swatch::action::Command
{
public:
  uGtAlgorithmBxMaskCommand(const std::string& aId,
    ::swatch::action::ActionableObject& aActionable);
  virtual ~uGtAlgorithmBxMaskCommand() = default;
  virtual State code(const ::swatch::core::XParameterSet& params);
};

} // namespace ugt
#endif /* ugt_swatch_commands_uGtAlgorithmBxMaskCommand_h */
