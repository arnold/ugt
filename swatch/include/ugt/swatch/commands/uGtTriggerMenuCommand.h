#ifndef ugt_swatch_commands_uGtTriggerMenuCommand_h
#define ugt_swatch_commands_uGtTriggerMenuCommand_h

#include "swatch/action/Command.hpp"

namespace ugt {

/**
 * This class implements uGt level-1 trigger menu command
 */
class uGtTriggerMenuCommand : public ::swatch::action::Command
{
public:
  uGtTriggerMenuCommand(const std::string& aId,
                        ::swatch::action::ActionableObject& aActionable);
  virtual ~uGtTriggerMenuCommand() = default;
  virtual State code(const ::swatch::core::XParameterSet& params);
};

} // namespace ugt
#endif /* ugt_swatch_commands_uGtTriggerMenuCommand_h */
