#ifndef ugt_swatch_commands_uGtResetCommand_h
#define ugt_swatch_commands_uGtResetCommand_h

#include "swatch/action/Command.hpp"


namespace ugt {

/**
 * This class implements uGt reset command
 */
class uGtResetCommand : public ::swatch::action::Command
{
public:
  uGtResetCommand(const std::string& aId,
                  ::swatch::action::ActionableObject& aActionable);
  virtual ~uGtResetCommand() = default;
  virtual State code(const ::swatch::core::XParameterSet& params);
};

} // namespace ugt
#endif /* ugt_swatch_commands_uGtResetCommand_h */
