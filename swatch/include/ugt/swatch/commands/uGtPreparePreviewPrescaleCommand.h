#ifndef ugt_swatch_commands_uGtPreparePreviewPrescaleCommand_h
#define ugt_swatch_commands_uGtPreparePreviewPrescaleCommand_h

#include "swatch/action/Command.hpp"


namespace ugt {

/**
 * This class loads prescales to memory only.
 * The corresponding command is uGtApplyPreviewPrescaleCommand
 * which activates the changed prescale values.
 */
class uGtPreparePreviewPrescaleCommand : public ::swatch::action::Command
{
public:
  uGtPreparePreviewPrescaleCommand(const std::string& aId,
                     ::swatch::action::ActionableObject& aActionable);
  virtual ~uGtPreparePreviewPrescaleCommand() = default;
  virtual State code(const ::swatch::core::XParameterSet& params);
};

} // namespace ugt
#endif /* ugt_swatch_commands_uGtPreparePreviewPrescaleCommand_h */
