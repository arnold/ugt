#ifndef ugt_swatch_commands_uGtVersionCommand_h
#define ugt_swatch_commands_uGtVersionCommand_h

#include "swatch/action/Command.hpp"

namespace ugt {

/**
 * This class implements uGt version command
 */
class uGtVersionCommand : public ::swatch::action::Command
{
public:
  uGtVersionCommand(const std::string& aId,
                    ::swatch::action::ActionableObject& aActionable);
  virtual ~uGtVersionCommand() = default;
  virtual State code(const ::swatch::core::XParameterSet& params);
};

} // namespace ugt
#endif /* ugt_swatch_commands_uGtVersionCommand_h */
