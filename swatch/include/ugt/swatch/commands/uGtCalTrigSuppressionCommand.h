#ifndef ugt_swatch_commands_uGtCalTrigSuppressionCommand_h
#define ugt_swatch_commands_uGtCalTrigSuppressionCommand_h

#include "swatch/action/Command.hpp"


namespace ugt {

/**
 * This class implements uGt calibration trigger suppression gap
 */
class uGtCalTrigSuppressionCommand : public ::swatch::action::Command
{
public:
  uGtCalTrigSuppressionCommand(const std::string& aId,
    ::swatch::action::ActionableObject& aActionable);
  virtual ~uGtCalTrigSuppressionCommand() = default;
  virtual State code(const ::swatch::core::XParameterSet& params);
};

} // namespace ugt
#endif /* ugt_swatch_commands_uGtCalTrigSuppressionCommand_h */
