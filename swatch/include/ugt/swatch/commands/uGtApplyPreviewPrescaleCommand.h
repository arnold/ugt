#ifndef ugt_swatch_commands_uGtApplyPreviewPrescaleCommand_h
#define ugt_swatch_commands_uGtApplyPreviewPrescaleCommand_h

#include "swatch/action/Command.hpp"


namespace ugt {

/**
 * This class activates previously prepared preview prescales.
 * Preview prescales loading is performed due to uGtPreparePreviewPrescaleCommand.
 */
class uGtApplyPreviewPrescaleCommand : public ::swatch::action::Command
{
public:
  uGtApplyPreviewPrescaleCommand(const std::string& aId,
                     ::swatch::action::ActionableObject& aActionable);
  virtual ~uGtApplyPreviewPrescaleCommand() = default;
  virtual State code(const ::swatch::core::XParameterSet& params);
};

} // namespace ugt
#endif /* ugt_swatch_commands_uGtApplyPreviewPrescaleCommand_h */
