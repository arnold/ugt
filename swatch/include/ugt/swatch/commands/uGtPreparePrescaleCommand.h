#ifndef ugt_swatch_commands_uGtPreparePrescaleCommand_h
#define ugt_swatch_commands_uGtPreparePrescaleCommand_h

#include "swatch/action/Command.hpp"


namespace ugt {

/**
 * This class loads prescales to memory only.
 * The corresponding command is uGtApplyPrescaleCommand
 * which activates the changed prescale values.
 */
class uGtPreparePrescaleCommand : public ::swatch::action::Command
{
public:
  uGtPreparePrescaleCommand(const std::string& aId,
                     ::swatch::action::ActionableObject& aActionable);
  virtual ~uGtPreparePrescaleCommand() = default;
  virtual State code(const ::swatch::core::XParameterSet& params);
};

} // namespace ugt
#endif /* ugt_swatch_commands_uGtPreparePrescaleCommand_h */
