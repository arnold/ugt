#ifndef ugt_swatch_commands_uGtSmartRebootCommand_h
#define ugt_swatch_commands_uGtSmartRebootCommand_h

#include "swatch/mp7/cmds/RebootFPGA.hpp"

namespace ugt {

/**
 * This class implements a conditional reboot FPGA command
 */
class uGtSmartRebootCommand : public ::swatch::mp7::cmds::RebootFPGA
{
public:
  uGtSmartRebootCommand(const std::string& aId,
                        ::swatch::action::ActionableObject& aActionable);
  virtual ~uGtSmartRebootCommand() = default;
  virtual State code(const ::swatch::core::XParameterSet& params);
};

} // namespace ugt
#endif /* ugt_swatch_commands_uGtSmartRebootCommand_h */
