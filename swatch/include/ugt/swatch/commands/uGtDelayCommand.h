#ifndef ugt_swatch_commands_uGtDelayCommand_h
#define ugt_swatch_commands_uGtDelayCommand_h

#include "swatch/action/Command.hpp"


namespace ugt {

/**
 * This class sets the L1A latency delay.
 */
class uGtDelayCommand : public ::swatch::action::Command
{
public:
  uGtDelayCommand(const std::string& aId,
    ::swatch::action::ActionableObject& aActionable);
  virtual ~uGtDelayCommand() = default;
  virtual State code(const ::swatch::core::XParameterSet& params);
};

} // namespace ugt
#endif /* ugt_swatch_commands_uGtDelayCommand_h */
