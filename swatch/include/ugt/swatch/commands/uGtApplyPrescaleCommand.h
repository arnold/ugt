#ifndef ugt_swatch_commands_uGtApplyPrescaleCommand_h
#define ugt_swatch_commands_uGtApplyPrescaleCommand_h

#include "swatch/action/Command.hpp"


namespace ugt {

/**
 * This class activates previously prepared prescales.
 * Prescales loading is performed due to uGtPreparePrescaleCommand.
 */
class uGtApplyPrescaleCommand : public ::swatch::action::Command
{
public:
  uGtApplyPrescaleCommand(const std::string& aId,
                     ::swatch::action::ActionableObject& aActionable);
  virtual ~uGtApplyPrescaleCommand() = default;
  virtual State code(const ::swatch::core::XParameterSet& params);
};

} // namespace ugt
#endif /* ugt_swatch_commands_uGtApplyPrescaleCommand_h */
