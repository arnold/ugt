#ifndef ugt_swatch_commands_uGtVetoMaskCommand_h
#define ugt_swatch_commands_uGtVetoMaskCommand_h

#include "swatch/action/Command.hpp"

namespace ugt {

/**
 * This class implements uGt veto mask command
 */
class uGtVetoMaskCommand : public ::swatch::action::Command
{
public:
  uGtVetoMaskCommand(const std::string& aId,
                    ::swatch::action::ActionableObject& aActionable);
  virtual ~uGtVetoMaskCommand() = default;
  virtual State code(const ::swatch::core::XParameterSet& params);
};

} // namespace ugt
#endif /* ugt_swatch_commands_uGtVetoMaskCommand_h */
