/**
 * @file    AMC502Processor.hpp
 * @author  Gregor Aradi, Bernhard Arnold
 * @brief   AMC502 board processor implementation
 * @date    March 2016
 */

#ifndef AMC502_AMC502PROCESSOR_HPP
#define AMC502_AMC502PROCESSOR_HPP


#include "swatch/mp7/MP7AbstractProcessor.hpp"
#include "swatch/mp7/ChannelDescriptor.hpp"
#include "ugt/swatch/AMC502Controller.h"

namespace amc502 {

class AMC502Processor : public ::swatch::mp7::MP7AbstractProcessor {
public:
  AMC502Processor(const swatch::core::AbstractStub& aStub);
  virtual ~AMC502Processor();

  virtual uint64_t retrieveFirmwareVersion() const;

  virtual std::string firmwareInfo() const;

  virtual AMC502Controller& driver();


private:
  AMC502Controller* mDriver;

  void buildPorts( const swatch::processor::ProcessorStub& aStub );


protected:
  virtual void retrieveMetricValues();

  static const std::string lUploadFwCmdId;

  struct CmdIds {
    static const std::string kUploadFw;
    static const std::string kDeleteFw;
    static const std::string kReboot;
    static const std::string kHardReset;
    static const std::string kScanSD;
    static const std::string kReset;
    static const std::string kPayloadReset;
    static const std::string kZeroInputs;
    static const std::string kSetId;
    static const std::string kCfgRxMGTs;
    static const std::string kCfgTxMGTs;
    static const std::string kAlignMGTs;
    static const std::string kAutoAlignMGTs;
    static const std::string kCfgRxBuffers;
    static const std::string kCfgTxBuffers;
    static const std::string kCaptureBuffers;
    static const std::string kSaveRxBuffers;
    static const std::string kSaveTxBuffers;
    static const std::string kCfgLatencyRxBuffers;
    static const std::string kCfgLatencyTxBuffers;
    static const std::string kCfgEasyRxLatency;
    static const std::string kCfgEasyTxLatency;
    static const std::string kCfgHdrFormatter;
    static const std::string kCfgDataValidFmt;
    static const std::string kSetupReadout;
    static const std::string kLoadReadoutMenu;
    static const std::string kResetCounter;
  };

};

} // namespace amc502


#endif /* AMC502_AMC502PROCESSOR_HPP */
