

#ifndef UGT_SWATCH_UGTFINORCOMMANDS_H
#define UGT_SWATCH_UGTFINORCOMMANDS_H

#include "swatch/action/Command.hpp"

namespace ugt
{


class uGtFinorInputMaskCommand : public ::swatch::action::Command
{
  public:
    uGtFinorInputMaskCommand(const std::string& aId,
                             ::swatch::action::ActionableObject& aActionable);
    virtual ~uGtFinorInputMaskCommand() {};
    virtual State code(const ::swatch::core::XParameterSet& params);
};


} // namespace ugt

#endif /* UGT_SWATCH_UGTFINORCOMMANDS_H */
/* eof */
