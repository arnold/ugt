/**
 * @file    uGtFinorPreviewAlgo.h
 * @author  Gregor Aradi, Bernhard Arnold
 * @brief   Finor preview board algo implementation
 * @date    2016-11-28
 */
#ifndef ugt_swatch_uGtFinorPreviewAlgo_h
#define ugt_swatch_uGtFinorPreviewAlgo_h

#include "ugt/swatch/AMC502Algo.h"

#include "swatch/core/SimpleMetric.hpp"

namespace amc502{
  class AMC502Controller;
}


namespace ugt
{

/**
 * This class implements AM502Algo
 */
class uGtFinorPreviewAlgo : public amc502::AMC502Algo
{
  public:
    uGtFinorPreviewAlgo(amc502::AMC502Controller& driver);
    virtual ~uGtFinorPreviewAlgo();

    uint32_t getBxNumberMax() const;
    //! Returns 64 bit orbit number.
    uint64_t getOrbitNumber() const;
    //! Returns current luminosity segment number.
    uint32_t getLuminositySection() const;


  protected:
    virtual void retrieveMetricValues();

  private:
    amc502::AMC502Controller& driver_;

    ::swatch::core::SimpleMetric <uint32_t>& metricBxNumberMax_;
    ::swatch::core::SimpleMetric <uint32_t>& metricLuminositySegmentNumber_;
    ::swatch::core::SimpleMetric <uint64_t>& metricOrbitCounter_;
    ::swatch::core::SimpleMetric <uint32_t>& metricRateCntFinorPreviewCounter_;
    ::swatch::core::SimpleMetric <double>& metricRateCntFinorPreviewRate_;

    std::vector< ::swatch::core::SimpleMetric <uint32_t>* > vecMetricRateCntLocFinorCounter_;
    std::vector< ::swatch::core::SimpleMetric <double>* > vecMetricRateCntLocFinorRate_;

};

} // namespace ugt


#endif /* ugt_swatch_uGtFinorPreviewAlgo_h */
/* eof */
