/**
 * @author      Takashi Matsushita
 */

#ifndef ugt_swatch_uGtProcessor_h
#define ugt_swatch_uGtProcessor_h

#include "swatch/mp7/MP7Processor.hpp"

namespace ugt
{

/**
 * This class implements MP7Processor for uGt
 */
class uGtProcessor : public ::swatch::mp7::MP7Processor
{
  public:
    uGtProcessor(const ::swatch::core::AbstractStub&);
    virtual ~uGtProcessor();
};

} // namespace ugt

#endif /* ugt_swatch_uGtProcessor_h */
/* eof */
