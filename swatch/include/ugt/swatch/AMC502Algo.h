/**
 * @author      Takashi Matsushita
 */
#ifndef ugt_swatch_AMC502Algo_h
#define ugt_swatch_AMC502Algo_h


#include "swatch/processor/AlgoInterface.hpp"
#include "swatch/core/SimpleMetric.hpp"

#include "xdata/Table.h"

namespace amc502
{

/**
 * This class implements AMC502 AlgoInterface
 */
class AMC502Algo : public ::swatch::processor::AlgoInterface
{
  public:
    AMC502Algo();
    virtual ~AMC502Algo();

  protected:
    virtual void retrieveMetricValues();
};

} // namespace ugt

#endif /* ugt_swatch_AMC502Algo_h */
/* eof */
