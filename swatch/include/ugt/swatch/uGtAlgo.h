/**
 * @author      Takashi Matsushita
 */

#ifndef ugt_swatch_uGtAlgo_h
#define ugt_swatch_uGtAlgo_h


#include "xdata/Table.h"

#include "swatch/processor/AlgoInterface.hpp"
#include "swatch/core/SimpleMetric.hpp"

#include "ugt/swatch/uGtTriggerMenu.h"


namespace mp7 { class MP7Controller; }


namespace ugt
{

extern const std::string ALGO_COUNT;
extern const std::string ALGO_RATE;
extern const std::string ALGO_PREVIEW_COUNT;
extern const std::string ALGO_PREVIEW_RATE;
extern const std::string ALGO_COUNT_BEFORE_PRESCALE;
extern const std::string ALGO_RATE_BEFORE_PRESCALE;
extern const std::string POST_DEADTIME_ALGO_COUNT;
extern const std::string POST_DEADTIME_ALGO_RATE;
extern const std::string PRESCALE;
extern const std::string SEPARATOR;

extern const std::string FINOR_COUNT;
extern const std::string FINOR_RATE;

/**
 * This class implements uGT AlgoInterface
 */
class uGtAlgo : public ::swatch::processor::AlgoInterface
{
  public:
    uGtAlgo(mp7::MP7Controller&);
    virtual ~uGtAlgo();

    //! Set uGT AMC slot number (1..6)
    void setSlotId(const uint32_t id);
    //! Returns module ID (0..5)
    uint32_t getModuleId() const;

    uGtTriggerMenu& getTriggerMenu();

    void setFinorMask(const xdata::Table* p);
    void setPrescales(const xdata::Table* p, const xdata::Table* pMask);
    void setAlgoBxMask(const xdata::Table* p);
    void setVetoMask(const xdata::Table* p);

    //! Returns maximum BX number of last orbit, this should return 3563 (0xdeb).
    uint32_t getBxNumberMax() const;
    //! Returns 64 bit orbit number.
    uint64_t getOrbitNumber() const;
    //! Returns current luminosity segment number.
    uint32_t getLuminositySection() const;
    //!
    double getSecondsBeforeLuminositySegmentUpdate(log4cplus::Logger* logger = 0) const;


  protected:
    virtual void retrieveMetricValues();

  private:
    ::mp7::MP7Controller& driver_;

    ::swatch::core::SimpleMetric <uint32_t>& metricPsIndex_;
    ::swatch::core::SimpleMetric <uint32_t>& metricPsIndexInUse_;
    ::swatch::core::SimpleMetric <uint32_t>& metricPreviewPsIndex_;
    ::swatch::core::SimpleMetric <uint32_t>& metricPreviewPsIndexInUse_;
    ::swatch::core::SimpleMetric <uint32_t>& metricBxNumberMax_;
    ::swatch::core::SimpleMetric <uint32_t>& metricLuminositySegmentNumber_;
    ::swatch::core::SimpleMetric <uint64_t>& metricOrbitCounter_;
    ::swatch::core::SimpleMetric <uint32_t>& metricFinOrCounter_;
    ::swatch::core::SimpleMetric <double>& metricFinOrRate_;
    ::swatch::core::SimpleMetric <uint32_t>& metricVetoCounter_;
    ::swatch::core::SimpleMetric <double>& metricVetoRate_;
    ::swatch::core::SimpleMetric <uint32_t>& metricL1aCounter_;
    ::swatch::core::SimpleMetric <double>& metricL1aRate_;
    ::swatch::core::SimpleMetric <uint32_t>& metricPreviewFinOrCounter_;
    ::swatch::core::SimpleMetric <double>& metricPreviewFinOrRate_;
    ::swatch::core::SimpleMetric <uint32_t>& metricOtf_;
    ::swatch::core::SimpleMetric <uint32_t>& metricOtfInUse_;
    ::swatch::core::SimpleMetric <uint32_t>& metricPreviewOtf_;
    ::swatch::core::SimpleMetric <uint32_t>& metricPreviewOtfInUse_;


    std::vector< ::swatch::core::SimpleMetric <uint32_t>* > metricRateCounters_;
    std::vector< ::swatch::core::SimpleMetric <double>* > metricRates_;
    std::vector< ::swatch::core::SimpleMetric <uint32_t>* > metricPreviewRateCounters_;
    std::vector< ::swatch::core::SimpleMetric <double>* > metricPreviewRates_;
    std::vector< ::swatch::core::SimpleMetric <uint32_t>* > metricRateCountersBeforePrescale_;
    std::vector< ::swatch::core::SimpleMetric <double>* > metricRatesBeforePrescale_;
    std::vector< ::swatch::core::SimpleMetric <uint32_t>* > metricPostDeadtimeRateCounters_;
    std::vector< ::swatch::core::SimpleMetric <double>* > metricPostDeadtimeRates_;
    std::vector< ::swatch::core::SimpleMetric <double>* > metricPrescales_;


    uint32_t slotId_;
    uGtTriggerMenu triggerMenu_;
    const xdata::Table* finorMask_;
    const xdata::Table* prescales_;
    const xdata::Table* algoBxMask_;
    const xdata::Table* vetoMask_;
};

} // namespace ugt

#endif /* ugt_swatch_uGtAlgo_h */
/* eof */
