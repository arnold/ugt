/**
 * @file    uGtExtCondProcessor.h
 * @author  Gregor Aradi, Bernhard Arnold
 * @brief   External Condition board processor implementation
 * @date    2016-02-24
 */
#ifndef UGT_UGTEXTCONDPROCESSOR_H
#define UGT_UGTEXTCONDPROCESSOR_H


#include "ugt/swatch/AMC502Processor.h"

#include "swatch/processor/ProcessorStub.hpp"

#include "uhal/HwInterface.hpp"
#include "uhal/ConnectionManager.hpp"


namespace amc502{
  class AMC502Processor;
}



namespace ugt
{

class uGtExtCondProcessor : public amc502::AMC502Processor
{
  public:

    uGtExtCondProcessor(const ::swatch::core::AbstractStub&);
    virtual ~uGtExtCondProcessor();


};

} // namespace ugt

#endif // UGT_UGTEXTCONDPROCESSOR_H
