#ifndef ugt_swatch_constants_h
#define ugt_swatch_constants_h

#include <cstdlib>
#include <cmath>

namespace ugt
{

extern const size_t kOrbitLength;
extern const size_t kMaxAlgorithms;
extern const size_t kMaxPrescaleSets;
extern const size_t kPrescalePrecision;
extern const size_t kMaxUgtModules;


extern const double kLhcClockFrequency;
extern const unsigned int kOrbitsPerLuminositySegment;
extern const unsigned int kMaximumCounts;
extern const double kLuminositySegmentInSeconds;
extern const double kToRate;

}
#endif // ugt_swatch_constants_h
