#ifndef UGT_TEST_TESTABSTRACTTABLE_H
#define UGT_TEST_TESTABSTRACTTABLE_H


#include <boost/pointer_cast.hpp>
#include "boost/algorithm/string/predicate.hpp"

#include "xdata/Table.h"

#include "ugt/swatch/toolbox/AbstractTable.h"
#include "ugt/swatch/uGtTriggerMenu.h"

#include <string>
#include <vector>
#include <map>
#include <utility>
#include <sstream>


namespace ugt {
namespace test {

class TestAbstractTable: public ugt::toolbox::AbstractTable {
public:
  TestAbstractTable(std::string tableType) { mTableType = tableType; };
  virtual ~TestAbstractTable() {};

  virtual void verify(const xdata::Table& table) {};
  virtual void verify(const xdata::Table& table, const ugt::uGtTriggerMenu& menu) {};
};

} // end namespace test
} // end namespace ugt

#endif /* UGT_TEST_TESTABSTRACTTABLE_H */
