#ifndef UGT_TEST_GATEKEEPERSETUP_HXX
#define UGT_TEST_GATEKEEPERSETUP_HXX

#include <string>
#include <vector>
#include <map>
#include <utility>
#include <sstream>

#include <boost/pointer_cast.hpp>
#include "boost/algorithm/string/predicate.hpp"

#include "xdata/Table.h"

#include "swatch/action/GateKeeper.hpp"
#include "swatch/xml/XmlGateKeeper.hpp"

#include "ugt/swatch/uGtTriggerMenu.h"


namespace ugt {
namespace test {

typedef std::map<std::string, bool> TestParameterList_t;

const std::string kBaseConfigFile = "config/test_ugt_base_key.xml";

const std::string kPrefixPass = "Pass_";
const std::string kPrefixFail = "Fail_";
const std::string kPostfixEmptyCell = "EmptyCell";

struct GateKeeperSetup
{
  std::vector<std::string> contextsToLookIn;
  swatch::xml::XmlGateKeeper* gk;
  uGtTriggerMenu menu;
  uGtTriggerMenu menuEmpty;

  GateKeeperSetup()
  {
    std::string lTestFile(kBaseConfigFile);
    gk = new swatch::xml::XmlGateKeeper(lTestFile, "RunKeyTest");

    contextsToLookIn.push_back("uGT.processors");
    contextsToLookIn.push_back("uGT.uGtProcessor");

    { // setup a working l1 menu
      swatch::action::GateKeeper::Parameter_t paramMenu = gk->get("", "", "l1menu", contextsToLookIn);
      const xdata::Table* xtable = boost::dynamic_pointer_cast<const xdata::Table>(boost::get_pointer(paramMenu));
      menu.setTriggerMenu(*xtable);
    }
    { // setup an empty l1 menu
      swatch::action::GateKeeper::Parameter_t paramMenu = gk->get("", "", "l1menuEmpty", contextsToLookIn);
      const xdata::Table* xtable = boost::dynamic_pointer_cast<const xdata::Table>(boost::get_pointer(paramMenu));
      menuEmpty.setTriggerMenu(*xtable);
    }
  }

  ~GateKeeperSetup()
  {
    delete(gk);
    gk = NULL;
  }

  /** Return a xdata table pointer addressed by it parameter in the GateKeeper */
  const xdata::Table*
  getXTable(std::string param)
  {
    swatch::action::GateKeeper::Parameter_t parameter = gk->get("", "", param, contextsToLookIn);
    return boost::dynamic_pointer_cast<const xdata::Table>(boost::get_pointer(parameter));
  }

  /** Return an empty L1 menu */
  const uGtTriggerMenu&
  getTriggerMenuEmpty()
  {
    return menuEmpty;
  }

  /** Return a working L1 menu */
  const uGtTriggerMenu&
  getTriggerMenu()
  {
    return menu;
  }

  /** Return TestParameterList_t for testing rules, containing all
      passing and failing test case tables with the 'tableParam' inside a parameter in the GateKeeper.
      E.g.: tableParam = finorMask ==> Fail_finorMaskDefAlgoMissing */
  TestParameterList_t
  getParamsRuleList(const std::string& tableParam)
  {
    // set up the fail/pass parameters for tableParam
    std::ostringstream ssPassTableParam;
    std::ostringstream ssFailTableParam;
    ssPassTableParam << kPrefixPass << tableParam;
    ssFailTableParam << kPrefixFail << tableParam;

    TestParameterList_t list;
    for(auto it = gk->parametersBegin(); it != gk->parametersEnd(); it++)
    {
      swatch::action::GateKeeper::ParametersContext_t paramsContext = it->second;
      swatch::action::GateKeeper::Parameters_t params = *paramsContext;
      for(auto cit = params.begin(); cit != params.end(); cit++)
      {
        std::string sParam = cit->first;
        if (boost::starts_with(sParam, ssFailTableParam.str()))
        {
          // collect cases that fail
          list.insert(std::pair<std::string,bool>(sParam, false));
        }
        else if (boost::starts_with(sParam, ssPassTableParam.str()))
        {
          // collect cases that pass
          list.insert(std::pair<std::string,bool>(sParam, true));
        }
      }
    } // end loop parameters

    return list;
  }

  /** Return TestParameterList_t for testing tables, containing all
      passing and failing test case tables with the 'tableParam' inside a
      parameter in the GateKeeper.
      Excluded is the test case using EmptyCell rule (Postfix: _EmptyCell) which
      is only used in the rule test case
      E.g.: tableParam = finorMask ==> Fail_finorMaskDefAlgoMissing */
  TestParameterList_t
  getParamsList(const std::string& tableParam)
  {
    TestParameterList_t paramsList = getParamsRuleList(tableParam);

    // remove table parameter for checking empty cells
    std::string paramEmptyCell = buildEmptyCellString(tableParam);
    paramsList.erase(paramEmptyCell);

    return paramsList;
  }

  /** Returns the constructed EmptyCell parameter from a 'tableParam' */
  std::string
  buildEmptyCellString(const std::string& tableParam) {
    std::ostringstream ssParam;
    ssParam << kPrefixFail << tableParam << kPostfixEmptyCell;
    return ssParam.str();
  }
};


} // end namespace test
} // end namespace ugt

#endif /* UGT_TEST_GATEKEEPERSETUP_HXX */
