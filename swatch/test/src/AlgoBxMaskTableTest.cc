#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE ugtswatchtest

#include <boost/test/unit_test.hpp>

#include "xdata/Table.h"

#include "swatch/core/XRule.hpp"
#include "ugt/swatch/rules/AlgoBxMaskRule.h"
#include "ugt/swatch/toolbox/AlgoBXMaskTable.h"

#include <string>
#include <ostream>

namespace ugt {
namespace test {

// ----------------------------------------------------------------------------
//  unittests for ugt swatch functions
// ----------------------------------------------------------------------------



BOOST_FIXTURE_TEST_SUITE(algoBxMaskTableTestSuite, GateKeeperSetup)


BOOST_AUTO_TEST_CASE(algoBxMaskTableVerify)
{
  std::ostringstream msg;
  GateKeeperSetup lGKS;
  TestParameterList_t lParams = lGKS.getParamsList("algorithmBxMask");

  ::ugt::toolbox::AlgoBXMaskTable container;


  uint32_t num = 1;
  for (const auto &it : lParams)
  {
    std::ostringstream message;
    message << "Starting verify table test case " << num++ << " " << it.first;
    BOOST_TEST_MESSAGE(message.str());

    const xdata::Table* xAlgoBxMask = lGKS.getXTable(it.first);

    if (not it.second)
    {
      BOOST_CHECK_THROW(container.verify(*xAlgoBxMask),
                      toolbox::AlgoBXMaskTableException);
      try
      {
        container.verify(*xAlgoBxMask);
      }
      catch (toolbox::AlgoBXMaskTableException& e)
      {
        BOOST_TEST_MESSAGE( "what: " << e.what());
      }
    }
    else
    {
      BOOST_REQUIRE_NO_THROW(container.verify(*xAlgoBxMask));
    }

    message.str(""); message.clear();
    message << "Done verify table test case: " << it.first << " " << std::endl;
    BOOST_TEST_MESSAGE(message.str());

  } // end loop lParams
} // end algoBxMaskTableVerify

BOOST_AUTO_TEST_CASE(algoBxMaskTableVerifyWithMenu)
{
  std::ostringstream msg;
  GateKeeperSetup lGKS;
  TestParameterList_t lParams = lGKS.getParamsList("algorithmBxMask");

  ::ugt::toolbox::AlgoBXMaskTable container;


  uint32_t num = 1;
  for (const auto &it : lParams)
  {
    std::ostringstream message;
    message << "Starting verifyWithMenu table test case " << num++ << " " << it.first;
    BOOST_TEST_MESSAGE(message.str());

    const xdata::Table* xAlgoBxMask = lGKS.getXTable(it.first);

    if (not it.second)
    {
      BOOST_CHECK_THROW(container.verify(*xAlgoBxMask, lGKS.getTriggerMenu()),
                      toolbox::AlgoBXMaskTableException);
      try
      {
        container.verify(*xAlgoBxMask, lGKS.getTriggerMenu());
      }
      catch (toolbox::AlgoBXMaskTableException& e)
      {
        BOOST_TEST_MESSAGE( "what: " << e.what());
      }
    }
    else
    {
      BOOST_REQUIRE_NO_THROW(container.verify(*xAlgoBxMask, lGKS.getTriggerMenu()));
    }

    message.str(""); message.clear();
    message << "Done verifyWithMenu table test case: " << it.first << " " << std::endl;
    BOOST_TEST_MESSAGE(message.str());

  } // end loop lParams
} // end algoBxMaskTableVerifyWithMenu


BOOST_AUTO_TEST_CASE(algoBxMaskTableLoad)
{
  std::ostringstream msg;
  GateKeeperSetup lGKS;
  TestParameterList_t lParams = lGKS.getParamsList("algorithmBxMask");

  ::ugt::toolbox::AlgoBXMaskTable container;


  uint32_t num = 1;
  for (const auto &it : lParams)
  {
    std::ostringstream message;
    message << "Starting load table test case " << num++ << " " << it.first;
    BOOST_TEST_MESSAGE(message.str());

    const xdata::Table* xAlgoBxMask = lGKS.getXTable(it.first);

    if (not it.second)
    {
      BOOST_CHECK_THROW(container.load(*xAlgoBxMask, lGKS.getTriggerMenu()),
                      toolbox::AlgoBXMaskTableException);
      try
      {
        container.load(*xAlgoBxMask, lGKS.getTriggerMenu());
      }
      catch (toolbox::AlgoBXMaskTableException& e)
      {
        BOOST_TEST_MESSAGE( "what: " << e.what());
      }
    }
    else
    {
      BOOST_REQUIRE_NO_THROW(container.load(*xAlgoBxMask, lGKS.getTriggerMenu()));
    }

    message.str(""); message.clear();
    message << "Done load table test case: " << it.first << " " << std::endl;
    BOOST_TEST_MESSAGE(message.str());

  } // end loop lParams
} // end algoBxMaskTableLoad

BOOST_AUTO_TEST_CASE(GetParsedRangedListTest)
{
  toolbox::AlgoBXMaskTable container;
  std::vector<uint32_t> vec;

  vec = container.getParsedRangeList("1");
  BOOST_CHECK_EQUAL(vec.size(), 1);
  BOOST_CHECK_EQUAL(*(vec.begin()), 1);

  vec.clear();
  vec = container.getParsedRangeList("");
  BOOST_CHECK_EQUAL(vec.size(), 0);

  vec.clear();
  vec = container.getParsedRangeList("5-6");
  BOOST_CHECK_EQUAL(vec.size(), 2);

  vec.clear();
  vec = container.getParsedRangeList("10-70");
  BOOST_CHECK_EQUAL(vec.size(), 61);

  vec.clear();
  vec = container.getParsedRangeList("1-3564");
  BOOST_CHECK_EQUAL(vec.size(), 3564);

  vec.clear();
  vec = container.getParsedRangeList("ALL");
  BOOST_CHECK_EQUAL(vec.size(), 3564);
} // end GetParsedRangedListTest

BOOST_AUTO_TEST_CASE(algoBxMaskRuleTests)
{
  GateKeeperSetup lGKS;
  TestParameterList_t lParams = lGKS.getParamsList("algorithmBxMask");

  ::ugt::rules::AlgoBxMaskRule rule;

  uint32_t num = 1;
  for (const auto &it : lParams)
  {
    std::ostringstream msg;
    msg << "Starting rule test case " << num++ << " " << it.first;
    BOOST_TEST_MESSAGE(msg.str());

    const xdata::Table* xAlgoBxMask = lGKS.getXTable(it.first);
    swatch::core::XMatch match = rule(*xAlgoBxMask);
    BOOST_CHECK_EQUAL(match.ok, it.second);

    msg.str(""); msg.clear();
    msg << "Done rule test case: " << it.first << " " << std::endl
        << " details: " << match.details << std::endl;
    BOOST_TEST_MESSAGE(msg.str());
  } // end loop
} // end algoBxMaskRuleTests

BOOST_AUTO_TEST_SUITE_END()

} // end namespace test
} // end namespace ugt
