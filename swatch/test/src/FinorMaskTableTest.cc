#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE ugtswatchtest

#include <boost/test/unit_test.hpp>

#include "xdata/Table.h"
#include "ugt/swatch/rules/FinorMaskRule.h"
#include "ugt/swatch/toolbox/MaskTable.h"

#include <string>
#include <ostream>

namespace ugt {
namespace test {

// ----------------------------------------------------------------------------
//  unittests for ugt swatch functions
// ----------------------------------------------------------------------------


BOOST_FIXTURE_TEST_SUITE(finorMaskTableTestSuite, GateKeeperSetup)

BOOST_AUTO_TEST_CASE(finorMaskTableVerify)
{
  GateKeeperSetup lGKS;
  TestParameterList_t lParams = lGKS.getParamsList("finorMask");

  toolbox::FinorMaskTable container;

  uint32_t num = 1;
  for (const auto &it : lParams)
  {
    std::ostringstream message;
    message << "Starting verify table test case " << num++ << " " << it.first;
    BOOST_TEST_MESSAGE(message.str());

    const xdata::Table* xFinorTable = lGKS.getXTable(it.first);
    if (not it.second)
    {
      BOOST_CHECK_THROW(container.verify(*xFinorTable),
                  toolbox::MaskTableException);
      try
      {
        container.verify(*xFinorTable);
      }
      catch (toolbox::MaskTableException& e)
      {
        BOOST_TEST_MESSAGE("what: " << e.what());
      }
    }
    else
    {
      BOOST_REQUIRE_NO_THROW(container.verify(*xFinorTable));
    }

    message.str(""); message.clear();
    message << "Done table test case: " << it.first << " " << std::endl;
    BOOST_TEST_MESSAGE(message.str());
  }
} // end finorMaskTableVerify

BOOST_AUTO_TEST_CASE(finorMaskTableVerifyWithMenu)
{
  GateKeeperSetup lGKS;
  TestParameterList_t lParams = lGKS.getParamsList("finorMask");

  toolbox::FinorMaskTable container;

  uint32_t num = 1;
  for (const auto &it : lParams)
  {
    std::ostringstream message;
    message << "Starting verifyWithMenu table test case " << num++ << " " << it.first;
    BOOST_TEST_MESSAGE(message.str());

    const xdata::Table* xFinorTable = lGKS.getXTable(it.first);
    if (not it.second)
    {
      BOOST_CHECK_THROW(container.verify(*xFinorTable,
                  lGKS.getTriggerMenu()),
                  toolbox::MaskTableException);
      try
      {
        container.verify(*xFinorTable, lGKS.getTriggerMenu());
      }
      catch (toolbox::MaskTableException& e)
      {
        BOOST_TEST_MESSAGE("what: " << e.what());
      }
    }
    else
    {
      BOOST_REQUIRE_NO_THROW(container.verify(*xFinorTable, lGKS.getTriggerMenu()));
    }

    message.str(""); message.clear();
    message << "Done verifyWithMenu table test case: " << it.first << " " << std::endl;
    BOOST_TEST_MESSAGE(message.str());
  }
} // end finorMaskTableVerifyWithMenu

BOOST_AUTO_TEST_CASE(finorMaskTableLoad)
{
  GateKeeperSetup lGKS;
  TestParameterList_t lParams = lGKS.getParamsList("finorMask");

  toolbox::FinorMaskTable container;

  uint32_t num = 1;
  for (const auto &it : lParams)
  {
    std::ostringstream message;
    message << "Starting load table test case " << num++ << " " << it.first;
    BOOST_TEST_MESSAGE(message.str());

    const xdata::Table* xFinorTable = lGKS.getXTable(it.first);
    if (not it.second)
    {
      BOOST_CHECK_THROW(container.load(*xFinorTable, lGKS.getTriggerMenu()),
                  toolbox::MaskTableException);
      try
      {
        container.load(*xFinorTable, lGKS.getTriggerMenu());
      }
      catch (toolbox::MaskTableException& e)
      {
        BOOST_TEST_MESSAGE("what: " << e.what());
      }
    }
    else
    {
      BOOST_REQUIRE_NO_THROW(container.load(*xFinorTable, lGKS.getTriggerMenu()));
    }

    message.str(""); message.clear();
    message << "Done load table test case: " << it.first << " " << std::endl;
    BOOST_TEST_MESSAGE(message.str());
  }
} // end finorMaskTableLoad

BOOST_AUTO_TEST_CASE(finorMaskRuleTests)
{
  GateKeeperSetup lGKS;
  TestParameterList_t lParams = lGKS.getParamsRuleList("finorMask");

  ::ugt::rules::FinorMaskRule rule;

  uint32_t num = 1;
  for (const auto &it : lParams)
  {
    std::ostringstream msg;
    msg << "Starting rule test case " << num++ << " " << it.first;
    BOOST_TEST_MESSAGE(msg.str());

    const xdata::Table* xFinorTable = lGKS.getXTable(it.first);
    swatch::core::XMatch match = rule(*xFinorTable);
    BOOST_CHECK_EQUAL(match.ok, it.second);

    msg.str(""); msg.clear();
    msg << "Done rule test case: " << it.first << " " << std::endl
        << " details: " << match.details << std::endl;
    BOOST_TEST_MESSAGE(msg.str());
  }
} // finorMaskRuleTests

BOOST_AUTO_TEST_SUITE_END()

} // end namespace test
} // end namespace ugt
