#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE ugtswatchtest

#include "TestAbstractTable.h"
#include "ugt/swatch/toolbox/AbstractTable.h"

#include "xdata/Table.h"

#include <boost/test/unit_test.hpp>

#include <string>
#include <ostream>

namespace ugt {
namespace test {

// ----------------------------------------------------------------------------
//  unittests for ugt swatch functions
// ----------------------------------------------------------------------------

const std::string kTableType = "TestAbstractTable";


BOOST_AUTO_TEST_SUITE(AbstractTableTestSuite)


BOOST_AUTO_TEST_CASE(abstractTableCommon)
{
  TestAbstractTable table(kTableType);

  // check table type
  BOOST_CHECK_EQUAL(table.getTableType(), kTableType);

  // check isDefaultAlgo
  BOOST_CHECK_EQUAL(table.isDefaultAlgo(ugt::toolbox::kDefaultAlgo), true);
  BOOST_CHECK_EQUAL(table.isDefaultAlgo("L1_ZeroBias"), false);

  // check menu size
  BOOST_CHECK_THROW(table.checkMenuSizeValid(0), toolbox::AbstractTableException);
  BOOST_REQUIRE_NO_THROW(table.checkMenuSizeValid(1));



  // check row size
  BOOST_CHECK_THROW(table.checkRowsSizeValid(0), toolbox::AbstractTableException);
  BOOST_REQUIRE_NO_THROW(table.checkRowsSizeValid(1));

  // check default table
  BOOST_CHECK_EQUAL(table.isDefaultTable(0, 0), true);
  BOOST_CHECK_EQUAL(table.isDefaultTable(1, 0), false);
  BOOST_CHECK_EQUAL(table.isDefaultTable(0, 1), false);
  BOOST_CHECK_EQUAL(table.isDefaultTable(1, 1), false);

} // end abstractTableCommon

BOOST_AUTO_TEST_CASE(abstractTableColumns)
{
  TestAbstractTable table(kTableType);
  std::vector<std::string> columns;
  columns.push_back("Col1");
  std::vector<std::string> columnExpected(columns);
  columnExpected.push_back("Col2");

  // columns contain one less than expected
  BOOST_CHECK_THROW(table.checkColumnsValid(columns, columnExpected), toolbox::AbstractTableException);
  try
  {
    table.checkColumnsValid(columns, columnExpected);
  }
  catch (toolbox::AbstractTableException& e)
  {
    BOOST_TEST_MESSAGE("what: " << e.what());
  }


  // pass columns and expected equal
  columns.push_back("Col2");
  BOOST_REQUIRE_NO_THROW(table.checkColumnsValid(columns, columnExpected));


  // columns contain one more than expected
  columns.push_back("Col3");
  BOOST_CHECK_THROW(table.checkColumnsValid(columns, columnExpected), toolbox::AbstractTableException);
  try
  {
    table.checkColumnsValid(columns, columnExpected);
  }
  catch (toolbox::AbstractTableException& e)
  {
    BOOST_TEST_MESSAGE("what: " << e.what());
  }

  // equal columns, but different content
  columns.clear();
  columns.push_back("A1");
  columns.push_back("A2");
  BOOST_CHECK_THROW(table.checkColumnsValid(columns, columnExpected), toolbox::AbstractTableException);
  try
  {
    table.checkColumnsValid(columns, columnExpected);
  }
  catch (toolbox::AbstractTableException& e)
  {
    BOOST_TEST_MESSAGE("what: " << e.what());
  }
} // end abstractTableColumns


BOOST_AUTO_TEST_CASE(abstractTableGenericCheckError)
{
  TestAbstractTable table(kTableType);

  const std::string single = "Single Error message";
  const std::string multi = "Multi Error message";
  std::vector<uint32_t> vec;
  std::set<std::string> s;

  // no error
  BOOST_REQUIRE_NO_THROW(table.genericCheckErrors(vec, single, multi));
  BOOST_REQUIRE_NO_THROW(table.genericCheckErrors(s, single, multi));

  // error with single message
  vec.push_back(1);
  s.insert("A");
  BOOST_CHECK_THROW(table.genericCheckErrors(vec, single, multi), toolbox::AbstractTableException);
  BOOST_CHECK_THROW(table.genericCheckErrors(s, single, multi), toolbox::AbstractTableException);

  // error with multi message
  vec.push_back(6);
  vec.push_back(32);
  s.insert("S");
  s.insert("H");
  BOOST_CHECK_THROW(table.genericCheckErrors(vec, single, multi), toolbox::AbstractTableException);
  BOOST_CHECK_THROW(table.genericCheckErrors(s, single, multi), toolbox::AbstractTableException);

} // end abstractTableGenericCheckError


BOOST_AUTO_TEST_SUITE_END()

} // end namespace test
} // end namespace ugt
