#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE ugtswatchtest

#include <boost/test/unit_test.hpp>

#include "xdata/Table.h"
#include "ugt/swatch/rules/VetoMaskRule.h"
#include "ugt/swatch/toolbox/MaskTable.h"

#include <string>
#include <ostream>

namespace ugt {
namespace test {

// ----------------------------------------------------------------------------
//  unittests for ugt swatch functions
// ----------------------------------------------------------------------------


BOOST_FIXTURE_TEST_SUITE(vetoMaskTableTestSuite, GateKeeperSetup)


BOOST_AUTO_TEST_CASE(vetoMaskTests)
{
  GateKeeperSetup lGKS;
  TestParameterList_t lParams = lGKS.getParamsList("vetoMask");

  toolbox::VetoMaskTable container;

  uint32_t num = 1;
  for (const auto &it : lParams)
  {
    std::ostringstream message;
    message << "Starting table test case " << num++ << " " << it.first;
    BOOST_TEST_MESSAGE(message.str());

    const xdata::Table* xVetoTable = lGKS.getXTable(it.first);
    if (not it.second)
    {
      BOOST_CHECK_THROW(container.verify(*xVetoTable),
                  toolbox::MaskTableException);
      BOOST_CHECK_THROW(container.verify(*xVetoTable, lGKS.getTriggerMenu()),
                  toolbox::MaskTableException);
      BOOST_CHECK_THROW(container.load(*xVetoTable, lGKS.getTriggerMenu()),
                  toolbox::MaskTableException);
      try
      {
        container.load(*xVetoTable, lGKS.getTriggerMenu());
      }
      catch (toolbox::MaskTableException& e)
      {
        BOOST_TEST_MESSAGE("what: " << e.what());
      }
    }
    else
    {
      BOOST_REQUIRE_NO_THROW(container.verify(*xVetoTable));
      BOOST_REQUIRE_NO_THROW(container.verify(*xVetoTable, lGKS.getTriggerMenu()));
      BOOST_REQUIRE_NO_THROW(container.load(*xVetoTable, lGKS.getTriggerMenu()));
      BOOST_CHECK_EQUAL(container.size(), lGKS.getTriggerMenu().size());
      // container.print();
    }

    message.str(""); message.clear();
    message << "Done table test case: " << it.first << " " << std::endl;
    BOOST_TEST_MESSAGE(message.str());
  }
}


BOOST_AUTO_TEST_CASE(vetoMaskRuleTests)
{
  GateKeeperSetup lGKS;
  TestParameterList_t lParams = lGKS.getParamsRuleList("vetoMask");

  ::ugt::rules::VetoMaskRule rule;

  uint32_t num = 1;
  for (const auto &it : lParams)
  {
    std::ostringstream msg;
    msg << "Starting rule test case " << num++ << " " << it.first;
    BOOST_TEST_MESSAGE(msg.str());

    const xdata::Table* xVetoTable = lGKS.getXTable(it.first);
    swatch::core::XMatch match = rule(*xVetoTable);
    BOOST_CHECK_EQUAL(match.ok, it.second);

    msg.str(""); msg.clear();
    msg << "Done rule test case: " << it.first << " " << std::endl
        << " details: " << match.details << std::endl;
    BOOST_TEST_MESSAGE(msg.str());
  }
}


BOOST_AUTO_TEST_SUITE_END()

} // end namespace test
} // end namespace ugt
