#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE ugtswatchtest

#include <boost/test/unit_test.hpp>
#include <boost/pointer_cast.hpp>

#include "swatch/action/GateKeeper.hpp"

#include "xdata/Table.h"
#include "swatch/core/XRule.hpp"

#include "ugt/swatch/rules/PrescaleRule.h"
#include "ugt/swatch/toolbox/PrescaleTable.h"
#include "ugt/swatch/toolbox/MaskTable.h"

#include <string>
#include <ostream>

namespace ugt {
namespace test {

// ----------------------------------------------------------------------------
//  unittests for ugt swatch functions
// ----------------------------------------------------------------------------

const std::string kParamFinorMask = "Pass_finorMask";

BOOST_FIXTURE_TEST_SUITE(prescaleTableTestSuite, GateKeeperSetup)


BOOST_AUTO_TEST_CASE(prescaleTableVerify)
{
  GateKeeperSetup lGKS;
  TestParameterList_t lParams = lGKS.getParamsList("prescale");

  toolbox::PrescaleTable container;

  uint32_t num = 1;
  for (const auto &it : lParams)
  {
    std::ostringstream message;
    message << "Starting verify table test case " << num++ << " " << it.first;
    BOOST_TEST_MESSAGE(message.str());

    const xdata::Table* xPrescaleTable = lGKS.getXTable(it.first);
    if (not it.second)
    {
      BOOST_CHECK_THROW(container.verify(*xPrescaleTable),
                  toolbox::PrescaleTableException);

      try
      {
        container.verify(*xPrescaleTable);
      }
      catch (toolbox::PrescaleTableException& e)
      {
        BOOST_TEST_MESSAGE("what: " << e.what());
      }
    }
    else
    {
      BOOST_REQUIRE_NO_THROW(container.verify(*xPrescaleTable));
    }

    message.str(""); message.clear();
    message << "Done verify table test case: " << it.first << " " << std::endl;
    BOOST_TEST_MESSAGE(message.str());
  }
} // end prescaleTableVerify

BOOST_AUTO_TEST_CASE(prescaleTableVerifyWithMenu)
{
  GateKeeperSetup lGKS;
  TestParameterList_t lParams = lGKS.getParamsList("prescale");

  toolbox::PrescaleTable container;

  uint32_t num = 1;
  for (const auto &it : lParams)
  {
    std::ostringstream message;
    message << "Starting verifyWithMenu table test case " << num++ << " " << it.first;
    BOOST_TEST_MESSAGE(message.str());

    const xdata::Table* xPrescaleTable = lGKS.getXTable(it.first);
    if (not it.second)
    {
      BOOST_CHECK_THROW(container.verify(*xPrescaleTable,
                  lGKS.getTriggerMenu()),
                  toolbox::PrescaleTableException);
      try
      {
        container.verify(*xPrescaleTable, lGKS.getTriggerMenu());
      }
      catch (toolbox::PrescaleTableException& e)
      {
        BOOST_TEST_MESSAGE("what: " << e.what());
      }
    }
    else
    {
      BOOST_REQUIRE_NO_THROW(container.verify(*xPrescaleTable, lGKS.getTriggerMenu()));
    }

    message.str(""); message.clear();
    message << "Done verifyWithMenu table test case: " << it.first << " " << std::endl;
    BOOST_TEST_MESSAGE(message.str());
  }
} // end prescaleTableVerifyWithMenu

BOOST_AUTO_TEST_CASE(prescaleTableLoad)
{
  GateKeeperSetup lGKS;
  TestParameterList_t lParams = lGKS.getParamsList("prescale");
  const xdata::Table* xFinorTable = lGKS.getXTable(kParamFinorMask);

  toolbox::PrescaleTable container;

  uint32_t num = 1;
  for (const auto &it : lParams)
  {
    std::ostringstream message;
    message << "Starting load table test case " << num++ << " " << it.first;
    BOOST_TEST_MESSAGE(message.str());

    const xdata::Table* xPrescaleTable = lGKS.getXTable(it.first);
    if (not it.second)
    {
      BOOST_CHECK_THROW(container.load(*xPrescaleTable,
                  *xFinorTable, lGKS.getTriggerMenu()),
                  toolbox::PrescaleTableException);
      try
      {
        container.load(*xPrescaleTable,
            *xFinorTable, lGKS.getTriggerMenu());
      }
      catch (toolbox::PrescaleTableException& e)
      {
        BOOST_TEST_MESSAGE("what: " << e.what());
      }
    }
    else
    {
      BOOST_REQUIRE_NO_THROW(container.load(*xPrescaleTable, *xFinorTable, lGKS.getTriggerMenu()));
    }

    message.str(""); message.clear();
    message << "Done load table test case: " << it.first << " " << std::endl;
    BOOST_TEST_MESSAGE(message.str());
  }
} // end prescaleTableLoad

BOOST_AUTO_TEST_CASE(prescaleRuleTests)
{
  GateKeeperSetup lGKS;
  TestParameterList_t lParams = lGKS.getParamsRuleList("prescale");

  ::ugt::rules::PrescaleRule rule;

  uint32_t num = 1;
  for (const auto &it : lParams)
  {
    std::ostringstream msg;
    msg << "Starting rule test case " << num++ << " " << it.first;
    BOOST_TEST_MESSAGE(msg.str());

    const xdata::Table* xPrescaleTable = lGKS.getXTable(it.first);
    swatch::core::XMatch match = rule(*xPrescaleTable);
    BOOST_CHECK_EQUAL(match.ok, it.second);

    msg.str(""); msg.clear();
    msg << "Done rule test case: " << it.first << " " << std::endl
        << " details: " << match.details << std::endl;
    BOOST_TEST_MESSAGE(msg.str());
  }
} // prescaleRuleTests

BOOST_AUTO_TEST_SUITE_END()

} // end namespace test
} // end namespace ugt
