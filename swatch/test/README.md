Unit Tests for uGT SWATCH Tables
================================

Tables (AlgoBXMaskTable, FinorMaskTable, VetoMaskTable, PrescaleTable) are tested with a separate test file and corresponding config file (.xml).

The test file (e.g.: FinorMaskTableTest) checks methods in the source table file (e.g.: FinorMaskTable). Additionally, each corresponding table rule (e.g.: FinorMaskRule) will be checked as well inside the test file.

Test cases are prepared in a configuration file (e.g.: test_finor_mask.xml) and a specific 'param' tag format that indicates whether to pass or to fail the test
and which table should be checked.

**'param' tag format:**
  * <"Pass_" | "Fail_"><table><meaningfulPostfix>

**Example for finor mask:**
  * Fail_finorMaskColumnOne - table causes the test case to fail
  * Pass_finorMask - table causes the test case to pass

**N.B.:** The postfix EmptyCell is only used in rule test cases.


## Folder structure

  * config: Configuration for a unit test, containing tables for passing/failing the tests
  * include: Include files
  * src: Source files
  * log4cplus.properties: Properties file
  * Makefile: Build source files
  * setup.sh: Setup the LD_LIBRARY_PATH to the compiled uGT SWATCH library


## Build unit tests

  $ source setup.sh
  $ make


## Run unit tests

Run all unit tests
  $ ./unitTest

Run a specific test suite
  $ ./unitTest -t finorMaskTableVerify

Run a test suite with additional messages (i.e, print thrown exception messages)
  $ ./unitTest --log_level=message


## Add a new test case for a table

  * Open a configuration file for a table
  * Add a new 'param' tag
    * Determine a passing/failing test case
    * Add the table name to test
    * Add a meaningful postfix for the test case
    * Add a comment describing the test case
  * Rebuild unit tests using the Makefile
  * Run unit tests
  * Verify the correctness of your test case


**Example:** Add a passing test case to FinorMaskTableTest
  * Open config/test_finor_mask.xml
  * Add a new 'param' tag and comment
    <!-- VALID: This test case will pass -->
    <param id="Pass_finorMaskWithCorrectTable" type="table">
      <columns>algo, mask</columns>
      <types>string, uint</types>
      <rows>
        <row>ALL, 1</row>
        <!-- Add rows that will pass the test case -->
      </rows>
    </param>
  * Rebuild, Run and Check your test case
