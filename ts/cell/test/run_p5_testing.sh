#!/bin/bash

OS_PLATFORM=x86_64_centos7
ROOT_DIR=$(pwd)/../../..
export swatchugt_ROOT=${ROOT_DIR}/ts/cell
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ROOT_DIR/swatch/lib/linux/${OS_PLATFORM}

# Set environment variable for default CellConfig folder (w/ address tables) if not available
if [ ! -n "$TDF_CELLCONFIG" ]; then
    export TDF_CELLCONFIG="$HOME/CellConfig"
fi

echo "using CellConfig path: $TDF_CELLCONFIG"

# Get configuration key, else use default
if [ $# -lt 1 ]
then
    CONFIG_KEY=p5_testing_v1
else
    CONFIG_KEY=$1
fi

echo "using configuration: $CONFIG_KEY"

export SUBSYSTEM_ID=ugt
export SUBSYSTEM_CELL_CLASS=ugt::Cell
export SUBSYSTEM_CELL_LIB_PATH="
    ${ROOT_DIR}/ts/cell/lib/linux/${OS_PLATFORM}/libugtTsCell.so;
    ${ROOT_DIR}/swatch/lib/linux/${OS_PLATFORM}/libugtswatch.so;
    ${XDAQ_ROOT}/lib/libtmutil.so;
    ${XDAQ_ROOT}/lib/libtmxsd.so;
    ${XDAQ_ROOT}/lib/libtmtable.so;
    ${XDAQ_ROOT}/lib/libtmgrammar.so;
    ${XDAQ_ROOT}/lib/libtmeventsetup.so;"
export SWATCH_DEFAULT_INIT_FILE=${ROOT_DIR}/ts/cell/test/config/$CONFIG_KEY/hw.xml
export SWATCH_DEFAULT_GATEKEEPER_XML=${ROOT_DIR}/ts/cell/test/config/$CONFIG_KEY/ugt_base_key.xml
export SWATCH_DEFAULT_GATEKEEPER_KEY=RunKeyTest

/opt/cactus/bin/swatchcell/runSubsystemCell.sh $@
RESULT=$?

# Remove temporary directory (not to enterfere with other users).
TMP_DIR=/tmp/${SUBSYSTEM_ID}cell
echo "removing temporary directory ${TMP_DIR} ..."
rm -rf $TMP_DIR

echo "done."
exit $RESULT
