import xml.etree.ElementTree as etree
import xml.dom.minidom
import argparse
import csv
import sys, os

AutoDetectParamId = {
    'algo': 'finorMask',
    'bx/algo': 'algorithmBxMask',
}
"""Mapping first CSV header entry signature to param ID."""

if __name__ == '__main__':
    # Cmdline arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('filename', help="CSV file to convert")
    parser.add_argument('--param-id', default=None, help="param id, default is auto detect")
    parser.add_argument('--context-id', default="uGtProcessor", help="context id, default is 'uGtProcessors'")
    parser.add_argument('-o', nargs='?', type=argparse.FileType('w'), default=sys.stdout)
    args = parser.parse_args()

    # Build tree
    algo = etree.Element("run-settings", id="uGT")
    context = etree.SubElement(algo, "context", id=args.context_id)

    # Read CSV data
    with open(args.filename, 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        header = reader.next()

        # Auto detect param ID
        if not args.param_id:
            args.param_id = AutoDetectParamId[header[0]]
        param = etree.SubElement(context, "param", id=args.param_id, type="table")

        columns = etree.SubElement(param, "columns")
        columns.text = ','.join(header)
        types = etree.SubElement(param, "types")
        types.text = ','.join(['uint'] * len(header))
        rows = etree.SubElement(param, "rows")
        for row in reader:
            elem = etree.SubElement(rows, "row")
            elem.text = ','.join(row)

    # Pretty print
    xml = xml.dom.minidom.parseString(etree.tostring(algo))
    content = xml.toprettyxml(indent="  ")
    content = '\n'.join(content.split('\n')[1:]) # whipe XML version tag
    args.o.write(content)
