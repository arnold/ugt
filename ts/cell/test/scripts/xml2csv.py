import xml.dom.minidom
import argparse
import sys, os

if __name__ == '__main__':
    # Cmdline arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('filename', help="XML file to convert")
    parser.add_argument('-o', nargs='?', type=argparse.FileType('w'), default=sys.stdout)
    args = parser.parse_args()

    # Parse XML usign minidom
    xml = xml.dom.minidom.parse(args.filename)
    lines = []

    # Fetch header
    header = xml.getElementsByTagName('columns')[0].firstChild.nodeValue
    lines.append(header.strip())

    # Fetch rows
    for node in xml.getElementsByTagName('row'):
      lines.append(','.join([value.strip() for value in node.firstChild.nodeValue.strip().split(',')]))

    lines.append('') # newline at eof
    args.o.write('\n'.join(lines))
