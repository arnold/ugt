#!/usr/bin/env python

"""
Converts a trigger menu XML to a gatekeeper compatible XML
and updated an existing configuration setup.
"""

from menu2xml import XmlMenu, generate_menu_infra, pprint_xml

from lxml import etree
from shutil import copyfile
from glob import glob
import argparse
import re
import sys, os

def build_t(s):
    if not re.match(r'^[a-fA-F\d]{4}$', s):
        raise ValueError("not a build number (4-digit hex)")
    return s

if __name__ == '__main__':
    # Cmdline arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('config', help="source config directory, eg. config/vienna_v2")
    parser.add_argument('filename', help="XML file to convert")
    parser.add_argument('build', type=build_t, help="firmware build number")
    parser.add_argument('--context-id', default="processors", help="context id, default is 'processors'")
    parser.add_argument('-o', help="target config directory, if omitted will overwrite source config directory")
    args = parser.parse_args()

    # Check source configuration.
    if not os.path.isdir(args.config):
        raise IOError("no such config directory: {0}".format(args.config))

    config_infra_mp7 = os.path.join(args.config, "infra_mp7.xml")
    if not os.path.isfile(config_infra_mp7):
        raise IOError("missing config file: {0}".format(config_infra_mp7))
    config_ugt_base_key = os.path.join(args.config, "ugt_base_key.xml")
    if not os.path.isfile(config_ugt_base_key):
        raise IOError("missing config file: {0}".format(config_ugt_base_key))

    # Overwrite source configuration?
    if not args.o:
        args.o = args.config
    if args.o != args.config:
        if not os.path.exists(args.o):
       	    os.makedirs(args.o)
        # Copy all XML files to new location
        for src in glob(os.path.join(args.config, '*.xml')):
            dst = os.path.join(args.o, os.path.basename(src))
            copyfile(src, dst)

    # Parse XML
    menu = XmlMenu(args.filename)

    # Generate content
    menu_filename = ".".join((menu.menu_name.lower(), "xml"))
    out_menu = os.path.join(args.o, menu_filename)
    with open(out_menu, 'wb') as fp:
        infra = generate_menu_infra(menu, args.context_id)
        content = pprint_xml(infra)
        fp.write(content)

    # Update base key
    out_infra_mp7 = os.path.join(args.o, os.path.basename(config_infra_mp7))
    with open(config_infra_mp7, 'rb') as fp:
        infra_mp7_context = etree.parse(fp)
        # replace menu node
        for context_node in infra_mp7_context.xpath('context'):
            if context_node.attrib['id'].startswith('MP7_slot'):
                # Get module ID from param element
                module_id = context_node.xpath('param[@id="module_id"]/text()')[0]
                # Update sdfile param element
                param_sdfile = context_node.xpath('param[@id="sdfile"]')[0]
                param_sdfile.text = "gt_mp7_xe_v{0}_module_{1}.bin".format(args.build, module_id)
                # Update uuid param element
                param_uuid = context_node.xpath('param[@id="uuid"]')[0]
                param_uuid.text = menu.uuid_menu
    with open(out_infra_mp7, 'wb') as fp:
        content = pprint_xml(infra_mp7_context)
        fp.write(content)

    # Update base key
    out_ugt_base_key = os.path.join(args.o, os.path.basename(config_ugt_base_key))
    with open(config_ugt_base_key, 'rb') as fp:
        ugt_base_key_context = etree.parse(fp)
        # replace menu node
        for load_node in ugt_base_key_context.xpath('key/load'):
            if load_node.attrib['module'].startswith('l1'): # kinda weak...
                load_node.attrib['module'] = menu_filename
                break
    with open(out_ugt_base_key, 'wb') as fp:
        content = pprint_xml(ugt_base_key_context)
        fp.write(content)
