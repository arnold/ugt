#!/usr/bin/env python

"""Converts a trigger menu XML to a gatekeeper compatible XML."""

from collections import namedtuple
from lxml import etree
import xml.dom.minidom
import argparse
import sys, os, re

def fast_iter(context, func, *args, **kwargs):
    """
    http://lxml.de/parsing.html#modifying-the-tree
    Based on Liza Daly's fast_iter
    http://www.ibm.com/developerworks/xml/library/x-hiperfparse/
    See also http://effbot.org/zone/element-iterparse.htm
    """
    for event, elem in context:
        func(elem, *args, **kwargs)
        # It's safe to call clear() here because no descendants will be accessed
        elem.clear()
        # Also eliminate now-empty references from the root node to elem
        for ancestor in elem.xpath('ancestor-or-self::*'):
            while ancestor.getprevious() is not None:
                del ancestor.getparent()[0]
    del context

def generate_menu_infra(menu, context_id):
    """Returns generated XML element.
    Argument *menu* has to be an instance of class XmlMenu.
    """
    # Build tree
    infra = etree.Element("infra", id="uGT")
    context = etree.SubElement(infra, "context", id=context_id)
    param = etree.SubElement(context, "param", id="l1menu", type="table")

    # Build table
    columns = etree.SubElement(param, "columns")
    columns.text = 'AlgoName,GlobalIndex,ModuleIndex,LocalIndex'
    types = etree.SubElement(param, "types")
    types.text = 'string,uint,uint,uint'
    rows = etree.SubElement(param, "rows")
    for algorithm in menu.algorithms:
        elem = etree.SubElement(rows, "row")
        elem.text = '{algorithm.name},{algorithm.index},{algorithm.module_id},{algorithm.module_index}'.format(algorithm=algorithm)

    # Additional params
    param = etree.SubElement(context, "param", id="uuid_fw", type="string")
    param.text = menu.uuid_firmware
    param = etree.SubElement(context, "param", id="menu_name", type="string")
    param.text = menu.menu_name

    return infra

def pprint_xml(element):
    """Pretty print formated XML element without xml version tag (SWATCH compliance)."""
    content = xml.dom.minidom.parseString(etree.tostring(element))
    content = content.toprettyxml(indent="  ")
    def is_toprettyxml_stub(line):
        """Fix for toprettyxml newline bug, removes all '\s+\n' stubs but maintain original newlines."""
        return line and not line.strip()
    content = '\n'.join([line for line in content.split('\n') if not is_toprettyxml_stub(line)][1:]) # whipe XML version tag, first line
    # HACK remove newlines from uint type params, not supported by swatch XML parser
    content = re.sub(r'(>)[\s\n\r]*([^<>\s]+)[\s\n\r]*(</)', r'\1\2\3', content)
    return content

Algorithm=namedtuple('Algorithm', 'index, name, expression, module_id, module_index')
"""Algorithm container."""

class XmlMenu(object):
    """Container holding some information of the XML menu.
    Menu attributes:
    *menu_name* is the menu's name
    *uuid_menu* is the menu's UUID
    *uuid_firmware* is the menu's firmware UUID (set by the VHDL producer)
    *algorithms* holds algorithms.
    """

    def __init__(self, filename = None):
        self.menu_name = None
        self.uuid_menu = None
        self.uuid_firmware = None
        self.algorithms = []
        if filename: self.read(filename)

    def read(self, filename):
        self.algorithms = []
        with open(filename, 'rb') as fp:
            # Access static elements
            context = etree.parse(fp)
            self.menu_name = context.xpath('name/text()')[0]
            self.uuid_menu = context.xpath('uuid_menu/text()')[0]
            self.uuid_firmware = context.xpath('uuid_firmware/text()')[0]
            # Access list of algorithms
            fp.seek(0)
            context = etree.iterparse(fp, tag='algorithm')
            def read_algorithm(elem):
                """Fetch information from an algorithm tag and appends it to the list of algorithms."""
                name = elem.xpath('name/text()')[0]
                index = int(elem.xpath('index/text()')[0])
                expression = elem.xpath('expression/text()')[0]
                module_id = int(elem.xpath('module_id/text()')[0])
                module_index = int(elem.xpath('module_index/text()')[0])
                self.algorithms.append(Algorithm(index, name, expression, module_id, module_index))
            fast_iter(context, read_algorithm)

if __name__ == '__main__':
    # Cmdline arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('filename', help="XML file to convert")
    parser.add_argument('--context-id', default="processors", help="context id, default is 'processors'")
    parser.add_argument('-o', nargs='?', type=argparse.FileType('w'), default=sys.stdout)
    args = parser.parse_args()

    # Parse XML
    menu = XmlMenu(args.filename)

    # Generate content
    infra = generate_menu_infra(menu, args.context_id)

    # Write to target (stdout or file)
    content = pprint_xml(infra)
    args.o.write(content)
