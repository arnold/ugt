Quick Test Setup
================


Get address tables
------------------

Check out address table

    $ python ../../../scripts/getCellConfig.py [YYYY_MM_DD | trunk]

Use optional

    --link  create a CellConfig symbolic link to checked out tag/trunk
    --force  overwrites already existing checked out directory (use with care)

Note: ugt/scripts folder needs to be checked out first!


P5 testing
----------

Run the uGT SWATCH cell

    $ ./run_p5_testing.sh [config_key]

An optional configuration key can be assigned, else a default key is used (see config/ directory).

Note: from outside CMS network you have to open a tunnel!

    http://srv-s2g18-07-01.cms:3333/urn:xdaq-application:lid=13


Vienna
------

Run the uGT SWATCH cell

    $ firefox --no-remote &
    $ ./run_vienna.sh [config_key]

An optional configuration key can be assigned, else a default key is used (see config/ directory).

Access the cell using the following URL whereas HOST is either gtslc1 or gtslc2.

    http://HOST:3333/urn:xdaq-application:lid=13


Update/create configuration with a new XML menu
-----------------------------------------------

To patch an existing configuration (or to create a new configuration based on an
existing one) assigning a different menu use the menu2config.py script.

    Usage: menu2config <config_dir> <xml_menu> <fw_build_number> [-o <target_dir>]

It will create a menu table inside the specified config directory and patch the
contents of infra\_mp7.xml and ugt\_base\_key.xml to load the menu. When using the
'-o' option to create a new configuration set, all existing XML files from the
source config directory will be copied to the newly created directory.

    $ python scripts/menu2config.py config/vienna_v2 L1Menu_Sample.xml 10af [-o config/vienna_v3]


Converting XML menu to gatekeeper XML
-------------------------------------

Note: this is not required when using menu2config.py script.

Trigger menus need to be converted to gatekeeper CLOBs and referenced in vienna_top.xml

    $ python scripts/menu2xml.py L1Menu_Sample.xml -o menu_sample.xml

Note: adjust ugt\_base\_key.xml and infra\_mp7.xml to use the generated menu.

The uHAL address tables are located in the uhal/ directory.
