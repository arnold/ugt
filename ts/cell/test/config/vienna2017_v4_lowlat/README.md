vienna2017_v4_lowlat
====================

Configuration for testing MP7 framework 2.4.0 (low latency links) for both MP7
and AMC502 modules.

Location: Vienna test create
Menu: L1Menu_Collisions2017_v4(slim), *subset of 2 modules*
ugt version: 0.19.x
