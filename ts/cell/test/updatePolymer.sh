#!/bin/bash

####################################################
# Update script copies all necessary polymer files #
# to the correct folder in vienna                  #
####################################################

ROOT_DIR=$(pwd)/../../..
export swatchugt_ROOT=${ROOT_DIR}/ts/cell

cp -r ${swatchugt_ROOT}/html /opt/xdaq/htdocs/ugt/ts/cell
echo Copy polymer files ... DONE
