Polymer({
    is: 'preview-panel',

    behaviors: [
      // Polymer.PaperInputBehavior
      throwsToast
    ],

    properties: {

      isSystemReady: {
        type: Object,
        value: { },
        observer: '_sysReadyChanged'
      },


      activeAlgoMsg: {
        type: String,
        value: ""
      },
      isShowActiveAlgo: {
        type: Boolean,
        value: true
      },

      //
      status: {
        type: String,
        value: ""
      },

      prescaleStatus: {
        type: Object,
        value: {}
      },

      // change to preview prescale
      // index and otf flag
      changeToPreviewPrescale: {
        type: Object,
        value: undefined
      },

      // currently applied preview prescale
      // index and otf flag
      currentPreviewPrescale: {
        type: Object,
        value: {
          index: 0,
          otf: false,
          fmtlabel:""
        }
      },

      // object containing the current prescale index
      refresheddata: {
        type: Object,
        value: undefined,
        observer: '_prescaleInUseChanged'
      },

      // status of the prescale change
      statusPrescaleChange: {
          type: String,
          value: ""
      },

      // holds the retrieved prescale set
      // with algo, value pairs and the otf flag
      currentPreviewPrescaleSet: {
        type: Object,
        value: {},
        observer:'_retreivedCurrentPreviewPrescaleSet'
      },


      isToastVisible: {
        type: Boolean,
        value: false
      }

    },

    observers: [
      // 'dosomething(someproperty, someotherproperty)'
    ],

    onError: function(event, detail, sender) {
      this.setIsSystemReady(false, false, false);

      if (!this.isToastVisible) {
        this.throwToast({
         'type': 'error',
         'message': 'Your session has expired',
         'options': ['reload'],
         'blocking': true,
         'callback': function(response) {
            page.replace(page.current);
          }.bind(this)
        });
        this.isToastVisible = true;
      }
    },

    // Toggle Button selection changed
    onToggleChange: function(e) {
      var togglebtn = e.target;
      this.setActiveAlgoMsgString(togglebtn.checked);
      this.generateGetTableRequest();
    },


    // Selected sort option has changed
    // update table with selected sorter
    onSortOptionSelected: function (e) {
      this.generateGetTableRequest();
    },


    // generates request to refresh the table with
    // the currently selected sorter and flag for
    // showing only active algos or all algos
    generateGetTableRequest: function() {
      var selSorter = this.$$('#selSort').selected;
      var togglebtn = this.$$('#togglebutton');
      var tab = this.$$('#tab');
      tab.generateRequest();
    },

    // Clicked prescale preview change button
    changePrescaleIndex: function(e) {
      var selectedItem = this.getPrescaleIndexItem();
      // console.log("changePrescaleIndex selected index: " + selectedItem);

      if (selectedItem !== undefined) {
        this.generateGetTableRequest();

        var prescaleIndex = selectedItem.index;
        var isotf = selectedItem.otf;
        var label = selectedItem.text;
        // console.log("selected Index: " + prescaleIndex + " otf: " + isotf);
        // if (prescaleIndex.toString() === this.refresheddata.processors[0].index.toString()) {
        // //   && isotf = this.refresheddata.processors[0].otf) {
        //   console.log("Current prescale index is same as selected " + prescaleIndex);
        //   return;
        // }


        var refreshdata = this.$$("#refreshdata");
        refreshdata.selectedIndex = prescaleIndex;

        var handler = this.$$("#handler");
        handler.selectedIndex = prescaleIndex;
        handler.usingOtfPrescales = isotf;
        handler.generateRequest();

        this.set("changeToPreviewPrescale", {
          index: parseInt(prescaleIndex),
          otf: isotf,
          fmtlabel: label
        });
        // console.log("Change preview prescale index to " + prescaleIndex
        //     + " with otf = " + isotf);

        this.setStatusPrescaleChangeString(true);
      }
    },


    // Clicked open popup button for prescaling on the fly
    // Get the currently selected prescale index in the dropdown
    // or the currently applied one when no selection.
    openPrescalesPopup: function() {
      var selectedItem = this.getPrescaleIndexItem();
      // console.log(selectedItem);

      var handler = this.$$('#handlerOtf');

      if (selectedItem !== undefined) {
        handler.selectedIndex = selectedItem.index;
        handler.isOtfSet = selectedItem.otf;
      } else {
        handler.selectedIndex = "";
        handler.isOtfSet = false;
      }
      handler.generateRequest();
    },

    // Clicked 'confirm' on popup
    dialogConfirmed: function() {
      // var dropdown = this.$$('#dropdown');
      var selectedItem = this.getPrescaleIndexItem();
      var selIndex = selectedItem.index;
      var handler = this.$$('#handler');

      handler.usingOtfPrescales = true;
      handler.selectedIndex = selIndex;

      var refreshdata = this.$$("#refreshdata");
      refreshdata.selectedIndex = selIndex;

      // console.log("use preview prescale index=" + handler.selectedIndex);
      var sum = 0;
      var inputArray = Polymer.dom(this.root).querySelectorAll('paper-input');
      var params = [];

      for (var i = 0; i < inputArray.length; ++i) {
        var input = inputArray[i];

        for (var j = 0; j < this.currentPreviewPrescaleSet.prescaleset.length; ++j) {
          var item = this.currentPreviewPrescaleSet.prescaleset[j];
          var sInputLabel = this.buildPopupAlgoLabel(item);
          if (input.label == sInputLabel) {

            // push each algo name as new parameter to array 'parameters'
            handler.push('parameters', item.algo);
            handler[item.algo] = input.value;

            // push each algo mask as new parameter to array 'parameters'
            var postfix = '_mask';
            var swid = item.algo + postfix;
            var maskSwitch = this.$$('#' + swid);
            handler.push('parameters', swid);
            handler[swid] = maskSwitch.value;


            // console.log("p " + item.algo +  " " + input.value);
            // console.log("pM " + swid +  " " + maskSwitch.value);
            // ++sum;
            break;
          }
        }
      }
      // console.log("sum " + sum);
      // });

      this.set("changeToPreviewPrescale", {
        index: parseInt(selIndex),
        otf: true,
        fmtlabel: selectedItem.text
      });
      this.setStatusPrescaleChangeString(true);

      // console.log("dialogConfirmed: selectedIndex = " + selIndex);
      handler.generateRequest();
    },

    onCheckedChanged: function(e) {
      var target = e.target;
      console.log("blas " + target.id + " " + target.checked);
      if (target.checked) {
        target.innerHTML = 1;
      } else {
        target.innerHTML = 0;
      }
      target.value = target.innerHTML;
    },

    showEmptyInputDialog: function() {
      var msg = "Empty prescales for algos are not allowed.";
      this.throwToast({
        'type': 'error',
        'message': msg,
        'options': ['OK'],
        'blocking': true,
        'callback': function(response) {
          console.log(response);
          this.openPrescalesPopup();
        }.bind(this)
      });
    },

    // Construct a label for each paper-input in the dialog.
    buildPopupAlgoLabel: function(item) {
      return item.index + " " + item.algo;
    },

    _retreivedCurrentPreviewPrescaleSet: function(newval, oldval) {
      if (oldval !== undefined && oldval !== newval) {
        this.currentPreviewPrescaleSet = newval;

        var dialog = this.$$('#dialog');
        dialog.open();
      }
    },

    _sysReadyChanged: function(newval, oldval) {
      if (oldval !== undefined && oldval !== newval && !newval) {
        this.set("res", {});
        this.setActiveAlgoMsgString(true);
        this.set("changeToPreviewPrescale", undefined);
        this.set("currentPreviewPrescale",
        {
          index: 0,
          otf: false,
          fmtlabel:""
        });
        this.set("statusPrescaleChange", "");

        var dropdown = this.$$('#dropdown');
        if (dropdown !== null)
          dropdown.select(undefined);

        var dialog = this.$$('#dialog');
        if (dialog !== null) {
          dialog.cancel();
        }

        this.set("tab", {});
      }
    },


    _prescaleInUseChanged: function(newval, oldval) {
      if (oldval === undefined || (newval !== undefined && oldval != newval)) {

         if (this.tab !== null && this.tab !== undefined) {
           this.showTable = this.displayTable(this.tab.presc, newval.showtable, this.tab.items);
          }

         if (newval.changetoprescaleindex !== undefined) {
          //  this.set("changeToPreviewPrescaleIndex", parseInt(newval.changetoprescaleindex));
           this.set("changeToPreviewPrescale", {
             index: parseInt(newval.changetoprescaleindex),
             otf: newval.changetoprescaleotf,
             fmtlabel: newval.changetoprescalefmtlabel
           });
          //  console.log("changed preview prescale index to = " + this.changeToPreviewPrescale.index
          //       + " otf=" + this.changeToPreviewPrescale.otf + " " + newval.changetoprescalefmtlabel);
           this.setStatusPrescaleChangeString(true, newval.changetoprescalefmtlabel);

           this.selectPrescaleIndexItem(
             parseInt(this.changeToPreviewPrescale.index),
             this.changeToPreviewPrescale.otf);
          //  console.log("selectPrescaleIndexitem in dropdown =" + this.changeToPreviewPrescale.index + " otf =" + this.changeToPreviewPrescale.otf);
        }

       }

        if (newval.processors !== undefined && newval.processors.length > 0) {
          var currentPreviewPSIdx = parseInt(newval.processors[0].index);
          var currentPreviewPSOtf = newval.processors[0].otf;
          var currentPreviewPSLabel = newval.processors[0].fmtlabel;

          // set current preview prescale
          this.set("currentPreviewPrescale", {
           index: parseInt(currentPreviewPSIdx),
           otf: currentPreviewPSOtf,
           fmtlabel: currentPreviewPSLabel
          });

          if (this.changeToPreviewPrescale !== undefined) {

            // console.log(this.changeToPreviewPrescale.index + "=" + this.currentPreviewPrescale.index);
            // console.log(this.changeToPreviewPrescale.otf + "=" + this.currentPreviewPrescale.otf);

             if (parseInt(this.changeToPreviewPrescale.index) === parseInt(this.currentPreviewPrescale.index)
               && this.changeToPreviewPrescale.otf === this.currentPreviewPrescale.otf) {

               this.setStatusPrescaleChangeString(false);

             }

           } else {
             // fill change to preview prescale element and dropdown
             // based on current preview prescale
             this.set("statusPrescaleChange", "");

             this.set("changeToPreviewPrescale", {
               index: this.currentPreviewPrescale.index,
               otf: this.currentPreviewPrescale.otf,
               fmtlabel: this.currentPreviewPrescale.fmtlabel
             });
          }
          if (this.getPrescaleIndexItem() === undefined) {
            this.selectPrescaleIndexItem(
               this.currentPreviewPrescale.index,
               this.currentPreviewPrescale.otf);
          }
       }
     },

    // Select the corresponding dropdown item based on
    // the prescale index and otf flag
    selectPrescaleIndexItem: function(prescaleIndex, otf) {
      var dropdown = this.$$('#dropdown');

      if (dropdown !== null) {
        var items = dropdown.items;
        for (var i = 0; i < items.length; i++) {
          var item = items[i].value;

          if (parseInt(item.index) === prescaleIndex && item.otf === otf) {
            dropdown.selectIndex(i);
            break;
          }
        }
      }
    },

    // Get the currently selected dropdown item
    getPrescaleIndexItem: function() {
      var dropdown = this.$$('#dropdown');
      var domDropdownItems = this.$$('#domDropdownItems');

      if (domDropdownItems !== undefined && dropdown !== null) {
        if (dropdown.selectedItem !== undefined) {
            // console.log(domDropdownItems.items[dropdown.selected]);
            return domDropdownItems.items[dropdown.selected];
        }
      }
      return undefined;
    },

    // Return the dropdown label for the element request by
    // its prescale index and otf flag
    getDropdownPrescaleLabel: function(prescaleIndex, otf) {
      var dropdown = this.$$('#dropdown');

      if (dropdown !== null) {
        var items = dropdown.items;
        for (var i = 0; i < items.length; i++) {
          var item = items[i].value;

          if ((parseInt(item.index) == prescaleIndex) && (item.otf == otf)) {
            return item.text;
            break;
          }
          if ((undefined == prescaleIndex) && (item.otf == otf)) {
            return item.text;
            break;
          }
        }
      }
      return "";
    },


    // Set string message for 'activeAlgoMsg'
    // depending on 'isActive' flag
    setActiveAlgoMsgString: function(isActive) {
      this.isShowActiveAlgo = isActive;
      if (isActive) {
        this.activeAlgoMsg = "Switch to all algos";
      } else {
        this.activeAlgoMsg = "Switch to active algos";
      }
    },

    // Set string message for 'statusPrescaleChange'
    // when waiting for prescale change depending
    // on 'isWaiting' flag
    setStatusPrescaleChangeString: function(isWaiting) {
        var label = this.getDropdownPrescaleLabel(this.changeToPreviewPrescale.index, this.changeToPreviewPrescale.otf);
      this.setStatusPrescaleText(isWaiting, label);
    },

    setStatusPrescaleChangeString: function(isWaiting, label) {
      this.setStatusPrescaleText(isWaiting, label);
    },

    setStatusPrescaleText: function(isWaiting, text) {
      if (isWaiting) {
        var msg = "";
        if (text !== undefined) {
          msg += "Change to prescale '" + text + "' ";
        } else {
          msg += "Changing prescale ";
        }
        msg += "(Waiting for lumisection update)";
        this.set("statusPrescaleChange", msg);
      } else {
        // this.set("statusPrescaleChange", "(Prescale changed)");
        this.set("statusPrescaleChange", "");
      }
    },

    // Show a row in 'showActiveAlgo' mode when
    // 1) prescale is set (>0)
    // 2) mask is enabled (==1)
    showRow: function(prescale, mask) {
      if (!this.isShowActiveAlgo) {
        return true;
      } else {
        return (prescale > 0 && mask === 1);
      }
    },

    // Check whether a mask is applied or not.
    // When 'bxmask' is not empty then,
    // return true (bx mask applied), or false (no bx mask)
    isBxMaskApplied: function(bxmask) {
      return (this.isNotEmpty(bxmask) && bxmask.length > 0)
    },

    // Return the text indicating an applied bx mask
    // String is used in table cell
    getBxMaskText: function(bxmask) {
      if (this.isBxMaskApplied(bxmask)) {
        return "applied";
      }
      return "";
    },

    // When mask is applied return the mask string
    // with text, otherwise return empty string.
    // String is used in the tooltip bubble
    getBxMaskAltText: function(bxmask) {
      if (this.isBxMaskApplied(bxmask)) {
        var out = "";
        for (var i=0; i<bxmask.length; i++) {
          out += bxmask[i]
          if (i+1<bxmask.length)
             out += ", ";
        }
        return out;
      }
      return "";
    },

    displayTable: function(tablePrescale, showTable, rows) {
      if (tablePrescale == null || showTable == null || rows == null)
        return false;

      // console.log("display: " + showTable + " " + rows.length + " "+ tablePrescale + "==" + this.currentPreviewPrescale.index);
      return showTable && rows.length > 0 &&
        (parseInt(tablePrescale) == parseInt(this.currentPreviewPrescale.index));
    },

    reloadTable: function (aRefreshTable, aIsRunningOrPaused) {
      return (aRefreshTable && aIsRunningOrPaused);
    },

    getLabelClass: function(status) {
      return 'label '+status+'-bkg';
    },

    isNotEmpty: function(status) {
      return !(status === undefined);
    },

    getRateLoadingMessage: function(remainingLumis) {
      var msg = "Loading rates and finor for new preview prescale index, wait ";
      if (remainingLumis === 1) {
        msg = msg + remainingLumis + " lumisection.";
      } else if (remainingLumis > 1) {
        msg = msg + remainingLumis + " lumisections.";
      } else if (remainingLumis === 0) {
        msg = msg + "a moment...";
      } else {
        console.log("getRateLoadingMessage: unknown value = " + remainingLumis);
        msg = msg + "a moment...";
      }
      return msg;
    },

    // Returns the prescaleIndex and an indicator for otf
    // prescale if otf is set true
    getPrescaleIndexString: function(prescaleIndex, otf) {
      var msg = prescaleIndex.toString();
      if (otf) {
        msg = msg + "*";
      }
      return msg;
    },


    makeSortedItems: function(items) {

      if (items == undefined || items.length == 0)
        return;

      var selSort = this.$$('#selSort');
      var selected = selSort.selected;
      var comparator = undefined;

      if (selected == 0) {
        comparator = this.propComparatorAsc('index');
      } else if (selected == 1) {
        comparator = this.propComparatorDesc('index');
      } else if (selected == 2) {
        comparator = this.propComparatorAsc('algo');
      } else if (selected == 3) {
        comparator = this.propComparatorDesc('algo');
      } else if (selected == 4) {
        comparator = this.propComparatorAsc('rate');
      } else if (selected == 5) {
        comparator = this.propComparatorDesc('rate');
      } else if (selected == 6) {
        comparator = this.propComparatorAsc('ratecounter');
      } else if (selected == 7) {
        comparator = this.propComparatorDesc('ratecounter');
      } else if (selected == 8) {
        comparator = this.propComparatorAsc('moduleid');
      } else if (selected == 9) {
        comparator = this.propComparatorDesc('moduleid');
      } else {
        console.log("Unknown sorter for id=" + selected);
        return;
      }

      var activeItems = [];
      for (var i = 0; i < items.length; i++) {
        var row = items[i];
        if (this.showRow(row.prescale, row.finor)) {
          activeItems.push(row);
        }
      }

      var sortedItems = activeItems.sort(comparator);

      for (var i = 0; i < sortedItems.length; i++) {
        var row = sortedItems[i];

        // format rate value using locale, eg. 1,000,000.00
        row.rate = this.formatRate(row.rate);

        // format counter value using locale, eg. 1,000,000
        row.ratecounter = this.formatCounter(row.ratecounter);
      }
      return sortedItems;
    },

    propComparatorAsc: function(prop) {
      return function (a, b) {
        if (a[prop] < b[prop]) {
          return -1;
        } else if (a[prop] > b[prop]) {
          return 1;
        } else {
          return a.index - b.index;
        }
      }
    },
    propComparatorDesc: function(prop) {
      return function (a, b) {
        if (a[prop] < b[prop]) {
          return 1;
        } else if (a[prop] > b[prop]) {
          return -1;
        } else {
          return a.index - b.index;
        }
      }
    },

    formatCounter: function(counter) {
      return counter.toLocaleString('en-US', {
        minimumFractionDigits: 0
      });
    },
    formatRate: function(rate) {
      return rate.toLocaleString('en-US', {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
      });
    },

    setIsSystemReady: function(aRunningOrPaused, aConfiguredOrAligned, aNotConfigured) {
      this.set("isSystemReady", {
        runningOrPaused: aRunningOrPaused,
        configuredOrAligned: aConfiguredOrAligned,
        notConfigured: aNotConfigured
      });
    },

    // Fires when an instance of the element is created
    // you have no data binding and the element does not contain html code yet
    created: function() {

    },

    // Fires when the local DOM has been fully prepared
    // data binding works and the template html is ready
    ready: function() {
      this.setIsSystemReady(false, false, false);
    },

    // Fires when the element was inserted into the document
    attached: function() {
      this.setActiveAlgoMsgString(true);
      this.showTable = false;
    },

    // Fires when the element was removed from the document
    detached: function() {},

    // Fires when an attribute was added, removed, or updated
    attributeChanged: function(name, type) {}
});
