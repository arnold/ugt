Polymer({
    is: 'prescales-panel',

    behaviors: [
      // Polymer.PaperInputBehavior
      throwsToast
    ],

    properties: {

      isSystemReady: {
        type: Boolean,
        value: false,
        observer: '_sysReadyChanged'
      },

      // selectedButton: {
      //   type: String,
      //   value: ""
      // },
      res: {
        type: Object,
        value: {}
      },
      currentPrescaleIndex: {
        type: Object,
        value: {},
        observer: '_prescaleInUseChanged'
      },

      statusPrescaleChange: {
        type: String,
        value: ""
      },

      isToastVisible: {
        type: Boolean,
        value: false
      }
    },
    observers: [
      // 'dosomething(someproperty, someotherproperty)'
    ],

    onError: function(event, detail, sender) {
      this.isSystemReady = false;

      if (!this.isToastVisible) {
        this.throwToast({
         'type': 'error',
         'message': 'Your session has expired',
         'options': ['reload'],
         'blocking': true,
         'callback': function(response) {
            page.replace(page.current);
          }.bind(this)
        });
        this.isToastVisible = true;
      }
    },


    changePrescaleIndex: function(e) {
      var dropdown = this.$$('#dropdown');
      var selectedItem = this.getPrescaleIndexItem();

      if (selectedItem != undefined)
      {
        var selPrescIdx = selectedItem.index.toString();
        var selPrescOtf = selectedItem.otf;
        if (selPrescIdx === this.currentPrescaleIndex.processors[0].index.toString()
            && selPrescOtf === this.currentPrescaleIndex.processors[0].otf)
        {
          console.log("Current prescale index is same as selected " + selPrescIdx);
          return;
        }

        var refreshCurrentPrescaleIndex = this.$$('#refreshCurrentPrescaleIndex');
        if (refreshCurrentPrescaleIndex !== null) {
          refreshCurrentPrescaleIndex.selectedButton = selPrescIdx;
        }

        var handler = this.$$('#handler');
        handler.selectedButton = selPrescIdx;
        handler.generateRequest();
        console.log("Change prescale index to " + selPrescIdx);

        this.setStatusPrescaleChangeString(true, selPrescIdx, false);
      }
    },


    onItemSelect: function() {

    },

    tableChanged: function() {

    },

    refreshGK: function(e) {
      console.log("refreshItems");

      // this.set("selectedButton", "");
      this.set("res", {});
      this.set("table", {});

      var dropdown = this.$$('#dropdown');
      if (dropdown !== null)
        dropdown.select(undefined);

      var refreshCurrentPrescaleIndex = this.$$('#refreshCurrentPrescaleIndex');
      if (refreshCurrentPrescaleIndex !== null)
        refreshCurrentPrescaleIndex.set("selectedButton", "");

      var getTableHandler = this.$$('#cbGetTable');
      if (getTableHandler !== null)
        getTableHandler.generateRequest();
    },

    _sysReadyChanged: function(newval, oldval) {
      if (oldval !== undefined && oldval !== newval) {
        if(newval) {
          this.refreshGK();
        } else {
          // this.set("selectedButton", "");
          this.set("res", {});
          this.set("table", {});

          var refreshCurrentPrescaleIndex = this.$$('#refreshCurrentPrescaleIndex');
          if (refreshCurrentPrescaleIndex !== null) {
            refreshCurrentPrescaleIndex.set("selectedButton", "");
          }

          var dropdown = this.$$('#dropdown');
          if (dropdown !== null) {
            dropdown.select(undefined);
          }
        }
      }
    },

    _prescaleInUseChanged: function(newval, oldval) {
      if (oldval !== undefined && newval !== undefined && oldval != newval) {

        if (newval.processors !== null) {
          var prescaleIndex = parseInt(newval.processors[0].index);
          var otf = newval.processors[0].otf;
          var fmtlabel = newval.processors[0].fmtlabel;


          var changeToPrescaleIndex = newval.changetoprescaleindex;
          var changeToPrescaleOtf = newval.changetoprescaleotf;

          if (prescaleIndex !== changeToPrescaleIndex ||
              otf !== changeToPrescaleOtf) {
            this.setStatusPrescaleChangeString(true, changeToPrescaleIndex, changeToPrescaleOtf);
          } else {
            this.setStatusPrescaleChangeString(false, prescaleIndex, false);
          }

          var dropdownItem = this.getPrescaleIndexItem()
          if (dropdownItem === undefined) {
            this.selectPrescaleIndexItem(changeToPrescaleIndex);
          }

        }
      }

    },


    // Select the corresponding dropdown item based on
    // the prescale index and otf flag
    selectPrescaleIndexItem: function(prescaleIndex) {
      var dropdown = this.$$('#dropdown');

      if (dropdown !== undefined && dropdown !== null) {
        var items = dropdown.items;
        for (var i = 0; i < items.length; i++) {
          var item = items[i].value;

          if (parseInt(item.index) === prescaleIndex) {
            dropdown.selectIndex(i);
            break;
          }
        }
      }
    },

    // Get the currently selected dropdown item
    getPrescaleIndexItem: function() {
      var dropdown = this.$$('#dropdown');
      var domDropdownItems = this.$$('#domDropdownItems');

      if (domDropdownItems !== undefined && dropdown !== null) {
        if (dropdown.selectedItem !== undefined) {
            // console.log(domDropdownItems.items[dropdown.selected]);
            return domDropdownItems.items[dropdown.selected];
        }
      }
      return undefined;
    },

    // Return the dropdown label for the element request by
    // its prescale index and otf flag
    getDropdownPrescaleLabel: function(prescaleIndex) {
      var dropdown = this.$$('#dropdown');

      if (dropdown !== null) {
        var items = dropdown.items;
        for (var i = 0; i < items.length; i++) {
          var item = items[i].value;

          if ((parseInt(item.index) == prescaleIndex)) {
            return item.text;
            break;
          }
        }
      }
      return "";
    },

    // Returns the prescaleIndex and an indicator for otf
    // prescale if otf is set true
    getPrescaleIndexString: function(prescaleIndex, otf) {
      var msg = prescaleIndex.toString();
      if (otf) {
        msg = msg + "*";
      }
      return msg;
    },

    // Set string message for 'statusPrescaleChange'
    // when waiting for prescale change depending
    // on 'isWaiting' flag
    setStatusPrescaleChangeString: function(isWaiting, prescaleIndex, otf) {
      if (isWaiting) {
        var label = this.getDropdownPrescaleLabel(parseInt(prescaleIndex));
        var sOtf = "";
        if (otf)
          sOtf = "*";


        this.set("statusPrescaleChange", "Change to prescale '" +
          label + sOtf + "' (Waiting for lumisection update)");
      } else {
        // this.set("statusPrescaleChange", "(Prescale changed)");
        this.set("statusPrescaleChange", "");
      }
    },



    // Fires when an instance of the element is created
    // you have no data binding and the element does not contain html code yet
    created: function() {},

    // Fires when the local DOM has been fully prepared
    // data binding works and the template html is ready
    ready: function() {},

    // Fires when the element was inserted into the document
    attached: function() {},

    // Fires when the element was removed from the document
    detached: function() {},

    // Fires when an attribute was added, removed, or updated
    attributeChanged: function(name, type) {}
});
