Polymer({
    is: 'rate-panel',

    behaviors: [
      // Polymer.PaperInputBehavior
      throwsToast
    ],

    properties: {

      isSystemReady: {
        type: Boolean,
        value: false
      },

      selectedOption: {
        type: String,
        value: ""
      },

      diagnosisMsg: {
        type: String,
        value: ""
      },
      isShowDiagnosis: {
        type: Boolean,
        value: false
      },

      activeAlgoMsg: {
        type: String,
        value: ""
      },
      isShowActiveAlgo: {
        type: Boolean,
        value: false
      },

      isShowDumpVersions: {
        type: Boolean,
        value: false
      },
      dumpVersions: {
        type: Object
      },

      isToastVisible: {
        type: Boolean,
        value: false
      }
    },

    observers: [
      // 'dosomething(someproperty, someotherproperty)'
    ],

    onError: function(event, detail, sender) {
      this.isSystemReady = false;

      if (!this.isToastVisible) {
        this.throwToast({
         'type': 'error',
         'message': 'Your session has expired',
         'options': ['reload'],
         'blocking': true,
         'callback': function(response) {
            page.replace(page.current);
          }.bind(this)
        });
      }
      this.isToastVisible = true;
    },


    // Toggle Button selection changed
    onToggleChange: function(e) {
      var togglebtn = e.target;
      this.setActiveAlgoMsgString(togglebtn.checked);
      this.generateGetTableRequest();
    },

    // Toggle Diagnosis Button selection changed
    onToggleDiagnosis: function(e) {
      var tbtDiagnosis = e.target;
      this.setDiagnosisMsgString(tbtDiagnosis.checked);
      var selSort = this.$$('#selSort');
      if (selSort != undefined && selSort != null) {
        if (!this.isShowDiagnosis) {
          if (selSort.selected > 7) {
            selSort.selectIndex(5);
          }
        }
      }
      this.generateGetTableRequest();

      var setupSortOptions = this.$$('#setupSortOptions');
      if (setupSortOptions != null) {
        setupSortOptions.showDiagnosis = tbtDiagnosis.checked;
        setupSortOptions.generateRequest();
      }
    },

    onToggleDumpVersions: function(e) {
      var togglebtn = e.target;
      this.setDumpVersionsMsgString(togglebtn.checked);

      // var dumpVersion = this.$$('#dumpVersionsCallback');
      // dumpVersion.generateRequest();
      // this.generateGetTableRequest();
    },

    // Selected sort option has changed
    // update table with selected sorter
    onSelectedSortOption: function (e) {
      this.generateGetTableRequest();
    },

    // generates request to refresh the table with
    // the currently selected sorter and flag for
    // showing only active algos or all algos
    generateGetTableRequest: function() {
      if (this.isSystemReady) {
        // var selSorter = this.$$('#selSort').selected;
        var togglebtn = this.$$('#togglebutton');
        var diagnosisbtn = this.$$('#toggleDiagnosis');
        var tab = this.$$('#tab');
        if (tab != null)
          tab.generateRequest();
      }
    },

    getRateLoadingMessage: function() {
      return "Loading rates and finor prescale, wait a moment...";
    },

    // Set string message for 'activeAlgoMsg'
    // depending on 'isActive' flag
    setActiveAlgoMsgString: function(isActive) {
      this.isShowActiveAlgo = isActive;
      if (isActive) {
        this.activeAlgoMsg = "Switch to all algos";
      } else {
        this.activeAlgoMsg = "Switch to active algos";
      }
    },

    // Set string message for 'diagnosisMsg'
    // depending on 'isActive' flag
    setDiagnosisMsgString: function(isActive) {
      this.isShowDiagnosis = isActive;
      if (isActive) {
        this.diagnosisMsg = "Switch to Shifter Mode";
      } else {
        this.diagnosisMsg = "Switch to Expert Mode";
      }
    },

    // Set string message for 'dumpVersionsMsg'
    // depending on 'isActive' flag
    setDumpVersionsMsgString: function(isActive) {
      this.isShowDumpVersions = isActive;
      if (isActive) {
        this.dumpVersionsMsg = "Hide Versions";
      } else {
        this.dumpVersionsMsg = "Show Versions";
      }
    },


    showRow: function(prescale, mask) {
      if (!this.isShowActiveAlgo) {
        return true;
      } else {
        return (prescale > 0 && mask === 1);
      }
    },

    // Check whether a mask is applied or not.
    // When 'bxmask' is not empty then,
    // return true (bx mask applied), or false (no bx mask)
    isBxMaskApplied: function(bxmask) {
      return (this.isNotEmpty(bxmask) && bxmask.length > 0)
    },

    // Return the text indicating an applied bx mask
    // String is used in table cell
    getBxMaskText: function(bxmask) {
      if (this.isBxMaskApplied(bxmask)) {
        return "applied";
      }
      return "";
    },

    // When mask is applied return the mask string
    // with text, otherwise return empty string.
    // String is used in the tooltip bubble
    getBxMaskAltText: function(bxmask) {
      if (this.isBxMaskApplied(bxmask)) {
        var out = "";
        for (var i=0; i<bxmask.length; i++) {
          out += bxmask[i]
          if (i+1<bxmask.length)
             out += ", ";
        }
        return out;
      }
      return "";
    },

    getLabelClass: function(status) {
      return 'label '+status+'-bkg';
    },

    isNotEmpty: function(status) {
      return !(status === undefined);
    },

    makeSortedItems: function(items) {

      if (items == undefined || items.length == 0)
        return;

      var selSort = this.$$('#selSort');
      var selected = selSort.selected;
      if (selected == null) {
        console.log("Selected is null in makeSortedItems");
        return;
      }

      var comparator = undefined;

      if (selected == 0) {
        comparator = this.propComparatorAsc('index');
      } else if (selected == 1) {
        comparator = this.propComparatorDesc('index');
      } else if (selected == 2) {
        comparator = this.propComparatorAsc('algo');
      } else if (selected == 3) {
        comparator = this.propComparatorDesc('algo');
      } else if (selected == 4) {
        comparator = this.propComparatorAsc('rate');
      } else if (selected == 5) {
        comparator = this.propComparatorDesc('rate');
      } else if (selected == 6) {
        comparator = this.propComparatorAsc('ratecounter');
      } else if (selected == 7) {
        comparator = this.propComparatorDesc('ratecounter');
      } else if (selected == 8) {
        comparator = this.propComparatorAsc('moduleid');
      } else if (selected == 9) {
        comparator = this.propComparatorDesc('moduleid');
      } else {
        console.log("Unknown sorter for id=" + selected);
        return;
      }

      var activeItems = [];
      for (var i = 0; i < items.length; i++) {
        var row = items[i];
        if (this.showRow(row.prescale, row.finor)) {
          activeItems.push(row);
        }
      }


      var sortedItems = activeItems.sort(comparator);

      for (var i = 0; i < sortedItems.length; i++) {
        var row = sortedItems[i];

        // format rate value using locale, eg. 1,000,000.00
        row.rate = this.formatRate(row.rate);

        // format counter value using locale, eg. 1,000,000
        row.ratecounter = this.formatCounter(row.ratecounter);
      }
      return sortedItems;
    },

    propComparatorAsc: function(prop) {
      return function (a, b) {
        if (a[prop] < b[prop]) {
          return -1;
        } else if (a[prop] > b[prop]) {
          return 1;
        } else {
          return a.index - b.index;
        }
      }
    },
    propComparatorDesc: function(prop) {
      return function (a, b) {
        if (a[prop] < b[prop]) {
          return 1;
        } else if (a[prop] > b[prop]) {
          return -1;
        } else {
          return a.index - b.index;
        }
      }
    },

    formatCounter: function(counter) {
      return counter.toLocaleString('en-US', {
        minimumFractionDigits: 0
      });
    },
    formatRate: function(rate) {
      return rate.toLocaleString('en-US', {
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
      });
    },

    calcDiffCounter: function(val1, val2) {
      return this.formatCounter(val1-val2);
    },
    calcDiffRate: function(val1, val2) {
      return this.formatRate(val1-val2);
    },

    // Returns the prescaleIndex and an indicator for otf
    // prescale if otf is set true
    getPrescaleIndexString: function(prescaleIndex, otf) {
      var msg = prescaleIndex.toString();
      if (otf) {
        msg = msg + "*";
      }
      return msg;
    },

    // Fires when an instance of the element is created
    // you have no data binding and the element does not contain html code yet
    created: function() {},

    // Fires when the local DOM has been fully prepared
    // data binding works and the template html is ready
    ready: function() {},

    // Fires when the element was inserted into the document
    attached: function() {
      this.setActiveAlgoMsgString(true);
      this.setDiagnosisMsgString(false);
      this.setDumpVersionsMsgString(false);

      this.showTable = false;
    },

    // Fires when the element was removed from the document
    detached: function() {},

    // Fires when an attribute was added, removed, or updated
    attributeChanged: function(name, type) {}
});
