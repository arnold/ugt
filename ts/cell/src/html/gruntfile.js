require('es6-shim');
module.exports = function(grunt) {
    grunt.initConfig({
        /*
          Compile SASS code to CSS
         */
        sass: {
            options: {
                sourcemap: 'none',
                // style: 'expanded',
                outputStyle: 'compressed',
                noCache: true
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: '',
                    src: [
                        'elements/**/*.scss'
                    ],
                    dest: '',
                    ext: '-min.css'
                }]
            }
        },
        /*
          Add css prefixes for compatibility (mainly for Firefox ESL)
         */
        postcss: {
            options: {
                map: false,
                processors: [
                    require('autoprefixer')({
                        browsers: ['firefox 24', 'IE 10', 'last 2 versions']
                    })
                ]
            },
            dist: {
                src: ['elements/**/*-min.css']
            }
        },
        /*
          Process JavaScript
         */
        uglify: {
            options: {
                preserveComments: false,
                srewIE8: true,
                sourceMap: false
            },
            /*
              Compile JavaScript on Polymer Elements
             */
            polymerjs: {
                options: {
                    mangle: false,
                    sourceMap: false
                },
                files: [{
                    expand: true,
                    src: ['elements/**/*.js'],
                    ext: '-min.js'
                }]
            }
        },

        inline: {

          dist: {
            files: [{
              expand: true,
              cwd: '',
              src: [
                  'elements/**/*.html'
              ],
              dest: '../../html/',
              ext: '.html'
            }]
          }
      	},

        clean: {
            cssfiles: {
                options: {
                    'no-write': false
                },
                src: ["elements/**/*-min.css"]
            },
            jsfiles: {
                options: {
                    'no-write': false
                },
                src: ["elements/**/*-min.js"]
            }
        },

        /*
         * Make package documentation
         */
        execute: {
          target: {
            src: ['elements/makeIndex.js'],
            options: {
              cwd: "./elements/"
            }
          }
        }
    });

    grunt.loadNpmTasks('grunt-execute');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-inline');

    grunt.registerTask('default', ['execute', 'sass', 'postcss', 'uglify', 'inline', 'clean']);
};
