#include "ugt/ts/cell/xmlMenu2Gatekeeper.h"

#include "tmEventSetup/esTriggerMenu.hh"
#include "tmEventSetup/tmEventSetup.hh"

#include <sstream>

namespace ugt
{


boost::shared_ptr<pugi::xml_document>
xmlMenu2Gatekeeper(const std::string& menu_clob)
{
  boost::shared_ptr<pugi::xml_document> pugi_xml(new pugi::xml_document());

  pugi::xml_node node_infra = pugi_xml->append_child("infra");
  node_infra.append_attribute("id") = "uGT";

  pugi::xml_node node_context =  node_infra.append_child("context");
  node_context.append_attribute("id") = "processors";

  pugi::xml_node node_param = node_context.append_child("param");
  node_param.append_attribute("id") = "l1menu";
  node_param.append_attribute("type") = "table";

  pugi::xml_node node =  node_param.append_child("columns").append_child(pugi::node_pcdata);
  node.set_value("AlgoName, GlobalIndex, ModuleIndex, LocalIndex");

  node = node_param.append_child("types").append_child(pugi::node_pcdata);
  node.set_value("string, uint, uint, uint");

  pugi::xml_node rows = node_param.append_child("rows");

  std::istringstream iss;
  iss.str(menu_clob);
  const tmeventsetup::esTriggerMenu* menu = tmeventsetup::getTriggerMenu(iss);
  const std::map<std::string, tmeventsetup::esAlgorithm>& algoMap = menu->getAlgorithmMap();

  for (std::map<std::string, tmeventsetup::esAlgorithm>::const_iterator cit = algoMap.begin();
       cit != algoMap.end(); cit++)
  {
    const tmeventsetup::esAlgorithm& algo = cit->second;
    node = rows.append_child("row").append_child(pugi::node_pcdata);
    std::stringstream ss;
    ss << "" << algo.getName() << ", " << algo.getIndex() << ", "
       << algo.getModuleId() << ", " << algo.getModuleIndex();
    node.set_value(ss.str().c_str());
  }

  pugi::xml_node uuid_node = node_context.append_child("param");
  uuid_node.append_attribute("id") = "uuid_fw";
  uuid_node.append_attribute("type") = "string";
  pugi::xml_node data = uuid_node.append_child(pugi::node_pcdata);
  std::string text = menu->getFirmwareUuid();
  data.set_value(text.c_str());

  pugi::xml_node name_node = node_context.append_child("param");
  name_node.append_attribute("id") = "menu_name";
  name_node.append_attribute("type") = "string";
  pugi::xml_node name = name_node.append_child(pugi::node_pcdata);
  text = menu->getName();
  name.set_value(text.c_str());

  pugi_xml->save(std::cout);

  return pugi_xml;
}

}
