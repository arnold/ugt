#include "ugt/ts/cell/MonitoringDataTypes.h"

#include "ugt/swatch/uGtAlgo.h"

#include <cmath>
#include <set>
#include <string>


namespace ugt
{

const unsigned long kMonitoringDataProducerPollInterval = 2;
const unsigned long kMonitoringDataConsumerPollInterval = 7;

const uint32_t kPhysicsGenFDLIndex = 9999;

const std::set<std::string> MONITORING_DATA_KEYS = {
  ALGO_COUNT,
  ALGO_COUNT_BEFORE_PRESCALE,
  POST_DEADTIME_ALGO_COUNT,
  FINOR_COUNT
};

};
