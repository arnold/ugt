#include "ugt/ts/cell/MonitoringDataConsumer.h"

#include "ugt/ts/cell/MonitoringDataProducer.h"
#include "ugt/ts/cell/DatabaseInterface.h"
#include "ugt/swatch/uGtProcessor.h"
#include "ugt/swatch/uGtAlgo.h"

#include "swatchcell/framework/CellAbstract.h"

// SWATCH cell headers
#include "swatchcell/framework/CellAbstract.h"
#include "ts/candela/DatabaseConnection.h"

// SWATCH headers
#include "swatch/system/System.hpp"
#include "swatch/core/AbstractMetric.hpp"

// boost headers
#include "boost/thread/lock_guard.hpp"
#include "boost/algorithm/string.hpp"


namespace ugt {

MonitoringDataConsumer::MonitoringDataConsumer(const std::string& aParentLoggerName, swatchcellframework::CellContext& aContext, size_t aPeriodInSeconds) :
  mLogger(log4cplus::Logger::getInstance(aParentLoggerName + ".MonitoringDataConsumer")),
  mContext(aContext),
  mPeriod(aPeriodInSeconds),
  mActive(false),
  mUpdateNow(false),
  mSelfDestruct(false),
  mNextUpdateTime(),
  mThread(boost::ref(*this)),
  producer_()
{
  LOG4CPLUS_INFO(mLogger, "Created monitoring thread (period = " << aPeriodInSeconds << "s)");
}


MonitoringDataConsumer::~MonitoringDataConsumer()
{
  LOG4CPLUS_INFO(mLogger, "Destroying monitoring thread");
  boost::lock_guard<boost::mutex> lGuard(mMutex);
  mSelfDestruct = true;
  mConditionVar.notify_one();
}


size_t MonitoringDataConsumer::getPeriod() const
{
  boost::lock_guard<boost::mutex> lGuard(mMutex);
  return mPeriod;
}


void MonitoringDataConsumer::setPeriod(size_t aPeriod)
{
  LOG4CPLUS_INFO(mLogger, "Monitoring thread period set to " << aPeriod << " seconds");
  boost::lock_guard<boost::mutex> lGuard(mMutex);
  mPeriod = aPeriod;
  mNextUpdateTime = Clock_t::now() + boost::chrono::seconds(mPeriod);
  mConditionVar.notify_one();
}


bool MonitoringDataConsumer::isActive() const
{
  boost::lock_guard<boost::mutex> lGuard(mMutex);
  return mActive;
}


void MonitoringDataConsumer::start()
{
  LOG4CPLUS_INFO(mLogger, "Starting montoring thread (period = " << mPeriod << "s)");
  boost::lock_guard<boost::mutex> lGuard(mMutex);

  if (!mActive) {
    mActive = true;
    mNextUpdateTime = Clock_t::now();
    mConditionVar.notify_one();
  }
}


void MonitoringDataConsumer::stop()
{
  LOG4CPLUS_INFO(mLogger, "Stopping monitoring thread");
  boost::lock_guard<boost::mutex> lGuard(mMutex);
  mActive = false;
}


void MonitoringDataConsumer::updateNow()
{
  LOG4CPLUS_INFO(mLogger, "Monitoring thread will update monitoring data again ASAP");
  boost::lock_guard<boost::mutex> lGuard(mMutex);
  mUpdateNow = true;
  mConditionVar.notify_one();
}


void MonitoringDataConsumer::operator() ()
{
  while (true) {
    LOG4CPLUS_DEBUG(mLogger, "Monitoring thread: operator()()  >>  Top of while(true)");
    {
      boost::unique_lock<boost::mutex> lGuard(mMutex);

      while (!((mActive && (mNextUpdateTime <= Clock_t::now())) || mUpdateNow || mSelfDestruct)) {
        if (mActive) {
          mConditionVar.wait_until(lGuard, mNextUpdateTime);
        } else
          mConditionVar.wait(lGuard);
      }
      mUpdateNow = false;

      // In case this instance is being destroyed, then return so that the thread can stop
      if (mSelfDestruct) {
        LOG4CPLUS_INFO(mLogger, "Monitoring thread: Main loop returning now");
        return;
      }
      // Otherwise, proceed with our main purpose (updating monitoring data) after unlocking mMutex
    }

    LOG4CPLUS_DEBUG(mLogger, "Monitoring thread: Updating monitoring data");

    Clock_t::time_point lUpdateStartTime = Clock_t::now();

    consumeLumiSectionData();

    LOG4CPLUS_DEBUG(mLogger, "Monitoring thread: Finished updating monitoring data");

    boost::unique_lock<boost::mutex> lGuard(mMutex);
    mNextUpdateTime = lUpdateStartTime + boost::chrono::seconds(mPeriod);
  }
}


void MonitoringDataConsumer::consumeLumiSectionData()
{
  LOG4CPLUS_INFO(mLogger, "consumeLumiSectionData() start ...");

  LumiSectionData ls_data;
  producer_->getLumiSectionData(ls_data);
  if (not ls_data.is_valid)
  {
    LOG4CPLUS_INFO(mLogger, "consumeLumiSectionData() no valid data available, fifo is empty (return)");
    return;
  }

  std::stringstream ss;
  ss << "consumeLumiSectionData() received data for LS = " << ls_data.lumi_section;
  LOG4CPLUS_INFO(mLogger, ss.str());

#if 0
  for (auto it = ls_data.data.begin(); it != ls_data.data.end(); it++)
  {
    const std::string &key = it->first;
    const AlgoData &algo_data = it->second;
    const unsigned long index = algo_data.first;
    const std::string &data = algo_data.second;
    std::cout << "  " << key << " " << index << " " << data << "\n";
  }
#endif

  try
  {
    // Implemented in this way, i.e. using DatabaseConnection for each DB access, as we had a problem
    // keep using the same DatabaseConnection (probably due to expiration of connection).
    // Could share the same DatabaseConnection in this block (after testing).

    tscandela::DatabaseConnection dbc0(mLogger, &mContext, ugtdbi::UserName, ugtdbi::PwdPath);
    ugtdbi::fillLumiSection(&dbc0, ls_data.lumi_section, ls_data.prescale_index, &mLogger);

    tscandela::DatabaseConnection dbc1(mLogger, &mContext, ugtdbi::UserName, ugtdbi::PwdPath);
    ugtdbi::fillAlgoScalers(&dbc1, ls_data, &mLogger);
  }
  catch (std::exception& e)
  {
    std::cout << e.what() << "\n";
    LOG4CPLUS_ERROR(mLogger, e.what());
  }

  LOG4CPLUS_INFO(mLogger, "consumeLumiSectionData() end ...");
}

} // ns: ugt
