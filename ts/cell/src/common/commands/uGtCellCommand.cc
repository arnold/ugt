#include "ugt/ts/cell/commands/uGtCellCommand.h"

#include "ugt/ts/cell/Cell.h"
#include "ugt/swatch/uGtProcessor.h"
#include "ugt/swatch/uGtTriggerMenu.h"

#include "swatchcell/framework/CellContext.h"

#include <boost/pointer_cast.hpp>


namespace ugt
{

uGtCellCommand::uGtCellCommand(log4cplus::Logger& log, tsframework::CellAbstractContext* context)
: CellCommand(log, context)
{
}

bool
uGtCellCommand::isProcessorsAndMenuAvailable()
{
  if (isProcessorsAvailable() && isMenuAvailable())
    return true;

  return false;
}

bool
uGtCellCommand::isProcessorsAvailable()
{
  const std::vector<const uGtProcessor*> uGtProcessors = getUgtCell()->getUgtProcessors();
  if (uGtProcessors.empty())
    return false;

  return true;
}

bool
uGtCellCommand::isMenuAvailable()
{
  const uGtTriggerMenu& menu = getUgtCell()->getUgtTriggerMenu();
  if (menu.size() <= 0)
    return false;

  return true;
}

bool
uGtCellCommand::isSystemReady()
{
  tsframework::CellOperationFactory* lOpFactory = getContext()->getOperationFactory();
  std::string state = lOpFactory->getOperation(swatchcellframework::CellContext::kRunControlOperationName).getFSM().getState();
  bool rc = ((state == "configured") or (state == "aligned") or (state == "running") or (state == "paused"));
  return rc;
}

ugt::Cell*
uGtCellCommand::getUgtCell()
{
  swatchcellframework::CellContext *swContext = dynamic_cast<swatchcellframework::CellContext*>(getContext());
  return dynamic_cast<ugt::Cell*>(swContext->getCell());
}

}
