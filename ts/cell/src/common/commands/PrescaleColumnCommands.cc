#include "ugt/ts/cell/commands/PrescaleColumnCommands.h"

#include "ugt/ts/cell/panels/PanelUtils.h"
#include "ugt/ts/cell/Cell.h"

#include "ugt/swatch/uGtProcessor.h"
#include "ugt/swatch/uGtAlgo.h"

#include "swatchcell/framework/CellContext.h"

#include "ts/framework/CellAbstract.h"
#include "ts/framework/CellWarning.h"

#include "xdata/String.h"

#include "log4cplus/loggingmacros.h"

#include <boost/pointer_cast.hpp>


namespace ugt
{

GetCurrentPrescaleColumn::GetCurrentPrescaleColumn(log4cplus::Logger& log, tsframework::CellAbstractContext* context)
:uGtCellCommand(log, context)
{
}

void
GetCurrentPrescaleColumn::code()
{
  std::string output;
  if (isSystemReady() and isProcessorsAndMenuAvailable())
  {
    Json::Value jsonOutput = generateJson();
    Json::FastWriter fastWriter;
    output = fastWriter.write(jsonOutput);
  }

  delete payload_;
  payload_ = new xdata::String(output);

  LOG4CPLUS_INFO(getLogger(), "Finished GetCurrentPrescaleColumn Cell command");
}

Json::Value
GetCurrentPrescaleColumn::generateJson()
{
  Json::Value root;

  try
  {
    uint32_t prescaleIndex = getPrescaleIndexInUse();
    bool otfFlag = isUsingOtfInUse();

    const uGtTriggerMenu& menu = getUgtCell()->getUgtTriggerMenu();
    auto it = menu.begin();
    if (it == menu.begin())
    {
      const Algorithm* algorithm = it->second;
      toolbox::PrescaleColumnItem prescaleColumnItem = algorithm->getPrescaleColumnItemForPsIndex(prescaleIndex);

      root["index"] = prescaleColumnItem.index;
      root["label"] = prescaleColumnItem.label;
      root["otf"] = otfFlag;
    }
  }
  catch (MetricValueUnknownException &e)
  {
    // do nothing, empty json will be return later
  }

  return root;
}


uint32_t
GetCurrentPrescaleColumn::getPrescaleIndexInUse()
{
  const std::string kPrescaleIndexInUse = "prescaleColumnIndexInUse";

  const std::vector<const uGtProcessor*> uGtProcessors = getUgtCell()->getUgtProcessors();

  auto pit = uGtProcessors.begin();
  const uGtProcessor* p = *pit;
  const uGtAlgo& algo = dynamic_cast<const uGtAlgo&>(p->getAlgo());

  const ::swatch::core::AbstractMetric& metric = algo.getMetric(kPrescaleIndexInUse);
  const ::swatch::core::MetricSnapshot& snapshot = metric.getSnapshot();
  if (not snapshot.isValueKnown())
  {
    LOG4CPLUS_ERROR(getLogger(), "GetCurrentPrescaleColumn prescale index in use is unknown");
    throw MetricValueUnknownException(kPrescaleIndexInUse);
  }

  uint32_t prescaleIndexInUse = snapshot.getValue<uint32_t>();
  return prescaleIndexInUse;
}
bool
GetCurrentPrescaleColumn::isUsingOtfInUse()
{
  const std::string kOtfInUse = "prescOtfInUse";
  const std::vector<const uGtProcessor*> uGtProcessors = getUgtCell()->getUgtProcessors();

  bool isOtfInUse = false;

  auto pit = uGtProcessors.begin();
  const uGtProcessor* p = *pit;
  const uGtAlgo& algo = dynamic_cast<const uGtAlgo&>(p->getAlgo());

  const ::swatch::core::AbstractMetric& metric_ls = algo.getMetric(kOtfInUse);
  const ::swatch::core::MetricSnapshot& ss_ls = metric_ls.getSnapshot();
  if (not ss_ls.isValueKnown())
  {
    LOG4CPLUS_ERROR(getLogger(), "GetCurrentPrescaleColumn otf in use flag  is unknown");
    throw MetricValueUnknownException(kOtfInUse);
  }

  if (ss_ls.getValue<uint32_t>() == 1)
    isOtfInUse = true;
  return isOtfInUse;
}







GetCurrentPrescaleColumnList::GetCurrentPrescaleColumnList(log4cplus::Logger& log, tsframework::CellAbstractContext* context)
:uGtCellCommand(log, context)
{
}

void
GetCurrentPrescaleColumnList::code()
{
  std::string output;

  if (isSystemReady() and isProcessorsAndMenuAvailable())
  {
    Json::Value jsonOutput = generateJson();
    Json::FastWriter fastWriter;
    output = fastWriter.write(jsonOutput);
  }

  delete payload_;
  payload_ = new xdata::String(output);

  LOG4CPLUS_INFO(getLogger(), "Finished GetCurrentPrescaleColumn Cell command");
}

Json::Value
GetCurrentPrescaleColumnList::generateJson()
{
  const uGtTriggerMenu& menu = getUgtCell()->getUgtTriggerMenu();

  Json::Value root(Json::arrayValue);

  auto it = menu.begin();
  if (it == menu.begin())
  {
    const Algorithm* algorithm = it->second;
    for (const auto& prescaleItem : algorithm->getPrescales())
    {
      toolbox::PrescaleColumnItem prescaleColumnItem = prescaleItem.first;

      Json::Value jsonCol;
      jsonCol["index"] = prescaleColumnItem.index;
      jsonCol["label"] = prescaleColumnItem.label;

      root.append(jsonCol);
    }
  }

  return root;
}


}
