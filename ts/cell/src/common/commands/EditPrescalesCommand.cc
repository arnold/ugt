#include "ugt/ts/cell/commands/EditPrescalesCommand.h"

#include "ugt/ts/cell/panels/PanelUtils.h"
#include "ugt/ts/cell/Cell.h"

#include "ugt/swatch/uGtProcessor.h"
#include "ugt/swatch/uGtAlgo.h"

#include "ugt/swatch/commands/uGtApplyPrescaleCommand.h"

#include "swatchcell/framework/CellContext.h"

#include "ts/framework/CellAbstract.h"
#include "ts/framework/CellWarning.h"

#include "uhal/HwInterface.hpp"
#include "mp7/MP7Controller.hpp"

#include "swatch/action/GateKeeper.hpp"
#include "swatch/core/ReadWriteXParameterSet.hpp"
#include "swatch/system/System.hpp"
#include "swatch/processor/Processor.hpp"
#include "swatch/processor/ProcessorStub.hpp"

#include "xdata/String.h"
#include "xdata/UnsignedInteger.h"

#include "log4cplus/loggingmacros.h"

#include <boost/pointer_cast.hpp>
#include "boost/chrono.hpp"


namespace ugt
{

EditPrescales::EditPrescales(log4cplus::Logger& log, tsframework::CellAbstractContext* context)
:uGtCellCommand(log, context)
,mImage(1, kMaxAlgorithms, 0x0)  // default value ??

{
  // check if system is in correct state
  if (not isSystemReady())
  {
    getWarning().setLevel(tsframework::CellWarning::ERROR);
    getWarning().setMessage("EditPrescales: System is not in correct state (configured, aligned, start, paused)");
    return;
  }



  // check for availabiliy of ugt processors
  if (not isProcessorsAvailable()) // no uGtProcessor in the crate
  {
    getWarning().setLevel(tsframework::CellWarning::ERROR);
    getWarning().setMessage("EditPrescales: no uGtProcessor found");
    return;
  }

  const std::vector<const uGtProcessor*> uGtProcessors = getUgtCell()->getUgtProcessors();

  // prepare registers
  auto pit = uGtProcessors.begin();
  if (pit == uGtProcessors.end())
  {
    getWarning().setLevel(tsframework::CellWarning::ERROR);
    getWarning().setMessage("EditPrescales: uGtProcessors is end");
    return;
  }
  uGtProcessor& proc = const_cast<uGtProcessor&>(*(*pit));
  ::mp7::MP7Controller& driver = (proc.driver());
  ::uhal::HwInterface& hw = driver.hw();

  const std::string reg = "gt_mp7_gtlfdl.prescale_factor";
  const uhal::Node& prescales = hw.getNode(reg);

  ::uhal::ValVector<uint32_t> data_prescale = prescales.readBlock(kMaxAlgorithms);

  hw.getClient().dispatch();

  // fill image
  for (size_t i = 0; i < data_prescale.size(); i++)
  {
    mImage.setValue(i, data_prescale.at(i));
  }

  uGtAlgo& algo = dynamic_cast<uGtAlgo&>(proc.getAlgo());
  uGtTriggerMenu& ugtMenu = algo.getTriggerMenu();

  // register command parameters according to TriggerMenu.
  for (auto cit = ugtMenu.begin(); cit != ugtMenu.end(); cit++)
  {
    const Algorithm *tmAlgo = cit->second;

    // std::string algoParameter = getAlgoParameter(tmAlgo->getIndex());
    // std::string algoParameter = getAlgoParameter(tmAlgo->getName());
    uint32_t defPrescale = data_prescale.at(tmAlgo->getIndex());
    getParamList().insert ( std::make_pair ( tmAlgo->getName(), new xdata::UnsignedInteger ( defPrescale ) ) );
  }

}

void
EditPrescales::code()
{
  LOG4CPLUS_INFO(getLogger(), "EditPrescales: command started");

  // check if system is in correct state
  if (not isSystemReady())
  {
    getWarning().setLevel(tsframework::CellWarning::ERROR);
    getWarning().setMessage("EditPrescales: System is not in correct state (configured, aligned, start, paused)");
    delete payload_;
    payload_ = new xdata::String("EditPrescales: System is not in correct state (configured, aligned, start, paused)");
    return;
  }

  if (not isProcessorsAvailable()) // no uGtProcessor in the crate
  {
    getWarning().setLevel(tsframework::CellWarning::ERROR);
    getWarning().setMessage("EditPrescales: no uGtProcessor found");
    return;
  }


  // check for correct system state
  swatchcellframework::CellContext *swContext = dynamic_cast<swatchcellframework::CellContext*>(getContext());
  swatchcellframework::CellContext::SharedGuard_t lGuard(*swContext);
  const std::string reg = "gt_mp7_gtlfdl.prescale_factor";

  const uGtTriggerMenu& ugtMenu = getUgtCell()->getUgtTriggerMenu();
  // get changed prescales from input and prepare image
  preparePrescalesImage(ugtMenu);

  const std::vector<const uGtProcessor*> uGtProcessors = getUgtCell()->getUgtProcessors();
  // write new prescale image to all processors
  for (auto pit = uGtProcessors.begin(); pit != uGtProcessors.end(); pit++)
  {
    uGtProcessor& proc = const_cast<uGtProcessor&>(*(*pit));
    ::mp7::MP7Controller& driver = (proc.driver());
    ::uhal::HwInterface& hw = driver.hw();

    // prepare prescales
    const uhal::Node& prescalesNode = hw.getNode(reg);
    prescalesNode.writeBlock(mImage.values());
    // prepare otf flag (write 1 since on-the-fly is used)
    hw.getNode("gt_mp7_gtlfdl.prescale_otf_flags.production_flag").write(1);

    hw.getClient().dispatch();
  }
  LOG4CPLUS_INFO(getLogger(), "EditPrescales: Prepared edited prescales");

  // secure enough time till the next lumisection boundary
  int nInconsistent = 0;
  bool bEnoughTimeLeft = false;
  uint32_t lumiSection_ref = 0;
  for (uint32_t ii = 0; ii < kRetryLimit; ii++)
  {
    bool isInconsistent = false;
    lumiSection_ref = (dynamic_cast<const uGtAlgo&>(uGtProcessors.at(0)->getAlgo())).getLuminositySection();
    uint32_t lumiSection = 0;
    for (auto it = uGtProcessors.begin(); it != uGtProcessors.end(); it++)
    {
      const uGtAlgo& algo = dynamic_cast<const uGtAlgo&>((*it)->getAlgo());
      lumiSection = algo.getLuminositySection();
      if (lumiSection != lumiSection_ref)
      {
        isInconsistent = true;
        nInconsistent++;
        break;
      }
    }

    if (isInconsistent)
    {
      double sleepMs = 1000;
      LOG4CPLUS_INFO(getLogger(), "EditPrescales: probably crossed lumisection boundary: "
                                  << lumiSection_ref << " != " << lumiSection
                                  << " sleep for " << sleepMs << " ms");
      boost::this_thread::sleep_for(boost::chrono::milliseconds(uint32_t(sleepMs)));
      continue;
    }


    // check the remaining time
    const uGtAlgo& algo = dynamic_cast<const uGtAlgo&>(uGtProcessors.at(0)->getAlgo());
    double remainingSeconds = algo.getSecondsBeforeLuminositySegmentUpdate(&getLogger());
    LOG4CPLUS_INFO(getLogger(), "EditPrescales: seconds before lumisection boundary: " << remainingSeconds);
    if (remainingSeconds >= kUpdateThreshold)
    {
      bEnoughTimeLeft = true;
      break;
    }

    double sleepMs = (remainingSeconds+kSleepDelta)*1000;
    LOG4CPLUS_INFO(getLogger(), "EditPrescales: sleep for " << sleepMs << " ms");
    boost::this_thread::sleep_for(boost::chrono::milliseconds(uint32_t(sleepMs)));
  }

  if (not bEnoughTimeLeft)
  {
    std::ostringstream msg;
    msg << "EditPrescales: not enough time left till the next lumisection after " << kRetryLimit << " trials."
        << " nInconsistent = " << nInconsistent;
    LOG4CPLUS_WARN(getLogger(), msg.str());
    return;
  }


  // Enough time left for updating the prescale setting
  for (auto it = uGtProcessors.begin(); it != uGtProcessors.end(); it++)
  {
    const uGtProcessor& ugt = dynamic_cast<const uGtProcessor&>(*(*it));
    uGtApplyPrescaleCommand &cmd = dynamic_cast<uGtApplyPrescaleCommand &>(const_cast<uGtProcessor&>(ugt).getCommand(cmd::kApplyPrescale));
    swatch::core::ReadWriteXParameterSet params;
    cmd.exec(params, false);

    if (cmd.getState() != ::swatch::action::Functionoid::State::kDone)
    {
      std::ostringstream msg;
      msg << "EditPrescales: cmd " << cmd::kApplyPrescale << " status msg:" << cmd.getStatus().getStatusMsg();
      LOG4CPLUS_ERROR(getLogger(), msg.str());
      return;
    }

    LOG4CPLUS_INFO(getLogger(), "EditPrescales: apply prescale for slot = " << (*it)->getSlot());
  }


  // check if still in the same lumisection
  bool bSuccess = true;
  for (auto it = uGtProcessors.begin(); it != uGtProcessors.end(); it++)
  {
    const uGtAlgo& algo = dynamic_cast<const uGtAlgo&>((*it)->getAlgo());
    uint32_t lumiSection = algo.getLuminositySection();
    if (lumiSection != lumiSection_ref)
    {
      bSuccess = false;
      break;
    }
  }

  std::ostringstream oss;
  if (bSuccess)
  {
    oss << "EditPrescales: changed Prescales successfully";
    LOG4CPLUS_INFO(getLogger(), oss.str());
  }
  else
  {
    oss << "EditPrescales: crossed lumisection boundary during the operation, wait for the next lumisection";
    LOG4CPLUS_WARN(getLogger(), oss.str());
  }

  oss << std::endl << "EditPrescales: command finished";

  delete payload_;
  payload_ = new xdata::String(oss.str());

  LOG4CPLUS_INFO(getLogger(), oss.str());
}


void
EditPrescales::preparePrescalesImage(const uGtTriggerMenu& ugtMenu)
{
  for (auto cit = ugtMenu.begin(); cit != ugtMenu.end(); cit++)
  {
    const Algorithm *tmAlgo = cit->second;

    uint32_t algoindex = tmAlgo->getIndex();
    std::string algoname = tmAlgo->getName();

    // std::string algoParameter = getAlgoParameter(algoname);
    xdata::UnsignedInteger* xAlgoParam = dynamic_cast<xdata::UnsignedInteger*>(getParamList()[algoname]);
    uint32_t prescale = *(dynamic_cast<xdata::UnsignedInteger*>(xAlgoParam));
    mImage.setValue(algoindex, prescale);
  }
}

}
