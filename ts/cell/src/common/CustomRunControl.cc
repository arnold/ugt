#include "ugt/ts/cell/CustomRunControl.h"

#include "ugt/ts/cell/Cell.h"
#include "ugt/ts/cell/DatabaseInterface.h"

#include "ugt/swatch/toolbox/PrescaleTable.h"
#include "ugt/swatch/toolbox/PrescaleSet.h"
#include "ugt/swatch/toolbox/AlgoBXMaskTable.h"
#include "ugt/swatch/uGtTriggerMenu.h"
#include "ugt/swatch/Algorithm.h"

#include "ts/candela/DatabaseConnection.h"

#include "swatchcell/framework/RunControl.h"
#include "swatchcell/framework/tools/utilities.h"

#include "swatch/action/GateKeeper.hpp"
#include "swatch/system/System.hpp"

#include <boost/pointer_cast.hpp>
#include <log4cplus/loggingmacros.h>

namespace ugt
{

CustomRunControl::CustomRunControl(log4cplus::Logger& log,
                                   tsframework::CellAbstractContext* context)
:
swatchcellframework::RunControl(log, context)
{
  LOG4CPLUS_INFO(getLogger(), "CustomRunControl : CTOR");
}


void
CustomRunControl::execPostStart()
{
  LOG4CPLUS_INFO(getLogger(), "CustomRunControl : execPostStart");
  swatchcellframework::CellContext::SharedGuard_t lGuard(mContext);
  swatch::system::System& lSys = mContext.getSystem(lGuard);
  // const swatch::action::GateKeeper* gatekeeper = mContext.getGateKeeper(lGuard);

  // force update enabled metrics
  swatchcellframework::tools::updateMetricsOfEnabledObjects(lSys);

  // reset database interface
  ugtdbi::reset();


  // run number
  const unsigned long run = getRunNumber();
  ugtdbi::setRunNumber(run);
  if (run == 0 || getTestModeFlag())
  {
    ugtdbi::disableDatabaseAccess();
  }
  else
  {
    ugtdbi::enableDatabaseAccess();
  }


  // start rate monitoring threads
  ugt::Cell* cell = dynamic_cast<ugt::Cell*>(mContext.getCell());
  MonitoringDataProducer& producer = cell->getMonitoringDataProducer();
  MonitoringDataConsumer& consumer = cell->getMonitoringDataConsumer();
  producer.start();
  consumer.setProducer(&producer);
  consumer.start();

  // L1 trigger menu
  const uGtTriggerMenu& l1menu = cell->getUgtTriggerMenu();
  l1menu.show();


  // setup prescales map for database in the format
  // < prescale index, <algo index, prescale value> >
  PrescalesDBMap prescalesMapDB = preparePrescaleDBMap(l1menu);
  AlgoBXMaskDBMap algoBXMaskMapDB = prepareAlgoBXMaskDBMap(l1menu);
  PrescaleNameDBMap prescaleNameMapDB = preparePrescaleNameDBMap(l1menu);

  // dump to database
  try
  {
    // Implemented in this way, i.e. using DatabaseConnection for each DB access, as we had a problem
    // keep using the same DatabaseConnection (probably due to expiration of connection).
    // Could share the same DatabaseConnection in this block (after testing).

    tscandela::DatabaseConnection dbc0(getLogger(), getContext(), ugtdbi::UserName, ugtdbi::PwdPath);
    ugtdbi::fillAlgoSetting(&dbc0, l1menu.get(), &getLogger());

    tscandela::DatabaseConnection dbc1(getLogger(), getContext(), ugtdbi::UserName, ugtdbi::PwdPath);
    ugtdbi::fillPrescales(&dbc1, prescalesMapDB, &getLogger());

    tscandela::DatabaseConnection dbc2(getLogger(), getContext(), ugtdbi::UserName, ugtdbi::PwdPath);
    ugtdbi::fillAlgoBxMask(&dbc2, algoBXMaskMapDB, &getLogger());

    tscandela::DatabaseConnection dbc3(getLogger(), getContext(), ugtdbi::UserName, ugtdbi::PwdPath);
    ugtdbi::fillPrescaleNames(&dbc3, prescaleNameMapDB, &getLogger());
  }
  catch (std::exception& e)
  {
    std::cout << "CustomRunControl: execPostStart: " << e.what() << "\n";
    LOG4CPLUS_ERROR(getLogger(), e.what());
  }

}


void
CustomRunControl::execPreStop()
{
  LOG4CPLUS_INFO(getLogger(), "CustomRunControl : execPreStop");

  // stop rate monitoring threads
  ugt::Cell* cell = dynamic_cast<ugt::Cell*>(mContext.getCell());
  MonitoringDataProducer& producer = cell->getMonitoringDataProducer();
  MonitoringDataConsumer& consumer = cell->getMonitoringDataConsumer();
  producer.stop();
  consumer.stop();


  // dump summary
  unsigned int retry = 0;
  while (retry < 3)
  {
    try
    {
      tscandela::DatabaseConnection dbc(getLogger(), getContext(), ugtdbi::UserName, ugtdbi::PwdPath);
      ugtdbi::fillAlgoScalersSummary(&dbc, &getLogger());
      ugtdbi::disableDatabaseAccess();
      break;
    }
    catch (std::exception& e)
    {
      std::cout << "CustomRunControl: execPreStop: " << e.what() << "\n";
      LOG4CPLUS_ERROR(getLogger(), e.what());
    }
    retry += 1;
    LOG4CPLUS_INFO(getLogger(), "CustomRunControl : execPreStop : retrying ... " << retry);
    usleep(100000);
  }
}


PrescalesDBMap
CustomRunControl::preparePrescaleDBMap(const uGtTriggerMenu& l1menu)
{
  PrescalesDBMap prescalesMapDB;
  std::pair<PrescalesDBMap::iterator, bool> rc;

  for (auto cit = l1menu.begin(); cit != l1menu.end(); cit++)
  {
    const Algorithm* algo = cit->second;
    const uint32_t algoIndex = algo->getIndex();

    for (const auto& it : algo->getPrescales())
    {
      const uint32_t psIndex = it.first.index;
      const double prescale = it.second;
      if (cit == l1menu.begin())
      {
        std::map<unsigned long, double> empty;
        rc = prescalesMapDB.insert(std::make_pair(psIndex, empty));
      }
      std::map<unsigned long, double> &map = prescalesMapDB[psIndex];
      map[algoIndex] = prescale;
    }
  }

  return prescalesMapDB;
}

AlgoBXMaskDBMap
CustomRunControl::prepareAlgoBXMaskDBMap(const uGtTriggerMenu& l1menu)
{
  AlgoBXMaskDBMap algoBXMaskMapDB;
  std::pair<AlgoBXMaskDBMap::iterator, bool> rc;
  for (auto cit = l1menu.begin(); cit != l1menu.end(); cit++)
  {
    const Algorithm* algo = cit->second;
    const uint32_t algoIndex = algo->getIndex();
    std::map<uint32_t, uint32_t> algoBXMask = algo->getAlgoBxMask();

    // setup algo bx mask map for ugt database < algoIndex, < bx , maskValue > >
    std::map<unsigned long, unsigned long> bxMaskMap;
    for (auto cit2 = algoBXMask.begin(); cit2 != algoBXMask.end(); cit2++)
    {
      const uint32_t bx = cit2->first;
      const uint32_t mask = cit2->second;
      if (mask)
        continue;
      bxMaskMap.insert(std::make_pair(bx, mask));
    }
    algoBXMaskMapDB.insert(std::make_pair(algoIndex, bxMaskMap));
  }

  return algoBXMaskMapDB;
}

PrescaleNameDBMap
CustomRunControl::preparePrescaleNameDBMap(const uGtTriggerMenu& l1menu)
{
  PrescaleNameDBMap prescaleNameDBMap;
  const auto& cit = l1menu.begin();
  const Algorithm* algo = cit->second;

  for (const auto& it : algo->getPrescales())
  {
    const auto column = it.first;
    prescaleNameDBMap.insert(std::make_pair(column.index, column.label));
  }

  return prescaleNameDBMap;
}

};

// end
