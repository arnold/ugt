#include "ugt/ts/cell/DatabaseInterface.h"

#include "ugt/ts/cell/MonitoringDataTypes.h"
#include "ugt/swatch/Constants.h"
#include "ugt/swatch/uGtAlgo.h"

#include "ts/candela/DatabaseConnection.h"
#include "ts/candela/Table.h"
#include "ts/candela/Row.h"
#include "ts/candela/QueryResult.h"

#include "boost/thread/lock_guard.hpp"
#include "boost/thread/mutex.hpp"

#include <stdio.h>
#include <time.h>
#include <iomanip>


namespace ugtdbi {

const std::string view = "urn:tstore-view-SQL:ugt-cell";
const std::string UserName = "cms_ugt_mon";
const std::string PwdPath = "/nfshome0/centraltspro/secure/";

static boost::mutex ugtdbiMutex_;
static ugt::RunSummaryData ugtdbiRunSummaryData_;
static unsigned int ugtdbiRunNumber_ = 0;
static std::string ugtdbiIdRunNumber_ = "0000000_";
static std::string ugtdbiRunNumberString_ = "0";
static bool ugtdbiNoDatabase_ = true;


template<typename T> T
convert(const std::string& ss)
{
  T data;
  std::istringstream(ss) >> data;
  return data;
}


/** Convert to string representation. */
template<typename T> std::string
toString(const T d)
{
  return static_cast<std::ostringstream*>(&(std::ostringstream() << d))->str();
}


/** Convert floating point value to string representation of fixed precision. */
template<typename T> std::string
toString(const T d, const size_t precision)
{
  return static_cast<std::ostringstream*>(&(std::ostringstream() << std::setprecision(precision) << std::fixed << d))->str();
}


std::string
trim(const std::string& source,
     const char* whites)
{
  std::string result(source);
  std::string::size_type index = result.find_last_not_of(whites);
  if (index != std::string::npos) result.erase(++index);

  index = result.find_first_not_of(whites);
  if (index != std::string::npos)
  {
    result.erase(0, index);
  }
  else
  {
    result.erase();
  }

  return result;
}


void
toLower(std::string& str)
{
  std::transform(str.begin(), str.end(), str.begin(), tolower);
}


void
insert(tscandela::DatabaseConnection* dbc,
       const std::string& name,
       const std::map<std::string, std::string>& params)
{
  if (not ugtdbiNoDatabase_)
  {
    tscandela::Table table = dbc->getTable(name);
    table.getInserter().set(params).insert();
  }
  else
  {
    for (auto it = params.begin(); it != params.end(); it++)
    {
      std::cout << it->first << " = " << it->second << " ";
    }
    std::cout << "\n";
  }
};


void
insert(tscandela::DatabaseConnection* dbc,
       const std::string& name,
       const std::vector<std::map<std::string, std::string> >& params)
{
  if (not params.size())
  {
    std::cout << "insert: empty data\n";
    return;
  }

  tscandela::Table::Inserter inserter = dbc->getTable(name).getInserter();

  unsigned int ii = 0;
  for (auto it = params.begin(); it != params.end(); ++it, ++ii)
  {
    if (not ugtdbiNoDatabase_)
    {
      std::map<std::string, std::string> data = *it;
      for (auto x = data.begin(); x != data.end(); x++)
      {
        inserter.setData(ii, x->first, x->second);
      }
    }
    else
    {
      std::map<std::string, std::string> data = *it;
      for (auto x = data.begin(); x != data.end(); x++)
      {
        std::cout << x->first << " = " << x->second << " ";
      }
      std::cout << "\n";
    }
  }

  if (not ugtdbiNoDatabase_)
  {
    inserter.insert();
  }
};


void
fillTable(const std::vector<std::string>& columns,
          tscandela::Row& data,
          std::map<std::string, std::string>& table)
{
  for (auto it = columns.begin(); it != columns.end(); ++it)
  {
    std::string name = *it;
    toLower(name);
    table[name] = trim(data.get<std::string>(name));
  }
}


void
fillTable(tscandela::DatabaseConnection* dbc,
          const std::string& name,
          std::map<std::string, std::string>& query,
          std::map<std::string, std::string>& table)
{
  const std::string op = "select";

  if (not ugtdbiNoDatabase_)
  {
    tscandela::QueryResult result = dbc->getTable(name).select(op, view, query);

    tscandela::QueryResultIterator cit = result.begin();
    tscandela::Row data(cit);
    fillTable(result.getColumns(), data, table);
  }
  else
  {
    std::cout << op << " : ";
    for (auto it = query.begin(); it != query.end(); it++)
    {
      std::cout << it->first << " = " << it->second << " ";
    }
    std::cout << "\n";
  }
}


const std::string
getLumiSectionId(const unsigned long lumi_section)
{
  std::stringstream id;
  id << ugtdbiIdRunNumber_
     << std::setfill('0') << std::setw(5) << lumi_section;

  return id.str();
}


const std::string
getScalerName(const std::string& key)
{
  std::string name;

  if (key == ugt::ALGO_COUNT)
  {
    name = "ALGORITHM_RATE_AFTER_PRESCALE";
  }
  else if (key == ugt::ALGO_COUNT_BEFORE_PRESCALE)
  {
    name = "ALGORITHM_RATE_BEFORE_PRESCALE";
  }
  else if (key == ugt::POST_DEADTIME_ALGO_COUNT)
  {
    name = "POST_DEADTIME_ALGORITHM_RATE_AFTER_PRESCALE";
  }
  else if (key == ugt::FINOR_COUNT)
  {
    name = "PHYSICS_GENERATED_FDL_GT";
  }
  else
  {
    std::stringstream ss;
    ss << "ugtdbi::getScalerName '" << key << "' unknown";
    throw std::runtime_error(ss.str());
  }

  return name;
}


const std::string
getScalerType(tscandela::DatabaseConnection* dbc,
              const std::string& name)
{
  std::map<std::string, std::string> query;
  query["table"] = "dual";
  query["columns"] = "get_scaler_type('" + name + "')";
  query["option"] = "";

  std::string rc = "999";
  if (not ugtdbiNoDatabase_)
  {
    // DB available
    std::map<std::string, std::string> table;
    fillTable(dbc, "CMS_UGT_MON.SCALER_NAMES", query, table);
    if (table.size() != 1)
      throw std::runtime_error("ugtdbi::getScalerType");

    auto it = table.begin();
    rc = it->second;
  }
  else
  {
    // DB not available
    std::ostringstream m;
    m << rc << "_" << name;
    rc = m.str();
  }
  return rc;
}


void
fillAlgoSetting(tscandela::DatabaseConnection *dbc,
                const ugt::TriggerMenu& menu,
                log4cplus::Logger* logger)
{
  if (logger) LOG4CPLUS_INFO(*logger, "ugtdbi::fillAlgoSetting:");

  std::vector<std::map<std::string, std::string> > setting;
  for (ugt::TriggerMenu::const_iterator cit = menu.begin(); cit != menu.end(); cit++)
  {
    const ugt::Algorithm* algorithm = cit->second;

    std::map<std::string, std::string> table;
    table.insert(std::make_pair("run_number", ugtdbiRunNumberString_));
    table.insert(std::make_pair("algo_index", toString(algorithm->getIndex())));
    table.insert(std::make_pair("algo_name", algorithm->getName()));
    table.insert(std::make_pair("algo_mask", toString(algorithm->getMask())));
    table.insert(std::make_pair("algo_veto", toString(algorithm->getVeto())));

    if (ugtdbiNoDatabase_)
    {
      // std::cout
      //   << std::setw(3) << algorithm->
      //   << " " << algorithm->module_id
      //   << " " << std::setw(3) << algorithm->local_id
      //   << " " << algorithm->name
      //   << " " << algorithm->mask
      //   << " " << algorithm->veto
      //   << "\n";
      algorithm->show();
    }
    setting.push_back(table);
  }

  if (not ugtdbiNoDatabase_)
  {
    if (logger) LOG4CPLUS_INFO(*logger, "ugtdbi::fillAlgoSetting:DB insert prepared");
    insert(dbc, "CMS_UGT_MON.UGT_RUN_ALGO_SETTING", setting);
    if (logger) LOG4CPLUS_INFO(*logger, "ugtdbi::fillAlgoSetting:DB insert done");
  }
}


void
fillPrescales(tscandela::DatabaseConnection* dbc,
              const std::map<unsigned long, std::map<unsigned long, double> >& prescales,
              log4cplus::Logger* logger)
{
  if (logger) LOG4CPLUS_INFO(*logger, "ugtdbi::fillPrescales:");

  // <ps-idx, <algo-idx, prescale> >
  std::vector<std::map<std::string, std::string> > setting;
  for (auto it1 = prescales.begin(); it1 != prescales.end(); it1++)
  {
    const unsigned long ps_id = it1->first;
    for (auto it2 = it1->second.begin(); it2 != it1->second.end(); it2++)
    {
      const unsigned long algo_id = it2->first;
      const double prescale = it2->second;

      std::map<std::string, std::string> table;
      table.insert(std::make_pair("run_number", ugtdbiRunNumberString_));
      table.insert(std::make_pair("prescale_index", toString(ps_id)));
      table.insert(std::make_pair("algo_index", toString(algo_id)));
      table.insert(std::make_pair("prescale", toString(prescale, ugt::kPrescalePrecision)));
      setting.push_back(table);

      if (ugtdbiNoDatabase_)
      {
        std::ostringstream oss;
        oss << std::setw(7) << ugtdbiRunNumber_;
        oss << " " << ps_id;
        oss << " " << std::setw(3) << algo_id;
        oss << " " << table["prescale"];
        std::cout << oss.str() << "\n";
      }
    }
  }

  if (not ugtdbiNoDatabase_)
  {
    if (logger) LOG4CPLUS_INFO(*logger, "ugtdbi::fillPrescales:DB insert prepared");
    insert(dbc, "CMS_UGT_MON.UGT_RUN_PRESCALE", setting);
    if (logger) LOG4CPLUS_INFO(*logger, "ugtdbi::fillPrescales:DB insert done");
  }
}


void
fillPrescaleNames(tscandela::DatabaseConnection* dbc,
    const std::map<unsigned long, std::string>& prescaleNameSet,
    log4cplus::Logger* logger)
{
  if (logger) LOG4CPLUS_INFO(*logger, "ugtdbi::fillPrescaleNames:");

  std::vector<std::map<std::string, std::string> > setting;
  for (const auto& cit : prescaleNameSet)
  {
    const unsigned long prescaleIndex = cit.first;
    const std::string prescaleName = cit.second;

    std::map<std::string, std::string> table;
    table.insert(std::make_pair("run_number", ugtdbiRunNumberString_));
    table.insert(std::make_pair("prescale_index", toString(prescaleIndex)));
    table.insert(std::make_pair("prescale_name", prescaleName));

    if (ugtdbiNoDatabase_)
    {
      std::cout
        << std::setw(7) << ugtdbiRunNumber_
        << " " << std::setw(3) << prescaleIndex
        << " " << prescaleName
        << std::endl;
    }
    setting.push_back(table);
  }

  if (not ugtdbiNoDatabase_)
  {
    if (logger) LOG4CPLUS_INFO(*logger, "ugtdbi::fillPrescaleNames:DB insert prepared");
    insert(dbc, "CMS_UGT_MON.RUN_PRESCALENAMES", setting);
    if (logger) LOG4CPLUS_INFO(*logger, "ugtdbi::fillPrescaleNames:DB insert done");
  }
}

void
fillAlgoBxMask(tscandela::DatabaseConnection* dbc,
               const std::map<unsigned long, std::map<unsigned long, unsigned long> >& algoBxMask,
               log4cplus::Logger* logger)
{
  if (logger) LOG4CPLUS_INFO(*logger, "ugtdbi::fillAlgoBxMask:");

  // <algo-idx, <bx-id, value> >

  std::vector<std::map<std::string, std::string> > setting;
  for (auto it1 = algoBxMask.begin(); it1 != algoBxMask.end(); it1++)
  {
    const unsigned long algo_id = it1->first;
    for (auto it2 = it1->second.begin(); it2 != it1->second.end(); it2++)
    {
      const unsigned long bx_id = it2->first;
      const unsigned long value = it2->second;

      std::map<std::string, std::string> table;
      table.insert(std::make_pair("run_number", ugtdbiRunNumberString_));
      table.insert(std::make_pair("algo_index", toString(algo_id)));
      table.insert(std::make_pair("bx_id", toString(bx_id)));
      table.insert(std::make_pair("algo_bx_mask", toString(value)));

      if (ugtdbiNoDatabase_)
      {
        // std::cout
        //   << std::setw(7) << ugtdbiRunNumber_
        //   << " " << std::setw(3) << algo_id
        //   << " " << bx_id
        //   << " " << value
        //   << "\n";
      }
      setting.push_back(table);
    }
  }

  if (not ugtdbiNoDatabase_)
  {
    if (logger) LOG4CPLUS_INFO(*logger, "ugtdbi::fillAlgoBxMask:DB insert prepared");
    insert(dbc, "CMS_UGT_MON.UGT_RUN_ALGO_BX_MASK", setting);
    if (logger) LOG4CPLUS_INFO(*logger, "ugtdbi::fillAlgoBxMask:DB insert done");
  }
}


void
fillLumiSection(tscandela::DatabaseConnection* dbc,
                const unsigned long lumi_section,
                const unsigned long prescale_index,
                log4cplus::Logger* logger)
{
  if (logger) LOG4CPLUS_INFO(*logger, "ugtdbi::fillLumiSection:");

  std::map<std::string, std::string> table;

  const std::string id = getLumiSectionId(lumi_section);
  table.insert(std::make_pair("id", id));
  table.insert(std::make_pair("run_number", ugtdbiRunNumberString_));
  table.insert(std::make_pair("lumi_section", toString(lumi_section)));
  table.insert(std::make_pair("prescale_index", toString(prescale_index)));

  if (not ugtdbiNoDatabase_)
  {
    if (logger) LOG4CPLUS_INFO(*logger, "ugtdbi::fillLumiSection:DB insert prepared");
    insert(dbc, "CMS_UGT_MON.LUMI_SECTIONS", table);
    if (logger) LOG4CPLUS_INFO(*logger, "ugtdbi::fillLumiSection:DB insert done");
  }
  else
  {
    std::cout << "ugtdbg::fillLumiSection:\n";
    std::cout << "  id = " << id
              << "  run_number = " << ugtdbiRunNumber_
              << "  lumi_section = " << lumi_section
              << "  prescale_index = " << prescale_index << "\n";
  }
}


void
fillAlgoScalers(tscandela::DatabaseConnection *dbc,
                const ugt::LumiSectionData& ls,
                log4cplus::Logger* logger)
{
  if (logger) LOG4CPLUS_INFO(*logger, "ugtdbi::fillAlgoScalers:");

  const std::string lumi_section = getLumiSectionId(ls.lumi_section);
  for (auto it = ugt::MONITORING_DATA_KEYS.begin(); it != ugt::MONITORING_DATA_KEYS.end(); ++it)
  {
    const std::string &key = *it;
    const std::string scaler_name = getScalerName(key);
    const std::string scaler_type = getScalerType(dbc, scaler_name);

    std::vector<std::map<std::string, std::string> > scalers;
    auto rc = ls.data.equal_range(key);
    for (auto x = rc.first; x != rc.second; ++x)
    {
      const ugt::AlgoData &algo = x->second;
      const unsigned long index = algo.first;
      const std::string& data = algo.second;

      const unsigned long count = convert<unsigned long>(data);
      const double rate = ugt::kToRate*count;

      char buf[BUFSIZ];
      snprintf(buf, sizeof(buf)-1, "%15.4f", rate);
      std::map<std::string, std::string> table;
      table.insert(std::make_pair("algo_index", toString(index)));
      table.insert(std::make_pair("algo_count", data));
      table.insert(std::make_pair("algo_rate", buf));
      table.insert(std::make_pair("lumi_sections_id", lumi_section));
      table.insert(std::make_pair("scaler_type", scaler_type));
      scalers.push_back(table);

      if (ugtdbiNoDatabase_)
      {
        std::cout << " algo_index = " << table["algo_index"]
                  << " algo_count = " << table["algo_count"]
                  << " algo_rate = " << table["algo_rate"]
                  << " lumi_sections_id = " << table["lumi_sections_id"]
                  << " scaler_type = " << table["scaler_type"]
                  << "\n";
      }

      //
      // for run summary
      //
      boost::lock_guard<boost::mutex> lGuard(ugtdbiMutex_);
      if (ugtdbiRunSummaryData_.num_ls) // not the first ls
      {
        ugt::SummaryData::iterator it1;
        it1 = ugtdbiRunSummaryData_.data.find(key);
        if (it1 == ugtdbiRunSummaryData_.data.end())
        {
          std::stringstream ss;
          ss << "ugtdbi::fillAlgoScalers::no run summary data found for key=" << key;
          throw std::runtime_error(ss.str());
        }

        ugt::AlgoSummaryData::iterator it2;
        it2 = (it1->second).find(index);
        if (it2 == (it1->second).end())
        {
          std::stringstream ss;
          std::cout << "index = " << index << std::endl;
          ss << "ugtdbi::fillAlgoScalers::no algo summary data found for index=" << index;
          throw std::runtime_error(ss.str());
        }
        it2->second += count;
      }
      else // the first ls
      {
        ugt::SummaryData::iterator it;
        it = ugtdbiRunSummaryData_.data.find(key);
        if (it != ugtdbiRunSummaryData_.data.end()) // counter already registered
        {
          ugt::AlgoSummaryData& algoSummary = it->second;
          auto rc = algoSummary.insert(std::make_pair(index, count));
          if (not rc.second)
          {
            std::stringstream ss;
            ss << "ugtdbi::fillAlgoScalers::(counter already registered) counter in run summary data already registered for index=" << index;
            throw std::runtime_error(ss.str());
          }
        }
        else // counter not registered yet
        {
          ugt::AlgoSummaryData algoSummary;
          auto rc = algoSummary.insert(std::make_pair(index, count));
          if (not rc.second)
          {
            std::stringstream ss;
            ss << "ugtdbi::fillAlgoScalers::(counter not registered) counter in run summary data already registered for index=" << index;
            throw std::runtime_error(ss.str());
          }
          ugtdbiRunSummaryData_.data.insert(std::make_pair(key, algoSummary));
        }
      }
    }

    if (not ugtdbiNoDatabase_)
    {
      if (logger) LOG4CPLUS_INFO(*logger, "ugtdbi::fillAlgoScalers:DB insert prepared");
      insert(dbc, "CMS_UGT_MON.ALGO_SCALERS", scalers);
      if (logger) LOG4CPLUS_INFO(*logger, "ugtdbi::fillAlgoScalers:DB insert done");
    }
  }

  boost::lock_guard<boost::mutex> lGuard(ugtdbiMutex_);
  if (ugtdbiRunSummaryData_.num_ls == 0)
  {
    ugtdbiRunSummaryData_.ls_first = ls.lumi_section;
  }
  ugtdbiRunSummaryData_.ls_last = ls.lumi_section;
  ugtdbiRunSummaryData_.num_ls++;
}


void
fillAlgoScalersSummary(tscandela::DatabaseConnection* dbc,
                       log4cplus::Logger* logger)
{
  if (logger) LOG4CPLUS_INFO(*logger, "ugtdbi::fillAlgoScalersSummary:");

  for (auto it = ugt::MONITORING_DATA_KEYS.begin(); it != ugt::MONITORING_DATA_KEYS.end(); ++it)
  {
    const std::string &key = *it;
    const std::string scaler_name = getScalerName(key);
    const std::string scaler_type = getScalerType(dbc, scaler_name);

    std::vector<std::map<std::string, std::string> > scalers;
    auto rc = ugtdbiRunSummaryData_.data.find(key);
    if (rc == ugtdbiRunSummaryData_.data.end()) continue;

    double toRate = 1./(ugt::kLuminositySegmentInSeconds * (ugtdbiRunSummaryData_.num_ls));
    const ugt::AlgoSummaryData &algo = rc->second;
    for (auto it = algo.begin(); it != algo.end(); ++it)
    {
      const unsigned long index = it->first;
      unsigned long long count = it->second;
      double rate = toRate*count;

      char buf[BUFSIZ];
      snprintf(buf, sizeof(buf)-1, "%15.4f", rate);
      std::map<std::string, std::string> table;
      table.insert(std::make_pair("run_number", ugtdbiRunNumberString_));
      table.insert(std::make_pair("algo_index", toString(index)));
      table.insert(std::make_pair("algo_count", toString(count)));
      table.insert(std::make_pair("algo_rate", buf));
      table.insert(std::make_pair("scaler_type", scaler_type));
      scalers.push_back(table);

      if (ugtdbiNoDatabase_)
      {
        std::cout << " run_number = " << table["run_number"]
                  << " algo_index = " << table["algo_index"]
                  << " algo_count = " << table["algo_count"]
                  << " algo_rate = " << table["algo_rate"]
                  << " scaler_type = " << table["scaler_type"]
                  << "\n";
      }
    }

    if (not ugtdbiNoDatabase_)
    {
      if (logger) LOG4CPLUS_INFO(*logger, "ugtdbi::fillAlgoScalersSummary:DB insert prepared");
      insert(dbc, "CMS_UGT_MON.ALGO_SCALERS_SUMMARY", scalers);
      if (logger) LOG4CPLUS_INFO(*logger, "ugtdbi::fillAlgoScalersSummary:DB insert done");
    }
  }
}


void
setRunNumber(const unsigned int run_number)
{
  std::cout << "ugtdbi::setRunNumber\n";

  std::stringstream id;
  id << std::setfill('0') << std::setw(7) << run_number << "_";

  boost::lock_guard<boost::mutex> lGuard(ugtdbiMutex_);
  ugtdbiRunNumber_ = run_number;
  ugtdbiIdRunNumber_ = id.str();
  ugtdbiRunNumberString_ = toString(run_number);
}


unsigned int
getRunNumber(const unsigned int run_number)
{
  std::cout << "ugtdbi::getRunNumber\n";

  boost::lock_guard<boost::mutex> lGuard(ugtdbiMutex_);
  return ugtdbiRunNumber_;
}


void
disableDatabaseAccess()
{
  std::cout << "ugtdbi::disableDatabaseAccess\n";

  boost::lock_guard<boost::mutex> lGuard(ugtdbiMutex_);
  ugtdbiNoDatabase_ = true;
}


void
enableDatabaseAccess()
{
  std::cout << "ugtdbi::enableDatabaseAccess\n";

  boost::lock_guard<boost::mutex> lGuard(ugtdbiMutex_);
  ugtdbiNoDatabase_ = false;
}


void
reset()
{
  std::cout << "ugtdbi::reset\n";

  boost::lock_guard<boost::mutex> lGuard(ugtdbiMutex_);
  ugtdbiRunNumber_ = 0;
  ugtdbiIdRunNumber_ = "0000000_";
  ugtdbiRunNumberString_ = "0";
  ugtdbiNoDatabase_ = true;
  ugtdbiRunSummaryData_.reset();
}


template unsigned long convert<unsigned long>(const std::string& s);

template std::string toString<unsigned long>(const unsigned long d);
template std::string toString<unsigned long long>(const unsigned long long d);

} // namespace ugtdbi
