#include "ugt/ts/cell/MonitoringDataProducer.h"

#include "ugt/ts/cell/Cell.h"
#include "ugt/ts/cell/DatabaseInterface.h"
#include "ugt/swatch/uGtProcessor.h"
#include "ugt/swatch/uGtAlgo.h"
#include "ugt/swatch/uGtFinorProcessor.h"
#include "ugt/swatch/uGtFinorAlgo.h"

// SWATCH cell headers
#include "swatchcell/framework/CellAbstract.h"

// SWATCH headers
#include "swatch/system/System.hpp"
#include "swatch/core/AbstractMetric.hpp"

// boost headers
#include "boost/thread/lock_guard.hpp"
#include "boost/algorithm/string.hpp"

// MP7 Headers
#include "swatch/mp7/MP7TTCInterface.hpp"


namespace ugt {

MonitoringDataProducer::MonitoringDataProducer(const std::string& aParentLoggerName, swatchcellframework::CellContext& aContext, size_t aPeriodInSeconds) :
  mLogger(log4cplus::Logger::getInstance(aParentLoggerName + ".MonitoringDataProducer")),
  mContext(aContext),
  mPeriod(aPeriodInSeconds),
  mActive(false),
  mUpdateNow(false),
  mSelfDestruct(false),
  mNextUpdateTime(),
  mThread(boost::ref(*this)),
  lastProcessedLumiSection_(0),
  lumiSections_(),
  fifo_()
{
  LOG4CPLUS_INFO(mLogger, "Created monitoring thread (period = " << aPeriodInSeconds << "s)");
}


MonitoringDataProducer::~MonitoringDataProducer()
{
  LOG4CPLUS_INFO(mLogger, "Destroying monitoring thread");
  boost::lock_guard<boost::mutex> lGuard(mMutex);
  mSelfDestruct = true;
  mConditionVar.notify_one();
}


size_t MonitoringDataProducer::getPeriod() const
{
  boost::lock_guard<boost::mutex> lGuard(mMutex);
  return mPeriod;
}


void MonitoringDataProducer::setPeriod(size_t aPeriod)
{
  LOG4CPLUS_INFO(mLogger, "Producer thread period set to " << aPeriod << " seconds");
  boost::lock_guard<boost::mutex> lGuard(mMutex);
  mPeriod = aPeriod;
  mNextUpdateTime = Clock_t::now() + boost::chrono::seconds(mPeriod);
  mConditionVar.notify_one();
}


bool MonitoringDataProducer::isActive() const
{
  boost::lock_guard<boost::mutex> lGuard(mMutex);
  return mActive;
}


void MonitoringDataProducer::start()
{
  LOG4CPLUS_INFO(mLogger, "Starting montoring thread (period = " << mPeriod << "s)");
  boost::lock_guard<boost::mutex> lGuard(mMutex);

  if (!mActive) {
    mActive = true;
    mNextUpdateTime = Clock_t::now();
    mConditionVar.notify_one();
  }

  // reset
  lastProcessedLumiSection_ = 0;
  lumiSections_.clear();

  std::queue<LumiSectionData> empty;
  std::swap(fifo_, empty);
}


void MonitoringDataProducer::stop()
{
  LOG4CPLUS_INFO(mLogger, "Stopping monitoring thread");
  boost::lock_guard<boost::mutex> lGuard(mMutex);
  mActive = false;
}


void MonitoringDataProducer::updateNow()
{
  LOG4CPLUS_INFO(mLogger, "Producer thread will update monitoring data again ASAP");
  boost::lock_guard<boost::mutex> lGuard(mMutex);
  mUpdateNow = true;
  mConditionVar.notify_one();
}


void MonitoringDataProducer::operator() ()
{
  while (true) {
    LOG4CPLUS_DEBUG(mLogger, "Producer thread: operator()()  >>  Top of while(true)");
    {
      boost::unique_lock<boost::mutex> lGuard(mMutex);

      while (!((mActive && (mNextUpdateTime <= Clock_t::now())) || mUpdateNow || mSelfDestruct)) {
        if (mActive) {
          mConditionVar.wait_until(lGuard, mNextUpdateTime);
        } else
          mConditionVar.wait(lGuard);
      }
      mUpdateNow = false;

      // In case this instance is being destroyed, then return so that the thread can stop
      if (mSelfDestruct) {
        LOG4CPLUS_INFO(mLogger, "Producer thread: Main loop returning now");
        return;
      }
      // Otherwise, proceed with our main purpose (updating monitoring data) after unlocking mMutex
    }

    LOG4CPLUS_DEBUG(mLogger, "Producer thread: Updating monitoring data");

    Clock_t::time_point lUpdateStartTime = Clock_t::now();

    assembleMetrics();

    LOG4CPLUS_DEBUG(mLogger, "Producer thread: Finished updating monitoring data");

    boost::unique_lock<boost::mutex> lGuard(mMutex);
    mNextUpdateTime = lUpdateStartTime + boost::chrono::seconds(mPeriod);
  }
}


void MonitoringDataProducer::getLumiSectionData(::ugt::LumiSectionData& data)
{
  data.is_valid = false;

  boost::lock_guard<boost::mutex> lGuard(mMutex);
  if (fifo_.empty()) return;
  data = fifo_.front();
  data.is_valid = true;
  fifo_.pop();
}


void MonitoringDataProducer::assembleMetrics()
{
  LOG4CPLUS_INFO(mLogger, "assembleMetrics: start ...");

  std::vector<uint32_t> oc0Counters;
  std::vector<uint32_t> currentLumiSegments;

  ugt::Cell* cell = dynamic_cast<ugt::Cell*>(mContext.getCell());
  const std::vector<const uGtProcessor*> uGtProcessors = cell->getUgtProcessors();
  const std::vector<const uGtFinorProcessor*> finorProcessor = cell->getFinorProcessor();

  // Retrieve luminositySegmentNumber metric from FinorProcessor board
  uint32_t finorCurrentLumiSegment = 0;
  for (auto it = finorProcessor.begin(); it != finorProcessor.end(); ++it)
  {
    const uGtFinorProcessor& processor = dynamic_cast<const uGtFinorProcessor&>(*(*it));
    const uGtFinorAlgo& algo = dynamic_cast<const uGtFinorAlgo&>(processor.getAlgo());

    const ::swatch::core::AbstractMetric& metric_ls = algo.getMetric("luminositySegmentNumber");
    const ::swatch::core::MetricSnapshot& ss_ls = metric_ls.getSnapshot();
    LOG4CPLUS_INFO(mLogger, "assembleMetrics: snapshot uGtFinorProcessor metric luminositySegmentNumber ...");
    if (not ss_ls.isValueKnown())
    {
      LOG4CPLUS_WARN(mLogger, "assembleMetrics: MetricSnapshot luminositySegmentNumber for uGtFinorProcessor is unknown [return]");
      return;
    }
    finorCurrentLumiSegment = ss_ls.getValue<uint32_t>();
  }

  // Check metrics consistency
  for (auto it = uGtProcessors.begin(); it != uGtProcessors.end(); ++it)
  {
    const uGtProcessor& processor = dynamic_cast<const uGtProcessor&>(*(*it));
    const uGtAlgo& algo = dynamic_cast<const uGtAlgo&>(processor.getAlgo());
    const swatch::mp7::MP7TTCInterface& ttc = dynamic_cast<const swatch::mp7::MP7TTCInterface&>(processor.getTTC());

    LOG4CPLUS_INFO(mLogger, "assembleMetrics: processor slot #" << processor.getSlot());

    LOG4CPLUS_INFO(mLogger, "assembleMetrics: get ttc metric oc0Counter ...");
    const ::swatch::core::AbstractMetric& metric_oc0 = ttc.getMetric("oc0Counter");
    LOG4CPLUS_INFO(mLogger, "assembleMetrics: get snapshot of oc0Counter ...");
    const ::swatch::core::MetricSnapshot& ss_oc0 = metric_oc0.getSnapshot();
    if (not ss_oc0.isValueKnown())
    {
      LOG4CPLUS_WARN(mLogger, "assembleMetrics: MetricSnapshot oc0Counter is unknown [return]");
      return;
    }

    uint32_t oc0counter = ss_oc0.getValue<uint32_t>();
    LOG4CPLUS_INFO(mLogger, "assembleMetrics: push back (oc0Counter = " << oc0counter << ") ...");
    oc0Counters.push_back(oc0counter);

    if (oc0Counters.size() and oc0Counters.front() != oc0Counters.back())
    {
      LOG4CPLUS_WARN(mLogger, "assembleMetrics: oc0 counter mismatch: " << oc0Counters.front() << " != " << oc0Counters.back());
    }

    LOG4CPLUS_INFO(mLogger, "assembleMetrics: get algo metric luminositySegmentNumber ...");
    const ::swatch::core::AbstractMetric& metric_ls = algo.getMetric("luminositySegmentNumber");
    LOG4CPLUS_INFO(mLogger, "assembleMetrics: get snapshot of luminositySegmentNumber ...");
    const ::swatch::core::MetricSnapshot& ss_ls = metric_ls.getSnapshot();
    if (not ss_ls.isValueKnown())
    {
      LOG4CPLUS_WARN(mLogger, "assembleMetrics: MetricSnapshot luminositySegmentNumber is unknown [return]");
      return;
    }
    uint32_t currentLumiSegment = ss_ls.getValue<uint32_t>();
    LOG4CPLUS_INFO(mLogger, "assembleMetrics: push back (currentLumiSegment = " << currentLumiSegment << ") ...");
    currentLumiSegments.push_back(currentLumiSegment);

    if (currentLumiSegments.size() and currentLumiSegments.front() != currentLumiSegments.back())
    {
      LOG4CPLUS_WARN(mLogger, "assembleMetrics: lumi segment mismatch: " << currentLumiSegments.front() << " != " << currentLumiSegments.back());
    }
  }

  if (currentLumiSegments.front() != finorCurrentLumiSegment)
  {
      LOG4CPLUS_WARN(mLogger, "assembleMetrics: lumi segment mismatch of FinorProcessor: " << currentLumiSegments.front() << " != " << finorCurrentLumiSegment);
  }
  LOG4CPLUS_INFO(mLogger, "assembleMetrics: Compared all luminositySegmentNumbers ...");

  unsigned int oc0Counter = oc0Counters.front();
  unsigned int currentLumiSegment = currentLumiSegments.front();

  // check OC0 counter
  LOG4CPLUS_INFO(mLogger, "assembleMetrics: oc0 counter = " << oc0Counter);
  if (oc0Counter < 1)
  {
    LOG4CPLUS_INFO(mLogger, "assembleMetrics: oc0 counter < 1 [return]");
    return;
  }
  if (oc0Counter > 1)
  {
    LOG4CPLUS_WARN(mLogger, "assembleMetrics: oc0 counter > 1: (counter = " << oc0Counter << ")");
  }

  // check lumisegment`
  if (lastProcessedLumiSection_ == currentLumiSegment)
  {
    LOG4CPLUS_INFO(mLogger, "assembleMetrics: LS = " << currentLumiSegment << " [return]");
    return;
  }
  else if (lastProcessedLumiSection_ + 1 == currentLumiSegment)
  {
    LOG4CPLUS_INFO(mLogger, "assembleMetrics: LS change: prev LS = " << lastProcessedLumiSection_ << ", curr LS = " << currentLumiSegment);
  }
  else // jump;
  {
    LOG4CPLUS_WARN(mLogger, "assembleMetrics: LS jump: prev LS = " << lastProcessedLumiSection_ << ", curr LS = " << currentLumiSegment);
  }
  lastProcessedLumiSection_ = currentLumiSegment;

  // Start publishing at LS=2 (then counters contain values from previous LS=1)
  if (lastProcessedLumiSection_ < 2) // valid count from LS = 1
  {
    LOG4CPLUS_INFO(mLogger, "assembleMetrics: last processed lumisection: " << lastProcessedLumiSection_ << " is < 2 [return]");
    return;
  }

  LumiSectionData data;

  // Loop over processors
  // Retrieve all counters for all MONITORING_DATA_KEYS except FINOR_COUNT
  for (auto it = uGtProcessors.begin(); it != uGtProcessors.end(); ++it)
  {
    const uGtProcessor& ugt = dynamic_cast<const uGtProcessor&>(*(*it));
    const uGtAlgo& algo = dynamic_cast<const uGtAlgo&>(ugt.getAlgo());

    swatch::core::MetricReadGuard metric_guard(algo);

    // grab all metrics data
    std::vector<std::string> names = algo.getMetrics();
    for (size_t ii = 0; ii < names.size(); ++ii)
    {
      if (it == uGtProcessors.begin())
      {
        const ::swatch::core::AbstractMetric& metric_ps = algo.getMetric("prescaleColumnIndexInUse");
        const ::swatch::core::MetricSnapshot& ss_ps = metric_ps.getSnapshot();
        data.prescale_index = ss_ps.getValue<uint32_t>();
      }


      std::vector<std::string> tokens;
      boost::split(tokens, names.at(ii), boost::is_any_of(SEPARATOR));
      if (tokens.size() != 2)
        continue;

      const std::string& type = tokens.at(0);
      if (MONITORING_DATA_KEYS.find(type) == MONITORING_DATA_KEYS.end()
        || MONITORING_DATA_KEYS.find(type) == MONITORING_DATA_KEYS.find(FINOR_COUNT))
        continue;

      const unsigned long index = std::stoul(tokens.at(1));

      const ::swatch::core::AbstractMetric& metric = algo.getMetric(names.at(ii));
      const ::swatch::core::MetricSnapshot& ss = metric.getSnapshot();
      if (ss.getMonitoringStatus() != swatch::core::monitoring::kEnabled)
        continue;

      data.data.insert(std::pair<std::string, AlgoData>(type, AlgoData(index, ss.getValueAsString())));

    } // for names
  } // for processors

  // Retrieve PhysicsGeneratedFDL counter/rate
  for (auto it = finorProcessor.begin(); it != finorProcessor.end(); ++it)
  {
    const uGtFinorProcessor& finor = dynamic_cast<const uGtFinorProcessor&>(*(*it));
    const uGtFinorAlgo& algo = dynamic_cast<const uGtFinorAlgo&>(finor.getAlgo());

    { // BLOCK Finor TCDS counter
      const ::swatch::core::AbstractMetric& metric = algo.getMetric("FinorTCDSCounter");
      const ::swatch::core::MetricSnapshot& ss = metric.getSnapshot();
      if (ss.getMonitoringStatus() != swatch::core::monitoring::kEnabled)
        continue;

      data.data.insert(std::pair<std::string, AlgoData>(FINOR_COUNT, AlgoData(kPhysicsGenFDLIndex, ss.getValueAsString())));
    }
  } // for finorProcesors

  // check the consistency of luminosity segment ?
  data.is_valid = true;
  data.lumi_section = currentLumiSegment - 1;

  boost::lock_guard<boost::mutex> guard(mMutex);
  fifo_.push(data);

  LOG4CPLUS_INFO(mLogger, "assembleMetrics: fifo size = " << fifo_.size());
  LOG4CPLUS_INFO(mLogger, "assembleMetrics: end ...");
}

} // ns: ugt
