#include "ugt/ts/cell/panels/PreviewPanel.h"

#include "ugt/swatch/Constants.h"
#include "ugt/swatch/uGtProcessor.h"
#include "ugt/swatch/uGtAlgo.h"
#include "ugt/swatch/uGtFinorPreviewProc.h"
#include "ugt/swatch/uGtFinorPreviewAlgo.h"

#include "ugt/swatch/commands/uGtPreparePreviewPrescaleCommand.h"
#include "ugt/swatch/commands/uGtApplyPreviewPrescaleCommand.h"

#include "swatchcell/framework/CellContext.h"
#include "swatch/core/ReadWriteXParameterSet.hpp"
#include "swatch/action/GateKeeper.hpp"

#include <boost/pointer_cast.hpp>

#include "ajax/toolbox.h"
#include "ajax/PolymerElement.h"
#include "jsoncpp/json/json.h"

#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"

#include <time.h>
#include <string>
#include <locale>


using namespace ugt;


// Metric keys
const std::string kPreviewPrescaleIndex = "prescColIndexPreview";
const std::string kPreviewPrescaleIndexInUse = "prescColIndexInUsePreview";
const std::string kLuminositySegmentNumber = "luminositySegmentNumber";
const std::string kRateFinOrPreview = "FinorPreviewRate";
const std::string kPreviewOtf = "prescOtfPreview";
const std::string kPreviewOtfInUse = "prescOtfInUsePreview";

const std::string kMetricValueUnknown = "n/a";


const std::string kAlgo = "algo";
const std::string kIndex = "index";
const std::string kRate = "rate";
const std::string kRateCounter = "ratecounter";
const std::string kPrescale = "prescale";
const std::string kFinor = "finor";
const std::string kBxMask = "bxmask";
const std::string kOtfPrescale = "hasOtfPrescale";

const std::string kPrescaleInUse = "presc";

const std::string kError = "error";

const std::string kClass = "class";


PreviewPanel::PreviewPanel( tsframework::CellAbstractContext* context, log4cplus::Logger& logger ) : tsframework::CellPanel(context,logger)
{
    mLumisectionPrev = 0;
    mLumisectionPrescChanged = 1;
    mWillChangeToPrescale = 0;
    mUsingOtf = false;
}


PreviewPanel::~PreviewPanel()
{
    remove();
}


void
PreviewPanel::layout(cgicc::Cgicc& cgi)
{
  remove();
  setEvent("PreviewPanel::getVersion", ajax::Eventable::OnClick, this, &PreviewPanel::getVersion);
  setEvent("PreviewPanel::allProcessorsAvailable", ajax::Eventable::OnClick, this, &PreviewPanel::allProcessorsAvailable);
  setEvent("PreviewPanel::getTable", ajax::Eventable::OnTime, this, &PreviewPanel::getTable);
  setEvent("PreviewPanel::isSystemReady", ajax::Eventable::OnTime, this, &PreviewPanel::isSystemReady);
  setEvent("PreviewPanel::setupSortOptions", ajax::Eventable::OnClick, this, &PreviewPanel::setupSortOptions);
  setEvent("PreviewPanel::changePreviewPrescale", ajax::Eventable::OnClick, this, &PreviewPanel::changePreviewPrescale);
  setEvent("PreviewPanel::refreshData", ajax::Eventable::OnTime, this, &PreviewPanel::refreshData);
  setEvent("PreviewPanel::getCurrentPreviewPrescales", ajax::Eventable::OnClick, this, &PreviewPanel::getCurrentPreviewPrescales);

  add(new ajax::PolymerElement("preview-panel"));

  mPrescaleIndexPrev = 0;
  mLumisectionPrescChanged = 1;
  mUsingOtf = false;

  try
  {
    if (isSystemReady())
    {
      mPrescaleIndexPrev = getPreviewPrescaleIndexPrevious();
      mWillChangeToPrescale = getPreviewPrescaleIndex();
      mUsingOtf = isUsingPreviewOtf();
    }
  }
  catch (MetricValueUnknownException &e)
  {
    LOG4CPLUS_ERROR(getLogger(), e.what());
  }
}

std::vector<const uGtProcessor*>
PreviewPanel::getProcessors()
{
  std::vector<const uGtProcessor*> processors;

  swatchcellframework::CellContext *context = dynamic_cast<swatchcellframework::CellContext*>(getContext());
  swatchcellframework::CellContext::SharedGuard_t lGuard(*context);
  ugt::Cell* cell = dynamic_cast<ugt::Cell*>(context->getCell());
  processors = cell->getUgtProcessors();

  return processors;
}

std::vector<const uGtFinorPreviewProc*>
PreviewPanel::getFinorPreviewProcessor()
{
  std::vector<const uGtFinorPreviewProc*> finorPreviewProcessor;

  swatchcellframework::CellContext *context = dynamic_cast<swatchcellframework::CellContext*>(getContext());
  swatchcellframework::CellContext::SharedGuard_t lGuard(*context);
  ugt::Cell* cell = dynamic_cast<ugt::Cell*>(context->getCell());
  finorPreviewProcessor = cell->getFinorPreviewProcessor();

  return finorPreviewProcessor;
}


const uGtProcessor*
PreviewPanel::getProcessorModule(const uint32_t moduleId)
{
  if (isSystemReady())
  {
    swatchcellframework::CellContext *context = dynamic_cast<swatchcellframework::CellContext*>(getContext());
    swatchcellframework::CellContext::SharedGuard_t lGuard(*context);
    ugt::Cell* cell = dynamic_cast<ugt::Cell*>(context->getCell());
    return cell->getUgtProcessorModule(moduleId);
  }

  return NULL;
}


void
PreviewPanel::getTable(cgicc::Cgicc& cgi,std::ostream& out)
{
  const std::string kAlgo = "algo";
  const std::string kIndex = "index";
  const std::string kRate = "rate";
  const std::string kRateCounter = "ratecounter";
  const std::string kPrescale = "prescale";
  const std::string kFinor = "finor";
  const std::string kBxMask = "bxmask";
  const std::string kOtfPrescale = "hasOtfPrescale";

  const std::string kPrescaleInUse = "presc";

  const std::string kError = "error";

  const std::string kClass = "class";

  Json::Value root;

  if (isSystemReady())
  {
    OtfPreviewSet& otfPreviewSet = getOtfPreviewSet();
    toolbox::PrescaleSet& pSet = otfPreviewSet.prescaleSet;
    try
    {
      if (isUsingPreviewOtfPrevious())
      {
        mPrescaleIndexPrev = pSet.getIndex();
      }
      else
        mPrescaleIndexPrev = getPreviewPrescaleIndexPrevious();
      root[kPrescaleInUse] = mPrescaleIndexPrev;
    }
    catch (MetricValueUnknownException &e)
    {
      LOG4CPLUS_ERROR(getLogger(), e.what());
      root[kPrescaleInUse] = kMetricValueUnknown;
    }


    try
    {
      // prepare table rows and fill with data from algos
      fillRowsFromMenu();
      if (mRows.empty())
      {
        LOG4CPLUS_WARN(getLogger(), "PreviewPanel: rows set is empty");
        return;
      }

      // Prepare dropdown entries with prescale label (or currently index)
      root = constructColumns(root);

      if (mShowTable)
      {
        // fill json array with each row
        root = constructRows(root);
      }
      else
      {
        // hide table
        out << root;
        return;
      }
    }
    catch (MetricValueUnknownException &e)
    {
      LOG4CPLUS_ERROR(getLogger(), e.what());
      root[kError] = e.what();
      out << root;
      return;
    }
    catch(TriggerMenuIsNullException &e)
    {
      LOG4CPLUS_ERROR(getLogger(), e.what());
      root[kError] = e.what();
      out << root;
      return;
    }
  }

  root = constructTableHeader(root);

  out << root;
}


bool
PreviewPanel::isSystemReady()
{
  return isSystemRunningOrPaused(getContext());
}


void
PreviewPanel::isSystemReady(cgicc::Cgicc& cgi,std::ostream& out)
{
  bool runningOrPaused = isSystemReady();
  bool configuredOrAligned = isSystemConfiguredOrAligned(getContext());
  bool notConfigured = isSystemNotConfigured(getContext());

  if (runningOrPaused)
  {
    if (getOtfPreviewSet().isEmpty()) {
      initOtfPreviewPrescales(getPreviewPrescaleIndex());
    }
  }
  else
  {
    mLumisectionPrescChanged = 1;
    mShowTable = false;
    mRows.clear();
  }

  if (notConfigured)
  {
    getOtfPreviewSet().clear();
  }

  Json::Value root;
  root["runningOrPaused"] = runningOrPaused;
  root["configuredOrAligned"] = configuredOrAligned;
  root["notConfigured"] = notConfigured;

  out << root;
}

void
PreviewPanel::allProcessorsAvailable(cgicc::Cgicc& cgi, std::ostream& out)
{
  Json::Value root;
  std::stringstream ss;
  bool isAvailable = true;
  if (getFinorPreviewProcessor().size() <= 0)
  {
    isAvailable = false;
    ss << "No FinorPreviewProcessor is available.";
  }
  if (getProcessors().size() <= 0)
  {
    isAvailable = false;
    if (ss.str().length() > 0)
      ss << " ";

    ss << "No uGtProcessor is available.";
  }

  root["isavailable"] = isAvailable;
  if (!isAvailable)
  {
    root["msg"] = ss.str();
  }

  out << root;
}


void
PreviewPanel::setupSortOptions(cgicc::Cgicc& cgi,std::ostream& out)
{
  const std::string sShowDiagnosis = ajax::toolbox::getSubmittedValue(cgi,"showDiagnosis");
  const bool isShowDiagnosis = (sShowDiagnosis=="true");

  out << getSortOptions(isShowDiagnosis);
}


void
PreviewPanel::changePreviewPrescale(cgicc::Cgicc& cgi, std::ostream& out)
{
  Json::Value root;
  const std::string keyStatus = "status";
  const std::string keyMsg = "msg";
  const std::string keyIndex = "index";
  const std::string sError = "error";
  const std::string sWarning = "warning";
  const std::string sDone = "done";

  std::stringstream ssMsg;

  const std::string sIndex = ajax::toolbox::getSubmittedValue(cgi,"selectedIndex");
  const std::string sUsingOtf = ajax::toolbox::getSubmittedValue(cgi,"usingOtfPrescales");
  const bool isUsingOtfPrescales = (sUsingOtf=="true");

  //////////////////


  LOG4CPLUS_INFO(getLogger(), "changePreviewPrescale: command started");


  const std::vector<const uGtProcessor*> uGtProcessors = getProcessors();

  if (uGtProcessors.empty())
  {
    std::ostringstream msg;
    msg << "changePreviewPrescale no uGtProcessor found";
    LOG4CPLUS_WARN(getLogger(), msg.str());
    root[keyMsg] = msg.str();
    root[keyStatus] = sError;
    out << root;
    return;
  }

  if (not isSystemReady())
  {
    std::ostringstream msg;
    msg << "changePreviewPrescale: System is not ready";
    LOG4CPLUS_INFO(getLogger(), msg.str());
    root[keyMsg] = msg.str();
    root[keyStatus] = sError;
    out << root;
    return;
  }



  if (isUsingOtfPrescales)
  {
    // using prescales on the fly

    const std::string reg = "gt_mp7_gtlfdl.preview.prescale_factor";
    const uGtTriggerMenu& menu = getUgtTriggerMenu(getContext());

    // get changed prescales from input and prepare image
    std::map<std::string, std::string> parameters = ajax::toolbox::getSubmittedValues(cgi);
    ugt::toolbox::MemoryImage<uint32_t>
    memImage = preparePrescalesImage(menu, parameters, sIndex);

    const uint32_t& previewIndex = getOtfPreviewSet().prescaleSet.getIndex();

    std::stringstream msg;
    msg << "changePreviewPrescale: Using preview prescales on the fly"
        << "based on index " << previewIndex;
    LOG4CPLUS_INFO(getLogger(), msg.str());

    // write new prescale image to all processors as well as preview prescale Index
    for (auto pit = uGtProcessors.begin(); pit != uGtProcessors.end(); ++pit)
    {
      uGtProcessor& proc = const_cast<uGtProcessor&>(*(*pit));
      ::mp7::MP7Controller& driver = (proc.driver());
      ::uhal::HwInterface& hw = driver.hw();

      // prepare preview prescales
      const uhal::Node& prescalesNode = hw.getNode(reg);
      prescalesNode.writeBlock(memImage.values());
      // prepare preview prescale index
      hw.getNode("gt_mp7_gtlfdl.preview.prescale_factor_set_index").write(previewIndex);
      // prepare preview otf flag
      hw.getNode("gt_mp7_gtlfdl.prescale_otf_flags.preview_flag").write(1);
      // write to hardware
      hw.getClient().dispatch();

    }
    LOG4CPLUS_INFO(getLogger(), "changePreviewPrescale: applied preview prescales on the fly");

  }
  else
  {
    // using prescales via dropdown menu
    LOG4CPLUS_INFO(getLogger(), "changePreviewPrescale: Using preview prescale column");

    // load prescale values to the uGtProcessors
    for (auto it = uGtProcessors.begin(); it != uGtProcessors.end(); ++it)
    {
      const uGtProcessor& ugt = dynamic_cast<const uGtProcessor&>(*(*it));
      std::set<std::string> commands = ugt.getCommands();

      std::set<std::string>::iterator sit = commands.find(cmd::kPreparePreviewPrescale);
      if (sit == commands.end())
      {
        std::ostringstream msg;
        msg << "changePreviewPrescale: command" << cmd::kPreparePreviewPrescale << " was not found";
        LOG4CPLUS_ERROR(getLogger(), msg.str());
        root[keyMsg] = msg.str();
        root[keyStatus] = sError;
        out << root;
        return;
      }

      swatchcellframework::CellContext *swContext = dynamic_cast<swatchcellframework::CellContext*>(getContext());
      swatchcellframework::CellContext::SharedGuard_t lGuard(*swContext);
      const swatch::action::GateKeeper* gk = swContext->getGateKeeper(lGuard);
      swatch::action::GateKeeper::Parameter_t gkPrescales = gk->get("",*sit,"prescales",ugt.getGateKeeperContexts());
      swatch::action::GateKeeper::Parameter_t gkFinorMask = gk->get("",*sit,"finorMask",ugt.getGateKeeperContexts());

      uGtPreparePreviewPrescaleCommand &cmd = dynamic_cast<uGtPreparePreviewPrescaleCommand &>(const_cast<uGtProcessor&>(ugt).getCommand(*sit));

      swatch::core::ReadWriteXParameterSet params;
      params.deepCopyFrom(cmd.getDefaultParams());
      params.get("index").fromString(sIndex);
      params.get("prescales").fromString(gkPrescales->toString());
      params.get("finorMask").fromString(gkFinorMask->toString());
      params.get("otf").fromString((isUsingOtfPrescales ? "1" : "0"));

      cmd.exec(params, false);

      if (cmd.getState() != swatch::action::Functionoid::State::kDone)
      {
        std::ostringstream msg;
        msg << "changePreviewPrescale: cmd " << cmd::kPreparePreviewPrescale << " status msg:" << cmd.getStatus().getStatusMsg();

        if (cmd.getState() == swatch::action::Functionoid::State::kError)
        {
          LOG4CPLUS_ERROR(getLogger(), msg.str());
          root[keyMsg] = cmd.getStatus().getStatusMsg();
          root[keyStatus] = sError;
          out << root;
          return;
        }

        LOG4CPLUS_WARN(getLogger(), msg.str());
        ssMsg << msg.str() << std::endl;
        root[keyStatus] = sWarning;
      }
    } // loop processors for preview prescale update
  }

  // secure enough time till the next lumisection boundary
  int nInconsistent = 0;
  bool bEnoughTimeLeft = false;
  uint32_t lumiSection_ref = 0;
  for (uint32_t ii = 0; ii < kRetryLimit; ++ii)
  {
    bool isInconsistent = false;
    lumiSection_ref = (dynamic_cast<const uGtAlgo&>(uGtProcessors.at(0)->getAlgo())).getLuminositySection();
    uint32_t lumiSection = 0;
    for (auto it = uGtProcessors.begin(); it != uGtProcessors.end(); ++it)
    {
      const uGtAlgo& algo = dynamic_cast<const uGtAlgo&>((*it)->getAlgo());
      lumiSection = algo.getLuminositySection();
      if (lumiSection != lumiSection_ref)
      {
        isInconsistent = true;
        ++nInconsistent;
        break;
      }
    }

    if (isInconsistent)
    {
      double sleepMs = 1000;
      LOG4CPLUS_INFO(getLogger(), "changePreviewPrescale: probably crossed lumisection boundary: "
                                  << lumiSection_ref << " != " << lumiSection
                                  << " sleep for " << sleepMs << " ms");
      boost::this_thread::sleep_for(boost::chrono::milliseconds(uint32_t(sleepMs)));
      continue;
    }


    // check the remaining time
    const uGtAlgo& algo = dynamic_cast<const uGtAlgo&>(uGtProcessors.at(0)->getAlgo());
    double remainingSeconds = algo.getSecondsBeforeLuminositySegmentUpdate(&getLogger());
    LOG4CPLUS_INFO(getLogger(), "changePreviewPrescale: seconds before lumisection boundary: " << remainingSeconds);
    if (remainingSeconds >= kUpdateThreshold)
    {
      bEnoughTimeLeft = true;
      break;
    }

    double sleepMs = (remainingSeconds+kSleepDelta)*1000;
    LOG4CPLUS_INFO(getLogger(), "changePreviewPrescale: sleep for " << sleepMs << " ms");
    boost::this_thread::sleep_for(boost::chrono::milliseconds(uint32_t(sleepMs)));
  }

  if (not bEnoughTimeLeft)
  {
    std::ostringstream msg;
    msg << "changePreviewPrescale: not enough time left till the next lumisection after " << kRetryLimit << " trials."
        << " nInconsistent = " << nInconsistent;
    LOG4CPLUS_WARN(getLogger(), msg.str());
    root[keyMsg] = msg.str();
    root[keyStatus] = sError;
    out << root;
    return;
  }


  // Enough time left for updating the prescale setting
  for (auto it = uGtProcessors.begin(); it != uGtProcessors.end(); ++it)
  {
    const uGtProcessor& ugt = dynamic_cast<const uGtProcessor&>(*(*it));
    uGtApplyPreviewPrescaleCommand &cmd = dynamic_cast<uGtApplyPreviewPrescaleCommand &>(const_cast<uGtProcessor&>(ugt).getCommand(cmd::kApplyPreviewPrescale));
    swatch::core::ReadWriteXParameterSet params;
    cmd.exec(params, false);

    if (cmd.getState() != swatch::action::Functionoid::State::kDone)
    {
      std::ostringstream msg;
      msg << "changePreviewPrescale: cmd " << cmd::kApplyPreviewPrescale << " status msg:" << cmd.getStatus().getStatusMsg();
      LOG4CPLUS_ERROR(getLogger(), msg.str());
      root[keyMsg] = msg.str();
      root[keyStatus] = sError;
      out << root;
      return;
    }

    LOG4CPLUS_INFO(getLogger(), "changePreviewPrescale: apply prescale for slot = " << (*it)->getSlot());
  }


  // check if still in the same lumisection
  bool bSuccess = true;
  for (auto it = uGtProcessors.begin(); it != uGtProcessors.end(); ++it)
  {
    const uGtAlgo& algo = dynamic_cast<const uGtAlgo&>((*it)->getAlgo());
    uint32_t lumiSection = algo.getLuminositySection();
    if (lumiSection != lumiSection_ref)
    {
      ssMsg << "changePreviewPrescale: crossed lumisection boundary during the operation, wait for the next lumisection" << std::endl;
      bSuccess = false;
      break;
    }
  }

  if (bSuccess)
  {
    LOG4CPLUS_INFO(getLogger(), "changePreviewPrescale: success");
    if (ssMsg.str().size() > 0)
    {
      root[keyStatus] = sWarning;
    }
    else
    {
      root[keyStatus] = sDone;
    }
    updateLumisectionPrescChanged(lumiSection_ref);
    mShowTable = false;
    root[keyIndex] = sIndex;
  }

  root[keyMsg] = ssMsg.str();

  LOG4CPLUS_INFO(getLogger(), "changePreviewPrescale: command finished");

  out << root;
}


void
PreviewPanel::refreshData(cgicc::Cgicc& cgi,std::ostream& out)
{
  LOG4CPLUS_DEBUG(getLogger(), "refreshData");

  const std::vector<const uGtProcessor*> uGtProcessors = getProcessors();

  const std::string kFinorRate = "finorrate";
  const std::string kFinorState = "finorstate";
  const std::string kLumiSeg = "lumiseg";
  const std::string kRefreshTable = "refreshTable";
  const std::string kShowTable = "showtable";
  const std::string kChangeToPrescaleIndex = "changetoprescaleindex";
  const std::string kChangeToPrescaleOtf = "changetoprescaleotf";

  const std::string kClassNormal = "Good";
  const std::string kClassAlert = "Error";
  const std::string kClassZero = "Unknown";
  const std::string kClassUnknown = "Unknown";


  Json::Value root;
  Json::Value processors(Json::arrayValue);

  const uGtTriggerMenu& menu = getUgtTriggerMenu(getContext());

  for (auto it = uGtProcessors.begin(); it != uGtProcessors.end(); ++it)
  {
    const uGtProcessor& ugt = dynamic_cast<const uGtProcessor&>(*(*it));
    ::mp7::MP7Controller& driver = const_cast<uGtProcessor&>(ugt).driver();
    ::uhal::HwInterface& hw = driver.hw();

    ::uhal::ValWord<uint32_t> dataCurrentPreviewPrescaleIndex = hw.getNode("gt_mp7_gtlfdl.preview.current_prescale_set_index").read();
    ::uhal::ValWord<uint32_t> dataCurrentPreviewPrescaleOtf = hw.getNode("gt_mp7_gtlfdl.prescale_preview_otf_applied.current_prescale_preview_otf").read();
    hw.getClient().dispatch();


    std::stringstream ss;

    Json::Value item;
    ss << ugt.getSlot();
    item["slot"] = ss.str();

    ss.str(""); ss.clear();
    ss << dataCurrentPreviewPrescaleIndex.value();
    item["index"] = ss.str();

    bool isOtf = false;
    if (dataCurrentPreviewPrescaleOtf.value() == 1)
    {
      isOtf = true;
    }
    item["otf"] = isOtf;

    auto itAlgo = menu.begin();
    if (itAlgo != menu.end())
    {
      const Algorithm* pAlgo = itAlgo->second;
      item["fmtlabel"] = pAlgo->getFormattedLabelForPsIndex(dataCurrentPreviewPrescaleIndex.value());
    }

    processors.append(item);

  }
  root["processors"] = processors;


/////////refresh finor
  bool refreshTab = false;

  try
  {
    double finorSum = getPreviewFinorRate();
    root[kFinorRate] = finorSum;

    if(finorSum > 100000.0)
      root[kFinorState] = kClassAlert;
    else if(finorSum == 0.0)
      root[kFinorState] = kClassZero;
    else
      root[kFinorState] = kClassNormal;
  }
  catch (MetricValueUnknownException &e)
  {
    root[kFinorRate] = kMetricValueUnknown;
    root[kFinorState] = kClassUnknown;
  }



  try
  {
    const uint32_t lumisection = getLuminositySection();
    // Note: displayed rates refer to previous luminosity segment!
    const uint32_t rateLumisection = lumisection - 1;

    uint32_t changeToPreviewPrescaleIndex = getPreviewPrescaleIndex();
    bool changeToPreviewPrescaleOtf = isUsingPreviewOtf();

    uint32_t remainingLumis = 0; // remaining lumisection for table update
    // check whether the preview prescale index or otf flag changed
    // (either due to prescale modification in PreviewPanel or PrescalePanel)
    if (mWillChangeToPrescale != changeToPreviewPrescaleIndex
      || mUsingOtf != changeToPreviewPrescaleOtf)
    {
      updateLumisectionPrescChanged(lumisection);
      mWillChangeToPrescale = changeToPreviewPrescaleIndex;
      root[kChangeToPrescaleIndex] = changeToPreviewPrescaleIndex;

      mUsingOtf = changeToPreviewPrescaleOtf;
      root[kChangeToPrescaleOtf] = changeToPreviewPrescaleOtf;

      auto itAlgo = menu.begin();
      if (itAlgo != menu.end())
      {
        const Algorithm* pAlgo = itAlgo->second;
        std::stringstream ssfmtlabel;
        if (changeToPreviewPrescaleOtf)
          ssfmtlabel << "preview on-the-fly (" << changeToPreviewPrescaleIndex << ")*";
        else
          ssfmtlabel << pAlgo->getFormattedLabelForPsIndex(changeToPreviewPrescaleIndex);
        root["changetoprescalefmtlabel"] = ssfmtlabel.str();
      }
    }

    if (mLumisectionPrescChanged >= rateLumisection)
    {
      remainingLumis = mLumisectionPrescChanged - rateLumisection; // remaining lumisection for table update
    }

    if(lumisection != mLumisectionPrev)
    {
      refreshTab = true;
      mLumisectionPrev = lumisection;
    }
    if (lumisection > 1)
    {
        // Note: displayed rates refer to previous luminosity segment!
        root[kLumiSeg] = rateLumisection;
        if (rateLumisection >= mLumisectionPrescChanged)
        {
          mShowTable = true;
        }
        else
        {
          mShowTable = false;
          refreshTab = true;
        }

    }
    else
    {
        // Still no valid data available.
        root[kLumiSeg] = kMetricValueUnknown;

        mShowTable = false;
        refreshTab = true;
        remainingLumis = 1;

        mWillChangeToPrescale = changeToPreviewPrescaleIndex;
        mUsingOtf = changeToPreviewPrescaleOtf;
    }
    root["lumisectionprescchanged"] = remainingLumis;
  }
  catch (MetricValueUnknownException &e)
  {
    root[kLumiSeg] = kMetricValueUnknown;
    mShowTable = false;
    refreshTab = true;
  }

  root[kShowTable] = mShowTable;
  root[kRefreshTable] = refreshTab;

  out << root;
}

void
PreviewPanel::initOtfPreviewPrescales(const uint32_t previewPrescale)
{
  const uint32_t selIndex = previewPrescale;

  OtfPreviewSet& otfPreviewSet = getOtfPreviewSet();
  otfPreviewSet.clear();

  try
  {
    const uGtTriggerMenu& menu = getUgtTriggerMenu(getContext());

    // set prescale set with currently applied set
    std::map<std::string, double> menuPrescales;
    for (const auto& cit : menu)
    {
      const Algorithm *tmAlgo = cit.second;
      const std::string& algoName = tmAlgo->getName();
      const double prescale = tmAlgo->getPrescale(selIndex);
      const uint32_t mask = tmAlgo->getMask();

      menuPrescales.insert(std::make_pair(algoName, prescale));
      otfPreviewSet.finorMaskTable.add(algoName, mask);
    }
    std::stringstream col;
    col << selIndex;

    otfPreviewSet.prescaleSet.updateSet(col.str(), menuPrescales);

  }
  catch(TriggerMenuIsNullException &e)
  {
    LOG4CPLUS_ERROR(getLogger(), e.what());
  }
}

void
PreviewPanel::getCurrentPreviewPrescales(cgicc::Cgicc& cgi, std::ostream& out)
{
  const std::string sSelIndex = ajax::toolbox::getSubmittedValue(cgi,"selectedIndex");
  const std::string sIsOtfSet = ajax::toolbox::getSubmittedValue(cgi,"isOtfSet");
  const bool otfEnabled = (sIsOtfSet == "true");

  uint32_t selIndex;
  if (sSelIndex.length() <= 0)
    selIndex = mPrescaleIndexPrev;
  else
    selIndex = boost::lexical_cast<uint32_t>(sSelIndex);

  Json::Value root;
  Json::Value prescaleArray(Json::arrayValue);

  OtfPreviewSet& otfPreviewSet = getOtfPreviewSet();
  toolbox::PrescaleSet& otfPrescaleSet = otfPreviewSet.prescaleSet;
  toolbox::FinorMaskTable& otfFinorMaskTable = otfPreviewSet.finorMaskTable;

  // check whether otf preview prescale set is empty, if so initialize it
  if (otfPreviewSet.isEmpty())
  {
    initOtfPreviewPrescales(selIndex);
  }

  const uGtTriggerMenu& menu = getUgtTriggerMenu(getContext());
  std::set<uint32_t> orderedIndices;

  if (otfEnabled)
  {
    // use otf prescale set
    for (const auto& cit : otfPrescaleSet)
    {
      const Algorithm *tmAlgo = menu.getAlgorithmByName(cit.first);
      orderedIndices.insert(tmAlgo->getIndex());
    }
  }
  else
  {
    // make an alphabetical sorted algo list
    for (const auto& cit : menu)
    {
      const Algorithm *tmAlgo = cit.second;
      orderedIndices.insert(tmAlgo->getIndex());
    }
  }

  // iterate over ordered algos
  for (auto oIndex : orderedIndices)
  {
    const Algorithm *tmAlgo = menu.getAlgorithm(oIndex);

    uint32_t algoIndex = tmAlgo->getIndex();
    std::string algoName = tmAlgo->getName();
    double prescale;
    uint32_t mask;
    bool maskflag;

    if (otfEnabled)
    {
      prescale = otfPrescaleSet.getPrescale(algoName);
      mask = otfFinorMaskTable.getMask(algoName);
      maskflag = mask==1;
    }
    else
    {
      prescale = tmAlgo->getPrescale(selIndex);
      mask = tmAlgo->getMask();
      maskflag = tmAlgo->getMask()==1;
    }

    Json::Value jsonPrescale;
    jsonPrescale["index"] = algoIndex;
    jsonPrescale["algo"] = algoName;
    jsonPrescale["prescale"] = prescale;
    jsonPrescale["mask"] = mask;
    jsonPrescale["maskflag"] = maskflag;

    prescaleArray.append(jsonPrescale);

  } // end loop orderedIndices


  uint32_t prescaleIndex;
  if (otfEnabled)
  {
    const uint32_t otfPrescaleIndex = otfPrescaleSet.getIndex();
    prescaleIndex = otfPrescaleIndex;
  }
  else
  {
    prescaleIndex = selIndex;
  }

  root["prescaleindex"] = prescaleIndex;
  root["otf"] = otfEnabled;
  root["prescaleset"] = prescaleArray;
  out << root;
}




void
PreviewPanel::getVersion(cgicc::Cgicc& cgi, std::ostream& out)
{
  out << getUgtCellVersion();
}


std::string
PreviewPanel::getPreviewRateId(const uint32_t id)
{
  std::stringstream ss;
  ss << ALGO_PREVIEW_RATE << SEPARATOR << std::setfill('0') << std::setw(3) << id;
  return ss.str();
}
std::string
PreviewPanel::getPreviewRateCounterId(const uint32_t id)
{
  std::stringstream ss;
  ss << ALGO_PREVIEW_COUNT << SEPARATOR << std::setfill('0') << std::setw(3) << id;
  return ss.str();
}




void
PreviewPanel::fillRowsFromMenu()
{
  mRows.clear();

  const uGtTriggerMenu& menu = getUgtTriggerMenu(getContext());
  for (const auto& cit : menu)
  {
    const Algorithm *tmAlgo = cit.second;

    Row r;

    // Fill id and name
    r.index = tmAlgo->getIndex();
    r.algo = tmAlgo->getName();

    // Fill veto
    r.veto = tmAlgo->getVeto();

    // fill bx mask
    std::vector<std::string> vecBxMask = getBxMaskList(tmAlgo->getAlgoBxMask());
    r.bxmask = vecBxMask;

    const uint32_t moduleId = tmAlgo->getModuleId();
    const uGtProcessor* proc = getProcessorModule(moduleId);
    if (proc == NULL)
    {
      LOG4CPLUS_WARN(getLogger(), "module pointer is null for id=" << moduleId);
      r.rate = 0.0;
      r.ratecounter = 0;
      continue;
    }

    const uGtAlgo& algo = dynamic_cast<const uGtAlgo&>(proc->getAlgo());
    swatch::core::MetricReadGuard metric_guard(algo);

    // fill rates
    const std::string rateId = getPreviewRateId(tmAlgo->getIndex());
    const ::swatch::core::AbstractMetric& metric_ls = algo.getMetric(rateId);
    const ::swatch::core::MetricSnapshot& ss_ls = metric_ls.getSnapshot();
    if (ss_ls.isValueKnown())
    {
      double rate = ss_ls.getValue<double>();
      r.rate = rate;
    }
    else
    {
      std::stringstream ss;
      ss << "PreviewPanel SNAPSHOT unknown for previewRateId=" << rateId;
      LOG4CPLUS_ERROR(getLogger(), ss.str());
      throw MetricValueUnknownException(ss.str());
    }

    // fill rate counters
    const std::string rateCounterId = getPreviewRateCounterId(tmAlgo->getIndex());
    const ::swatch::core::AbstractMetric& metric_rc = algo.getMetric(rateCounterId);
    const ::swatch::core::MetricSnapshot& ss_rc = metric_rc.getSnapshot();
    if (ss_rc.isValueKnown())
    {
      double ratecounter = ss_rc.getValue<uint32_t>();
      r.ratecounter = ratecounter;
    }
    else
    {
      std::stringstream ss;
      ss << "PreviewPanel SNAPSHOT unknown for previewRateCounterId=" << rateCounterId;
      LOG4CPLUS_ERROR(getLogger(), ss.str());
      throw MetricValueUnknownException(ss.str());
    }


    // fill prescale & mask based on prescale column or otf prescales
    if (not isUsingPreviewOtfPrevious())
    {
      // using prescale column index
      r.prescale = tmAlgo->getPrescale(mPrescaleIndexPrev);
      // fill mask
      r.finor = tmAlgo->getMask();
      //
      r.hasOtfPrescale = false;
    }
    else
    {
      // use otf prescale set
      OtfPreviewSet& otfPreviewSet = getOtfPreviewSet();
      toolbox::PrescaleSet& otfPreviewPrescaleSet = otfPreviewSet.prescaleSet;
      toolbox::FinorMaskTable& otfFinorMaskTable = otfPreviewSet.finorMaskTable;

      // make diff from otf and base prescale set
      const double basePrescale = tmAlgo->getPrescale(otfPreviewPrescaleSet.getIndex());
      const uint32_t baseMask = tmAlgo->getMask();

      const std::string& algoName = tmAlgo->getName();
      const double otfPrescale = otfPreviewPrescaleSet.getPrescale(algoName);
      const uint32_t otfMask = otfFinorMaskTable.getMask(algoName);

      if (basePrescale != otfPrescale || baseMask != otfMask)
      {
        // modified prescale
        r.prescale = otfPrescale;
        r.finor = otfMask;
        r.hasOtfPrescale = true;
      }
      else
      {
        // original prescale
        r.prescale = basePrescale;
        r.finor = baseMask;
        r.hasOtfPrescale = false;
      }

    }

    // add all used algos to table
    // do sorting and in javascript
    mRows.push_back(r);
  } // end menu loop

}


Json::Value&
PreviewPanel::constructColumns(Json::Value& root)
{
  Json::Value columns(Json::arrayValue);

  const uGtTriggerMenu& menu = getUgtTriggerMenu(getContext());
  auto itAlgo = menu.begin();

  const Algorithm* tmAlgo = itAlgo->second;
  const std::map<toolbox::PrescaleColumnItem, double>& presc = tmAlgo->getPrescales();
  for(auto it = presc.begin(); it != presc.end(); ++it)
  {
    const toolbox::PrescaleColumnItem& column = it->first;

    Json::Value item;
    std::stringstream ss;
    // prescale column index
    ss << column.index;
    item["index"] = ss.str();
    ss.str(""); ss.clear();
    // prescale column formatted text
    ss << column.getFormattedLabel();
    item["text"] = ss.str();
    ss.str(""); ss.clear();
    // (un)modified prescale set
    item["otf"] = false;

    columns.append(item);
  } // end presc loop

  { // block start
    // add prescales-on-the-fly dropdown item
    Json::Value otfitem;

    OtfPreviewSet& otfPreviewSet = getOtfPreviewSet();
    toolbox::PrescaleSet& pSet = otfPreviewSet.prescaleSet;
    uint32_t ppIdx = pSet.getIndex();

    std::stringstream txt;
    txt << "preview on-the-fly (" << ppIdx << ")*";
    otfitem["text"] = txt.str();
    txt.str(""); txt.clear();

    txt << ppIdx;
    otfitem["index"] = txt.str();

    otfitem["otf"] = true;
    columns.append(otfitem);

  } // block end
  root["buttons"] = columns;

  return root;
}

Json::Value&
PreviewPanel::constructRows(Json::Value& root)
{
  Json::Value items(Json::arrayValue);

  // fill json array with each row
  for (auto it = mRows.begin(); it != mRows.end(); ++it)
  {
    Json::Value row;

    Row &r = *it;
    row[kIndex] = r.index;
    row[kAlgo] = r.algo;
    row[kPrescale] = r.prescale;
    row[kFinor] = r.finor;

    row[kRate] = r.rate;
    row[kRateCounter] = r.ratecounter;

    row[kOtfPrescale] = r.hasOtfPrescale;
    if (r.hasOtfPrescale)
      row[kClass] = "otf";
    else
      row[kClass] = "normal";

    Json::Value bxMaskArr(Json::arrayValue);
    for (auto bxIt = r.bxmask.begin(); bxIt != r.bxmask.end(); ++bxIt)
    {
      bxMaskArr.append(*bxIt);
    }
    row[kBxMask] = bxMaskArr;

    items.append(row);
  }
  root["items"] = items;

  return root;
}


Json::Value&
PreviewPanel::constructTableHeader(Json::Value& root)
{
  Json::Value colIndex;
  colIndex["name"] = "Index";
  colIndex[kClass] = kIndex;
  root["colindex"] = colIndex;

  Json::Value colAlgo;
  colAlgo["name"] = "Algorithm";
  colAlgo[kClass] = kAlgo;
  root["colalgo"] = colAlgo;

  Json::Value colBxMask;
  colBxMask["name"] = "Bx Mask";
  colBxMask[kClass] = kBxMask;
  root["colbxmask"] = colBxMask;

  Json::Value colPrescale;
  colPrescale["name"] = "Prescale";
  colPrescale[kClass] = kPrescale;
  root["colprescale"] = colPrescale;

  Json::Value colFinor;
  colFinor["name"] = "Mask";
  colFinor[kClass] = kFinor;
  root["colfinor"] = colFinor;

  Json::Value colRate;
  colRate["name"] = "Rate after/prescales (Hz)";
  colRate[kClass] = kRate;
  root["colrate"] = colRate;

  Json::Value colRateCounter;
  colRateCounter["name"] = "Counter after/prescales";
  colRateCounter[kClass] = kRateCounter;
  root["colratecounter"] = colRateCounter;

  Json::Value colOtfPresc;
  colOtfPresc["name"] = kOtfPrescale;
  colOtfPresc[kClass] = kAlgo;
  root["colotfpresc"] = colOtfPresc;

  return root;
}



double
PreviewPanel::getPreviewFinorRate()
{
  double finorSum = 0.0;

  // should be only one processor
  std::vector<const uGtFinorPreviewProc*> finorPreviewProcessor = getFinorPreviewProcessor();
  for (const auto& pFinor : finorPreviewProcessor)
  {
    const uGtFinorPreviewAlgo& algo = dynamic_cast<const uGtFinorPreviewAlgo&>(pFinor->getAlgo());

    const ::swatch::core::AbstractMetric& metric_ls = algo.getMetric(kRateFinOrPreview);
    const ::swatch::core::MetricSnapshot& ss_ls = metric_ls.getSnapshot();
    if (not ss_ls.isValueKnown())
    {
      LOG4CPLUS_ERROR(getLogger(), "PreviewPanel SNAPSHOT unknown for metric kRateFinOrPreview");
      throw MetricValueUnknownException(kRateFinOrPreview);
    }
    else
    {
      finorSum += ss_ls.getValue<double>();
    }
  }

  return finorSum;
}


uint32_t
PreviewPanel::getLuminositySection()
{
  std::vector<const uGtProcessor*> uGtProcessors = getProcessors();

  uint32_t lumiSeg = 0;
  auto pit = uGtProcessors.begin();
  if (pit != uGtProcessors.end())
  {
    const uGtProcessor* p = *pit;
    const uGtAlgo& algo = dynamic_cast<const uGtAlgo&>(p->getAlgo());

    const ::swatch::core::AbstractMetric& metric_ls = algo.getMetric(kLuminositySegmentNumber);
    const ::swatch::core::MetricSnapshot& ss_ls = metric_ls.getSnapshot();
    if (not ss_ls.isValueKnown())
    {
      LOG4CPLUS_ERROR(getLogger(), "PreviewPanel luminosity segment nr. is unknown");
      throw MetricValueUnknownException(kLuminositySegmentNumber);
    }
    lumiSeg = ss_ls.getValue<uint32_t>();
  }
  return lumiSeg;
}


uint32_t
PreviewPanel::getPreviewPrescaleIndexPrevious()
{
  std::vector<const uGtProcessor*> uGtProcessors = getProcessors();

  uint32_t presc = 0;
  auto pit = uGtProcessors.begin();
  if (pit != uGtProcessors.end())
  {
    const uGtProcessor* p = *pit;
    const uGtAlgo& algo = dynamic_cast<const uGtAlgo&>(p->getAlgo());

    const ::swatch::core::AbstractMetric& metric_ls = algo.getMetric(kPreviewPrescaleIndexInUse);
    const ::swatch::core::MetricSnapshot& ss_ls = metric_ls.getSnapshot();
    if (not ss_ls.isValueKnown())
    {
      LOG4CPLUS_ERROR(getLogger(), "PreviewPanel preview prescale column index in use is unknown");
      throw MetricValueUnknownException(kPreviewPrescaleIndexInUse);
    }
    presc = ss_ls.getValue<uint32_t>();
  }
  return presc;
}

uint32_t
PreviewPanel::getPreviewPrescaleIndex()
{
  std::vector<const uGtProcessor*> uGtProcessors = getProcessors();

  uint32_t presc = 0;
  auto pit = uGtProcessors.begin();
  if (pit != uGtProcessors.end())
  {
    const uGtProcessor* p = *pit;
    const uGtAlgo& algo = dynamic_cast<const uGtAlgo&>(p->getAlgo());

    const ::swatch::core::AbstractMetric& metric_ls = algo.getMetric(kPreviewPrescaleIndex);
    const ::swatch::core::MetricSnapshot& ss_ls = metric_ls.getSnapshot();
    if (not ss_ls.isValueKnown())
    {
      LOG4CPLUS_ERROR(getLogger(), "PreviewPanel preview prescale column index is unknown");
      throw MetricValueUnknownException(kPreviewPrescaleIndex);
    }
    presc = ss_ls.getValue<uint32_t>();
  }
  return presc;
}

bool
PreviewPanel::isUsingPreviewOtf()
{
  // check hardware register whether otf flag is set or not
  std::vector<const uGtProcessor*> uGtProcessors = getProcessors();

  bool isOtf = false;
  auto pit = uGtProcessors.begin();
  if (pit != uGtProcessors.end())
  {
    const uGtProcessor* p = *pit;
    const uGtAlgo& algo = dynamic_cast<const uGtAlgo&>(p->getAlgo());

    const ::swatch::core::AbstractMetric& metric_ls = algo.getMetric(kPreviewOtf);
    const ::swatch::core::MetricSnapshot& ss_ls = metric_ls.getSnapshot();
    if (not ss_ls.isValueKnown())
    {
      LOG4CPLUS_ERROR(getLogger(), "PreviewPanel preview otf flag is unknown");
      throw MetricValueUnknownException(kPreviewOtf);
    }

    if (ss_ls.getValue<uint32_t>() == 1)
      isOtf = true;
  }
  return isOtf;
}

bool
PreviewPanel::isUsingPreviewOtfPrevious()
{
  std::vector<const uGtProcessor*> uGtProcessors = getProcessors();

  bool isOtfPrevious = false;
  auto pit = uGtProcessors.begin();
  if (pit != uGtProcessors.end())
  {
    const uGtProcessor* p = *pit;
    const uGtAlgo& algo = dynamic_cast<const uGtAlgo&>(p->getAlgo());

    const ::swatch::core::AbstractMetric& metric_ls = algo.getMetric(kPreviewOtfInUse);
    const ::swatch::core::MetricSnapshot& ss_ls = metric_ls.getSnapshot();
    if (not ss_ls.isValueKnown())
    {
      LOG4CPLUS_ERROR(getLogger(), "PreviewPanel preview otf flag in use is unknown");
      throw MetricValueUnknownException(kPreviewOtfInUse);
    }

    if (ss_ls.getValue<uint32_t>() == 1)
      isOtfPrevious = true;
  }
  return isOtfPrevious;
}

void
PreviewPanel::updateLumisectionPrescChanged(const uint32_t value)
{
  if (value > 1)
  {
    // at the calculated lumisection, updated rates will be published
    mLumisectionPrescChanged = (value -1 + 2);
  }
  else
    mLumisectionPrescChanged = 1;
}

ugt::toolbox::MemoryImage<uint32_t>
PreviewPanel::preparePrescalesImage(const uGtTriggerMenu& menu,
  const std::map<std::string, std::string>& parameters, const std::string& column)
{
  OtfPreviewSet& otfPreviewSet = getOtfPreviewSet();
  otfPreviewSet.clear();

  std::map<std::string, double> otfPrescalesMap;
  ugt::toolbox::MemoryImage<uint32_t> image(1, kMaxAlgorithms, 0x0);
  for (const auto& cit : menu)
  {
    const Algorithm *tmAlgo = cit.second;

    uint32_t algoindex = tmAlgo->getIndex();
    std::string algoname = tmAlgo->getName();

    // check that menu algo name is available in retrieved parameters list
    // and get prescale value
    auto it = parameters.find(algoname);
    if (it == parameters.end())
    {
      std::stringstream msg;
      msg << "PreviewPanel could not find algo '" << algoname;
      msg << "' in parameters for preparePrescaleImage";
      LOG4CPLUS_WARN(getLogger(), msg.str());
      continue;
    }
    // Fractional prescale string to integer representation.
    const double scale = std::pow(10, ugt::kPrescalePrecision);
    const double prescale = std::round(std::stod(it->second) * scale) / scale; // TODO very ugly
    otfPrescalesMap.insert(std::make_pair(algoname, prescale));


    // get mask value
    std::ostringstream ssMaskId;
    ssMaskId << algoname << "_mask";
    it = parameters.find(ssMaskId.str());
    if (it == parameters.end())
    {
      std::stringstream msg;
      msg << "PreviewPanel could not find mask for algo '" << algoname;
      msg << "' in parameters for preparePrescaleImage";
      LOG4CPLUS_WARN(getLogger(), msg.str());
      continue;
    }
    uint32_t mask = boost::lexical_cast<uint32_t>(it->second);
    otfPreviewSet.finorMaskTable.add(algoname, mask);


    // prepare image with the merged presale value
    uint32_t mergedPrescale = static_cast<uint32_t>(prescale * scale) * mask;
    image.setValue(algoindex, mergedPrescale);
  }

  // update otf preview prescales set
  toolbox::PrescaleSet& pSet = otfPreviewSet.prescaleSet;
  pSet.updateSet(column, otfPrescalesMap);

  return image;
}


OtfPreviewSet&
PreviewPanel::getOtfPreviewSet()
{
  swatchcellframework::CellContext *context = dynamic_cast<swatchcellframework::CellContext*>(getContext());
  swatchcellframework::CellContext::SharedGuard_t lGuard(*context);
  ugt::Cell* cell = dynamic_cast<ugt::Cell*>(context->getCell());
  return cell->getOtfPreviewSet();
}
