#include "ugt/ts/cell/panels/PrescalesPanel.h"

#include "ugt/ts/cell/panels/PanelUtils.h"
#include "ugt/ts/cell/Cell.h"

#include <boost/pointer_cast.hpp>

#include "ajax/toolbox.h"
#include "ajax/PolymerElement.h"
#include "jsoncpp/json/json.h"

#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"

#include "swatch/action/GateKeeper.hpp"
#include "swatch/core/ReadWriteXParameterSet.hpp"
#include "swatch/system/System.hpp"
#include "swatchcell/framework/CellContext.h"

#include "ugt/swatch/Constants.h"
#include "ugt/swatch/uGtTriggerMenu.h"
#include "ugt/swatch/toolbox/PrescaleTable.h"
#include "ugt/swatch/toolbox/PrescaleSet.h"
#include "ugt/swatch/uGtProcessor.h"
#include "ugt/swatch/uGtAlgo.h"

#include "ugt/swatch/commands/uGtPreparePrescaleCommand.h"
#include "ugt/swatch/commands/uGtPreparePreviewPrescaleCommand.h"
#include "ugt/swatch/commands/uGtApplyPrescaleCommand.h"

#include <iostream>
#include <time.h>
#include <string>

using namespace ugt;

// Metric keys
const std::string kPrescaleIndex = "prescaleColumnIndex";
const std::string kOtf = "prescOtf";


PrescalesPanel::PrescalesPanel( tsframework::CellAbstractContext* context, log4cplus::Logger& logger ) : tsframework::CellPanel(context,logger)
{
}


PrescalesPanel::~PrescalesPanel() {
    remove();
}


void PrescalesPanel::layout(cgicc::Cgicc& cgi) {
  remove();
  setEvent("PrescalesPanel::getVersion", ajax::Eventable::OnClick, this, &PrescalesPanel::getVersion);
  setEvent("PrescalesPanel::getTable", ajax::Eventable::OnClick, this, &PrescalesPanel::getTable);
  setEvent("PrescalesPanel::changePrescale", ajax::Eventable::OnClick, this, &PrescalesPanel::changePrescale);
  setEvent("PrescalesPanel::isSystemReady", ajax::Eventable::OnTime, this, &PrescalesPanel::isSystemReady);
  setEvent("PrescalesPanel::readCurrentPrescaleIndex", ajax::Eventable::OnTime, this, &PrescalesPanel::readCurrentPrescaleIndex);

  add(new ajax::PolymerElement("prescales-panel"));

}


void PrescalesPanel::getVersion(cgicc::Cgicc& cgi, std::ostream& out)
{
  out << getUgtCellVersion();
}

void PrescalesPanel::getTable(cgicc::Cgicc& cgi,std::ostream& out) {

    Json::Value root;
    Json::Value items(Json::arrayValue);
    Json::Value buttons(Json::arrayValue);
    Json::Value rows(Json::arrayValue);

    // get Trigger Menu reference
    const uGtTriggerMenu& menu = getUgtTriggerMenu(getContext());

    {
      std::string id_command = "";
      std::string id_parameter = "";
      const std::string id_sequence = "uGtSeqConfigure";
      std::vector<std::string> id_contexts;
      id_contexts.push_back("uGT.processors");
      id_contexts.push_back("uGT.uGtProcessor");


      swatchcellframework::CellContext *context = dynamic_cast<swatchcellframework::CellContext*>(getContext());
      swatchcellframework::CellContext::SharedGuard_t lGuard(*context);
      const swatch::action::GateKeeper* gk = context->getGateKeeper(lGuard);

      if (not gk)
      {
          std::cout << "GateKeeper is not ready" << std::endl;
          return;
      }

      // reset prescale table
      mPrescaleTable.clear();

      try
      {
        // get prescales
        id_command = cmd::kPreparePrescale;
        id_parameter = "prescales";
        swatch::action::GateKeeper::Parameter_t parameter = gk->get(id_sequence, id_command, id_parameter, id_contexts);
        if (not parameter) throw std::runtime_error("prescales: null pointer");
        const xdata::Table* table = boost::dynamic_pointer_cast<const xdata::Table>(boost::get_pointer(parameter));

        // get finor mask
        id_parameter = "finorMask";
        parameter = gk->get(id_sequence, id_command, id_parameter, id_contexts);
        if (not parameter) throw std::runtime_error("finorMask: null pointer");
        const xdata::Table* tableMask = boost::dynamic_pointer_cast<const xdata::Table>(boost::get_pointer(parameter));

        mPrescaleTable.load(*table, *tableMask, menu);
      }
      catch (toolbox::PrescaleTableException &e)
      {
        std::stringstream os;
        os << "PrescalePanel prepareTable: " << e.what();
        LOG4CPLUS_ERROR(getLogger(), os.str());
        root["error"] = os.str();
        Json::Value columns(Json::arrayValue);
        root["items"] = items;
        root["columns"] = columns;
        out << root;
        return;
      }
    }

    for (auto cit = menu.begin(); cit != menu.end(); cit++)
    {
      const Algorithm *tmAlgo = cit->second;

      if (cit == menu.begin())
      {
        // Prepare table columns
        Json::Value columns(Json::arrayValue);

        Json::Value column1;
        column1["name"] = "Algo/Prescale-Index";
        column1["sortable"] = false;
        columns.append(column1);

        // set up prescale indices
        const std::map<toolbox::PrescaleColumnItem, double>& presc = tmAlgo->getPrescales();
        for (auto it = presc.begin(); it != presc.end(); it++)
        {
          const toolbox::PrescaleColumnItem& column = it->first;

          // setup dropdown elements (buttons)
          Json::Value item;
          std::stringstream ss;
          ss << "bt" << column.index;
          item["id"] = ss.str();
          ss.str(""); ss.clear();

          ss << column.index;
          item["index"] = ss.str();
          ss.str(""); ss.clear();

          ss << column.getFormattedLabel();
          item["text"] = ss.str();

          item["otf"] = false;
          buttons.append(item);


          // setup columns
          Json::Value columni;
          columni["name"] = column.getFormattedLabel();
          columni["sortable"] = false;
          columns.append(columni);
        }

        root["buttons"] = buttons;
        root["columns"] = columns;
      }

      std::ostringstream msg;
      Json::Value row;

      Json::Value rowi;
      Json::Value rowpresc(Json::arrayValue);

      uint32_t algo = tmAlgo->getIndex();
      std::string name = tmAlgo->getName();


      msg << algo << " " << name;
      row["Algo/Prescale-Index"] = msg.str();
      rowi["algo"] = msg.str();


      // Prepare a table row with algo and corresponding prescale values
      const std::map<toolbox::PrescaleColumnItem, double>& presc = tmAlgo->getPrescales();
      for (auto it = presc.begin(); it != presc.end(); it++)
      {
        const toolbox::PrescaleColumnItem& column = it->first;
        std::string formattedLabel = column.getFormattedLabel();
        const double prescale = it->second;

        msg.str("");
        msg.clear();
        msg << std::setprecision(2) << std::fixed << prescale;
        row[formattedLabel] = msg.str();
        rowpresc.append(msg.str());
      }
      items.append(row);

      rowi["prescales"] = rowpresc;
      rows.append(rowi);
    }
    root["items"] = items;
    root["rows"] = rows;

    out << root;
}


void PrescalesPanel::changePrescale(cgicc::Cgicc& cgi, std::ostream& out)
{
  Json::Value root;
  std::string keyStatus = "status";
  std::string keyMsg = "msg";
  std::string keyIndex = "index";
  std::string sError = "error";
  std::string sWarning = "warning";
  std::string sDone = "done";

  std::stringstream ssMsg;

  std::string sIndex = ajax::toolbox::getSubmittedValue(cgi,"selectedButton");
  root[keyIndex] = "";

  // get the 'next' available prescale column index
  uint32_t currentIdx = convert<uint32_t>(sIndex);
  uint32_t nextIdx = mPrescaleTable.getNextPrescaleSet(currentIdx).getIndex();
  std::ostringstream cstream;
  cstream << nextIdx;
  std::string sNextIndex = cstream.str();


  //////////////////

  LOG4CPLUS_INFO(getLogger(), "changePrescale: command started");


  swatchcellframework::CellContext *swContext = dynamic_cast<swatchcellframework::CellContext*>(getContext());
  const std::vector<const uGtProcessor*> uGtProcessors = getProcessors();

  if (uGtProcessors.empty())
  {
    std::ostringstream msg;
    msg << "changePrescale no uGtProcessor found";
    LOG4CPLUS_WARN(getLogger(), msg.str());
    root[keyMsg] = msg.str();
    root[keyStatus] = sError;
    out << root;
    return;
  }

  if (not isSystemReady())
  {
    std::ostringstream msg;
    msg << "changePrescale: System is not ready";
    LOG4CPLUS_INFO(getLogger(), msg.str());
    root[keyMsg] = msg.str();
    root[keyStatus] = sError;
    out << root;
    return;
  }

  // get current preview prescale index
  auto pit = uGtProcessors.begin();
  if (pit == uGtProcessors.end())
  {
    std::ostringstream msg;
    msg << "changePrescale: No uGtProcessors available";
    LOG4CPLUS_INFO(getLogger(), msg.str());
    root[keyMsg] = msg.str();
    root[keyStatus] = sError;
    out << root;
    return;
  }
  const uGtProcessor& ugt = dynamic_cast<const uGtProcessor&>(*(*pit));
  ::mp7::MP7Controller& driver = const_cast<uGtProcessor&>(ugt).driver();
  ::uhal::HwInterface& hw = driver.hw();
  ::uhal::ValWord<uint32_t> uhalPreviewPrescIndex = hw.getNode("gt_mp7_gtlfdl.preview.current_prescale_set_index").read();
  hw.getClient().dispatch();
  uint32_t currentPreviewPSIdx = uhalPreviewPrescIndex.value();



  // load prescale values to the uGtProcessors
  for (auto it = uGtProcessors.begin(); it != uGtProcessors.end(); it++)
  {
    const uGtProcessor& ugt = dynamic_cast<const uGtProcessor&>(*(*it));
    std::set<std::string> commands = ugt.getCommands();

    std::set<std::string>::iterator sit = commands.find(cmd::kPreparePrescale);
    if (sit == commands.end())
    {
      std::ostringstream msg;
      msg << "changePrescale: command" << cmd::kPreparePrescale << " was not found";
      LOG4CPLUS_ERROR(getLogger(), msg.str());
      root[keyMsg] = msg.str();
      root[keyStatus] = sError;
      out << root;
      return;
    }

    swatchcellframework::CellContext::SharedGuard_t lGuard(*swContext);
    const swatch::action::GateKeeper* gk = swContext->getGateKeeper(lGuard);
    swatch::action::GateKeeper::Parameter_t gkPrescales = gk->get("",*sit,"prescales",ugt.getGateKeeperContexts());
    swatch::action::GateKeeper::Parameter_t gkFinorMask = gk->get("",*sit,"finorMask",ugt.getGateKeeperContexts());

    uGtPreparePrescaleCommand &cmd = dynamic_cast<uGtPreparePrescaleCommand &>(const_cast<uGtProcessor&>(ugt).getCommand(*sit));

    swatch::core::ReadWriteXParameterSet params;
    params.deepCopyFrom(cmd.getDefaultParams());
    params.get("index").fromString(sIndex);
    params.get("prescales").fromString(gkPrescales->toString());
    params.get("finorMask").fromString(gkFinorMask->toString());
    params.get("otf").fromString("0"); // using production prescales (write 0)

    cmd.exec(params, false);

    if (cmd.getState() != swatch::action::Functionoid::State::kDone)
    {
      std::ostringstream msg;
      msg << "changePrescale: cmd " << cmd::kPreparePrescale << " status msg:" << cmd.getStatus().getStatusMsg();

      if (cmd.getState() == swatch::action::Functionoid::State::kError)
      {
        LOG4CPLUS_ERROR(getLogger(), msg.str());
        root[keyMsg] = cmd.getStatus().getStatusMsg();
        root[keyStatus] = sError;
        out << root;
        return;
      }

      LOG4CPLUS_WARN(getLogger(), msg.str());
      ssMsg << msg.str() << std::endl;
      root[keyStatus] = sWarning;
    }

    // load preview prescales to uGtProcessor
    sit = commands.find(cmd::kPreparePreviewPrescale);
    if (sit == commands.end())
    {
      std::ostringstream msg;
      msg << "changePrescale: command" << cmd::kPreparePreviewPrescale << " was not found";
      LOG4CPLUS_ERROR(getLogger(), msg.str());
      root[keyMsg] = msg.str();
      root[keyStatus] = sError;
      out << root;
      return;
    }


    if (currentPreviewPSIdx != nextIdx)
    {
      uGtPreparePreviewPrescaleCommand &cmdPreview = dynamic_cast<uGtPreparePreviewPrescaleCommand &>(const_cast<uGtProcessor&>(ugt).getCommand(*sit));
      swatch::core::ReadWriteXParameterSet paramsPreview;
      paramsPreview.deepCopyFrom(cmdPreview.getDefaultParams());
      paramsPreview.get("index").fromString(sNextIndex);
      paramsPreview.get("prescales").fromString(gkPrescales->toString());
      paramsPreview.get("finorMask").fromString(gkFinorMask->toString());
      params.get("otf").fromString("0"); // using production prescales (write 0)

      cmdPreview.exec(paramsPreview, false);
      if (cmdPreview.getState() != swatch::action::Functionoid::State::kDone)
      {
        std::ostringstream msg;
        msg << "changePrescale: cmd " << cmd::kPreparePreviewPrescale << " status msg:" << cmdPreview.getStatus().getStatusMsg();

        if (cmdPreview.getState() == swatch::action::Functionoid::State::kError)
        {
          LOG4CPLUS_ERROR(getLogger(), msg.str());
          root[keyMsg] = cmdPreview.getStatus().getStatusMsg();
          root[keyStatus] = sError;
          out << root;
          return;
        }

        LOG4CPLUS_WARN(getLogger(), msg.str());
        ssMsg << msg.str() << std::endl;
        root[keyStatus] = sWarning;
      }
    }
    else
    {
      std::ostringstream msg;
      msg << "Preview prescales are already set to index " << sNextIndex;
      LOG4CPLUS_ERROR(getLogger(), msg.str());
    }
  }


  // secure enough time till the next lumisection boundary
  int nInconsistent = 0;
  bool bEnoughTimeLeft = false;
  uint32_t lumiSection_ref = 0;
  for (uint32_t ii = 0; ii < kRetryLimit; ii++)
  {
    bool isInconsistent = false;
    lumiSection_ref = (dynamic_cast<const uGtAlgo&>(uGtProcessors.at(0)->getAlgo())).getLuminositySection();
    uint32_t lumiSection = 0;
    for (auto it = uGtProcessors.begin(); it != uGtProcessors.end(); it++)
    {
      const uGtAlgo& algo = dynamic_cast<const uGtAlgo&>((*it)->getAlgo());
      lumiSection = algo.getLuminositySection();
      if (lumiSection != lumiSection_ref)
      {
        isInconsistent = true;
        nInconsistent++;
        break;
      }
    }

    if (isInconsistent)
    {
      double sleepMs = 1000;
      LOG4CPLUS_INFO(getLogger(), "changePrescale: probably crossed lumisection boundary: "
                                  << lumiSection_ref << " != " << lumiSection
                                  << " sleep for " << sleepMs << " ms");
      boost::this_thread::sleep_for(boost::chrono::milliseconds(uint32_t(sleepMs)));
      continue;
    }


    // check the remaining time
    const uGtAlgo& algo = dynamic_cast<const uGtAlgo&>(uGtProcessors.at(0)->getAlgo());
    double remainingSeconds = algo.getSecondsBeforeLuminositySegmentUpdate(&getLogger());
    LOG4CPLUS_INFO(getLogger(), "changePrescale: seconds before lumisection boundary: " << remainingSeconds);
    if (remainingSeconds >= kUpdateThreshold)
    {
      bEnoughTimeLeft = true;
      break;
    }

    double sleepMs = (remainingSeconds+kSleepDelta)*1000;
    LOG4CPLUS_INFO(getLogger(), "changePrescale: sleep for " << sleepMs << " ms");
    boost::this_thread::sleep_for(boost::chrono::milliseconds(uint32_t(sleepMs)));
  }

  if (not bEnoughTimeLeft)
  {
    std::ostringstream msg;
    msg << "changePrescale: not enough time left till the next lumisection after " << kRetryLimit << " trials."
        << " nInconsistent = " << nInconsistent;
    LOG4CPLUS_WARN(getLogger(), msg.str());
    root[keyMsg] = msg.str();
    root[keyStatus] = sError;
    out << root;
    return;
  }


  // Enough time left for updating the prescale setting
  for (auto it = uGtProcessors.begin(); it != uGtProcessors.end(); it++)
  {
    const uGtProcessor& ugt = dynamic_cast<const uGtProcessor&>(*(*it));
    uGtApplyPrescaleCommand &cmd = dynamic_cast<uGtApplyPrescaleCommand &>(const_cast<uGtProcessor&>(ugt).getCommand(cmd::kApplyPrescale));
    swatch::core::ReadWriteXParameterSet params;
    cmd.exec(params, false);

    if (cmd.getState() != swatch::action::Functionoid::State::kDone)
    {
      std::ostringstream msg;
      msg << "changePrescale: cmd " << cmd::kApplyPrescale << " status msg:" << cmd.getStatus().getStatusMsg();
      LOG4CPLUS_ERROR(getLogger(), msg.str());
      root[keyMsg] = msg.str();
      root[keyStatus] = sError;
      out << root;
      return;
    }

    LOG4CPLUS_INFO(getLogger(), "changePrescale: apply prescale for slot = " << (*it)->getSlot());
  }


  // check if still in the same lumisection
  bool bSuccess = true;
  for (auto it = uGtProcessors.begin(); it != uGtProcessors.end(); it++)
  {
    const uGtAlgo& algo = dynamic_cast<const uGtAlgo&>((*it)->getAlgo());
    uint32_t lumiSection = algo.getLuminositySection();
    if (lumiSection != lumiSection_ref)
    {
      ssMsg << "changePrescale: crossed lumisection boundary during the operation, wait for the next lumisection" << std::endl;
      bSuccess = false;
      break;
    }
  }

  if (bSuccess)
  {
    LOG4CPLUS_INFO(getLogger(), "changePrescale: success");
    if (ssMsg.str().size() > 0)
    {
      root[keyStatus] = sWarning;
    }
    else
    {
      root[keyStatus] = sDone;
    }

    root[keyIndex] = sIndex;
  }

  root[keyMsg] = ssMsg.str();

  LOG4CPLUS_INFO(getLogger(), "changePrescale: command finished");

  out << root;
}


void PrescalesPanel::isSystemReady(cgicc::Cgicc& cgi,std::ostream& out)
{
  bool rc = isSystemReady();
  out << rc;
}

bool PrescalesPanel::isSystemReady()
{
  typedef ::swatchcellframework::RunControl tRunControlState;
  tsframework::CellOperationFactory* lOpFactory = getContext()->getOperationFactory();
  std::string state = lOpFactory->getOperation(swatchcellframework::CellContext::kRunControlOperationName).getFSM().getState();
  bool rc = ((state == tRunControlState::kStateConfigured)
            or (state == tRunControlState::kStateAligned)
            or (state == tRunControlState::kStateRunning)
            or (state == tRunControlState::kStatePaused));
  return rc;
}


void PrescalesPanel::readCurrentPrescaleIndex(cgicc::Cgicc& cgi,std::ostream& out)
{
  LOG4CPLUS_DEBUG(getLogger(), "readCurrentPrescaleIndex");

  const std::vector<const uGtProcessor*> uGtProcessors = getProcessors();

  std::string sIndex = ajax::toolbox::getSubmittedValue(cgi,"selectedButton");
  std::string sStatus = ajax::toolbox::getSubmittedValue(cgi,"status");

  Json::Value root;
  Json::Value processors(Json::arrayValue);

  const uGtTriggerMenu& menu = getUgtTriggerMenu(getContext());

  root["changetoprescaleindex"] = getPrescaleIndex();
  root["changetoprescaleotf"] = isUsingOtf();


  uint32_t prescalIndexRef = 0;
  for (auto it = uGtProcessors.begin(); it != uGtProcessors.end(); it++)
  {
    const uGtProcessor& ugt = dynamic_cast<const uGtProcessor&>(*(*it));
    ::mp7::MP7Controller& driver = const_cast<uGtProcessor&>(ugt).driver();
    ::uhal::HwInterface& hw = driver.hw();

    ::uhal::ValWord<uint32_t> data_index = hw.getNode("gt_mp7_gtlfdl.current_prescale_set_index").read();
    ::uhal::ValWord<uint32_t> data_otf = hw.getNode("gt_mp7_gtlfdl.prescale_otf_applied.current_prescale_otf").read();
    hw.getClient().dispatch();
    prescalIndexRef = data_index.value();

    std::stringstream ss;

    Json::Value item;
    ss << ugt.getSlot();
    item["slot"] = ss.str();

    ss.str(""); ss.clear();

    ss << data_index.value();
    item["index"] = ss.str();

    bool isOtf = false;
    if (data_otf.value() == 1)
    {
      isOtf = true;
    }
    item["otf"] = isOtf;

    auto itAlgo = menu.begin();
    if (itAlgo != menu.end())
    {
      const Algorithm* pAlgo = itAlgo->second;
      item["fmtlabel"] = pAlgo->getFormattedLabelForPsIndex(data_index.value());
    }

    processors.append(item);
  }
  root["processors"] = processors;


  if (sStatus == "error")
  {
    sIndex = "undefined";
  }

  std::stringstream o;
  o << prescalIndexRef;
  const std::string sPrescale = o.str();
  if (sIndex != "" && sIndex != "undefined")
  {
    if (sPrescale == sIndex)
    {
      root["status"] = "(Prescale changed)";
    }
    else
    {
      root["status"] = "(Waiting for lumisection update)";
    }
  }
  else
  {
    root["status"] = "";
  }

  out << root;
}


uint32_t
PrescalesPanel::getPrescaleIndex()
{
  std::vector<const uGtProcessor*> uGtProcessors = getProcessors();

  uint32_t presc = 0;
  auto pit = uGtProcessors.begin();
  if (pit != uGtProcessors.end())
  {
    const uGtProcessor& p = dynamic_cast<const uGtProcessor&>(*(*pit));
    const uGtAlgo& algo = dynamic_cast<const uGtAlgo&>(p.getAlgo());

    const ::swatch::core::AbstractMetric& metric_ls = algo.getMetric(kPrescaleIndex);
    const ::swatch::core::MetricSnapshot& ss_ls = metric_ls.getSnapshot();
    if (not ss_ls.isValueKnown())
    {
      LOG4CPLUS_ERROR(getLogger(), "PrescalePanel prescale column index is unknown");
      throw MetricValueUnknownException(kPrescaleIndex);
    }
    presc = ss_ls.getValue<uint32_t>();
  }
  return presc;
}

bool
PrescalesPanel::isUsingOtf()
{
  // check hardware register whether otf flag is set or not
  std::vector<const uGtProcessor*> uGtProcessors = getProcessors();

  bool isOtf = false;
  auto pit = uGtProcessors.begin();
  if (pit != uGtProcessors.end())
  {
    const uGtProcessor& p = dynamic_cast<const uGtProcessor&>(*(*pit));
    const uGtAlgo& algo = dynamic_cast<const uGtAlgo&>(p.getAlgo());

    const ::swatch::core::AbstractMetric& metric_ls = algo.getMetric(kOtf);
    const ::swatch::core::MetricSnapshot& ss_ls = metric_ls.getSnapshot();
    if (not ss_ls.isValueKnown())
    {
      LOG4CPLUS_ERROR(getLogger(), "PrescalePanel otf flag is unknown");
      throw MetricValueUnknownException(kOtf);
    }

    if (ss_ls.getValue<uint32_t>() == 1)
      isOtf = true;
  }
  return isOtf;
}

std::vector<const uGtProcessor*>
PrescalesPanel::getProcessors()
{
  std::vector<const uGtProcessor*> processors;

  {
    swatchcellframework::CellContext *context = dynamic_cast<swatchcellframework::CellContext*>(getContext());
    swatchcellframework::CellContext::SharedGuard_t lGuard(*context);
    ugt::Cell* cell = dynamic_cast<ugt::Cell*>(context->getCell());
    processors = cell->getUgtProcessors();
  }

  return processors;
}
