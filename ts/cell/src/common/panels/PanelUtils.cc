#include "ugt/ts/cell/panels/PanelUtils.h"

#include "ugt/ts/cell/Cell.h"
#include "ugt/ts/cell/version.h"

#include "swatchcell/framework/CellAbstract.h"
#include "swatchcell/framework/CellContext.h"

#include "ts/framework/CellAbstractContext.h"
#include "ts/framework/CellOperationFactory.h"
#include "ts/framework/CellOperation.h"



namespace ugt {

typedef ::swatchcellframework::RunControl tRunControlState;

namespace cmd {
  const std::string kPreparePrescale        = "uGtPreparePrescale";
  const std::string kApplyPrescale          = "uGtApplyPrescale";
  const std::string kPreparePreviewPrescale = "uGtPreparePreviewPrescale";
  const std::string kApplyPreviewPrescale   = "uGtApplyPreviewPrescale";
} // namespace cmd

const std::string kRoleProcessor          = "uGtProcessor";

// Locale used for formatting large numbers.
const char* LOCALE_UTF8 = "en_US.UTF-8";

// Constants for changing the prescales
const double    kUpdateThreshold    = 2.;   // seconds left before the lumisection increases to have enough time left for changing the prescales
const uint32_t  kRetryLimit         = 4;    // cycles trying to get into a new lumisection
const double    kSleepDelta         = 0.5;  // additional time (in sec) for sleeping

const uint32_t kDefaultSorter = 5;    // default selection for dropdown menu


bool
sortIndexAsc(const ugt::Row &a, const ugt::Row &b)
{
    return a.index < b.index;
}
bool
sortIndexDesc(const ugt::Row &a, const ugt::Row &b)
{
    return a.index > b.index;
}
bool
sortRateAsc(const ugt::Row &a, const ugt::Row &b)
{
    return a.rate < b.rate;
}
bool
sortRateDesc(const ugt::Row &a, const ugt::Row &b)
{
    return a.rate > b.rate;
}
bool sortAlgoAsc(const ugt::Row &a, const ugt::Row &b)
{
    return a.algo < b.algo;
}
bool
sortAlgoDesc(const ugt::Row &a, const ugt::Row &b)
{
    return a.algo > b.algo;
}
bool
sortRateCounterAsc(const ugt::Row &a, const ugt::Row &b)
{
    return a.ratecounter < b.ratecounter;
}
bool
sortRateCounterDesc(const ugt::Row &a, const ugt::Row &b)
{
    return a.ratecounter > b.ratecounter;
}

bool
sortModuleIdAsc(const ugt::Row &a, const ugt::Row &b)
{
    return a.moduleId < b.moduleId;
}
bool
sortModuleIdDesc(const ugt::Row &a, const ugt::Row &b)
{
    return a.moduleId > b.moduleId;
}


bool
isSystemRunningOrPaused(tsframework::CellAbstractContext *context)
{
  std::string state = getRunControlState(context);
  bool rc = ((state == tRunControlState::kStateRunning)
            or (state == tRunControlState::kStatePaused));
  return rc;
}

bool
isSystemConfiguredOrAligned(tsframework::CellAbstractContext* context)
{
  std::string state = getRunControlState(context);
  bool rc = ((state == tRunControlState::kStateConfigured)
            or (state == tRunControlState::kStateAligned));
  return rc;
}

bool
isSystemNotConfigured(tsframework::CellAbstractContext* context)
{
  std::string state = getRunControlState(context);
  bool rc = ((state == tRunControlState::kStateHalted)
            or (state == tRunControlState::kStateEngaged)
            or (state == tRunControlState::kStateSynchronized));
  return rc;
}

std::string
getRunControlState(tsframework::CellAbstractContext* context)
{
  tsframework::CellOperationFactory* lOpFactory = context->getOperationFactory();
  std::string state = lOpFactory->getOperation(swatchcellframework::CellContext::kRunControlOperationName).getFSM().getState();
  return state;
}


const ugt::uGtTriggerMenu&
getUgtTriggerMenu(tsframework::CellAbstractContext *context)
{
  swatchcellframework::CellContext *con = dynamic_cast<swatchcellframework::CellContext*>(context);
  swatchcellframework::CellContext::SharedGuard_t lGuard(*con);
  ugt::Cell* cell = dynamic_cast<ugt::Cell*>(con->getCell());
  return cell->getUgtTriggerMenu();
}

std::vector<std::string>
getBxMaskList(const std::map<unsigned int, unsigned int>& bxMaskMap)
{
  std::vector<std::string> vec;
  std::ostringstream oss;

  uint32_t sum = 0;
  for (auto cit = bxMaskMap.begin(); cit != bxMaskMap.end(); ++cit)
  {
    sum += cit->second;
  }

  if (sum >= kOrbitLength)
  {
    // algo is active
    return vec;
  }
  else if (sum == 0)
  {
    // algo is not active
    vec.push_back("none");
    return vec;
  }


  uint32_t swMask = 0;
  auto startIt = bxMaskMap.begin();
  auto it = bxMaskMap.begin();
  for (; it != bxMaskMap.end(); ++it)
  {
    uint32_t mask = it->second;
    if (mask == 1)
    {
      if (swMask == 0)
      {
        // rising edge 0->1

        // first 1 found
        startIt = it;
      }
    }
    else
    {
      if (swMask == 1)
      {
        // falling edge 1->0

        // found 0, thus last element was a 1
        auto endIt = it;
        --endIt;

        if (startIt->first == endIt->first)
          oss << startIt->first;
        else
          oss << startIt->first << "-"  << endIt->first;

        vec.push_back(oss.str());

        oss.str(""); oss.clear();
      }
    }

    swMask = mask;
  } // end loop bxMaskMap


  if (swMask == 1)
  {
    --it;
    if (startIt->first == it->first)
      oss << startIt->first;
    else
      oss << startIt->first << "-"  << it->first;

    vec.push_back(oss.str());
  }

  return vec;
}



Json::Value getUgtCellVersion()
{
  Json::Value root;
  std::string kUgtVersions = "ugtversion";


  std::stringstream oss;
  oss << SWATCHCELLUGT_VERSION_MAJOR << "."
    << SWATCHCELLUGT_VERSION_MINOR << "."
    << SWATCHCELLUGT_VERSION_PATCH;

  root[kUgtVersions] = oss.str();
  return root;
}

Json::Value getSortOptions(bool isShowDiagnosis)
{
    std::map<uint32_t, std::string> sortOptions;

    sortOptions.insert(std::make_pair(0, "Index (Lowest)"));
    sortOptions.insert(std::make_pair(1, "Index (Highest)"));
    sortOptions.insert(std::make_pair(2, "Algo (A-Z)"));
    sortOptions.insert(std::make_pair(3, "Algo (Z-A)"));
    sortOptions.insert(std::make_pair(4, "Rate (Lowest)"));
    sortOptions.insert(std::make_pair(5, "Rate (Highest)"));
    sortOptions.insert(std::make_pair(6, "Rate Counter (Lowest)"));
    sortOptions.insert(std::make_pair(7, "Rate Counter (Highest)"));

    if (isShowDiagnosis)
    {
      sortOptions.insert(std::make_pair(8, "Module Id (Lowest)"));
      sortOptions.insert(std::make_pair(9, "Module Id (Highest)"));
    }

    Json::Value root;
    Json::Value options(Json::arrayValue);

    const std::string kId = "id";
    const std::string kName = "name";
    const std::string kDefault = "default";
    const std::string kOptions = "options";

    for (const auto& it : sortOptions)
    {
      Json::Value opt;
      opt[kId] = it.first;
      opt[kName] = it.second;
      if (it.first == kDefaultSorter)
      {
        root[kDefault] = opt;
      }

      options.append(opt);
    }
    root[kOptions] = options;

    return root;
}



std::string translateVersion(uint32_t value)
{
  std::ostringstream oss;
  std::vector<uint32_t> vec = bitsplit(value, 3, 8);

  oss << vec[2] << "." << vec[1] << "." << vec[0];
  return oss.str();
}
std::string translateHex(uint32_t value)
{
  std::ostringstream oss;
  oss << "0x" << std::hex << std::setw(8) << std::setfill('0') << value;
  return oss.str();
}
std::string translateTimestamp(uint32_t value)
{
  std::ostringstream oss;
  std::time_t t = value;
  char buffer[20];
  std::strftime(buffer,20,"%Y-%m-%dT%H:%M:%S",std::localtime(&t));
  oss << buffer;
  return oss.str();
}
std::string translateUUID(std::vector<uint32_t> values)
{
  std::ostringstream oss;
  std::vector<uint32_t> slices;
  for (auto cit = values.rbegin(); cit != values.rend(); cit++)
  {
    std::vector<uint32_t> ret = bitsplit(*cit, 4, 8);
    for (auto cit2 = ret.rbegin(); cit2 != ret.rend(); cit2++)
    {
      slices.push_back(*cit2);
    }
  }

  uint32_t sep = 0;
  for (auto cit = slices.begin(); cit != slices.end(); cit++)
  {
      oss << std::hex << *cit << std::hex;
      if (sep == 3 || sep == 5 || sep == 7 || sep == 9)
        oss << "-";
      sep++;
  }

  return oss.str();
}

std::string translateString(std::vector<uint32_t> values)
{
  std::stringstream oss;
  for (auto cit = values.begin(); cit != values.end(); cit++)
  {
    std::vector<uint32_t> ret = bitsplit(*cit, 4, 8);
    for (auto cit2 = ret.begin(); cit2 != ret.end(); cit2++)
    {
      char c = static_cast<char>(*cit2);
      if (c == '\0')
        return oss.str();

      oss << c;
    }
  }
  return oss.str();
}


uint32_t bitmask(uint32_t n)
{
  return (1 << n) - 1;
}
uint32_t bitjoin(std::vector<uint32_t> values, uint32_t width)
{
  uint32_t mask = bitmask(width);
  uint32_t result = 0;
  for (uint32_t i = 0; i < values.size(); i++)
  {
    uint32_t value = values[i];
    result |= ((value & mask) << (i * width));
  }
  return result;
}

std::vector<uint32_t> bitsplit(uint32_t value, uint32_t n, uint32_t width)
{
  std::vector<uint32_t> vec;
  uint32_t mask = bitmask(width);

  for (uint32_t i = 0; i < n; i++)
  {
    uint32_t slice = ((value >> (i*width)) & mask);
    vec.push_back(slice);
  }

  return vec;
}

} // namespace ugt
