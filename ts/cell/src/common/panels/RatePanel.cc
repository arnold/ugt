#include <iostream>
#include <time.h>
#include <string>
#include <locale>

#include <boost/pointer_cast.hpp>

#include "ajax/toolbox.h"
#include "ajax/PolymerElement.h"
#include "jsoncpp/json/json.h"

#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"

#include "swatch/action/GateKeeper.hpp"
#include "swatchcell/framework/CellContext.h"
#include "ugt/swatch/uGtProcessor.h"
#include "ugt/swatch/uGtAlgo.h"
#include "ugt/swatch/uGtFinorProcessor.h"
#include "ugt/swatch/uGtFinorAlgo.h"
#include "ugt/ts/cell/Cell.h"
#include "ugt/ts/cell/panels/RatePanel.h"

using namespace ugt;


// Metric keys
const std::string kPrescaleIndexInUse = "prescaleColumnIndexInUse";
const std::string kLuminositySegmentNumber = "luminositySegmentNumber";
const std::string kRateFinOrTCDS = "FinorTCDSRate";
const std::string kRateLocalFinOr = "RATE_LocalFinOR";
const std::string kCounterLocalFinOr = "LocalFinOR";
const std::string kOtfInUse = "prescOtfInUse";


const std::string kMetricValueUnknown = "n/a";

const uint32_t defaultSel = 5;    // default selection for dropdown menu


RatePanel::RatePanel( tsframework::CellAbstractContext* context, log4cplus::Logger& logger ) : tsframework::CellPanel(context,logger)
{
    mLumisectionPrev = 0;
}


RatePanel::~RatePanel()
{
    remove();
}


void RatePanel::layout(cgicc::Cgicc& cgi)
{
  remove();
  setEvent("RatePanel::getVersion", ajax::Eventable::OnClick, this, &RatePanel::getVersion);
  setEvent("RatePanel::getDumpVersions", ajax::Eventable::OnClick, this, &RatePanel::getDumpVersions);
  setEvent("RatePanel::getTable", ajax::Eventable::OnTime, this, &RatePanel::getTable);
  setEvent("RatePanel::refreshFinor", ajax::Eventable::OnTime, this, &RatePanel::refreshFinor);
  setEvent("RatePanel::isSystemReady", ajax::Eventable::OnTime, this, &RatePanel::isSystemReady);
  setEvent("RatePanel::setupSortOptions", ajax::Eventable::OnTime, this, &RatePanel::setupSortOptions);

  add(new ajax::PolymerElement("rate-panel"));

  mPrescaleIndexPrev = 0;

  try
  {
    if (isSystemRunningOrPaused(getContext()))
    {
      mPrescaleIndexPrev = getPrescaleIndexPrevious();
    }
  }
  catch (MetricValueUnknownException &e)
  {
    LOG4CPLUS_ERROR(getLogger(), e.what());
  }
}


std::vector<const uGtFinorProcessor*> RatePanel::getFinorProcessor()
{
  std::vector<const uGtFinorProcessor*> finorProcessor;

  // if (isSystemRunningOrPaused(getContext()))
  {
    swatchcellframework::CellContext *context = dynamic_cast<swatchcellframework::CellContext*>(getContext());
    ugt::Cell* cell = dynamic_cast<ugt::Cell*>(context->getCell());
    finorProcessor = cell->getFinorProcessor();
  }

  return finorProcessor;
}


std::vector<const uGtProcessor*> RatePanel::getProcessors()
{
  std::vector<const uGtProcessor*> processors;

  // if (isSystemRunningOrPaused(getContext()))
  {
    swatchcellframework::CellContext *context = dynamic_cast<swatchcellframework::CellContext*>(getContext());
    ugt::Cell* cell = dynamic_cast<ugt::Cell*>(context->getCell());
    processors = cell->getUgtProcessors();
  }

  return processors;
}


const uGtProcessor* RatePanel::getProcessorModule(const uint32_t moduleId)
{
  // if (isSystemRunningOrPaused(getContext()))
  {
    swatchcellframework::CellContext *context = dynamic_cast<swatchcellframework::CellContext*>(getContext());
    ugt::Cell* cell = dynamic_cast<ugt::Cell*>(context->getCell());
    return cell->getUgtProcessorModule(moduleId);
  }

  return NULL;
}




void RatePanel::getTable(cgicc::Cgicc& cgi,std::ostream& out)
{
  const std::string kAlgo = "algo";
  const std::string kIndex = "index";
  const std::string kRate = "rate";
  const std::string kRateCounter = "ratecounter";
  const std::string kPrescale = "prescale";
  const std::string kFinor = "finor";
  const std::string kBxMask = "bxmask";
  const std::string kVeto = "veto";
  const std::string kModuleId = "moduleid";

  const std::string kPrescaleInUse = "presc";
  const std::string kPrescaleLabelInUse = "presclabel";
  const std::string kPrescaleOtf = "prescotf";

  const std::string kClass = "class";

  Json::Value root;

  if (isSystemRunningOrPaused(getContext()))
  {

    try
    {
      mPrescaleIndexPrev = getPrescaleIndexPrevious();
      root[kPrescaleInUse] = mPrescaleIndexPrev;
      root[kPrescaleOtf] = isUsingOtfPrevious();
    }
    catch (MetricValueUnknownException &e)
    {
      LOG4CPLUS_ERROR(getLogger(), e.what());
      root[kPrescaleInUse] = kMetricValueUnknown;
      root[kPrescaleOtf] = false;
    }


    try
    {
      Json::Value items(Json::arrayValue);


      // reset rows
      mRows.clear();

      const uGtTriggerMenu& menu = getUgtTriggerMenu(getContext());

      auto itAlgo = menu.begin();
      if (itAlgo != menu.end())
      {
        const Algorithm* pAlgo = itAlgo->second;
        root[kPrescaleLabelInUse] = pAlgo->getFormattedLabelForPsIndex(mPrescaleIndexPrev);
      }

      // prepare table rows and fill with data from algos
      fillRows(menu);

      if (mRows.empty())
      {
        LOG4CPLUS_WARN(getLogger(), "RatePanel: rows set is empty");
        return;
      }

      for (auto it = mRows.begin(); it != mRows.end(); it++)
      {
        Json::Value row;

        Row &r = *it;
        row[kIndex] = r.index;
        row[kAlgo] = r.algo;
        // row[kBxMask] = r.bxmask;
        row[kPrescale] = r.prescale;
        row[kFinor] = r.finor;
        row[kVeto] = r.veto;
        row[kModuleId] = r.moduleId;

        row[kRate] = r.rate;
        row[kRateCounter] = r.ratecounter;

        Json::Value bxMaskArr(Json::arrayValue);
        for (auto bxIt = r.bxmask.begin(); bxIt != r.bxmask.end(); bxIt++)
        {
          bxMaskArr.append(*bxIt);
        }
        row[kBxMask] = bxMaskArr;

        items.append(row);
      }

      root["items"] = items;
    }
    catch (MetricValueUnknownException &e)
    {
      LOG4CPLUS_ERROR(getLogger(), e.what());
      out << "[]";
      return;
    }
  }

  // Create column header row
  Json::Value columns(Json::arrayValue);

  Json::Value colIndex;
  colIndex["name"] = "Index";
  colIndex[kClass] = kIndex;
  root["colindex"] = colIndex;

  Json::Value colAlgo;
  colAlgo["name"] = "Algorithm";
  colAlgo[kClass] = kAlgo;
  root["colalgo"] = colAlgo;

  Json::Value colBxMask;
  colBxMask["name"] = "Bx Mask";
  colBxMask[kClass] = kBxMask;
  root["colbxmask"] = colBxMask;

  Json::Value colPrescale;
  colPrescale["name"] = "Prescale";
  colPrescale[kClass] = kPrescale;
  root["colprescale"] = colPrescale;

  Json::Value colFinor;
  colFinor["name"] = "Mask";
  colFinor[kClass] = kFinor;
  root["colfinor"] = colFinor;

  Json::Value colRate;
  colRate["name"] = "Rate after/prescales (Hz)";
  colRate[kClass] = kRate;
  root["colrate"] = colRate;

  Json::Value colRateCounter;
  colRateCounter["name"] = "Counter after/prescales";
  colRateCounter[kClass] = kRateCounter;
  root["colratecounter"] = colRateCounter;

  Json::Value colVeto;
  colVeto["name"] = "Veto";
  colVeto[kClass] = kVeto;
  root["colveto"] = colVeto;

  Json::Value colModuleId;
  colModuleId["name"] = "Module Id";
  colModuleId[kClass] = kModuleId;
  root["colmoduleid"] = colModuleId;

  out << root;
}


void RatePanel::refreshFinor(cgicc::Cgicc& cgi,std::ostream& out)
{

  if (!isSystemRunningOrPaused(getContext()))
  {
      out << "[]";
      return;
  }


  const std::string kFinorRate = "finorrate";
  const std::string kFinorState = "finorstate";
  const std::string kLumiSeg = "lumiseg";
  const std::string kRefreshTable = "refreshTable";
  const std::string kShowTable = "showtable";

  const std::string kClassNormal = "Good";
  const std::string kClassAlert = "Error";
  const std::string kClassUnknown = "Unknown";

  Json::Value root;
  bool refreshTab = false;
  bool showTable = true;
  std::string finorstate = kClassUnknown;

  std::ostringstream oss;

  try
  {
    double finorSum = getFinorRate();
    oss.imbue(std::locale(LOCALE_UTF8));
    oss << std::fixed << std::setprecision(2) << finorSum;

    if(finorSum > 100000.0)
      finorstate = kClassAlert;
    else if(finorSum == 0.0)
      finorstate = kClassUnknown;
    else
      finorstate = kClassNormal;
  }
  catch (MetricValueUnknownException &e)
  {
    oss << kMetricValueUnknown;
    finorstate = kClassUnknown;
    showTable = false;
  }
  root[kFinorRate] = oss.str();
  root[kFinorState] = finorstate;

  oss.str(""); oss.clear();

  try
  {
    const uint32_t lumisection = getLuminositySection();
    if(lumisection != mLumisectionPrev)
    {
      refreshTab = true;
      mLumisectionPrev = lumisection;
    }
    if (lumisection > 1)
    {
        // Note: displayed rates refer to previous luminosity segment!
        root[kLumiSeg] = lumisection - 1;
    }
    else
    {
        // Still no valid data available.
        root[kLumiSeg] = kMetricValueUnknown;
        root[kFinorRate] = kMetricValueUnknown;
        root[kFinorState] = kClassUnknown;
        showTable = false;
    }
  }
  catch (MetricValueUnknownException &e)
  {
    root[kLumiSeg] = kMetricValueUnknown;
    showTable = false;
  }

  root[kRefreshTable] = refreshTab;
  root[kShowTable] = showTable;


  std::map<uint32_t, LocalFinorData> mapLocalfinorsMP7 = getLocalFinorRateMP7();
  Json::Value localfinor(Json::arrayValue);
  std::map<uint32_t, LocalFinorData> mapLocalfinorsFinorBoard = getLocalFinorRateFinorBoard();
  Json::Value localfinorFinorBoard(Json::arrayValue);

  const std::string kFinorModuleId = "moduleid";
  const std::string kFinorRateMP7 = "ratemp7";
  const std::string kFinorRateFinorBoard = "ratefinorboard";
  const std::string kFinorCounterMP7 = "countermp7";
  const std::string kFinorCounterFinorBoard = "counterfinorboard";

  for(auto cit = mapLocalfinorsMP7.begin(); cit != mapLocalfinorsMP7.end(); cit++)
  {
    uint32_t moduleId = cit->first;
    LocalFinorData finorDataMP7 = cit->second;

    Json::Value val;
    std::stringstream os;
    os << moduleId;

    auto citFinorBoard = mapLocalfinorsFinorBoard.find(moduleId);
    if (citFinorBoard == mapLocalfinorsFinorBoard.end())
    {
      std::stringstream ss;
      ss << "RatePanel: Could not find local finor on Finorboard for moduleId: " << moduleId;
      LOG4CPLUS_WARN(getLogger(), ss.str());
      continue;
    }
    LocalFinorData finorDataFinorBoard = citFinorBoard->second;

    val[kFinorModuleId] = moduleId;

    val[kFinorRateMP7] =  finorDataMP7.finorRate;
    val[kFinorCounterMP7] = finorDataMP7.finorCounter;

    val[kFinorRateFinorBoard] = finorDataFinorBoard.finorRate;
    val[kFinorCounterFinorBoard] = finorDataFinorBoard.finorCounter;

    localfinor.append(val);
  }
  root["localfinors"] = localfinor;


  out << root;
}


bool RatePanel::isGateKeeperReady()
{
  typedef ::swatchcellframework::RunControl tRunControlState;
  tsframework::CellOperationFactory* lOpFactory = getContext()->getOperationFactory();
  std::string state = lOpFactory->getOperation(swatchcellframework::CellContext::kRunControlOperationName).getFSM().getState();
  bool rc = ((state == tRunControlState::kStateRunning)
            or (state == tRunControlState::kStatePaused));
  return rc;
}


void RatePanel::isSystemReady(cgicc::Cgicc& cgi,std::ostream& out)
{
  out << isSystemRunningOrPaused(getContext());
}


void RatePanel::setupSortOptions(cgicc::Cgicc& cgi,std::ostream& out)
{
  std::string sShowDiagnosis = ajax::toolbox::getSubmittedValue(cgi,"showDiagnosis");
  bool isShowDiagnosis = (sShowDiagnosis=="true");

  std::vector<Sorter> vecSortOptions;

  vecSortOptions.push_back(Sorter(0, "Index (Lowest)", &sortIndexAsc));
  vecSortOptions.push_back(Sorter(1, "Index (Highest)", &sortIndexDesc));
  vecSortOptions.push_back(Sorter(2, "Algo (A-Z)", &sortAlgoAsc));
  vecSortOptions.push_back(Sorter(3, "Algo (Z-A)", &sortAlgoDesc));
  vecSortOptions.push_back(Sorter(4, "Rate (Lowest)", &sortRateAsc));
  vecSortOptions.push_back(Sorter(5, "Rate (Highest)", &sortRateDesc));
  vecSortOptions.push_back(Sorter(6, "Rate Counter (Lowest)", &sortRateCounterAsc));
  vecSortOptions.push_back(Sorter(7, "Rate Counter (Highest)", &sortRateCounterDesc));

  if (isShowDiagnosis)
  {
    vecSortOptions.push_back(Sorter(8, "Module Id (Lowest)", &sortModuleIdAsc));
    vecSortOptions.push_back(Sorter(9, "Module Id (Highest)", &sortModuleIdDesc));
  }

  Json::Value root;
  Json::Value options(Json::arrayValue);

  const std::string kId = "id";
  const std::string kName = "name";
  const std::string kDefault = "default";
  const std::string kOptions = "options";

  for (auto it = vecSortOptions.begin(); it != vecSortOptions.end(); it++)
  {
    Json::Value opt;
    opt[kId] = it->id;
    opt[kName] = it->name;
    if (it->id == defaultSel)
    {
      root[kDefault] = opt;
    }

    options.append(opt);

  }
  root[kOptions] = options;

  out << root;
}

void RatePanel::getVersion(cgicc::Cgicc& cgi,std::ostream& out)
{
  out << getUgtCellVersion();
}
void RatePanel::getDumpVersions(cgicc::Cgicc& cgi,std::ostream& out)
{
  Json::Value root;

  Json::Value mp7Header(Json::arrayValue);
  Json::Value mp7Body(Json::arrayValue);


  Json::Value l1tm_name(Json::arrayValue);
  Json::Value module_id(Json::arrayValue);
  Json::Value uuid(Json::arrayValue);
  Json::Value uuid_fw(Json::arrayValue);
  Json::Value gtl(Json::arrayValue);
  Json::Value fdl(Json::arrayValue);
  Json::Value l1tm_compiler(Json::arrayValue);
  Json::Value timestamp(Json::arrayValue);
  Json::Value hostname(Json::arrayValue);
  Json::Value username(Json::arrayValue);
  Json::Value ugt_fw_build(Json::arrayValue);
  Json::Value mp7_fw_version(Json::arrayValue);
  Json::Value mp7_board(Json::arrayValue);
  Json::Value frame(Json::arrayValue);

  //inital column
  module_id.append("module_id");
  mp7Header.append("Register");
  l1tm_name.append("l1tm_name");
  uuid.append("l1tm UUID");
  uuid_fw.append("l1tm UUID fw");
  gtl.append("gtl firmware version");
  fdl.append("fdl firmware version");
  l1tm_compiler.append("VHDL producer version");
  timestamp.append("timestamp (synthesis)");
  hostname.append("hostname");
  username.append("username");
  ugt_fw_build.append("uGT fw build version");
  mp7_fw_version.append("MP7 firmware version");
  mp7_board.append("MP7 board revision");
  frame.append("payload (frame) version");

  std::vector<const uGtProcessor*> uGtProcessors = getProcessors();
  if (uGtProcessors.size() < 1)
  {
    out << "[]";
    return;
  }

  for (auto it = uGtProcessors.begin(); it != uGtProcessors.end(); it++)
  {
      const uGtProcessor& p = dynamic_cast<const uGtProcessor&>(*(*it));
      ::mp7::MP7Controller& driver = const_cast<uGtProcessor&>(p).driver();
      ::uhal::HwInterface& hw = driver.hw();

      // read registers from hardware
      const ::uhal::Node& node_l1tm_name = hw.getNode("gt_mp7_gtlfdl.read_versions.l1tm_name");
      ::uhal::ValVector<uint32_t> data_l1tm_name = node_l1tm_name.readBlock(node_l1tm_name.getSize());

      const ::uhal::Node& node_l1tm_uuid = hw.getNode("gt_mp7_gtlfdl.read_versions.l1tm_uuid");
      ::uhal::ValVector<uint32_t> data_l1tm_uuid = node_l1tm_uuid.readBlock(node_l1tm_uuid.getSize());

      const ::uhal::Node& node_l1tm_fw_uuid = hw.getNode("gt_mp7_gtlfdl.read_versions.l1tm_fw_uuid");
      ::uhal::ValVector<uint32_t> data_l1tm_uuid_fw = node_l1tm_fw_uuid.readBlock(node_l1tm_fw_uuid.getSize());

      const ::uhal::Node& node_hostname = hw.getNode("gt_mp7_frame.module_info.hostname");
      ::uhal::ValVector<uint32_t> data_hostname = node_hostname.readBlock(node_hostname.getSize());

      const ::uhal::Node& node_username = hw.getNode("gt_mp7_frame.module_info.username");
      ::uhal::ValVector<uint32_t> data_username = node_username.readBlock(node_username.getSize());




      ::uhal::ValWord<uint32_t> data_l1tm_compiler = hw.getNode("gt_mp7_gtlfdl.read_versions.l1tm_compiler_version").read();
      ::uhal::ValWord<uint32_t> data_gtl = hw.getNode("gt_mp7_gtlfdl.read_versions.gtl_fw_version").read();
      ::uhal::ValWord<uint32_t> data_fdl = hw.getNode("gt_mp7_gtlfdl.read_versions.fdl_fw_version").read();
      ::uhal::ValWord<uint32_t> data_module_id = hw.getNode("gt_mp7_gtlfdl.read_versions.module_id").read();
      ::uhal::ValWord<uint32_t> data_frame = hw.getNode("gt_mp7_frame.module_info.frame_version").read();

      ::uhal::ValWord<uint32_t> data_ugt_fw_build = hw.getNode("gt_mp7_frame.module_info.build_version").read();
      ::uhal::ValWord<uint32_t> data_timestamp = hw.getNode("gt_mp7_frame.module_info.timestamp").read();

      ::uhal::ValWord<uint32_t> data_fwrev_design = hw.getNode("ctrl.id.fwrev.design").read();
      ::uhal::ValWord<uint32_t> data_fwrev_a = hw.getNode("ctrl.id.fwrev.a").read();
      ::uhal::ValWord<uint32_t> data_fwrev_b = hw.getNode("ctrl.id.fwrev.b").read();
      ::uhal::ValWord<uint32_t> data_fwrev_c = hw.getNode("ctrl.id.fwrev.c").read();

      hw.getClient().dispatch();


      Json::Value mp7;
      std::ostringstream oss;

      mp7["role"] = p.getStub().role;
      mp7["slot"] = p.getStub().slot;
      uint32_t moduleid = data_module_id.value();

      l1tm_name.append(translateString(data_l1tm_name.value()));
      module_id.append(moduleid);
      uuid.append(translateUUID(data_l1tm_uuid.value()));
      uuid_fw.append(translateUUID(data_l1tm_uuid_fw.value()));


      gtl.append(translateVersion(data_gtl.value()));
      fdl.append(translateVersion(data_fdl.value()));
      l1tm_compiler.append(translateVersion(data_l1tm_compiler.value()));

      timestamp.append(translateTimestamp(data_timestamp.value()));
      hostname.append(translateString(data_hostname.value()));
      username.append(translateString(data_username.value()));

      ugt_fw_build.append(translateHex(data_ugt_fw_build.value()));

      oss << data_fwrev_a.value() << "." << data_fwrev_b.value() << "." << data_fwrev_c.value();
      mp7_fw_version.append(oss.str());

      mp7_board.append(data_fwrev_design.value());

      frame.append(translateVersion(data_frame.value()));

      oss.clear(); oss.str("");
      // oss << "Module " << moduleid;
      oss << "Slot " << p.getStub().slot;

      mp7Header.append(oss.str());
  }

  mp7Body.append(module_id);
  mp7Body.append(l1tm_name);
  mp7Body.append(uuid);
  mp7Body.append(uuid_fw);
  mp7Body.append(l1tm_compiler);
  mp7Body.append(timestamp);
  mp7Body.append(hostname);
  mp7Body.append(username);
  mp7Body.append(ugt_fw_build);
  mp7Body.append(mp7_fw_version);
  mp7Body.append(mp7_board);
  mp7Body.append(frame);
  mp7Body.append(gtl);
  mp7Body.append(fdl);

  root["mp7Header"] = mp7Header;
  root["mp7Body"] = mp7Body;

  out << root;
}


std::string RatePanel::getRateId(const uint32_t id)
{
  std::stringstream ss;
  ss << ALGO_RATE << SEPARATOR << std::setfill('0') << std::setw(3) << id;
  return ss.str();
}
std::string RatePanel::getRateCounterId(const uint32_t id)
{
  std::stringstream ss;
  ss << ALGO_COUNT << SEPARATOR << std::setfill('0') << std::setw(3) << id;
  return ss.str();
}
std::string RatePanel::getLocalFinorRateId(const uint32_t id)
{
  std::stringstream ss;
  ss << "LocalFinorRate" << std::setfill('0') << std::setw(1) << id;
  return ss.str();
}
std::string RatePanel::getLocalFinorRateCounterId(const uint32_t id)
{
  std::stringstream ss;
  ss << "LocalFinorCounter" << std::setfill('0') << std::setw(1) << id;
  return ss.str();
}


void RatePanel::fillRows(const ugt::uGtTriggerMenu& menu)
{
  for (auto cit = menu.begin(); cit != menu.end(); cit++)
  {
    const Algorithm *tmAlgo = cit->second;

    Row r;
    uint32_t algoIndex = tmAlgo->getIndex();
    // Fill id and name
    r.index = algoIndex;
    r.algo = tmAlgo->getName();

    // Fill veto
    r.veto = tmAlgo->getVeto();

    // fill mask
    r.finor = tmAlgo->getMask();

    // fill prescale
    r.prescale = tmAlgo->getPrescale(mPrescaleIndexPrev);

    // fill bx mask
    std::vector<std::string> vecBxMask = getBxMaskList(tmAlgo->getAlgoBxMask());
    r.bxmask = vecBxMask;

    const uint32_t moduleId = tmAlgo->getModuleId();
    // fill module id
    r.moduleId = moduleId;

    const uGtProcessor* proc = getProcessorModule(moduleId);
    if (proc == NULL)
    {
      std::cout << "module pointer is null" << std::endl;
      r.rate = 0.0;
      r.ratecounter = 0;
      continue;
    }

    const uGtAlgo& algo = dynamic_cast<const uGtAlgo&>(proc->getAlgo());
    swatch::core::MetricReadGuard metric_guard(algo);

    // fill rates
    const std::string rateId = getRateId(tmAlgo->getIndex());
    const ::swatch::core::AbstractMetric& metric_ls = algo.getMetric(rateId);
    const ::swatch::core::MetricSnapshot& ss_ls = metric_ls.getSnapshot();
    if (ss_ls.isValueKnown())
    {
      double rate = ss_ls.getValue<double>();
      r.rate = rate;
    }
    else
    {
      std::stringstream ss;
      ss << "RatePanel SNAPSHOT unknown for rateId=" << rateId;
      LOG4CPLUS_ERROR(getLogger(), ss.str());
      throw MetricValueUnknownException(ss.str());
    }

    // fill rate counters
    const std::string rateCounterId = getRateCounterId(tmAlgo->getIndex());
    const ::swatch::core::AbstractMetric& metric_rc = algo.getMetric(rateCounterId);
    const ::swatch::core::MetricSnapshot& ss_rc = metric_rc.getSnapshot();
    if (ss_rc.isValueKnown())
    {
      double ratecounter = ss_rc.getValue<uint32_t>();
      r.ratecounter = ratecounter;
    }
    else
    {
      std::stringstream ss;
      ss << "RatePanel SNAPSHOT unknown for rateCounterId=" << rateCounterId;
      LOG4CPLUS_ERROR(getLogger(), ss.str());
      throw MetricValueUnknownException(ss.str());
    }


    // add all used algos to table
    // do sorting and in javascript
    mRows.push_back(r);
  }
}



double RatePanel::getFinorRate()
{
  double finorSum = 0.0;

  swatchcellframework::CellContext *context = dynamic_cast<swatchcellframework::CellContext*>(getContext());
  swatchcellframework::CellContext::SharedGuard_t lGuard(*context);

  std::vector<const uGtFinorProcessor*> finorProcessor = getFinorProcessor(); // should be only one processor
  for (auto it = finorProcessor.begin(); it != finorProcessor.end(); it++)
  {
    const uGtFinorProcessor& proc = dynamic_cast<const uGtFinorProcessor&>(*(*it));
    const uGtFinorAlgo& algo = dynamic_cast<const uGtFinorAlgo&>(proc.getAlgo());

    const ::swatch::core::AbstractMetric& metric_ls = algo.getMetric(kRateFinOrTCDS);
    const ::swatch::core::MetricSnapshot& ss_ls = metric_ls.getSnapshot();
    if (not ss_ls.isValueKnown())
    {
      LOG4CPLUS_ERROR(getLogger(), "RatePanel SNAPSHOT unknown for metric FinorTCDSRate");
      throw MetricValueUnknownException(kRateFinOrTCDS);
    }
    else
    {
      finorSum += ss_ls.getValue<double>();
    }
  }

  return finorSum;
}



std::map<uint32_t, LocalFinorData> RatePanel::getLocalFinorRateMP7()
{
  std::map<uint32_t, LocalFinorData> result;

  swatchcellframework::CellContext *context = dynamic_cast<swatchcellframework::CellContext*>(getContext());
  swatchcellframework::CellContext::SharedGuard_t lGuard(*context);

  std::vector<const uGtProcessor*> processors = getProcessors();
  for (auto it = processors.begin(); it != processors.end(); it++)
  {
    const uGtProcessor& proc = dynamic_cast<const uGtProcessor&>(*(*it));
    const uGtAlgo& algo = dynamic_cast<const uGtAlgo&>(proc.getAlgo());

    swatch::core::MetricReadGuard metric_guard(algo);

    LocalFinorData localFinorData;
    localFinorData.moduleId = algo.getModuleId();

    { // MP7 FINOR RATE
      const ::swatch::core::AbstractMetric& metric_ls = algo.getMetric(kRateLocalFinOr);
      const ::swatch::core::MetricSnapshot& ss_ls = metric_ls.getSnapshot();

      localFinorData.finorRate = ss_ls.isValueKnown() ? ss_ls.getValue<double>() : 0.0;
    }

    { // MP7 FINOR COUNTER
      const ::swatch::core::AbstractMetric& metric_ls = algo.getMetric(kCounterLocalFinOr);
      const ::swatch::core::MetricSnapshot& ss_ls = metric_ls.getSnapshot();

      localFinorData.finorCounter = ss_ls.isValueKnown() ? ss_ls.getValue<uint32_t>() : 0;
    }

    // add LocalFinorData to result map
    result.insert(std::make_pair(algo.getModuleId(), localFinorData));
  }
  return result;
}



std::map<uint32_t, LocalFinorData> RatePanel::getLocalFinorRateFinorBoard()
{
  std::map<uint32_t, LocalFinorData> result;

  swatchcellframework::CellContext *context = dynamic_cast<swatchcellframework::CellContext*>(getContext());
  swatchcellframework::CellContext::SharedGuard_t lGuard(*context);

  std::vector<const uGtFinorProcessor*> processors = getFinorProcessor();
  for (auto it = processors.begin(); it != processors.end(); it++)
  {
    const uGtFinorProcessor& proc = dynamic_cast<const uGtFinorProcessor&>(*(*it));
    const uGtFinorAlgo& algo = dynamic_cast<const uGtFinorAlgo&>(proc.getAlgo());

    swatch::core::MetricReadGuard metric_guard(algo);

    for (size_t moduleid = 0; moduleid < kMaxUgtModules; moduleid++)
    {
      LocalFinorData localFinorData;
      localFinorData.moduleId = moduleid;

      { // AMC502(FINORBOARD) FINOR RATE
        const std::string localFinorRateId = getLocalFinorRateId(moduleid);
        const ::swatch::core::AbstractMetric& metric_ls = algo.getMetric(localFinorRateId);
        const ::swatch::core::MetricSnapshot& ss_ls = metric_ls.getSnapshot();

        localFinorData.finorRate = ss_ls.isValueKnown() ? ss_ls.getValue<double>() : 0.0;
      }

      { // AMC502(FINORBOARD) FINOR COUNTER
        const std::string localFinorRateCounterId = getLocalFinorRateCounterId(moduleid);
        const ::swatch::core::AbstractMetric& metric_ls = algo.getMetric(localFinorRateCounterId);
        const ::swatch::core::MetricSnapshot& ss_ls = metric_ls.getSnapshot();

        localFinorData.finorCounter = ss_ls.isValueKnown() ? ss_ls.getValue<uint32_t>() : 0;
      }

      // add LocalFinorData to result map
      result.insert(std::make_pair(moduleid, localFinorData));
    }
  }
  return result;
}



uint32_t RatePanel::getLuminositySection()
{
  swatchcellframework::CellContext *context = dynamic_cast<swatchcellframework::CellContext*>(getContext());
  swatchcellframework::CellContext::SharedGuard_t lGuard(*context);

  std::vector<const uGtProcessor*> uGtProcessors = getProcessors();

  uint32_t lumiSeg = 0;
  auto pit = uGtProcessors.begin();
  if (pit != uGtProcessors.end())
  {
    const uGtProcessor& p = dynamic_cast<const uGtProcessor&>(*(*pit));
    const uGtAlgo& algo = dynamic_cast<const uGtAlgo&>(p.getAlgo());

    const ::swatch::core::AbstractMetric& metric_ls = algo.getMetric(kLuminositySegmentNumber);
    const ::swatch::core::MetricSnapshot& ss_ls = metric_ls.getSnapshot();
    if (not ss_ls.isValueKnown())
    {
      LOG4CPLUS_ERROR(getLogger(), "RatePanel luminosity segment nr. is unknown");
      throw MetricValueUnknownException(kLuminositySegmentNumber);
    }
    lumiSeg = ss_ls.getValue<uint32_t>();
  }
  return lumiSeg;
}


uint32_t RatePanel::getPrescaleIndexPrevious()
{
  swatchcellframework::CellContext *context = dynamic_cast<swatchcellframework::CellContext*>(getContext());
  swatchcellframework::CellContext::SharedGuard_t lGuard(*context);

  std::vector<const uGtProcessor*> uGtProcessors = getProcessors();

  uint32_t presc = 0;
  auto pit = uGtProcessors.begin();
  if (pit != uGtProcessors.end())
  {
    const uGtProcessor& p = dynamic_cast<const uGtProcessor&>(*(*pit));
    const uGtAlgo& algo = dynamic_cast<const uGtAlgo&>(p.getAlgo());

    const ::swatch::core::AbstractMetric& metric_ls = algo.getMetric(kPrescaleIndexInUse);
    const ::swatch::core::MetricSnapshot& ss_ls = metric_ls.getSnapshot();
    if (not ss_ls.isValueKnown())
    {
      LOG4CPLUS_ERROR(getLogger(), "RatePanel prescale column index in use is unknown");
      throw MetricValueUnknownException(kPrescaleIndexInUse);
    }
    presc = ss_ls.getValue<uint32_t>();
  }
  return presc;
}

bool
RatePanel::isUsingOtfPrevious()
{
  std::vector<const uGtProcessor*> uGtProcessors = getProcessors();

  bool isOtfPrevious = false;
  auto pit = uGtProcessors.begin();
  if (pit != uGtProcessors.end())
  {
    const uGtProcessor& p = dynamic_cast<const uGtProcessor&>(*(*pit));
    const uGtAlgo& algo = dynamic_cast<const uGtAlgo&>(p.getAlgo());

    const ::swatch::core::AbstractMetric& metric_ls = algo.getMetric(kOtfInUse);
    const ::swatch::core::MetricSnapshot& ss_ls = metric_ls.getSnapshot();
    if (not ss_ls.isValueKnown())
    {
      LOG4CPLUS_ERROR(getLogger(), "RatePanel otf flag in use is unknown");
      throw MetricValueUnknownException(kOtfInUse);
    }

    if (ss_ls.getValue<uint32_t>() == 1)
      isOtfPrevious = true;
  }
  return isOtfPrevious;
}
