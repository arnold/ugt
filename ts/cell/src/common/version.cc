#include "ugt/ts/cell/version.h"
#include "config/version.h"

GETPACKAGEINFO(ugt)

void
ugt::checkPackageDependencies() throw (config::PackageInfo::VersionException)
{
  	CHECKDEPENDENCY(config);
}


std::set<std::string, std::less<std::string> >
ugt::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;
	ADDDEPENDENCY(dependencies,config);
	return dependencies;
}
// eof
