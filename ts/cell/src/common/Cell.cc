#include "ugt/ts/cell/Cell.h"

#include "ugt/ts/cell/xmlMenu2Gatekeeper.h"
#include "ugt/ts/cell/CustomRunControl.h"
#include "ugt/ts/cell/commands/EditPrescalesCommand.h"
#include "ugt/ts/cell/commands/PrescaleColumnCommands.h"

#include "ugt/ts/cell/panels/PrescalesPanel.h"
#include "ugt/ts/cell/panels/RatePanel.h"
#include "ugt/ts/cell/panels/PreviewPanel.h"

#include "ugt/swatch/uGtProcessor.h"
#include "ugt/swatch/uGtAlgo.h"
#include "ugt/swatch/uGtFinorProcessor.h"
#include "ugt/swatch/uGtFinorPreviewProc.h"
#include "ugt/swatch/uGtTriggerMenu.h"

#include "swatchcell/framework/RedirectPanel.h"
#include "swatchcell/framework/tools/utilities.h"

#include "ts/framework/CellFactory.h"
#include "ts/framework/CellPanelFactory.h"

#include "swatch/system/System.hpp"

#include <boost/pointer_cast.hpp>
#include <unistd.h>

XDAQ_INSTANTIATOR_IMPL(ugt::Cell)

namespace ugt
{

Cell::Cell(xdaq::ApplicationStub * s)
:
swatchcellframework::CellAbstract(s, TypeCarrier<CustomRunControl>()),
monitoringDataProducer_("Prod", *getContext(), kMonitoringDataProducerPollInterval),
monitoringDataConsumer_("Consumer", *getContext(), kMonitoringDataConsumerPollInterval)
{
}


Cell::~Cell()
{
}


void
Cell::init()
{
  getContext()->addImport("/ugt/ts/cell/html/elements/elements.html");
  addGenericSwatchComponents();

  tsframework::CellPanelFactory* lPanelFactory = getContext()->getPanelFactory();
  lPanelFactory->add<swatchcellframework::RedirectPanel>("Home");
  lPanelFactory->add<ugt::PrescalesPanel>("uGT Prescales");
  lPanelFactory->add<ugt::RatePanel>("uGT Trigger Rates");
  lPanelFactory->add<ugt::PreviewPanel>("uGT Preview Rates");

  getContext()->getMonitoringThread().setPeriod(5);

  LOG4CPLUS_INFO(getLogger(), "processing xml Menu ...");
  getContext()->registerCustomConfigDbClob("L1_MENU", ::ugt::xmlMenu2Gatekeeper);
  LOG4CPLUS_INFO(getLogger(), "processing xml Menu ... done");


  tsframework::CellFactory* cmd_factory = getContext()->getCommandFactory();
  cmd_factory->add<EditPrescales>("uGT", "EditPrescales");
  cmd_factory->add<GetCurrentPrescaleColumn>("uGT", "GetCurrentPrescaleColumn");
  cmd_factory->add<GetCurrentPrescaleColumnList>("uGT", "GetCurrentPrescaleColumnList");
}


const std::vector<const uGtFinorProcessor*>
Cell::getFinorProcessor()
{
  std::vector<const uGtFinorProcessor*> finorProcessor;

  swatchcellframework::CellContext& context = *getContext();
  swatchcellframework::CellContext::SharedGuard_t lGuard(context);
  swatch::system::System& lSys = context.getSystem(lGuard);

  typedef std::deque <swatch::processor::Processor*>::const_iterator ProcessorIt_t;
  for (ProcessorIt_t lIt = lSys.getProcessors().begin(); lIt != lSys.getProcessors().end(); lIt++)
  {
    const swatch::processor::Processor* proc = *lIt;
    if (not proc->getStatus().isEnabled()) continue;
    if (proc->getStub().creator == "ugt::uGtFinorProcessor")
    {
      finorProcessor.push_back(dynamic_cast<const uGtFinorProcessor*>(proc));
    }
  }

  return finorProcessor;
}

const std::vector<const uGtFinorPreviewProc*>
Cell::getFinorPreviewProcessor()
{
  std::vector<const uGtFinorPreviewProc*> finorPreviewProcessor;

  swatchcellframework::CellContext& context = *getContext();
  swatchcellframework::CellContext::SharedGuard_t lGuard(context);
  swatch::system::System& lSys = context.getSystem(lGuard);

  typedef std::deque <swatch::processor::Processor*>::const_iterator ProcessorIt_t;
  for (ProcessorIt_t lIt = lSys.getProcessors().begin(); lIt != lSys.getProcessors().end(); lIt++)
  {
    const swatch::processor::Processor* proc = *lIt;
    if (not proc->getStatus().isEnabled()) continue;
    if (proc->getStub().creator == "ugt::uGtFinorPreviewProc")
    {
      finorPreviewProcessor.push_back(dynamic_cast<const uGtFinorPreviewProc*>(proc));
    }
  }
  return finorPreviewProcessor;
}

const std::vector<const uGtProcessor*>
Cell::getUgtProcessors()
{
  std::vector<const uGtProcessor*> uGtProcessors;

  swatchcellframework::CellContext& context = *getContext();
  swatchcellframework::CellContext::SharedGuard_t lGuard(context);
  swatch::system::System& lSys = context.getSystem(lGuard);

  typedef std::deque <swatch::processor::Processor*>::const_iterator ProcessorIt_t;
  for (ProcessorIt_t lIt = lSys.getProcessors().begin(); lIt != lSys.getProcessors().end(); lIt++)
  {
    const swatch::processor::Processor* proc = *lIt;
    if (not proc->getStatus().isEnabled()) continue;
    if (proc->getStub().creator == "ugt::uGtProcessor")
    {
      uGtProcessors.push_back(dynamic_cast<const uGtProcessor*>(proc));
    }
  }

  return uGtProcessors;
}

const uGtProcessor*
Cell::getUgtProcessorModule(const uint32_t moduleId)
{
  std::vector<const uGtProcessor*> uGtProcessors = getUgtProcessors();

  swatchcellframework::CellContext& context = *getContext();
  swatchcellframework::CellContext::SharedGuard_t lGuard(context);

  for (auto pit = uGtProcessors.begin(); pit != uGtProcessors.end(); pit++)
  {
    const uGtProcessor* proc = dynamic_cast<const uGtProcessor*>(*pit);
    const uGtAlgo& algo = dynamic_cast<const uGtAlgo&>(proc->getAlgo());

    if (moduleId == algo.getModuleId())
      return proc;
  }

  std::cout << "pointer is null" << std::endl;
  return NULL;
}

const uGtTriggerMenu&
Cell::getUgtTriggerMenu()
{
  uGtProcessor* processor = NULL;

  swatchcellframework::CellContext& context = *getContext();
  swatchcellframework::CellContext::SharedGuard_t lGuard(context);
  swatch::system::System& lSys = context.getSystem(lGuard);

  typedef std::deque <swatch::processor::Processor*>::const_iterator ProcessorIt_t;
  for (ProcessorIt_t lIt = lSys.getProcessors().begin(); lIt != lSys.getProcessors().end(); lIt++)
  {
    swatch::processor::Processor* proc = *lIt;
    if (not proc->getStatus().isEnabled()) continue;
    if (proc->getStub().creator == "ugt::uGtProcessor")
    {
      processor = (dynamic_cast<uGtProcessor*>(proc));
      break;
    }
  }

  if (processor == NULL)
  {
    throw TriggerMenuIsNullException("could not load uGtTriggerMenu, no processor available");
  }

  uGtAlgo& algo = dynamic_cast<uGtAlgo&>(processor->getAlgo());
  uGtTriggerMenu& menu = algo.getTriggerMenu();
  return menu;
}

} // end
