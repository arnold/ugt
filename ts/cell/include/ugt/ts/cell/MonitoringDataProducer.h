/**
 * @author      Takashi Matsushita
 * @note        based on MonitoringThread.h by tom
 *
 */
/** @todo make this work */

#ifndef _ugt_ts_cell_MonitoringDataProducer_h
#define	_ugt_ts_cell_MonitoringDataProducer_h


// Standard headers
#include <stddef.h>
#include <queue>
#include <set>

// boost headers
#include "boost/chrono.hpp"
#include "boost/noncopyable.hpp"
#include "boost/thread/condition_variable.hpp"
#include "boost/thread/mutex.hpp"
#include "boost/thread/thread.hpp"

// log4cplus headers
#include "log4cplus/logger.h"

#include "ugt/ts/cell/MonitoringDataTypes.h"


namespace swatchcellframework
{
  class CellContext;
}


namespace ugt
{

class MonitoringDataProducer : public boost::noncopyable
{
public:
  MonitoringDataProducer(const std::string& aParentLoggerName,
                         ::swatchcellframework::CellContext& aContext,
                         size_t aPeriodInSeconds);

  ~MonitoringDataProducer();

  size_t getPeriod() const;

  //! Sets the new period for monitoring updates, which takes effect immediately
  void setPeriod(size_t aPeriod);

  bool isActive() const;

  //! Starts the monitoring update loop; does nothing if monitoring loop is already started
  void start();

  //! Stops the monitoring update loop; current iteration of monitoring loop will continue, but thread will sleep before next iteration of loop
  void stop();

  //! Triggers the monitoring thread to wake up and update metrics now (if currently asleep); returns immediately
  void updateNow();

  //! Method
  void operator() ();

  void getLumiSectionData(LumiSectionData& data);

private:
  //! Iterates over the enabled processors in the system, assemble rate monitoring data from metrics
  void assembleMetrics();

  //! Mutex, locked to prevent corruption of member data that's read/written from multiple threads (i.e. settable from public API)
  mutable boost::mutex mMutex;

  log4cplus::Logger mLogger;

  ::swatchcellframework::CellContext& mContext;

  //! Minimum time (in seconds) between monitoring updates cycles
  size_t mPeriod;

  //! Indicates if monitoring loop has been stopped / started
  bool mActive;

  //! Indicates if the monitoring loop should update immediately upon waking; set via updateNow method
  bool mUpdateNow;

  //! Indicates if destructor has been called
  bool mSelfDestruct;

  typedef boost::chrono::steady_clock Clock_t;

  //! Time at which the next update will be performed
  Clock_t::time_point mNextUpdateTime;

  //! Thread in which the monitoring loop is executed
  boost::thread mThread;

  //! Condition variable for notifying monitoring thread of
  boost::condition_variable mConditionVar;

  unsigned long lastProcessedLumiSection_;
  std::set<unsigned long> lumiSections_;
  std::queue<LumiSectionData> fifo_;
};


} // end ns: ugt


#endif	/* _ugt_ts_cell_MonitoringDataProducer_h */
