#ifndef _UGT_TS_CELL_PANELS_PANELUTILS_H_
#define _UGT_TS_CELL_PANELS_PANELUTILS_H_

#include <iostream>
#include <iomanip>
#include <vector>
#include "jsoncpp/json/json.h"
#include "swatch/core/exception.hpp"
#include "swatchcell/framework/CellContext.h"
#include "ugt/swatch/Constants.h"
#include "ugt/swatch/uGtTriggerMenu.h"

namespace tsframework {
  class CellAbstractContext;
  class CellOperationFactory;
  class CellOperation;
}

namespace ugt {

DEFINE_SWATCH_EXCEPTION(MetricValueUnknownException);

namespace toolbox {
  class PrescaleTable;
  class PrescaleSet;
}

namespace cmd {
  // uGt swatch command constants
  extern const std::string kPreparePrescale;
  extern const std::string kApplyPrescale;
  extern const std::string kPreparePreviewPrescale;
  extern const std::string kApplyPreviewPrescale;
} // namespace cmds

extern const std::string kRoleProcessor;

// Locale used for formatting large numbers.
extern const char* LOCALE_UTF8;

// Constants for changing the prescales
extern const double    kUpdateThreshold;   // in seconds
extern const uint32_t  kRetryLimit;        // cycles trying to get into a new lumisection
extern const double    kSleepDelta;        // additional time (in sec) for sleeping




// Represents one row of the rates table
struct Row
{
  public:
    Row()
      : index ()
      , algo ()
      , rate (0.0)
      , ratecounter (0)
      , veto (0)
      , finor (1)
      , prescale (0)
      , bxmaskStr()
      , bxmask()
      , moduleId()
      , hasOtfPrescale (false)
      { }

    uint32_t index;   // algo index
    std::string algo; // algo name
    double rate;      // rate
    uint32_t ratecounter; // rate counter
    uint32_t veto;
    uint32_t finor;
    uint32_t prescale; // current used prescale
    std::string bxmaskStr; // bx mask
    std::vector<std::string> bxmask; // bx mask
    uint32_t moduleId;
    bool hasOtfPrescale; // true...customized prescale (has been changed on the fly),
                         // false...normal prescale from prescale table
};

typedef bool (*funct_t)(const Row &a, const Row &b);


class Sorter
{
  public:
    Sorter(uint32_t id, std::string name, funct_t sort)
      : id(id)
      , name(name)
      , sort(sort)
      { }

    uint32_t id;       // sort identifier
    std::string name;  // displayed name
    funct_t sort;      // sorter method
};


template<typename T> T
convert(const std::string& ss)
{
  T data;
  std::istringstream(ss) >> data;
  return data;
}

bool sortIndexAsc(const Row &a, const Row &b);
bool sortIndexDesc(const Row &a, const Row &b);
bool sortRateAsc(const Row &a, const Row &b);
bool sortRateDesc(const Row &a, const Row &b);
bool sortAlgoAsc(const Row &a, const Row &b);
bool sortAlgoDesc(const Row &a, const Row &b);
bool sortRateCounterAsc(const Row &a, const Row &b);
bool sortRateCounterDesc(const Row &a, const Row &b);

bool sortModuleIdAsc(const Row &a, const Row &b);
bool sortModuleIdDesc(const Row &a, const Row &b);

bool isSystemRunningOrPaused(tsframework::CellAbstractContext *context);
bool isSystemConfiguredOrAligned(tsframework::CellAbstractContext *context);
bool isSystemNotConfigured(tsframework::CellAbstractContext *context);
std::string getRunControlState(tsframework::CellAbstractContext* context);

const ugt::uGtTriggerMenu& getUgtTriggerMenu(tsframework::CellAbstractContext *context);

std::vector<std::string> getBxMaskList(const std::map<unsigned int, unsigned int>& bxMaskMap);

Json::Value getUgtCellVersion();
Json::Value getSortOptions(bool isShowDiagnosis);

uint32_t bitmask(uint32_t n);
uint32_t bitjoin(std::vector<uint32_t> values, uint32_t width);
std::vector<uint32_t> bitsplit(uint32_t value, uint32_t n, uint32_t width);


std::string translateVersion(uint32_t value);
std::string translateHex(uint32_t value);
std::string translateTimestamp(uint32_t value);
std::string translateUUID(std::vector<uint32_t> values);
std::string translateString(std::vector<uint32_t> values);

} // namespace ugt

#endif
