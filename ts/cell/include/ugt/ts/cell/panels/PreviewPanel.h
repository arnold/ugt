#ifndef _UGT_TS_CELL_PANELS_PREVIEWPANEL_H_
#define _UGT_TS_CELL_PANELS_PREVIEWPANEL_H_

#include "ts/framework/CellPanel.h"

#include "ugt/ts/cell/Cell.h"
#include "ugt/ts/cell/panels/PanelUtils.h"
#include "ugt/swatch/toolbox/MemoryImage.h"
#include "ugt/swatch/toolbox/PrescaleSet.h"

#include "xdata/Table.h"
#include "cgicc/Cgicc.h"

#include <iostream>


namespace ugt {

class uGtFinorPreviewProc;
class uGtFinorPreviewAlgo;
class uGtProcessor;

class PreviewPanel: public tsframework::CellPanel
{

public:
  PreviewPanel(tsframework::CellAbstractContext* context, log4cplus::Logger& logger);
  ~PreviewPanel();

  void layout(cgicc::Cgicc& cgi);

private:
  uint32_t mLumisectionPrev;     // previous lumisection
  uint32_t mPrescaleIndexPrev;   // previous prescale index in use

  uint32_t mLumisectionPrescChanged; // lumisection where rate update is expected
  uint32_t mWillChangeToPrescale;    // preview prescale index to change to
  bool mUsingOtf;  // preview prescale otf flag to change to

  std::vector<Row> mRows;
  bool mShowTable;


  void getVersion(cgicc::Cgicc& cgi,std::ostream& out);
  void getTable(cgicc::Cgicc& cgi,std::ostream& out);
  void isSystemReady(cgicc::Cgicc& cgi,std::ostream& out);
  void setupSortOptions(cgicc::Cgicc& cgi,std::ostream& out);
  void changePreviewPrescale(cgicc::Cgicc& cgi,std::ostream& out);
  void refreshData(cgicc::Cgicc& cgi,std::ostream& out);
  void allProcessorsAvailable(cgicc::Cgicc& cgi,std::ostream& out);
  void getCurrentPreviewPrescales(cgicc::Cgicc& cgi,std::ostream& out);

  bool isSystemReady();

  std::string getPreviewRateId(const uint32_t id);
  std::string getPreviewRateCounterId(const uint32_t id);

  void fillRowsFromMenu();
  Json::Value& constructColumns(Json::Value& root);
  Json::Value& constructRows(Json::Value& root);
  Json::Value& constructTableHeader(Json::Value& root);

  std::vector<const uGtFinorPreviewProc*> getFinorPreviewProcessor();
  std::vector<const uGtProcessor*> getProcessors();
  const uGtProcessor* getProcessorModule(const uint32_t moduleId);
  double getPreviewFinorRate();
  uint32_t getLuminositySection();
  uint32_t getPreviewPrescaleIndexPrevious();
  uint32_t getPreviewPrescaleIndex();
  bool isUsingPreviewOtf();
  bool isUsingPreviewOtfPrevious();

  void initOtfPreviewPrescales(const uint32_t previewPrescale);

  void updateLumisectionPrescChanged(const uint32_t value);

  // prepares the memory image for changing the prescale values on the fly
  // and updates the otf prescale set based on the retrieved parameters
  ugt::toolbox::MemoryImage<uint32_t> preparePrescalesImage(const uGtTriggerMenu& menu,
    const std::map<std::string, std::string>& parameters, const std::string& column);

  OtfPreviewSet& getOtfPreviewSet();

};

} // namespace ugt

#endif
