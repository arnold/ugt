#ifndef _UGT_TS_CELL_PANELS_RATEPANEL_H_
#define _UGT_TS_CELL_PANELS_RATEPANEL_H_

#include <iostream>

#include "xdata/Table.h"

#include "cgicc/Cgicc.h"

#include "ts/framework/CellPanel.h"


#include "ugt/swatch/uGtTriggerMenu.h"
#include "ugt/swatch/uGtProcessor.h"
#include "ugt/ts/cell/panels/PanelUtils.h"

namespace ugt {

struct LocalFinorData {
  uint32_t moduleId;
  double finorRate;
  uint32_t finorCounter;
};

class RatePanel: public tsframework::CellPanel
{

 public:
  RatePanel(tsframework::CellAbstractContext* context, log4cplus::Logger& logger);
  ~RatePanel();

  void layout(cgicc::Cgicc& cgi);

 private:
   uint32_t mLumisectionPrev;     // previous lumisection
   uint32_t mPrescaleIndexPrev;   // previous prescale index in use

   std::vector<Row> mRows;

   void getVersion(cgicc::Cgicc& cgi,std::ostream& out);
   void getDumpVersions(cgicc::Cgicc& cgi,std::ostream& out);
   void getTable(cgicc::Cgicc& cgi,std::ostream& out);
   void refreshFinor(cgicc::Cgicc& cgi,std::ostream& out);
   void isSystemReady(cgicc::Cgicc& cgi,std::ostream& out);
   void setupSortOptions(cgicc::Cgicc& cgi,std::ostream& out);

   bool isGateKeeperReady();

   std::string getRateId(const uint32_t id);
   std::string getRateCounterId(const uint32_t id);
   std::string getLocalFinorRateId(const uint32_t id);
   std::string getLocalFinorRateCounterId(const uint32_t id);

   void fillRows(const ugt::uGtTriggerMenu& menu);

   // get local finor rate/counter of all MP7 board (sent to AMC502 FinorBoard)
   std::map<uint32_t, LocalFinorData> getLocalFinorRateMP7();
   // get local finor rate/counter of AMC502 FinorBoard (received from MP7 board)
   std::map<uint32_t, LocalFinorData> getLocalFinorRateFinorBoard();

   std::vector<const uGtFinorProcessor*> getFinorProcessor();
   std::vector<const uGtProcessor*> getProcessors();
   const uGtProcessor* getProcessorModule(const uint32_t moduleId);
   double getFinorRate();
   uint32_t getLuminositySection();
   uint32_t getPrescaleIndexPrevious();
   bool isUsingOtfPrevious();


};

} // namespace ugt

#endif
