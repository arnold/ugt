#ifndef _UGT_TS_CELL_PANELS_PRESCALESPANEL_H_
#define _UGT_TS_CELL_PANELS_PRESCALESPANEL_H_

#include "ts/framework/CellPanel.h"

#include "ugt/swatch/toolbox/PrescaleTable.h"

#include "swatch/core/exception.hpp"

#include "xdata/Table.h"
#include "cgicc/Cgicc.h"

#include <iostream>

namespace ugt {

class uGtProcessor;


class PrescalesPanel: public tsframework::CellPanel
{

public:
  PrescalesPanel(tsframework::CellAbstractContext* context, log4cplus::Logger& logger);
  ~PrescalesPanel();

  void layout(cgicc::Cgicc& cgi);

private:

   toolbox::PrescaleTable mPrescaleTable;

   void getVersion(cgicc::Cgicc& cgi,std::ostream& out);
   void getTable(cgicc::Cgicc& cgi,std::ostream& out);
   void changePrescale(cgicc::Cgicc& cgi, std::ostream& out);
   void isSystemReady(cgicc::Cgicc& cgi,std::ostream& out);
   void readCurrentPrescaleIndex(cgicc::Cgicc& cgi,std::ostream& out);

   bool isSystemReady();

   uint32_t getPrescaleIndex();
   bool isUsingOtf();

   std::vector<const uGtProcessor*> getProcessors();
};

} // namespace ugt

#endif
