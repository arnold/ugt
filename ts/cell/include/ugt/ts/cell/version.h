#ifndef _swatchcell_ugt_version_h_
#define _swatchcell_ugt_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define SWATCHCELLUGT_VERSION_MAJOR 0
#define SWATCHCELLUGT_VERSION_MINOR 24
#define SWATCHCELLUGT_VERSION_PATCH 0


//
// Template macros
//
#define SWATCHCELLUGT_VERSION_CODE PACKAGE_VERSION_CODE(LEVEL1SWATCHUGT_VERSION_MAJOR,LEVEL1SWATCHUGT_VERSION_MINOR,LEVEL1SWATCHUGT_VERSION_PATCH)
#ifndef SWATCHCELLUGT_PREVIOUS_VERSIONS
#define SWATCHCELLUGT_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(LEVEL1SWATCHUGT_VERSION_MAJOR,LEVEL1SWATCHUGT_VERSION_MINOR,LEVEL1SWATCHUGT_VERSION_PATCH)
#else
#define SWATCHCELLUGT_FULL_VERSION_LIST  LEVEL1SWATCHUGT_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(LEVEL1SWATCHUGT_VERSION_MAJOR,LEVEL1SWATCHUGT_VERSION_MINOR,LEVEL1SWATCHUGT_VERSION_PATCH)
#endif

namespace ugt
{
	const std::string package  = "ugt";
	const std::string versions = SWATCHCELLUGT_FULL_VERSION_LIST;
	const std::string description = "uGT Trigger Supervisor cell for SWATCH-based systems";
	const std::string authors = "Gregor Aradi, Bernhard Arnold, Takashi Matsushita";
	const std::string summary = "Contains a uGT Trigger Supervisor cell for a SWATCH-based control system";
	const std::string link = "https://cactus.web.cern.ch";

	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
