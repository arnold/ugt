/**
 * @author    Takashi Matsushita
 */
/** @todo nope */

#ifndef _ugt_ts_cell_MonitoringDataTypes_h
#define _ugt_ts_cell_MonitoringDataTypes_h

#include <utility>
#include <map>
#include <set>
#include <string>


namespace ugt
{

extern const unsigned long kMonitoringDataProducerPollInterval;
extern const unsigned long kMonitoringDataConsumerPollInterval;

extern const uint32_t kPhysicsGenFDLIndex;


extern const std::set<std::string> MONITORING_DATA_KEYS;

typedef std::pair<unsigned long, std::string> AlgoData;
typedef std::multimap <std::string, AlgoData > MonitoringData;

struct LumiSectionData
{
  bool is_valid;
  unsigned long lumi_section;
  unsigned long prescale_index;
  MonitoringData data;

  LumiSectionData()
    : is_valid(false), lumi_section(-1), prescale_index(-1), data() { };

  void reset()
  {
    is_valid = false;
    lumi_section = -1;
    prescale_index = -1;
    data.clear();
  };
};


typedef std::map<unsigned long, unsigned long long> AlgoSummaryData;
typedef std::map <std::string, AlgoSummaryData > SummaryData;
struct RunSummaryData
{
  bool is_valid;
  unsigned long ls_first;
  unsigned long ls_last;
  unsigned long num_ls;
  SummaryData data;

  RunSummaryData()
    : is_valid(false), ls_first(-1), ls_last(-1), num_ls(0), data() { };

  void reset()
  {
    is_valid = false;
    ls_first = -1;
    ls_last = -1;
    num_ls = 0;
    data.clear();
  };
};

} // end ns: ugt

#endif // _ugt_ts_cell_MonitoringDataTypes_h
