/**
 * @author        Takashi Matsushita
 */
/** @todo make this work */

#ifndef _ugt_ts_cell_DatabaseInterface_h
#define _ugt_ts_cell_DatabaseInterface_h

#include "ugt/swatch/uGtTriggerMenu.h"
#include "log4cplus/loggingmacros.h"

#include <string>
#include <map>

namespace tscandela
{
  class DatabaseConnection;
  class Row;
}

namespace ugt
{
  class LumiSectionData;
  class RunSummaryData;
}


namespace ugtdbi {

  extern const std::string UserName;
  extern const std::string PwdPath;

  template<typename T> T convert(const std::string& s);

  template<typename T> std::string toString(const T d);

  std::string trim(const std::string& source,
                   char const* whites = " \t\r\n");

  void toLower(std::string& str);

  void fillLumiSection(tscandela::DatabaseConnection* dbc,
                       const unsigned long lumi_section,
                       const unsigned long prescale_index,
                       log4cplus::Logger* logger=0);

  void fillAlgoScalers(tscandela::DatabaseConnection* dbc,
                       const ugt::LumiSectionData& ls,
                       log4cplus::Logger* logger=0);

  void fillAlgoScalersSummary(tscandela::DatabaseConnection* dbc,
                       log4cplus::Logger* logger=0);

  void fillAlgoSetting(tscandela::DatabaseConnection* dbc,
                       const ugt::TriggerMenu& menu,
                       log4cplus::Logger* logger=0);

  void fillPrescales(tscandela::DatabaseConnection* dbc,
                     const std::map<unsigned long, std::map<unsigned long, double> >& prescales,
                     log4cplus::Logger* logger=0);

  void fillPrescaleNames(tscandela::DatabaseConnection* dbc,
           const std::map<unsigned long, std::string>& prescaleNameSet,
           log4cplus::Logger* logger=0);

  void fillAlgoBxMask(tscandela::DatabaseConnection* dbc,
                      const std::map<unsigned long, std::map<unsigned long, unsigned long> >& algoBxMask,
                      log4cplus::Logger* logger=0);

  void setRunNumber(const unsigned int run_number);
  unsigned int getRunNumber();

  void disableDatabaseAccess();
  void enableDatabaseAccess();

  void reset();

} // ns: ugtdbi

#endif // _ugt_ts_cell_DatabaseInterface_h
