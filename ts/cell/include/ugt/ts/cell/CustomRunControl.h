#ifndef _ugt_ts_CustomRunControl_h_
#define _ugt_ts_CustomRunControl_h_

#include <map>
#include "swatchcell/framework/RunControl.h"

namespace ugt
{

class uGtTriggerMenu;

typedef std::map< unsigned long, std::map<unsigned long, double> > PrescalesDBMap;
typedef std::map< unsigned long, std::map<unsigned long, unsigned long> > AlgoBXMaskDBMap;
typedef std::map< unsigned long, std::string > PrescaleNameDBMap;


class CustomRunControl : public swatchcellframework::RunControl
{
public:
  CustomRunControl(log4cplus::Logger& log,
                   tsframework::CellAbstractContext* context);
  ~CustomRunControl() = default;

private:

  void execPostStart();
  void execPreStop();

  PrescalesDBMap preparePrescaleDBMap(const uGtTriggerMenu& l1menu);
  AlgoBXMaskDBMap prepareAlgoBXMaskDBMap(const uGtTriggerMenu& l1menu);
  PrescaleNameDBMap preparePrescaleNameDBMap(const uGtTriggerMenu& l1menu);
};

} // end ns ugt

# endif
