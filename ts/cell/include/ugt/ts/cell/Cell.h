#ifndef _ugt_ts_Cell_h_
#define _ugt_ts_Cell_h_

#include "swatchcell/framework/CellAbstract.h"

#include "ugt/ts/cell/MonitoringDataProducer.h"
#include "ugt/ts/cell/MonitoringDataConsumer.h"

#include "ugt/swatch/toolbox/PrescaleSet.h"
#include "ugt/swatch/toolbox/MaskTable.h"

#include "swatch/core/exception.hpp"

namespace ugt {
  
class uGtProcessor;
class uGtAlgo;
class uGtFinorProcessor;
class uGtFinorAlgo;
class uGtFinorPreviewProc;
class uGtTriggerMenu;

DEFINE_SWATCH_EXCEPTION(TriggerMenuIsNullException);

/** Structure gathering the on-the-fly prescales
    and corresponding on-the-fly finor mask **/
struct OtfPreviewSet {
  toolbox::PrescaleSet prescaleSet;
  toolbox::FinorMaskTable finorMaskTable;

  OtfPreviewSet()
    : prescaleSet(), finorMaskTable()
  { }

  bool isEmpty() const
  {
    return prescaleSet.isEmpty() && (finorMaskTable.size() <= 0);
  }

  void clear()
  {
    prescaleSet.clear();
    finorMaskTable.clear();
  }

};

class Cell : public swatchcellframework::CellAbstract
{


public:

  XDAQ_INSTANTIATOR();

  Cell(xdaq::ApplicationStub * s);

  ~Cell();

  //!This method should be filled with addCommand, addOperation and addPanel that corresponds to that Cell
  void init();

  void addSwatchComponents();

  MonitoringDataProducer& getMonitoringDataProducer() { return monitoringDataProducer_; };
  MonitoringDataConsumer& getMonitoringDataConsumer() { return monitoringDataConsumer_; };

  const std::vector<const uGtFinorProcessor*> getFinorProcessor();
  const std::vector<const uGtFinorPreviewProc*> getFinorPreviewProcessor();
  const std::vector<const uGtProcessor*> getUgtProcessors();
  const uGtProcessor* getUgtProcessorModule(const uint32_t moduleId);
  const uGtTriggerMenu& getUgtTriggerMenu();

  OtfPreviewSet& getOtfPreviewSet() { return mOtfPreviewSet; };

private:
  Cell(const Cell&);

  MonitoringDataProducer monitoringDataProducer_;
  MonitoringDataConsumer monitoringDataConsumer_;

   // contains preview prescales and finor mask on-the-fly
  OtfPreviewSet mOtfPreviewSet;
};

} // end ns ugt

#endif
