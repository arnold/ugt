#ifndef XmlMenu2Gatekeeper_h

#include "boost/shared_ptr.hpp"
#include "pugixml.hpp"

#include <string>

namespace ugt
{
  boost::shared_ptr<pugi::xml_document> xmlMenu2Gatekeeper(const std::string& menu);
}

#endif
