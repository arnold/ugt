#ifndef _UGT_TS_COMMANDS_PRESCALECOLUMN_H_
#define _UGT_TS_COMMANDS_PRESCALECOLUMN_H_

#include "ugt/ts/cell/commands/uGtCellCommand.h"
#include "log4cplus/logger.h"

#include "ts/framework/CellAbstractContext.h"

#include "jsoncpp/json/json.h"

namespace ugt
{

class uGtTriggerMenu;

class GetCurrentPrescaleColumn: public uGtCellCommand
{
public:
  GetCurrentPrescaleColumn(log4cplus::Logger& log, tsframework::CellAbstractContext* context);
  virtual void code();

private:
  Json::Value generateJson();
  uint32_t getPrescaleIndexInUse();
  bool isUsingOtfInUse();
};


class GetCurrentPrescaleColumnList: public uGtCellCommand
{
public:
  GetCurrentPrescaleColumnList(log4cplus::Logger& log, tsframework::CellAbstractContext* context);
  virtual void code();

private:
  Json::Value generateJson();
};

}
#endif /* _UGT_TS_COMMANDS_PRESCALECOLUMN_H_ */
