#ifndef _UGT_TS_COMMANDS_EDITPRESCALES_H_
#define _UGT_TS_COMMANDS_EDITPRESCALES_H_

#include "ugt/ts/cell/commands/uGtCellCommand.h"
#include "log4cplus/logger.h"

#include "ts/framework/CellAbstractContext.h"
#include "ugt/swatch/toolbox/MemoryImage.h"

namespace ugt
{

class uGtTriggerMenu;

class EditPrescales: public uGtCellCommand
{
public:
  EditPrescales(log4cplus::Logger& log, tsframework::CellAbstractContext* context);
  virtual void code();

private:
  void preparePrescalesImage(const uGtTriggerMenu& ugtMenu);

  toolbox::MemoryImage<uint32_t> mImage;

};

}
#endif /* _UGT_TS_COMMANDS_EDITPRESCALES_H_ */
