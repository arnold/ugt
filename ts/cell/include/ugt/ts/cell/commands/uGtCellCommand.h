#ifndef _UGT_TS_COMMANDS_UGTCELLCOMMAND_H_
#define _UGT_TS_COMMANDS_UGTCELLCOMMAND_H_

#include "log4cplus/logger.h"

#include "ts/framework/CellAbstractContext.h"
#include "ts/framework/CellCommand.h"

namespace ugt
{

class Cell;

class uGtCellCommand: public tsframework::CellCommand
{
public:
  uGtCellCommand(log4cplus::Logger& log, tsframework::CellAbstractContext* context);
  virtual void code() = 0;

  bool isProcessorsAndMenuAvailable();
  bool isProcessorsAvailable();
  bool isMenuAvailable();
  bool isSystemReady();

  ugt::Cell* getUgtCell();

};

}
#endif /* _UGT_TS_COMMANDS_UGTCELLCOMMAND_H_ */
