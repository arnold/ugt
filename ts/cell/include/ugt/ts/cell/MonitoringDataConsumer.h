/**
 * @author      Takashi Matsushita
 * @note        based on MonitoringThread.h by tom
 *
 */
/** @todo set run_number_ */

#ifndef _ugt_ts_cell_MonitoringDataConsumer_h
#define	_ugt_ts_cell_MonitoringDataConsumer_h


// Standard headers
#include <stddef.h>
#include <queue>
#include <set>

// boost headers
#include "boost/chrono.hpp"
#include "boost/noncopyable.hpp"
#include "boost/thread/condition_variable.hpp"
#include "boost/thread/mutex.hpp"
#include "boost/thread/thread.hpp"

// log4cplus headers
#include "log4cplus/logger.h"

#include "ugt/ts/cell/MonitoringDataTypes.h"


namespace swatchcellframework
{
  class CellContext;
}


namespace ugt
{
class MonitoringDataProducer;


class MonitoringDataConsumer : public boost::noncopyable
{
public:
  MonitoringDataConsumer(const std::string& aParentLoggerName,
                         ::swatchcellframework::CellContext& aContext,
                         size_t aPeriodInSeconds);

  ~MonitoringDataConsumer();

  size_t getPeriod() const;

  //! Sets the new period for monitoring updates, which takes effect immediately
  void setPeriod(size_t aPeriod);

  bool isActive() const;

  //! Starts the monitoring update loop; does nothing if monitoring loop is already started
  void start();

  //! Stops the monitoring update loop; current iteration of monitoring loop will continue, but thread will sleep before next iteration of loop
  void stop();

  //! Triggers the monitoring thread to wake up and update metrics now (if currently asleep); returns immediately
  void updateNow();

  //! Method
  void operator() ();

  void setProducer(MonitoringDataProducer* producer) { producer_ = producer; };

private:
  //! Iterates over the enabled processors & AMC13s in the system, and calls the SWATCH API's "update metrics" methods
  void consumeLumiSectionData();

  //! Mutex, locked to prevent corruption of member data that's read/written from multiple threads (i.e. settable from public API)
  mutable boost::mutex mMutex;

  log4cplus::Logger mLogger;

  ::swatchcellframework::CellContext& mContext;

  //! Minimum time (in seconds) between monitoring updates cycles
  size_t mPeriod;

  //! Indicates if monitoring loop has been stopped / started
  bool mActive;

  //! Indicates if the monitoring loop should update immediately upon waking; set via updateNow method
  bool mUpdateNow;

  //! Indicates if destructor has been called
  bool mSelfDestruct;

  typedef boost::chrono::steady_clock Clock_t;

  //! Time at which the next update will be performed
  Clock_t::time_point mNextUpdateTime;

  //! Thread in which the monitoring loop is executed
  boost::thread mThread;

  //! Condition variable for notifying monitoring thread of
  boost::condition_variable mConditionVar;

  MonitoringDataProducer *producer_;
};


} // end ns: ugt


#endif	/* _ugt_ts_cell_MonitoringDataConsumer_h */
