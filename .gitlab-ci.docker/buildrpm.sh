#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
IFS=$'\n\t'

cd `dirname $0`/..
imagename=gitlab-registry.cern.ch/cms-cactus/svn2git/software/projects/ugt/buildenv
dockerdir=/xdaq/makefiles/demand/a/ridiculously/long/path/wherin/to/put/your/project/fun/fun/fun

docker run -it --mount type=bind,source="$(pwd)",target=$dockerdir $imagename "cd $dockerdir && rm -rf */rpm && bash && make clean && make rpm"
