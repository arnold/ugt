#!/usr/bin/env python

"""Downloading prescales from Google docs spreadsheet in CSV format and writing
to XML table ready to be imported to the database.

Usage: python prescaleconv.py <URL> [-o <FILE>]
"""

import xml.etree.ElementTree as etree
import xml.dom.minidom as minidom
import argparse
import urllib2
import csv
import re
import sys, os

# ------------------------------------------------------------------------------
#  Constants
# ------------------------------------------------------------------------------

DefaultParamId='prescales'
DefaultContextId='uGtProcessor'
DefaultPrescaleSet=0
MaxAlgorithms=512
ExportParams='/export?format=csv'

# Regular expression for Google spreadsheet URL scheme
# https://docs.google.com/spreadsheets/d/1sJFxiJzS_f38Oxi_NJHLGncs6QOksLz6UzRqbjauFMY/
UrlRegex = re.compile(r'(https://docs\.google\.com/spreadsheets/[\w\d]+/[\w\d_-]+)/?.*')

# ------------------------------------------------------------------------------
#  Application
# ------------------------------------------------------------------------------

def prescale_row(algo, values, count):
    """Returns list representing row data in XML, algorithm index leaded by prescale set values."""
    values = [value for value in values[0:count]] # limit to max prescale sets
    values.extend(['0'] * (count - len(values))) # extend missing columns
    return [str(value) for value in [algo] + values] # prepend index, cast to strings

def spreadsheet_url(url):
    """Returns cleaned Google docs CSV export url."""
    result = UrlRegex.match(url)
    if result:
        return ''.join([result.group(1), ExportParams])
    raise ValueError("Not a vaild Google docs spreadsheet url: {url}".format(url=url))

def main():
    # Command line args
    parser = argparse.ArgumentParser()
    parser.add_argument('url', type=spreadsheet_url, help="URL to Google prescales spreadsheet")
    parser.add_argument('--param-id', default=DefaultParamId, help="overwrite param id, default is '{default}'".format(default=DefaultParamId))
    parser.add_argument('--context-id', default=DefaultContextId, help="overwrite context id, default is '{default}'".format(default=DefaultContextId))
    parser.add_argument('--index', metavar='<n>', default=0, type=int, help="default prescale index, default is '{default}'".format(default=DefaultPrescaleSet))
    parser.add_argument('--indexPreview', metavar='<n>', default=0, type=int, help="default preview prescale index, default is '{default}'".format(default=DefaultPrescaleSet))
    parser.add_argument('-o', metavar='<file>', type=argparse.FileType('w'), default=sys.stdout, help="write output to file, default is stdout")
    args = parser.parse_args()

    # Fetch data from url
    fp = urllib2.urlopen(args.url)
    reader = csv.reader(fp, delimiter=',')
    header = reader.next()
    # print header

    # Read in prescales
    rowsMap = {}
    for row in reader:
        # format: <index> <name> <values...>, omitting index:

        if not row[0].isdigit():    # skip rows with text in bit column
            continue

        algo = row[1]

        if not algo:    # skip rows with empty algo name column
            continue;

        # index = int(row[0])
        values = row[2:]
        rowsMap[algo] = prescale_row(algo, values, len(values))

    # Build tree
    algo = etree.Element('run-settings', id='uGT')
    context = etree.SubElement(algo, 'context', id=args.context_id)
    index_param = etree.SubElement(context, 'param', id='index', type='uint')
    index_param.text = str(args.index)

    indexpreview_param = etree.SubElement(context, 'param', id='indexPreview', type='uint')
    indexpreview_param.text = str(args.indexPreview)

    param = etree.SubElement(context, 'param', id=args.param_id, type='table')

    columns_values = ['algo/prescale-index',] + [str(i) for i in range(len(rowsMap.values()[0])-1)]
    columns = etree.SubElement(param, 'columns')
    columns.text = ','.join(columns_values)

    types = etree.SubElement(param, "types")
    types.text = 'string,' + ','.join(['uint'] * (len(columns_values) - 1))
    rows = etree.SubElement(param, "rows")

    for row in sorted(rowsMap):
        if not row:
            continue
        values = rowsMap[row]
        elem = etree.SubElement(rows, "row")
        elem.text = ','.join(values)

    # Pretty print
    xml = minidom.parseString(etree.tostring(algo))
    content = xml.toprettyxml(indent="  ")
    content = '\n'.join(content.split('\n')[1:]) # whipe XML version tag
    # HACK remove newlines from uint type params, not supported by swatch XML parser
    content = re.sub(r'(>)[\s\n\r]*([^<>\s]+)[\s\n\r]*(</)', r'\1\2\3', content)
    args.o.write(content)

if __name__ == '__main__':
    sys.exit(main())
