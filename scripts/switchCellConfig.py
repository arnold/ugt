#!/usr/bin/env python
#
# Switch local CellConfig address tables
#
# List available tags:
#
#  $ python switchCellConfig.py
#
# Set link to a specific tag:
#
#  $ python switchCellConfig.py 2017_07_19
#

import argparse
import logging
import sys, os

CELL_CONFIG = 'CellConfig'

def parse():
    """Argument parser."""
    parser = argparse.ArgumentParser()
    parser.add_argument('tag', nargs='?', help="set CellConfig to directory")
    return parser.parse_args()

def main():
    """Main routine."""
    args = parse()

    logging.getLogger().setLevel(logging.INFO)

    if args.tag:
        basedir = os.path.abspath(args.tag)
        if not os.path.exists(basedir):
            raise IOError("not such directory: '{0}'".format(basedir))
        path = os.path.abspath(CELL_CONFIG)
        if os.path.exists(path) or os.path.islink(path):
            if os.path.islink(path):
                logging.warning("removing already existing symbolic link: '%s'", path)
                os.unlink(path)
            else:
                raise IOError("not a symbolic link: '{0}'".format(path))
        logging.info("creating symbolic link: '%s' -> '%s'", CELL_CONFIG, basedir)
        os.symlink(os.path.relpath(basedir), CELL_CONFIG)

    else:
        logging.info("list of available CellConfigs...")
        for node in os.listdir(os.getcwd()):
            if os.path.isdir(node) and not os.path.islink(node):
                if node.startswith(CELL_CONFIG):
                    print node

    sys.exit()

if __name__ == '__main__':
    main()
