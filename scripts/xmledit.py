#!/usr/bin/env python

"""Graphical editor for XML database tables.

Currently supports algorithm BX mask table, FinOR and Veto mask tables.

Usage
=====

$ xmledit [filename] [--menu <file>]

Use command line option --menu to load algorithm names from an XML menu file.

Dependencies
============

 python-lxml

Contents of this module
=======================

 def iconFactory() ... icon factory function
 def fastIter() ... fast XML iteration function
 def readAlgorithms() ... extracts algorithm index an names from XML file

 class TableModel ... generic table model for integer tables

 class FinOrMaskModel ... FinOR specific table model
 class VetoMaskModel ... veto mask specific table model
 class AlgorithmBxMaskModel ... algorithm BX mask specific table model

 class XmlReader ... XML table file reader
 class XmlWriter ... XML table file writer

 class TableView ... customized table view widget

 class MainWindow ... main application window with menu and toolbar

 def main() ... application execution

"""

import argparse
import sys, os

from PyQt4.QtCore import *
from PyQt4.QtGui import *

from lxml import etree
import xml.dom.minidom as minidom

# NOTE: Bugfix for PyQt4.6
if not hasattr(QKeySequence, 'Quit'):
    QKeySequence.Quit = QKeySequence(Qt.CTRL + Qt.Key_Q)

# ------------------------------------------------------------------------------
#  Constants
# ------------------------------------------------------------------------------

OrbitLength=3564
MaxAlgorithms=512
DefaultParamId=None
DefaultContextId="uGtProcessor"

# ------------------------------------------------------------------------------
#  Helpers
# ------------------------------------------------------------------------------

def iconFactory(*names):
    """Factory function, creates a multi resolution theme icon. Multiple icon
    names for fallback situations."""
    for name in names:
        # For PyQt > 4.6
        if hasattr(QIcon, "fromTheme"):
            icon = QIcon.fromTheme(name)
            if not icon.isNull():
                return icon
        # For PyQt <= 4.6
        icon = QIcon()
        for root, dirs, files in os.walk("/usr/share/icons"):
            for file in files:
                if name == os.path.splitext(os.path.basename(file))[0]:
                    icon.addFile(os.path.join(root, file))
        if not icon.isNull():
            return icon

def fastIter(context, func, *args, **kwargs):
    """See http://lxml.de/parsing.html#modifying-the-tree"""
    for event, elem in context:
        func(elem, *args, **kwargs)
        elem.clear() # It's safe to call clear() here because no descendants will be accessed
        # Also eliminate now-empty references from the root node to elem
        for ancestor in elem.xpath('ancestor-or-self::*'):
            while ancestor.getprevious() is not None:
                del ancestor.getparent()[0]
    del context

def readAlgorithms(filename):
    """Returns dictionary with algrithm index and names from XML menu file."""
    algorithms = {}
    with open(filename, 'rb') as fp:
        context = etree.iterparse(fp, tag='algorithm')
        def pickup(elem):
            index = int(elem.xpath('index/text()')[0])
            name = str(elem.xpath('name/text()')[0])
            algorithms[index] = name
        fastIter(context, pickup)
    return algorithms

# ------------------------------------------------------------------------------
#  Generic table model class
# ------------------------------------------------------------------------------

class TableModel(QAbstractTableModel):
    """Implements generic base table model."""

    def __init__(self, header, data, parent=None):
        super(TableModel, self).__init__(parent)
        self.header = header
        self.__data = data
        self.__edited = set()
        self.algorithmMap = {}

    def setAlgorithmMap(self, d):
        self.algorithmMap = d

    def rowCount(self, parent):
        return len(self.__data)

    def columnCount(self, parent):
        return len(self.__data[0]) if len(self.__data) else 0

    def rawData(self):
        return self.__data

    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid():
            return QVariant()
        row, column = index.row(), index.column()
        if role == Qt.DisplayRole:
            return self.__data[row][column]
        if role == Qt.TextColorRole:
            if (row, column) in self.__edited:
                return QColor(Qt.red)
        return QVariant()

    def setData (self, index, value, role=Qt.EditRole):
        if not index.isValid():
            return False
        row, column = index.row(), index.column()
        if role == Qt.EditRole:
            self.__data[row][column] = value
            self.__edited.add((row, column))

    def flags(self, index):
        return Qt.ItemIsEnabled | Qt.ItemIsSelectable

# ------------------------------------------------------------------------------
#  Custom table model classes
# ------------------------------------------------------------------------------

class FinORMaskModel(TableModel):
    """Implements FinOR table model."""
    def __init__(self, header, data, parent=None):
        super(FinORMaskModel, self).__init__(header, data, parent)

    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid():
            return QVariant()
        if role == Qt.ToolTipRole:
            row = index.row()
            if row in self.algorithmMap.keys():
                return QString("%1 %2").arg(row).arg(self.algorithmMap[row])
        return super(FinORMaskModel, self).data(index, role)

    def headerData(self, section, orientation, role):
        if orientation == Qt.Horizontal:
            if role == Qt.DisplayRole:
                return self.tr("FinOR")
        if orientation == Qt.Vertical:
            if role == Qt.DisplayRole:
                return "A{index:d}".format(index=section)
            if role == Qt.ToolTipRole:
                if section in self.algorithmMap.keys():
                    return QString("%1 %2").arg(section).arg(self.algorithmMap[section])

class VetoMaskModel(TableModel):
    """Implements Veto table model."""
    def __init__(self, header, data, parent=None):
        super(VetoMaskModel, self).__init__(header, data, parent)

    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid():
            return QVariant()
        if role == Qt.ToolTipRole:
            row = index.row()
            if row in self.algorithmMap.keys():
                return QString("%1 %2").arg(row).arg(self.algorithmMap[row])
        return super(VetoMaskModel, self).data(index, role)

    def headerData(self, section, orientation, role):
        if orientation == Qt.Horizontal:
            if role == Qt.DisplayRole:
                return self.tr("Veto")
        if orientation == Qt.Vertical:
            if role == Qt.DisplayRole:
                return "A{index:d}".format(index=section)
            if role == Qt.ToolTipRole:
                if section in self.algorithmMap.keys():
                    return QString("%1 %2").arg(section).arg(self.algorithmMap[section])


class AlgorithmBxMaskModel(TableModel):
    """Implements algorithm BX mask table model."""
    def __init__(self, header, data, parent=None):
        super(AlgorithmBxMaskModel, self).__init__(header, data, parent)

    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid():
            return QVariant()
        if role == Qt.ToolTipRole:
            column = index.column()
            if column in self.algorithmMap.keys():
                return QString("%1 %2").arg(column).arg(self.algorithmMap[column])
        return super(AlgorithmBxMaskModel, self).data(index, role)

    def headerData(self, section, orientation, role):
        if orientation == Qt.Horizontal:
            if role == Qt.DisplayRole:
                return "A{index:d}".format(index=section)
            if role == Qt.ToolTipRole:
                if section in self.algorithmMap.keys():
                    return QString("%1 %2").arg(section).arg(self.algorithmMap[section])
        if orientation == Qt.Vertical:
            if role == Qt.DisplayRole:
                return "BX {bx:04d}".format(bx=section)
        return QVariant()

class AlgorithmTableModel(QAbstractTableModel):
    """Algorithm table model for the dock widget"""

    def __init__(self, data, parent=None):
        super(AlgorithmTableModel, self).__init__(parent)
        self.__data = data

    def rowCount(self, parent):
        return len(self.__data)

    def columnCount(self, parent):
        return len(self.__data[0]) if len(self.__data) else 0

    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid():
            return QVariant()
        row, column = index.row(), index.column()
        if role == Qt.DisplayRole:
            return self.__data[row][column]
        return QVariant()

# ------------------------------------------------------------------------------
#  XML reader and writer classes
# ------------------------------------------------------------------------------

class XmlReader(object):
    """XML table file reader."""
    def __init__(self, filename):
        self.filename = filename
        self.xml = minidom.parse(self.filename)

    def header(self):
        """Returns columns tag content list."""
        columns = self.xml.getElementsByTagName('columns')[0].firstChild.nodeValue.strip()
        return [str(column.strip()) for column in columns.split(',')] # cast from unicode to str!

    def rows(self):
        """Returns list of integer row values with omitted index column."""
        rows = []
        for node in self.xml.getElementsByTagName('row'):
            columns = []
            line = node.firstChild.nodeValue.strip()
            # Note: make sure to strip the first column (containing the bx or algorithm index).
            for column in line.split(",")[1:]:
                columns.append(int(column))
            rows.append(columns)
        return rows

    def isAlgorithmBxMask(self):
        return self.header()[:4] == ['bx/algo', '0', '1', '2']

    def isFinORMask(self):
        return self.header()[:2] == ['algo', 'mask']

    def isVetoMask(self):
        return self.header()[:2] == ['algo', 'veto']

class XmlWriter(object):
    """XML table file writer."""

    ParamIdMapping = {
        FinORMaskModel: 'finorMask',
        VetoMaskModel: 'vetoMask',
        AlgorithmBxMaskModel: 'algorithmBxMask',
    }

    def __init__(self, model, paramId, contextId):
        """Argument *model* requires the table views model. The model contains
        the data to be written and determines also the format and headers."""
        self.model = model
        self.paramId = paramId
        self.contextId = contextId

    def write(self, filename, indent="  "):
        """Writes model data to XML file."""
        # Try to auto detect param ID if set to None
        paramId = self.paramId if self.paramId else self.ParamIdMapping[type(self.model)]
        # Build XML tree
        algo = etree.Element("run-settings", id="uGT")
        context = etree.SubElement(algo, "context", id=self.contextId)
        param = etree.SubElement(context, "param", id=paramId, type="table")
        columns = etree.SubElement(param, "columns")
        columns.text = ','.join(self.model.header)
        types = etree.SubElement(param, "types")
        types.text = ','.join(['uint'] * len(self.model.header))
        rows = etree.SubElement(param, "rows")
        for index, row in enumerate(self.model.rawData()):
            row = [index] + row # Prepend bx/algorithm index!
            elem = etree.SubElement(rows, "row")
            elem.text = ','.join([str(cell) for cell in row]) # cast integers to strings
        # Pretty print
        xml = minidom.parseString(etree.tostring(algo))
        content = xml.toprettyxml(indent=indent)
        content = '\n'.join(content.split('\n')[1:]) # whipe XML version tag
        with open(filename, 'w') as fp:
            fp.write(content)
            fp.write('\n') # append newline


# ------------------------------------------------------------------------------
#  Table view specific classes
# ------------------------------------------------------------------------------

class TableView(QTableView):
    """Custom table view widget."""
    ColumnSectionSizes = {

        AlgorithmBxMaskModel: 52,
    }
    def __init__(self, parent=None):
        super(TableView, self).__init__(parent)
        self.horizontalHeader().setDefaultSectionSize(46)
        self.verticalHeader().setDefaultSectionSize(24)
        self.doubleClicked.connect(self.onDoubleClicked)

    def setModel(self, model):
        """Overloaded setModel() method, sets column withs according to model."""
        if not isinstance(model, AlgorithmBxMaskModel):
            self.horizontalHeader().setDefaultSectionSize(52)
        super(TableView, self).setModel(model)

    def onDoubleClicked(self, index):
        """Toggle cell data on double click."""
        state = not bool(self.model().data(index))
        self.model().setData(index, int(state))
        self.model().dataChanged.emit(index, index)

# ------------------------------------------------------------------------------
#  Dock widget
# ------------------------------------------------------------------------------

class FilterLineEdit(QLineEdit):
    """Line edit with a clear button on the right side.
    Used for filter and search inputs.
    """
    def __init__(self, parent = None):
        """Constructur, takes optional reference to parent widget."""
        super(FilterLineEdit, self).__init__(parent)
        self.clearButton = QToolButton(self)
        self.clearButton.setIcon(iconFactory("edit-clear"))
        self.clearButton.setCursor(Qt.ArrowCursor)
        self.clearButton.setStyleSheet("QToolButton { border: none; padding: 0; }")
        self.clearButton.hide()
        self.clearButton.clicked.connect(self.clear)
        self.textChanged.connect(self._updateClearButton)
        frameWidth = self.style().pixelMetric(QStyle.PM_DefaultFrameWidth)
        self.setStyleSheet(QString("QLineEdit { padding-right: %1px; } ").arg(self.clearButton.sizeHint().width() + frameWidth + 1))
        msz = self.minimumSizeHint()
        self.setMinimumSize(max(msz.width(), self.clearButton.sizeHint().height() + frameWidth - 2),
                            max(msz.height(), self.clearButton.sizeHint().height() + frameWidth - 2))
    def resizeEvent(self, event):
        """Takes care of drawing the clear button on the right side."""
        sz = self.clearButton.sizeHint()
        frameWidth = self.style().pixelMetric(QStyle.PM_DefaultFrameWidth)
        self.clearButton.move(self.rect().right() - frameWidth - sz.width(),
                              (self.rect().bottom() + 2 - sz.height()) / 2)
    def _updateClearButton(self, text):
        """Hide clear button when line edit contains no text."""
        self.clearButton.setVisible(not text.isEmpty())

class AlgorithmWidget(QWidget):
    """Algorithm list imported from a XML menu."""
    algorithmSelected = pyqtSignal()
    def __init__(self, parent=None):
        super(AlgorithmWidget, self).__init__(parent)
        self.algorithmMap = {}
        self.filterLineEdit = FilterLineEdit(self)
        self.filterLineEdit.textChanged.connect(self.setFilterText)
        self.tableView = QTableView(self)
        self.tableView.setShowGrid(False)
        self.tableView.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.tableView.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.tableView.horizontalHeader().setStretchLastSection(True)
        self.tableView.horizontalHeader().hide()
        self.tableView.verticalHeader().setDefaultSectionSize(20)
        self.tableView.verticalHeader().hide()
        self.model = AlgorithmTableModel([], self)
        self.proxyModel = QSortFilterProxyModel(self)
        self.proxyModel.setFilterKeyColumn(-1)
        self.proxyModel.setFilterCaseSensitivity(Qt.CaseInsensitive)
        self.tableView.setModel(self.proxyModel)
        self.setAlgorithmMap({}) # init model
        self.selectButton = QPushButton(self.tr("&Select"), self)
        # Emit signal on selection
        self.selectButton.clicked.connect(lambda: self.algorithmSelected.emit())
        self.tableView.doubleClicked.connect(lambda: self.algorithmSelected.emit())
        # Setup layout
        layout = QVBoxLayout()
        layout.addWidget(self.filterLineEdit)
        layout.addWidget(self.tableView)
        layout.addWidget(self.selectButton)
        layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)

    def setAlgorithmMap(self, d):
        self.algorithmMap = d
        self.model = AlgorithmTableModel([(k, v) for k, v in d.iteritems()], self)
        self.proxyModel.setSourceModel(self.model)
        self.tableView.setModel(self.proxyModel)

    def setFilterText(self, text):
        self.proxyModel.setFilterFixedString(text)

    def selectedAlgorithmIndices(self):
        indices = []
        for index in self.tableView.selectionModel().selectedRows(0):
            indices.append(int(index.data().toPyObject()))
        return indices

# ------------------------------------------------------------------------------
#  Main application window classes
# ------------------------------------------------------------------------------

class MainWindow(QMainWindow):
    """Custom application main window."""
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.algorithmMap = {}
        self.setParamId(DefaultParamId)
        self.setContextId(DefaultContextId)
        self.setWindowTitle(self.tr("XML Table Editor"))
        self.resize(800, 600)
        self.createActions()
        self.createMenus()
        self.createToolbar()
        self.createStatusBar()
        self.createDocks()
        self.resetWindow()

    def createActions(self):
        # Action for opening an existing file.
        self.openAct = QAction(self.tr("&Open..."), self)
        self.openAct.setShortcut(QKeySequence.Open)
        self.openAct.setStatusTip(self.tr("Open an existing file"))
        self.openAct.setIcon(iconFactory("document-open"))
        self.openAct.triggered.connect(self.onOpen)
        # Action for saving the current file with a different name.
        self.saveAsAct = QAction(self.tr("Save &As..."), self)
        self.saveAsAct.setShortcut(QKeySequence.SaveAs)
        self.saveAsAct.setStatusTip(self.tr("Save the current file with a different name"))
        self.saveAsAct.setIcon(iconFactory("document-save-as"))
        self.saveAsAct.triggered.connect(self.onSaveAs)
        # Action for importing XML menu
        # Action for opening an existing file.
        self.importAct = QAction(self.tr("&Import XML menu..."), self)
        self.importAct.setStatusTip(self.tr("Import algorithms from XML menu file"))
        self.importAct.setIcon(iconFactory("document-import", "insert-object"))
        self.importAct.triggered.connect(self.onImport)
        # Action for quitting the program.
        self.quitAct = QAction(self.tr("&Quit"), self)
        self.quitAct.setShortcut(QKeySequence.Quit)
        self.quitAct.setStatusTip(self.tr("Quit the programm"))
        self.quitAct.setIcon(iconFactory("application-exit"))
        self.quitAct.triggered.connect(self.close)
        # Action for importing from another file.
        self.setAct = QAction(self.tr("Set"), self)
        self.setAct.setStatusTip(self.tr("Set value(s)"))
        self.setAct.setIcon(iconFactory("list-add"))
        self.setAct.triggered.connect(self.onSet)
        # Action for importing from another file.
        self.unsetAct = QAction(self.tr("Unset"), self)
        self.unsetAct.setStatusTip(self.tr("Unset value(s)"))
        self.unsetAct.setIcon(iconFactory("list-remove"))
        self.unsetAct.triggered.connect(self.onUnset)

    def createMenus(self):
        # File menu
        self.fileMenu = self.menuBar().addMenu(self.tr("&File"))
        self.fileMenu.addAction(self.openAct)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.saveAsAct)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.importAct)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.quitAct)
        self.editMenu = self.menuBar().addMenu(self.tr("&Edit"))
        self.editMenu.addAction(self.setAct)
        self.editMenu.addAction(self.unsetAct)

    def createToolbar(self):
        """Create main toolbar and pin to top area."""
        self.toolbar = self.addToolBar("Toolbar")
        self.toolbar.setMovable(False)
        self.toolbar.setFloatable(False)
        self.toolbar.addAction(self.openAct)
        self.toolbar.addSeparator()
        self.toolbar.addAction(self.saveAsAct)
        self.toolbar.addSeparator()
        self.toolbar.addAction(self.importAct)
        self.toolbar.addSeparator()
        self.toolbar.addAction(self.setAct)
        self.toolbar.addAction(self.unsetAct)

    def createStatusBar(self):
        """Create status bar and populate with status labels."""
        self.statusBar()

    def createDocks(self):
        self.dockWidget = QDockWidget(self.tr("Algorithms"), self)
        self.dockWidget.setAllowedAreas(Qt.RightDockWidgetArea)
        self.algorithmWidget = AlgorithmWidget(self)
        self.dockWidget.setWidget(self.algorithmWidget)
        self.addDockWidget(Qt.RightDockWidgetArea, self.dockWidget)
        self.algorithmWidget.algorithmSelected.connect(self.onAlgorithmSelected)

    def setParamId(self, id):
        self.paramId = id

    def setContextId(self, id):
        self.contextId = id

    def setStatusMessage(self, message):
        self.statusMessage.setText(message)

    def onOpen(self):
        """Slot, raises a file open dialog and loads a table from XML file."""
        filename = str(QFileDialog.getOpenFileName(self, self.tr("Open file..."),
            os.getcwd(), self.tr("XML Table (*.xml)")))
        if filename:
            self.loadFile(filename)

    def onSaveAs(self):
        """Slot, raises a save file name dialog and writes the table to file."""
        filename = str(QFileDialog.getSaveFileName(self, self.tr("Save as..."),
            os.getcwd(), self.tr("XML Table (*.xml)")))
        # On cancel, bail out
        if not filename:
            return
        # Append .xml if not done by user
        if not filename.endswith(".xml"):
            filename = '.'.join(filename, 'xml')
        # Save table to file
        self.saveFile(filename)

    def onImport(self):
        """Slot, raises a file open dialog and loads algorithms from XML menu file."""
        filename = str(QFileDialog.getOpenFileName(self, self.tr("Open XML menu file..."),
            os.getcwd(), self.tr("XML Menu (*.xml)")))
        if filename:
            self.loadMenu(filename)

    def onSet(self):
        """Slot, sets all selected table cells to 1."""
        table = self.centralWidget()
        if not table: return
        setData = table.model().setData
        emitDataChanged = table.model().dataChanged.emit
        for index in table.selectionModel().selectedIndexes():
            setData(index, 1)
            emitDataChanged(index, index)

    def onUnset(self):
        """Slot, sets all selected table cells to 0."""
        table = self.centralWidget()
        if not table: return
        setData = table.model().setData
        emitDataChanged = table.model().dataChanged.emit
        for index in table.selectionModel().selectedIndexes():
            setData(index, 0)
            emitDataChanged(index, index)

    def onSelectionChanged(self, selected, deselected):
        """Slot, to be called when tabel view's selection changed, updates status bar information."""
        self.statusBar().showMessage("")
        if not selected:
             return
        # For algorithm bx mask
        if isinstance(self.centralWidget().model(), AlgorithmBxMaskModel):
            bx = selected[0].top()
            index = selected[0].left()
            name = self.algorithmMap[index] if index in self.algorithmMap else "n/a"
            message = QString("Selection: BX %1 / Algorithm %2 %3").arg(bx).arg(index).arg(name)
            self.statusBar().showMessage(message)
        elif isinstance(self.centralWidget().model(), FinORMaskModel):
            index = selected[0].top()
            name = self.algorithmMap[index] if index in self.algorithmMap else "n/a"
            message = QString("Selection: Algorithm %2 %3").arg(index).arg(name)
            self.statusBar().showMessage(message)
        elif isinstance(self.centralWidget().model(), VetoMaskModel):
            index = selected[0].top()
            name = self.algorithmMap[index] if index in self.algorithmMap else "n/a"
            message = QString("Selection: Algorithm %2 %3").arg(index).arg(name)
            self.statusBar().showMessage(message)

    def onAlgorithmSelected(self):
        """Select columns/rows selected in algorithms dock widget."""
        # TODO: somewhat slow and awkward...
        if isinstance(self.centralWidget(), TableView):
            self.centralWidget().clearSelection()
            # Swich to multi selection mode.
            self.centralWidget().setSelectionMode(QAbstractItemView.MultiSelection)
            for index in self.algorithmWidget.selectedAlgorithmIndices():
                if isinstance(self.centralWidget().model(), AlgorithmBxMaskModel):
                    # Select columns for algorithm BX mask
                    self.centralWidget().selectColumn(index)
                elif isinstance(self.centralWidget().model(), FinORMaskModel):
                    # Select rows for FinOR mask
                    self.centralWidget().selectRow(index)
                elif isinstance(self.centralWidget().model(), VetoMaskModel):
                    # Select rows for veto mask
                    self.centralWidget().selectRow(index)
            # Swich back to extended selection mode.
            self.centralWidget().setSelectionMode(QAbstractItemView.ExtendedSelection)

    def resetWindow(self):
        # Free central widget
        self.setCentralWidget(QWidget(self))
        self.saveAsAct.setEnabled(False)
        self.setAct.setEnabled(False)
        self.unsetAct.setEnabled(False)

    def loadFile(self, filename):
        """Load table from XML file."""
        self.resetWindow()
        try:
            reader = XmlReader(str(filename))
            if reader.isAlgorithmBxMask():
                model = AlgorithmBxMaskModel(reader.header(), reader.rows(), self)
                model.setAlgorithmMap(self.algorithmMap)
            elif reader.isFinORMask():
                model = FinORMaskModel(reader.header(), reader.rows(), self)
                model.setAlgorithmMap(self.algorithmMap)
            elif reader.isVetoMask():
                model = VetoMaskModel(reader.header(), reader.rows(), self)
                model.setAlgorithmMap(self.algorithmMap)
            else:
                raise RuntimeError("The requested file is not a valid XML table.")
        except Exception, e:
            QMessageBox.critical(self, self.tr("Failed to load XML table"), str(e))
            raise
        table = TableView(self)
        table.setModel(model)
        table.selectionModel().selectionChanged.connect(self.onSelectionChanged)
        self.setCentralWidget(table)
        self.saveAsAct.setEnabled(True)
        self.setAct.setEnabled(True)
        self.unsetAct.setEnabled(True)

    def saveFile(self, filename):
        """Save table to XML file."""
        try:
            model = self.centralWidget().model()
            writer = XmlWriter(model, self.paramId, self.contextId)
            writer.write(filename)
        except Exception, e:
            QMessageBox.critical(self, self.tr("Failed to write XML table"), str(e))
            raise

    def loadMenu(self, filename):
        """Load algorithm mapping from XML file."""
        try:
            self.algorithmMap = readAlgorithms(filename)
            self.algorithmWidget.setAlgorithmMap(self.algorithmMap)
            if isinstance(self.centralWidget(), TableView):
                self.centralWidget().model().setAlgorithmMap(self.algorithmMap)
        except Exception, e:
            QMessageBox.critical(self, self.tr("Failed to load XML menu"), str(e))
            raise

# ------------------------------------------------------------------------------
#  Application execution
# ------------------------------------------------------------------------------

def main():
    # Command line args
    parser = argparse.ArgumentParser()
    parser.add_argument('filename', nargs='?', help="XML file to load")
    parser.add_argument('--menu', metavar='<file>', type=os.path.abspath, help="load XML menu to display algorithm names")
    parser.add_argument('--param-id', default=DefaultParamId, help="overwrite param id, default is auto detect")
    parser.add_argument('--context-id', default=DefaultContextId, help="overwrite context id, default is '{default}'".format(default=DefaultContextId))
    args = parser.parse_args()
    # Create application
    app = QApplication(sys.argv)
    window = MainWindow()
    if args.menu:
        window.loadMenu(args.menu)
    window.setParamId(args.param_id) # None performs auto detect!
    window.setContextId(args.context_id)
    window.show()
    if args.filename:
        window.loadFile(args.filename)
    return app.exec_()

if __name__ == '__main__':
    sys.exit(main())
