AMC502 upgrade
==============

Firmware upload script for AMC502 modules.

### Features

*   Upload a firmware file to an AMC502 board (upload)
*   Load a specific firmware (reboot)
*   List available firmware files on all boards (list)
*   Remove a specific firmware file from all boards (remove)

### Basic usage

    $ ./amc502upgrade.py -c {ugtts,ugtdev,vienna} -t {finor,finor_preview,extcond} [-p <passwd>] {list, reboot, upload}

Upload a new firmware file from your local drive

    $ ./amc502upgrade.py ... upload <localpath/filename>.bit

Load a specific firmware file from the AMC502

    $ ./amc502upgrade.py ... reboot <remotefile>.bit

List all available firmware files from each board

    $ ./amc502upgrade.py ... list

In the case you want to upload a firmware file and reboot the board at once, use the flag --reboot

    $ ./amc502upgrade.py ... upload <localpath/filename>.bit --reboot

Remove a specific firmware file from all AMC502 boards

    $ ./amc502upgrade.py ... remove <remotefile>.bit

**N.B.** Currently the standard password is set, therefore no "-p" flag is necessary at the moment.

### Format of hosts.json

    {
      "<crate>": {
        "<module-type>": [
          "<hostname-a>",
          "<hostname-b>"
        ]
      }
    }
