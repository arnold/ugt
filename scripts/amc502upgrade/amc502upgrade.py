#!/usr/bin/env python
# -*- coding: utf-8 -*-
# amc502upgrade.py
#
# External Condition board (AM502) script for uploading and rebooting new firmware
# to the board.
#
# Usage: amc502upgrade.py -c <crate> -t <type> <command> [args ...]
#
# Format of hosts.json configuration:
#
#   {
#     "<crate>": {
#       "<module-type>": [
#         "<hostname-a>",
#         "<hostname-b>"
#       ]
#     }
#   }
#

from threading import Thread
from datetime import datetime

import argparse
import logging
import hashlib
import select
import socket
import json
import time
import re
import sys, os

try:
    from scp import SCPClient
except ImportError:
    raise RuntimeError("Dependency module 'scp.py' is missing. Please download it from 'https://github.com/jbardin/scp.py'")

try:
    import paramiko
except ImportError:
    raise RuntimeError("Dependency package 'paramiko' is missing. Please install 'yum install python-paramiko' on your machine.")

# -----------------------------------------------------------------------------
#  Classes
# -----------------------------------------------------------------------------

DefaultUsername = "root"         # username for ssh on the board
DefaultPassword = "root"         # password for ssh on the board
DefaultRetries = 10              # retry attempts to connect to the board
DefaultTimeout = 7               # connection timeout in seconds
DefaultRemotePath = '/upgrade/'  # path on the board with read/write permission for file upload
DefaultHostsFile = 'hosts.json'  # JSON file containing hosts grouped by crate and module type
SupportedModules = (             # module types refered by JSON hosts file
    'finor',
    'finor_preview',
    'extcond',
)

# -----------------------------------------------------------------------------
#  Classes
# -----------------------------------------------------------------------------

class ConnectionManager(object):
    """Connection manager performing various SSH commands.

    >>> cm = ConnectionManager()
    >>> cm.connect(host="example.cms", username="root", password="spam")
    >>> cm.exec_command("ls -l")
    >>> cm.close()
    """

    def __init__(self, retries=10, timeout=7):
        self.timeout = timeout
        self.retries = retries
        self.hostname = None
        self.client = None

    def is_active(self):
        """Returns True if client is connected and active."""
        if self.client and self.client.get_transport():
            return self.client.get_transport().is_active()
        return False

    def connect(self, hostname, username, password):
        self.hostname = hostname

        for attempt in range(self.retries):
            message = "{0} attempt to connect {1}".format(hostname, attempt + 1)
            logging.info(message)
            localstate = ""

            try:
                self.client = paramiko.SSHClient()
                self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                self.client.connect(hostname, username=username, password=password, timeout=self.timeout)
                message = "{0}: connection established.".format(self.hostname)
                logging.info(message)
                return
            except paramiko.BadHostKeyException:
                localstate = "Exception: host could not be verified"
                logging.info(localstate)
                continue
            except paramiko.AuthenticationException:
                localstate = "Exception: Authentication failed"
                logging.info(localstate)
                continue
            except paramiko.SSHException:
                localstate = "Exception: SSH Exception"
                logging.info(localstate)
                continue

        message = "{0}: Failed to connect.{1}".format(self.hostname, localstate)
        raise RuntimeError(message)

    def invoke_shell(self):
        chan = self.client.invoke_shell()
        chan.settimeout(self.timeout)
        return chan

    def get_transport(self):
        return self.client.get_transport()

    def exec_command(self, command):
        return self.client.exec_command(command)

    def copy(self, filename, remotepath):
        scp = SCPClient(self.get_transport())
        scp.put(filename, remote_path=remotepath)
        scp.close()

    def close(self):
        """Close SSH connection."""
        if self.is_active():
            self.client.close()
            logging.info("%s: closed connection.", self.hostname)
        self.client = None
        self.hostname = None

class Board(object):
    """Board abstraction class.

    >>> board = Board(host, username, password, remotepath, retries, timeout)
    """

    FileExtensions = ['.bit']
    """List of valid file extensions for images."""

    def __init__(self, host, username, password, remotepath, retries, timeout):
        self.hostname = host
        self.remotepath = remotepath
        self.cache = ""
        self.filesize = 0
        self.cm = ConnectionManager(retries, timeout)
        self.cm.connect(host, username, password)

    def validate_filename(self, filename):
        """Raises exception if filename is not a valid image file."""
        _, extension = os.path.splitext(filename)
        if extension not in self.FileExtensions:
            raise RuntimeError("invalid image filename '{0}'".format(filename))

    def process_fpga_upgrade(self, chan, buffsize=64):
        """Helper function for flashfpga method."""
        while True:
            r, w, e = select.select([chan], [], [])
            if chan in r:
                try:
                    statusmsg = chan.recv(buffsize)
                    self.cache += statusmsg

                    if ("SUCCESS" in self.cache):
                        chan.send("exit")
                        break
                    time.sleep(.1)
                except socket.timeout:
                    raise RuntimeError("process_fpga_upgrade Socket Timeout")
                    break

    def flashfpga(self, filename):
        """Reboot FPGA with image file. This may take some time.

        >>> board.flashfpga("image.bit")
        """
        self.validate_filename(filename)
        filename = os.path.basename(filename)
        self.cache = "" # clear carefully
        try:
            for _ in range(self.cm.retries):
                if self.cm.is_active():
                    chan = self.cm.invoke_shell()
                    # flash fpga
                    remote_filename = os.path.join(self.remotepath, filename)
                    command = 'echo y | fpga_upgrade "{0}" \n'.format(remote_filename)
                    chan.send(command)
                    self.process_fpga_upgrade(chan)
                    chan.close()
                    break
        except socket.timeout:
            chan.close()
            self.close()
            raise RuntimeError("flash fpga Socket Timeout")

    def md5sum(self, filename):
        """Returns MD5 hexdigest of remote file.

        >>> board.md5sum('/upgrade/image.bit')
        'fe347cbe634f676ab31e44b43791431a'
        """
        remotefilename = os.path.join(self.remotepath, filename)
        command = 'md5sum "{0}"'.format(remotefilename)
        stdin, stdout, stderr = self.cm.exec_command(command)
        return stdout.read().split()[0].strip()

    def file_exists(self, filename):
        self.validate_filename(filename)
        if self.cm.is_active():
            # check if file already exists on remote machine

            filename = os.path.basename(filename) # get filename from complete path
            remotefilename = os.path.join(self.remotepath, filename) # concatinate remote path and filename

            command = "test -e {filename}; echo $?".format(filename=remotefilename)
            stdin, stdout, stderr = self.cm.client.exec_command(command)
            line = stdout.readline().strip()
            if line == '0':  # file exists
                return True
        return False

    def upload_image(self, filename):
        self.validate_filename(filename)
        # copy file to remote machine
        if self.cm.is_active():
            self.cm.copy(filename, self.remotepath)
            self.filesize = self.image_size(filename)

    def reboot_status(self):
        """Retruns current reboot status and percentage.

        >>> board.status()
        ('writing data', 42)
        """
        state = "preparing"
        percent = 0

        regex = re.compile("(?P<state>[\w\s]+)\:.* (?P<value>\d+).*\/(?P<total>\d+).*\((?P<percent>\d+)\%\)")

        item = None
        # Find last occurance
        for result in regex.finditer(self.cache):
            item = result.groupdict()

        if item:
            state = item["state"].strip().lower()
            value = int(item["value"].strip())
            total = int(item["total"].strip())

            item_percent = int(item["percent"].strip())

            if total == 0: # special case for broken VadaTech percentage
                percent = round(value * 100.0 / item_percent)
                if percent >= 100:
                    percent = 100
            else:
                percent = round(value * 100.0 / total)

        return state, percent

    def image_size(self, filename):
        """Returns remote image size in bytes.

        >>>board.image_size("image.bit")
        2048
        """
        self.validate_filename(filename)
        if self.cm.is_active():
            # get file size from remote machine
            filename = os.path.basename(filename)
            remotefilename = os.path.join(self.remotepath, filename)
            command = "stat -c %s " + remotefilename
            stdin, stdout, stderr = self.cm.client.exec_command(command)
            line = stdout.readline().strip()
            return int(line or '0')

    def remove_image(self, filename):
        """Remove remote image file from board.

        >>>board.remove_image("image.bit")
        """
        self.validate_filename(filename)
        if self.cm.is_active():
            filename = os.path.basename(filename) # get filename from complete path
            remotefilename = os.path.join(self.remotepath, filename) # concatinate remote path and filename
            command = "rm {filename}".format(filename=remotefilename)
            stdin, stdout, stderr = self.cm.client.exec_command(command)
            # line = stdout.readline().strip()
            # print line
            errors = stderr.read()
            if errors:
                raise RuntimeError(errors)
            if not self.file_exists(filename):
                logging.info("Removed file {0} from {1}".format(filename, self.hostname))
            else:
                logging.info("File {0} still exists on {1}".format(filename, self.hostname))

    def list_images(self):
        """Returns list of available images located on SD card.

        >>> board.list_images()
        ['image_foo.bit', 'image_bar.bit', 'image_baz.bit']
        """
        if not self.cm.is_active():
            logging.warning("unable to connect to %s", self.hostname)
            return

        # Command to be executed
        command = ' '.join(('ls', self.remotepath))
        stdin, stdout, stderr = self.cm.exec_command(command)

        filenames = []
        for filename in stdout:
            filename = filename.strip()
            _, extension = os.path.splitext(filename)
            if extension in self.FileExtensions:
                filenames.append(filename)
        return filenames

    def close(self):
        self.cm.close()

class ThreadManager(object):
    """Thread manager for flashing threads.

    >>> manager = ThreadManager(boards)
    >>> manager.reboot("image.bit")
    """

    def __init__(self, boards):
        self.boards = boards
        self.threads = []

    def reboot(self, filename):
        """Reboot with image file on all boards using threading.

        >>> manager.reboot("image.bit")
        """
        # TTY stuff
        tty_rows = len(self.boards)
        tty_up = "\033[{0}A".format(tty_rows)
        tty_down = "\033[{0}B".format(tty_rows)

        missing_files = 0

        logging.info("checking for image files...")
        for board in self.boards:
            if not board.file_exists(filename):
                logging.error("%s: *** missing file '%s'", board.hostname, filename)
                missing_files += 1

        if missing_files:
            appname = os.path.basename(__file__)
            logging.info("Upload file on boards first by using 'python {0} upload <filename>.bit'".format(appname))
            return

        # Create threads
        self.create_threads(filename)

        logging.info("flashing FPGAs on all boards...")

        time_start = datetime.now()

        while self.threads_alive():
            for board in self.boards:
                status, percent = board.reboot_status()
                sys.stdout.write("{board.hostname:20}      {status:17} {percent:5.1f}%\n".format(**locals()))

            sys.stdout.write(tty_up)
            sys.stdout.flush()
            time.sleep(.5)

        # Join threads
        self.join_threads()

        sys.stdout.write(tty_down)
        sys.stdout.write(os.linesep)
        sys.stdout.flush()

        time_end = datetime.now()
        time_delta = time_end - time_start

        minutes = time_delta.seconds / 60
        seconds = time_delta.seconds % 60

        logging.info("flashing FPGAs finished within %s min %s sec", minutes, seconds)

    def create_threads(self, filename):
        """Creates and starts threads."""
        if self.threads_alive():
            logging.error("threads are already active...")
            return

        self.threads = []
        for board in self.boards:
            thread = Thread(target=board.flashfpga, args=(filename, ))
            thread.daemon = True
            self.threads.append(thread)

        for thread in self.threads:
            thread.start()

    def threads_alive(self):
        """Returns True if one or more threads are active and running."""
        for thread in self.threads:
            if thread.is_alive():
                return True
        return False

    def join_threads(self):
        """Joins all active threads."""
        for thread in self.threads:
            if thread.is_alive():
                thread.join()

# -----------------------------------------------------------------------------
#  Commands
# -----------------------------------------------------------------------------

def cmd_upload(boards, args):
    """Upload command."""
    filename = args.filename
    ## check file existance on local drive
    if not os.path.isfile(filename):
        raise IOError("file '{0}' does not exist.".format(filename))

    existing = []
    local_filename = os.path.abspath(filename)

    logging.info("checking for already existing image files...")
    filename = os.path.basename(filename)
    for board in boards:
        if board.file_exists(filename):
            logging.warning("%s: image file already exists.", board.hostname)
            existing.append(board)

            # Additional MD5 checksum
            remote_md5sum = board.md5sum(filename)
            local_md5sum = hashlib.md5(open(local_filename, 'rb').read()).hexdigest()
            if remote_md5sum != local_md5sum:
                logging.warning("MD5SUM missmatch of existing file:")
                logging.warning("  %s  %s:%s", remote_md5sum, board.hostname, filename)
                logging.warning("  %s  %s", local_md5sum, local_filename)

    overwrite = False
    if existing:
        prompt = "Do you want to override existing image file '{0}' on {1} board(s) [y/N]? ".format(filename, len(existing))
        overwrite = userPromptYesNo(prompt, False)

    for board in boards:
        if board in existing:
            if not overwrite:
                continue # skip existing file
        # write some fancy dots while uploading
        thread = Thread(target=board.upload_image, args=(local_filename, ))
        thread.start()
        while thread.is_alive():
            sys.stdout.write(".")
            sys.stdout.flush()
            time.sleep(.1)
        sys.stdout.write(os.linesep)
        sys.stdout.flush()
        thread.join()
        logging.info("%s: copied file '%s'", board.hostname, filename)

    if args.reboot:
        cmd_reboot(boards, args)

def cmd_reboot(boards, args):
    """Reboot command."""
    # strip path from filename
    filename = os.path.basename(args.filename)

    threadManager = ThreadManager(boards)
    threadManager.reboot(filename)

def cmd_remove(boards, args):
    """Remove command."""
    # strip path from filename
    filename = os.path.basename(args.filename)

    logging.info("remove file '{0}' from all boards".format(filename))
    for board in boards:
        board.remove_image(filename)

def cmd_list(boards, args):
    """List command."""
    width = 80
    hr = "="

    logging.info("list available files for all boards")
    for board in boards:
        filenames = board.list_images()
        print " files on SD card of '{0}' ".format(board.hostname).center(width, hr)
        for filename in filenames:
            print " ", filename
        print hr * width

# -----------------------------------------------------------------------------
#  Helpers
# -----------------------------------------------------------------------------

def userPromptYesNo(prompt, default=False, retries=8):
    valid = {"y":True, "yes":True,
             "n":False, "no":False, "":False}
    for retry in range(retries):
        choice = raw_input(prompt).lower()
        if not choice in valid:
            print "Invalid answer '{0}'. Please try again.".format(choice)
            continue
        return valid[choice]
    return default

# -----------------------------------------------------------------------------
#  Parser
# -----------------------------------------------------------------------------

def parse_hosts(filename):
    """Retruns hosts and crates from JSON configuration file.

    >>> hosts, crates = parse_hosts("sample.json")
    """
    with open(filename) as fp:
        hosts = json.load(fp)
        crates = map(str, hosts.keys()) # cast from unicode to str
    return hosts, crates

def parse_args():
    """Parse command line arguments."""
    hosts, crates = parse_hosts(DefaultHostsFile)

    parser = argparse.ArgumentParser()
    parser.add_argument('--crate', '-c', required=True, choices=crates, help="crate to perform command on ({0})".format(', '.join(crates)))
    parser.add_argument('--type', '-t', required=True, choices=SupportedModules, help="module type ({0})".format(', '.join(SupportedModules)))
    parser.add_argument('--username', '-u', default=DefaultUsername, help="username for ssh connection")
    parser.add_argument('--password', '-p', default=DefaultPassword, help="password for ssh connection")
    parser.add_argument('--retries', default=DefaultRetries, help="number of connection attempts (default {0})".format(DefaultRetries))
    parser.add_argument('--timeout', default=DefaultTimeout, type=int, help="connection timout (default {0})".format(DefaultTimeout))
    parser.add_argument('--remotepath', default=DefaultRemotePath, help="path on the board to upload the file (default {0})".format(DefaultRemotePath))

    sub = parser.add_subparsers()

    parserUpload = sub.add_parser('upload')
    parserUpload.add_argument('filename', help="local file to be uploaded onto boards and flashed into the chip")
    parserUpload.add_argument('--reboot', action="store_true", help="executes a 'reboot' after uploading the bit file")
    parserUpload.set_defaults(call=cmd_upload)

    parserReboot = sub.add_parser('reboot')
    parserReboot.add_argument('filename', help="file on remote machines that will be flashed into the chip. It is necessary that the file is available on all boards")
    parserReboot.set_defaults(call=cmd_reboot)

    parserRemove = sub.add_parser('remove')
    parserRemove.add_argument('filename', help="remove file from all remote machines")
    parserRemove.set_defaults(call=cmd_remove)

    parserList = sub.add_parser('list')
    parserList.set_defaults(call=cmd_list)

    args = parser.parse_args()
    args.hosts = hosts[args.crate][args.type]

    return args

# -----------------------------------------------------------------------------
#  Main
# -----------------------------------------------------------------------------

def main():
    """Main Routine."""

    args = parse_args()

    # Setup console logging
    logging.basicConfig(format = '%(levelname)s: %(message)s', level=logging.INFO)

    # Create and connect boards
    boards = []
    for hostname in args.hosts:
        board = Board(hostname, args.username, args.password, args.remotepath, args.retries, args.timeout)
        boards.append(board)

    # Call command
    args.call(boards, args)

    # Close connections.
    for board in boards:
        board.close()

    logging.info("done.")

# Main routine
if __name__ == '__main__':
    main()
