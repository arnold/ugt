#!/bin/env python

import argparse
import pexpect
import sys


parser = argparse.ArgumentParser()
parser.add_argument("--lpm", action='store', dest='useLpm',
                    default=0, help='use LPM instead of CPM')
results = parser.parse_args()


base_dir = "/nfshome0/ugtts/software/tcds/"

script = base_dir + "simpleTcdsControl.py"
host = "tcds-control-trig.cms"
port = 2110
fed_mask = "'1404&3%'"

pi_id = 504
pi_cfg = base_dir + "pi_mp7bgos.txt"

ici_id = 304
ici_cfg = base_dir + "ici_config_cpm.txt"
#if results.useLpm:
#  ici_cfg = base_dir + "ici_config_lpm.txt"


def enable(command):
  print "executing %s ... " % command
  proc = pexpect.spawn(command)
  proc.logfile = sys.stdout

  proc.expect("Enter command :")
  proc.sendline("Enable")

  proc.expect("Enter command :")
  proc.sendline("q")


pi_cmd = """%s --host %s --port %s --id %s --keepLease -c Halt --hwconfig %s --fedEnableMask %s --reqId latency_Test""" % (script, host, port, pi_id, pi_cfg, fed_mask)

enable(pi_cmd)

ici_cmd = """%s --host %s --port %s --id %s --keepLease -c Halt --hwconfig %s --reqId latency_Test""" % (script, host, port, ici_id, ici_cfg)

enable(ici_cmd)

# eof
