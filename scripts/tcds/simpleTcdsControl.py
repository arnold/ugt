#!/usr/bin/python

# Python script to send SOAP commands to a TCDS XDAQ application
# 
# Author: Christos Lazaridis

import sys, getopt, httplib
import optparse, os.path
import time, datetime
import signal 
import threading
import socket
 
def main(argv):
  ##### Default values : BEGIN
  # These can also be set from the command line
  tcds_xdaq_hostname = "cactus-worker-6b.cern.ch"
  tcds_xdaq_port = "2020"
  tcds_xdaq_id = "100"
  #
  ##### Default values : END
  
  # parse command line arguments
  parser = optparse.OptionParser("usage: %prog [options]",add_help_option=False)
  
  parser.add_option("-h", "--help", action="store_true")
  parser.add_option("-c", "--command", action="store")
  parser.add_option("-v", "--verbose", action="store_true",default=False)
  parser.add_option("--host", action="store")
  parser.add_option("--hwconfig", action="store")
  parser.add_option("--port", action="store", type="int")
  parser.add_option("--id", action="store", type="int")
  parser.add_option("--fedEnableMask", action="store")
  parser.add_option("--runNr", action="store", type="int")
  parser.add_option("--reqId", action="store")
  parser.add_option("--keepLease", action="store_true", dest="keepLease")
  parser.add_option("--bgo", action="store", type="int")
  parser.add_option("--l1a", action="store_true")
  parser.add_option("--l1a_delay", action="store", type="int",default=10)
  
  (options, args) = parser.parse_args()  
  
  if options.help :
    usage()
    sys.exit(0)
    
  if options.command == None and options.bgo == None and not options.l1a:
    print "ERROR : No command was specified\n"
    usage()
    sys.exit(0)

  if options.host:
    tcds_xdaq_hostname = options.host
    
  if options.port:
    tcds_xdaq_port = options.port

  if options.id:
    tcds_xdaq_id = options.id
  
  command_creator = TcdsCommands(tcds_xdaq_hostname, tcds_xdaq_port, tcds_xdaq_id, options.verbose)
  
  command_creator.set_l1a_delay(options.l1a_delay)
  
  if options.hwconfig:
    command_creator.set_hw_config_path(options.hwconfig)

  if options.fedEnableMask:
    command_creator.set_fed_enable_mask(options.fedEnableMask)
    
  if options.runNr:
    command_creator.set_run_number(options.runNr)
    
  if options.reqId:
    command_creator.set_action_requestor_id(options.reqId)

  if ( options.command == "Configure" and not options.hwconfig ) :
    print "ERROR. Configure command needs a hardwareConfigurationString via --hwconfig <string>."
    sys.exit(2)        
  
  if ( options.command == "Enable" and not options.runNr ) :
    print "WARNING. Enable command without a run number requested. Will use default value of",run_number

  # all looks ok, so let's send the command
  if options.l1a :
    command_creator.print_command_summary("SendL1A")
    command_creator.send_command("SendL1A") 
  elif options.bgo :
    command_creator.print_command_summary(options.bgo)
    command_creator.send_command(options.bgo)
  else :
    command_creator.print_command_summary(options.command)
    command_creator.send_command(options.command)

  # renew the lease if asked to do so
  if options.keepLease :    
    if options.command == "Halt" :
      if options.hwconfig == None :
        print "ERROR : Configuration file required to renew lease."
        sys.exit(2)
      
      print "Cannot renew hardware lease when in Halt. Waiting and will attempt to move to 'Configured'."
      time.sleep(6)
      command_creator.print_command_summary("Configure")
      command_creator.send_command("Configure")
      command_creator.show_available_actions()

    command_creator.keep_lease( timeout = 5 )
      
      
      
#
# print usage
#
def usage():
  print """Usage: """+sys.argv[0]+""" [OPTIONS]
           
Simple python script to send SOAP commands to a TCDS cell.
If no parameters are provided, hardcoded values will be used to send
the SOAP command.
           
Options : 
  -c <command>  		Define command to send to the TCDS XDAQ application.
                		Possible values are: 
                		Halt, Stop, Pause, Resume, Configure, Enable, 
                		RenewHwLease, ReadHardwareConfiguration,
                		EnableTTCSpy, DisableTTCSpy, ResetTTCSpyLog,
                		TTCResync, TTCHardReset
                		Configure requires the definition of hardwareConfigurationString 
                		via the 'hwconfig' command line parameter
                		Enable requires the definition of runNumber via the 'runNr' command 
                		line parameter
  --host <hostname>    		the Hostname of the TCDS XDAQ application 
  --port <port>        		the port number of the TCDS XDAQ application
  --id   <id>          		the application ID (shows up as lid on the URL of the TCDS 
                       		application)
  --fedEnableMask <mask>        the FedEnableMask as documented in the RCMS manual.
                                Required for PIs configured in AMC13 mode.

  --hwconfig <string>  		path to file containing the hardwareConfigurationString 
                       		required for Configure
  --runNr <number>     		the runNumber required for Enable
  --reqId <number>     		the actionRequestorId required for leasing the TCDS hardware. 
                       		If none is defined a default value will be used.
  --keepLease          		after executing the command the program will not exit but 
                       		enter an 'interactive' mode from where it will be renewing the 
                       		hardware lease until stopped and it can also allow to go through 
                       		the various FSM states
   --bgo <bgo>         		send a single B-go (possible values 0-15)
   --l1a               		send a single L1A
   --l1a_delay <#>     		define delay in # ms for sending periodical L1As in the background 
                       		when in 'interactive' mode
  -h, --help           		show this help message and exit
"""




####################################################################################
class TcdsCommands :
  """Creates commands to send to a particular TCDS XDAQ"""
  def __init__(self, tcds_xdaq_hostname, tcds_xdaq_port, tcds_xdaq_id, verbose):
    self.commands_with_params = ["Configure", "Enable"];
    self.commands_without_params = ["Halt", "Stop", "Pause", "Resume", "RenewHardwareLease", 
                                    "ReadHardwareConfiguration", 
                                    "SendL1A", 
                                    "InitCyclicGenerators",
                                    "EnableTTCSpy","DisableTTCSpy","ResetTTCSpyLog",
                                    "TTCResync", "TTCHardReset","State"];
    
    self.bgosequences = [ ["start",     [ 5, 8, 9, 7 ] ], 
                          ["resync",    [ 5, 7 ] ], 
                          ["hard reset",[ 6, 5, 7 ] ] ];

    self.commands_all = self.commands_with_params + self.commands_without_params

    self.tcds_xdaq_hostname = tcds_xdaq_hostname
    self.tcds_xdaq_port = tcds_xdaq_port
    self.tcds_xdaq_id = tcds_xdaq_id
    
    self.action_requestor_id = "simpleTcdsControl__"+socket.gethostname()+"__"+str(time.time())
    self.hardware_configuration_string = ""
    self.hw_config_path = None
    self.fed_enable_mask = None
    self.run_number = 0    
    self.verbose = verbose
    self.running = True # used to stop the thread; in some cases it failed to exit FIXME
    self.periodic_l1a_on = False
    self.l1a_thread = threading.Thread(target=self.send_periodic_l1a)
    self.l1a_thread.daemon = True
    self.num_l1as = 0


  #
  # set action_requestor_id
  #
  def set_action_requestor_id(self, action_requestor_id) :
    self.action_requestor_id = action_requestor_id


  #
  # set run number
  #
  def set_run_number(self, run_number) :
    self.run_number = run_number



  def set_l1a_delay(self, l1aDelay) :
    self.periodic_l1a_delay = l1aDelay / 1000.0


  #
  # set and read configuration file
  #
  def set_hw_config_path(self, hw_config_path) :
    self.hw_config_path = hw_config_path
    
    if not os.path.isfile(hw_config_path) :
      print "ERROR. Can not open file "+hw_config_path+""
      sys.exit(2)
    else :
      self.hardware_configuration_string = "" 
      
      ifile = open (hw_config_path, "r")
      
      for line in ifile :
        li=line.strip()
        if not li.startswith("#") :
            self.hardware_configuration_string += line
            
      ifile.close()
      
  def set_fed_enable_mask(self, fed_enable_mask) :
    self.fed_enable_mask = fed_enable_mask
      
  #
  # print command summary
  #
  def print_command_summary(self, command):
    print "  Sending command : '"+str(command)+"'"
    print "  URL : http://"+self.tcds_xdaq_hostname+":"+str(self.tcds_xdaq_port)+"/urn:xdaq-application:lid="+str(self.tcds_xdaq_id)
    print "  Lease actionRequestorId : '"+self.action_requestor_id+"'"
    if self.hw_config_path and command == "Configure" :
      print "  HwConfig from: '"+self.hw_config_path+"'"
    if self.fed_enable_mask and command == "Configure" :
      print "  FedEnableMask: '"+self.fed_enable_mask+"'"
    if self.run_number and command == "Enable" :
      print "  Run # : "+str(self.run_number)

      
      
  #
  # send a command
  #
  def send_command(self, command):
    if command not in self.commands_all and not (str(command).isdigit() and int(command) >= 0 and int(command) <= 15) :
      print "ERROR. Unknown command '"+command+"' requested."
      sys.exit(2)    

    soapMessage = self.make_soap_message(command)

    if self.verbose :
      print "==============================================================================================="
      print "Sending SOAP message..."
      print soapMessage
      print "==============================================================================================="
    
    statuscode, statusmessage, header, soapReply = self.send_soap_message(soapMessage)

    if self.verbose :
      print "Collecting reply..."
      print "Server response: ", statuscode, statusmessage
      print "Headers: ", header
      print "SOAP reply was : "
      print soapReply
      print "==============================================================================================="

    return soapReply



  #
  # make the SOAP command
  # returns the full string of the command
  #
  def make_soap_message(self, command):
    if command == "Configure" :
      soapHardwareConfigCommand = """<xdaq:hardwareConfigurationString xsi:type="xsd:string">%s</xdaq:hardwareConfigurationString>"""
      if self.fed_enable_mask:
        SM_TEMPLATE = self.make_command_header(command) + """<xdaq:fedEnableMask xsi:type="xsd:string">%s</xdaq:fedEnableMask>""" + soapHardwareConfigCommand + self.make_command_footer(command)
        from xml.sax.saxutils import escape
        soapMessage = SM_TEMPLATE%(escape(self.fed_enable_mask), self.hardware_configuration_string)
      else:
        SM_TEMPLATE = self.make_command_header(command) + soapHardwareConfigCommand + self.make_command_footer(command)
        soapMessage = SM_TEMPLATE%(self.hardware_configuration_string)
    
    elif command == "Enable" :
      SM_TEMPLATE = self.make_command_header(command) + """<xdaq:runNumber xsi:type="xsd:unsignedInt">%s</xdaq:runNumber>""" + self.make_command_footer(command)

      soapMessage = SM_TEMPLATE%(self.run_number)
    elif str(command).isdigit() :
      SM_TEMPLATE = self.make_command_header("SendBgo") + """<xdaq:bgoNumber xsi:type="xsd:unsignedInt">%s</xdaq:bgoNumber>""" + self.make_command_footer("SendBgo")

      soapMessage = SM_TEMPLATE%(command)
    elif command == "State" :
      #SM_TEMPLATE = self.make_command_header(command) + """<xdaq:hardwareConfigurationString xsi:type="xsd:string">%s</xdaq:hardwareConfigurationString>""" + self.make_command_footer(command)

      #soapMessage = SM_TEMPLATE%(self.hardware_configuration_string)
      soapMessage = """<?xml version="1.0" ?>
<soap-env:Envelope soap-env:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:soap-env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><soap-env:Header/><soap-env:Body><xdaq:ParameterGet xmlns:xdaq="urn:xdaq-soap:3.0"><p:properties xmlns:p="urn:xdaq-application:tcds::deadwood::DeadWood" xsi:type="soapenc:Struct"><p:stateName xsi:type="xsd:string"/></p:properties></xdaq:ParameterGet></soap-env:Body></soap-env:Envelope>"""
    else :
      soapMessage = self.make_command_header(command) + self.make_command_footer(command)
    
    return soapMessage



  # 
  # template for command headers
  #
  def make_command_header(self, command) :
    template = """<?xml version="1.0" ?>
<env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<env:Header/><env:Body><xdaq:%s xdaq:actionRequestorId="%s" xmlns:xdaq="urn:xdaq-soap:3.0">"""

    return template %(command,self.action_requestor_id)
  
  def make_command_footer(self, command) :
    template = """</xdaq:%s></env:Body></env:Envelope>"""
    return template %(command)



  #
  # send a soap command
  # returns the status code and message, header and SOAP reply
  #
  def send_soap_message(self, soapMessage) :
    # Okay, we should be all good to send 
    webservice = httplib.HTTP(self.tcds_xdaq_hostname+":"+str(self.tcds_xdaq_port))
    webservice.putrequest("POST", "/urn:xdaq-application:lid="+str(self.tcds_xdaq_id))
    webservice.putheader("Content-type", "text/xml; charset=\"UTF-8\"")
    webservice.putheader("Content-length", "%d" % len(soapMessage))
    webservice.endheaders()
    webservice.send(soapMessage)

    # Now let's get the response
    statuscode, statusmessage, header = webservice.getreply()
    return statuscode, statusmessage, header, webservice.getfile().read()



  #
  # renew hardware lease
  #
  def keep_lease(self, timeout):
    print "Renewing hardware lease as actionRequestorId '"+self.action_requestor_id+"'... (q to quit)"
    
    try :
      self.l1a_thread.start()
    except :
      print "ERROR: Unable to start periodic L1A thread!"
      sys.exit(2)
            
    while True:
      command = nonBlockingRawInput(timeout)
      
      if command in self.commands_all :
        print "Renewing hardware lease as actionRequestorId '"+self.action_requestor_id+"'... (q to quit)"
        self.print_command_summary(command)
        self.send_command(command)
        self.show_available_actions()
      elif command.isdigit() and int(command) >= 0 and int(command) <= 15 :
        print "Sending Bgo" + command + " ("+self.bgo_number_to_name(int(command))+")"
        self.print_command_summary(command)
        self.send_command(command)
        self.show_available_actions()
      elif command.lower() == 'l1a' :
        print "Sending L1A"
        self.print_command_summary("SendL1A")
        self.send_command("SendL1A")
        self.show_available_actions()
      elif command.lower() == 'seqstart' :
        self.send_bgo_sequence(0)
        self.show_available_actions()
      elif command.lower() == 'seqresync' :
        self.send_bgo_sequence(1)
        self.show_available_actions()
      elif command.lower() == 'seqhardreset' :
        self.send_bgo_sequence(2)
        self.show_available_actions()
      elif command.lower() == 'l1astart' :
        if not self.periodic_l1a_on :
          self.periodic_l1a_on = True
        else :
          print "Oops! Periodic L1As already started!"
      elif command.lower() == 'l1astop' :
        if self.periodic_l1a_on :
          self.periodic_l1a_on = False
          print "\nStopping periodic L1As (sent "+str(self.num_l1as)+" L1As in total)"
          self.num_l1as = 0
        else :
          print "Oops! Periodic L1As not started!"          
      elif command.lower() == 'q' :
        self.running = False
        self.periodic_l1a_on = False
        print "Exiting..."
        sys.exit(0)
      
      statuscode, statusmessage, header, soapReply = self.send_soap_message(self.make_soap_message("RenewHardwareLease"))
      
      if soapReply == '<env:Envelope env:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:env="http://schemas.xmlsoap.org/soap/envelope/"><env:Header/><env:Body><xdaq:RenewHardwareLeaseResponse xmlns:xdaq="urn:xdaq-soap:3.0"/></env:Body></env:Envelope>' :
        
        if  self.periodic_l1a_on :
          print "\r# periodic L1As : "+str(self.num_l1as)+" --- Lease last renewed : "+datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')+"     Enter command : ",
        else:  
          print "\rLease last renewed : "+datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')+"     Enter command : ",
        sys.stdout.flush()
      else :
        print "Lost hardware lease."
        print 
        print "Response: ", statuscode, statusmessage
        print "Headers: ", header
        print "SOAP reply was : "
        print soapReply
        print  
        break
      
      
  #
  # threaded function to send periodic L1As
  #      
  def send_periodic_l1a(self) :
    while self.running == True:
      if self.periodic_l1a_on :
        self.send_command("SendL1A")
        self.num_l1as += 1
        time.sleep(self.periodic_l1a_delay) 
      else :
        time.sleep(3) # to reduce CPU usage when not sending L1As

      
      
  #
  # send BGO sequences (defined in init)
  #
  def send_bgo_sequence(self, seqNr) :
    print "Executing "+self.bgosequences[seqNr][0]+" B-go sequence"
    for bgo in self.bgosequences[seqNr][1] :
      time.sleep(0.001) # delay is in seconds
      print "Sending Bgo" + str(bgo) + " ("+self.bgo_number_to_name(bgo)+")"
      soapReply = self.send_command(bgo)
      
      if not soapReply == '<env:Envelope env:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:env="http://schemas.xmlsoap.org/soap/envelope/"><env:Header/><env:Body><xdaq:SendBgoResponse xmlns:xdaq="urn:xdaq-soap:3.0"/></env:Body></env:Envelope>' :
        print "ERROR. Sending Bgo" + str(bgo) + " ("+self.bgo_number_to_name(bgo)+") failed!"
        print 
        print "Response: ", statuscode, statusmessage
        print "Headers: ", header
        print "SOAP reply was : "
        print soapReply
        sys.exit(2)    
  
  
  
  #
  # print list of available commands
  #
  def show_available_actions(self) :
    print """\nCommands : 
   Halt, Stop, Pause, Resume, Configure, Enable, 
   EnableTTCSpy, DisableTTCSpy, ResetTTCSpyLog,
   TTCResync, TTCHardReset, L1A, or a BGO number (0-15)
Start/Stop Periodic L1As :
   L1AStart, L1AStop
Available B-go sequences : 
   seqStart, seqResync, seqHardReset\n"""


      
  #
  # translate B-go numbers to names
  #
  def bgo_number_to_name(self, nr) :
    if nr == 0 :
      return "not_used"
    elif nr == 1 :
      return "BC0"
    elif nr == 2 :
      return "TestEnable"
    elif nr == 3 :
      return "PrivateGap"
    elif nr == 4 :
      return "PrivateOrbit"
    elif nr == 5 :
      return "Resync"
    elif nr == 6 :
      return "HardReset"
    elif nr == 7 :
      return "EC0"
    elif nr == 8 :
      return "OC0"
    elif nr == 9 :
      return "Start"
    elif nr == 10 :
      return "Stop"
    elif nr == 11 :
      return "StartOfGap"
    elif nr == 12 :
      return "not_used"
    elif nr == 13 :
      return "WarningTestEnable"
    elif nr == 14 :
      return "not_used"
    elif nr == 15 :
      return "not_used"

# class TcdsCommands : end  
#  
####################################################################################


####################################################################################
# 
# used for non-blocking input
# http://www.garyrobinson.net/2009/10/non-blocking-raw_input-for-python.html
#
class AlarmException(Exception):
    pass

def alarmHandler(signum, frame):
    raise AlarmException

def nonBlockingRawInput(timeout, prompt=''):
    signal.signal(signal.SIGALRM, alarmHandler)
    signal.alarm(timeout)
    try:
        text = raw_input(prompt)
        signal.alarm(0)
        return text
    except AlarmException:
        return ''
    signal.signal(signal.SIGALRM, signal.SIG_IGN)
    return ''
# 
####################################################################################


if __name__ == "__main__":
   main(sys.argv[1:])
