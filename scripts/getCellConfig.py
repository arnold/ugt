#!/usr/bin/env python
#
# Check out uGT address tables from tag or trunk
#
# List available tags:
#
#  $ python getCellConfig.py
#
# Check out a tag:
#
#  $ python getCellConfig.py 2017_07_19
#
# Check out from trunk and create symbolic link CellConfig -> CellConfig_trunk:
#
#  $ python getCellConfig.py trunk --link
#
# Options:
#
#  --link  create a CellConfig symbolic link to checked out tag/trunk
#  --force  overwrites already existing checked out directory (use with care)
#

import argparse
import tempfile
import subprocess
import shutil
import logging
import sys, os
import re

SVN_EXEC = 'svn'
SVN_LIST = 'list'
SVN_CHECKOUT = 'co'

TRUNK = 'trunk'
ADDR_DIR = 'addr_tab'
CELL_CONFIG = 'CellConfig'

REGEX_TAGS = r'20\d\d_.*' # tags must match something like '2017_10_06_foo'
URL_SEP = '/'

BASE_TRUNK_URL = 'https://svn.cern.ch/reps/cactus/trunk/cactusupgrades/projects/ugt'
BASE_TAG_URL = 'https://svn.cern.ch/reps/cactus/tags/ugt'

MODULES = (
    'amc502_extcond',
    'amc502_finor',
    'amc502_finor_pre',
    'mp7_ugt',
)

def url_join(*args):
    """Joins a web url.
    >>> url_join('http://example.org', 'foo', 'bar')
    'http://example.org/foo/bar'
    """
    return URL_SEP.join([format(arg) for arg in args])

def svn_list(url):
    """Returns SVN directory listing.
    >>> svn_list('https://example.org/svn/trunk')
    ['foo/\n', 'bar/\n', 'baz/\n']
    """
    cmd = [SVN_EXEC, SVN_LIST, url]
    with tempfile.SpooledTemporaryFile() as fp:
        subprocess.check_call(cmd, stdout=fp)
        fp.seek(0)
        return fp.readlines()

def svn_checkout(url, dest=None):
    """Checks out a SVN directory.
    >>> svn_checkout('https://example.org/svn/trunk', 'trunk_code')
    """
    cmd = [SVN_EXEC, SVN_CHECKOUT, url]
    if dest: cmd.append(dest)
    subprocess.check_call(cmd)

def parse():
    """Argument parser."""
    parser = argparse.ArgumentParser()
    parser.add_argument('tag', nargs='?', help="select ugt tag name or '{0}', if not supplied lists all available tags".format(TRUNK))
    parser.add_argument('--link', action='store_true', help="set symbolic link '{0}' to checked out tag/trunk. Note: removes an existing symbolic link".format(CELL_CONFIG))
    parser.add_argument('--force', action='store_true', help="force overwrite exisiting tags/trunk directory")
    return parser.parse_args()

def main():
    """Main routine."""
    args = parse()

    logging.getLogger().setLevel(logging.INFO)

    # Dump list of available tags and exit if nothing is supplied.
    if not args.tag:
       for tag in svn_list(BASE_TAG_URL):
           if re.match(REGEX_TAGS, tag):
               print tag.strip("\n\r\/")
       print TRUNK
       sys.exit()

    # Select base url for trunk or tags.
    if args.tag == TRUNK:
        baseurl = BASE_TRUNK_URL
    else:
        baseurl = url_join(BASE_TAG_URL, args.tag)

    # Create and check for base directory.
    basedir = os.path.abspath("{0}_{1}".format(CELL_CONFIG, args.tag))
    if os.path.exists(basedir):
        if args.force:
            logging.warning("overwriting already existing directory: '%s'", basedir)
            shutil.rmtree(basedir)
        else:
            logging.error("destination already exists: '%s'", basedir)
            raise IOError("already exists: '{0}'".format(basedir))

    # Check out address tables for all modules.
    for module in MODULES:
        url = url_join(args.tag, module, ADDR_DIR,)
        dest = os.path.join(basedir, module)
        svn_checkout(url_join(baseurl, module, ADDR_DIR,), dest)

    if args.link:
        path = os.path.abspath(CELL_CONFIG)
        if os.path.exists(path):
            if os.path.islink(path):
                logging.warning("removing already existing symbolic link: '%s'", path)
                os.unlink(path)
            else:
                raise IOError("not a symbolic link: '{0}'".format(path))
        logging.info("creating symbolic link: '%s' -> '%s'", CELL_CONFIG, basedir)
        os.symlink(os.path.relpath(basedir), CELL_CONFIG)

    sys.exit()

if __name__ == '__main__':
    main()
