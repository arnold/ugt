#!/usr/bin/env python

"""Algorithm BX mask XML table generator.

Usage: python xmlgen.py <infile> <outfile>

  <infile>     input spec file, see format belofw
  <outfile>    file to write XML output

Input file format
-----------------

<algo>|<algo>-<algo>[, ...] : [!]<bx>|[!]<bx>-<bx>[, ...]

Use single algorithm or BX indices (42) or ranges (42-166), specify multiple
indices and ranges by separating using commas (2, 4, 8, 42-66). BX indices can
be inverted (=not masked) enable disabled indices (see example 2 below).

Note: BX are enumerated from 0 upt to 3563!

Example 1
---------

Masking a fictional dark gap at the end of the orbit from BX 3500 up to 3563:

0-511: 3500-3563     # mask last BXs_ of the orbit

Example 2
---------

Enabling only a singel BX in a singe algorithm 42, this shows how to combine
masking and unmasking using an ! operator.

0-511: 0-3563      # mask everything
42: !108           # enables only BX 103 for algorithm 42

"""

import re
import argparse
import string

from lxml import etree
import xml.dom.minidom as minidom

MAX_ALGORITHMS=512
ORBIT_LENGTH=3564

class XmlWriter(object):
    """Algorithm BX mask XML table file writer."""

    def __init__(self, data, paramId, contextId):
        self.header = ['bx/algo'] + [str(i) for i in range(MAX_ALGORITHMS)]
        self.data = data
        self.paramId = paramId
        self.contextId = contextId

    def write(self, filename, indent="  "):
        """Writes model data to XML file."""
        # Build XML tree
        algo = etree.Element("run-settings", id="uGT")
        context = etree.SubElement(algo, "context", id=self.contextId)
        param = etree.SubElement(context, "param", id=self.paramId, type="table")
        columns = etree.SubElement(param, "columns")
        columns.text = ','.join(self.header)
        types = etree.SubElement(param, "types")
        types.text = ','.join(['uint'] * len(self.header))
        rows = etree.SubElement(param, "rows")
        for index, row in enumerate(self.data):
            row = [index] + row # Prepend bx/algorithm index!
            elem = etree.SubElement(rows, "row")
            elem.text = ','.join([str(cell) for cell in row]) # cast integers to strings
        # Pretty print
        xml = minidom.parseString(etree.tostring(algo))
        content = xml.toprettyxml(indent=indent)
        content = '\n'.join(content.split('\n')[1:]) # whipe XML version tag
        with open(filename, 'w') as fp:
            fp.write(content)
            fp.write('\n') # append newline

# [!]<value>|<value>-<value>[, ...]
RegexCapture = re.compile(r'([\!]?)(?:(?:(\d+)\-(\d+))|(\d+))')

r_value = r'(\d+)'
r_range = r'(\d+\-\d+)'
r_value_or_range = r'({r_range}|{r_value})'.format(**locals())
r_neg_value_or_range = r'\!?({r_range}|{r_value})'.format(**locals())
r_list = r'(({r_value_or_range})(\s*\,\s*{r_value_or_range})*)'.format(**locals())
r_neg_list = r'(({r_neg_value_or_range})(\s*\,\s*{r_neg_value_or_range})*)'.format(**locals())
r_expr = r'^\s*({r_list})\s*\:\s*({r_neg_list})\s*$'.format(**locals())

RegexValidate = re.compile(r_expr)

def validate(expression):
    """Validate expression syntax."""
    if not RegexValidate.match(expression):
        raise RuntimeError(expression)

def getrange(expression):
    """getrange resolved list of indices.
    >>> getmask("4, 6-8, !42")
    [(4, 0), (6, 0), (7, 0), (8, 0), (42, 1)]
    """
    indices = set()
    for neg, first, last, single in RegexCapture.findall(expression):
        if single:
            indices.add((int(single), 1 if neg else 0))
        else:
            for value in range(int(first), int(last) + 1):
                indices.add((int(value), 1 if neg else 0))
    return sorted(indices)

def getmask(expression):
    """Returns resolved list of indices for algorithms and BX.
    >>> getmask("4, 8-10: 42, !44")
    ([(4, 0), (8, 0), (9, 0), (10, 0)], [(42, 0), (44, 1)])
    """
    validate(expression)
    algorithms, bunches = expression.split(':')
    return getrange(algorithms), getrange(bunches)

if __name__ == '__main__':
    # Command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('infile')
    parser.add_argument('outfile')
    args = parser.parse_args()

    # Generate default masking table (512 x 3564)
    masks = [ [1] * MAX_ALGORITHMS for bx in range(ORBIT_LENGTH) ]

    # Read masking from spec file
    with open(args.infile) as fp:
        for expression in fp:
            expression = expression.strip()
            # Ignore empty lines
            if not expression:
                continue
            # Ignore comments
            if expression.startswith('#'):
                continue
            algorithms, bunches = getmask(expression)
            for bunch, v in bunches:
                for index, _ in algorithms:
                    masks[bunch][index] = v # overwrite default mask

    # Write to XML file
    writer = XmlWriter(masks, paramId='algorithmBxMask', contextId='uGtProcessor')
    writer.write(args.outfile)
