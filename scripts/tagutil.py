#!/usr/bin/env python2

from PyQt4.QtCore import *
from PyQt4.QtGui import *

import datetime
import sys, os

REPO_URL = "https://svn.cern.ch/reps/cactus"

class Item(object):
    def __init__(self, name, path, rev="HEAD"):
        self.name = name
        self.path = QLineEdit(path, None)
        self.rev = QLineEdit(rev, None)
        self.rev.setMaximumWidth(64)

class Dialog(QDialog):

    def __init__(self, parent=None):
        super(Dialog, self).__init__(parent)
        self.setWindowTitle(self.tr("Create ugt tag"))
        self.resize(640, 280)
        self.nameLineEdit = QLineEdit(self.autoTagName(), self)
        self.items = [
            Item("amc502_extcond", "trunk/cactusupgrades/projects/ugt/amc502_extcond"),
            Item("amc502_finor", "trunk/cactusupgrades/projects/ugt/amc502_finor"),
            Item("amc502_finor_pre", "trunk/cactusupgrades/projects/ugt/amc502_finor_pre"),
            Item("mp7_ugt", "trunk/cactusupgrades/projects/ugt/mp7_ugt"),
            Item("ugt", "trunk/cactusprojects/ugt"),
            Item("utm", "trunk/cactusprojects/utm"),
        ]
        buttonBox = QDialogButtonBox(self)
        buttonBox.setOrientation(Qt.Horizontal)
        buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)
        buttonBox.accepted.connect(self.accept)
        buttonBox.rejected.connect(self.reject)
        row = 0
        layout = QGridLayout()
        layout.addWidget(QLabel(self.tr("name"), self), row, 0)
        layout.addWidget(self.nameLineEdit, row, 1)
        for item in self.items:
            row += 1
            layout.addWidget(QLabel(item.name, self), row, 0)
            layout.addWidget(item.path, row, 1)
            layout.addWidget(QLabel("@", self), row, 2)
            layout.addWidget(item.rev, row, 3)
        row += 1
        layout.addItem(QSpacerItem(0, 0, QSizePolicy.Minimum, QSizePolicy.Expanding), row, 0, 1, 4)
        row += 1
        layout.addWidget(buttonBox, row, 0, 1, 4)
        self.setLayout(layout)
        # Startup hints
        QMessageBox.information(self,
            self.tr("Help on creating ugt tags..."),
            self.tr("<b>Welcome!</b><br><br>This tool will assist you in "
                    "creating new ugt SVN tags.<br><br>On submit it will "
                    "<b>print</b> a list of required SVN commands to stdout. "
                    "Please check the generated output for correctness before "
                    "executing any SVN commands.<br><br>"
                    "This tool will not perform any magic stuff!")
        )

    def autoTagName(self):
        now = datetime.datetime.now()
        return now.strftime("%Y_%m_%d")

    def accept(self):
        tagname = self.nameLineEdit.text()
        print "### Generated SVN commands:"
        print
        print "svn mkdir {}/tags/ugt/{}".format(REPO_URL, tagname)
        for item in self.items:
            rev = item.rev.text()
            rev = "@{}".format(rev) if rev != "HEAD" else ""
            print
            print "svn cp {}/{}{} {}/tags/ugt/{}".format(REPO_URL, item.path.text(), rev, REPO_URL, tagname)
        print
        print "### done."
        print
        print " ~ always be careful, check *twice* before committing a tag!"
        print
        super(Dialog, self).accept()

def main():
    app = QApplication(sys.argv)
    window = Dialog()
    window.show()
    return app.exec_()

if __name__ == '__main__':
    sys.exit(main())
