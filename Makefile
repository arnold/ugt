BUILD_HOME:=$(shell pwd)

Project=cactusprojects/ugt

Packages= \
	swatch \
	ts/cell

# XDAQ makefiles want a pretty specific folder structure
# provide it using a symlink to free developers to put their git repos anywhere they fancy
$(shell test ! -d $(Project) && mkdir -p `dirname $(Project)` && ln -s .. $(Project))

include /opt/cactus/build-utils/mfCommonDefs.mk

include $(XDAQ_ROOT)/config/mfAutoconf.rules
include $(XDAQ_ROOT)/config/mfDefs.$(XDAQ_OS)

export PACKAGE_VER_MAJOR=0
export PACKAGE_VER_MINOR=24
export PACKAGE_VER_PATCH=0

export PACKAGE_RELEASE=1

include $(XDAQ_ROOT)/config/Makefile.rules
include $(XDAQ_ROOT)/config/mfRPM.rules
