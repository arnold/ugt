--------------------------------------------------------------------------------
-- Synthesizer : ISE 14.6
-- Platform    : Linux Ubuntu 10.04
-- Targets     : Synthese
--------------------------------------------------------------------------------
-- This work is held in copyright as an unpublished work by HEPHY (Institute
-- of High Energy Physics) All rights reserved.  This work may not be used
-- except by authorized licensees of HEPHY. This work is the
-- confidential information of HEPHY.
--------------------------------------------------------------------------------
-- $HeadURL: svn://heros.hephy.at/GlobalTriggerUpgrade/l1tm/L1Menu_CaloMuonCorrelation_2015_hb_test/vhdl/module_0/src/gtl_module.vhd $
-- $Date: 2015-08-24 11:49:40 +0200 (Mon, 24 Aug 2015) $
-- $Author: bergauer $
-- $Revision: 4173 $
--------------------------------------------------------------------------------

-- HB 2015-08-10: generated without TME for correlation condition tests - used std_logic_vector for calculations in DETA,DPHI, DR and MASS.

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all; -- for function "CONV_INTEGER"

use work.gtl_pkg.all;

entity gtl_module is
    port(
        lhc_clk : in std_logic;
        eg_data : in calo_objects_array(0 to NR_EG_OBJECTS-1);
        jet_data : in calo_objects_array(0 to NR_JET_OBJECTS-1);
        tau_data : in calo_objects_array(0 to NR_TAU_OBJECTS-1);
        ett_data : in std_logic_vector(MAX_ESUMS_BITS-1 downto 0);
        ht_data : in std_logic_vector(MAX_ESUMS_BITS-1 downto 0);
        etm_data : in std_logic_vector(MAX_ESUMS_BITS-1 downto 0);
        htm_data : in std_logic_vector(MAX_ESUMS_BITS-1 downto 0);
        muon_data : in muon_objects_array(0 to NR_MUON_OBJECTS-1);
        external_conditions : in std_logic_vector(NR_EXTERNAL_CONDITIONS-1 downto 0);
        algo_o : out std_logic_vector(NR_ALGOS-1 downto 0));
end gtl_module;

architecture rtl of gtl_module is

    constant external_conditions_pipeline_stages: natural := 2; -- pipeline stages for "External conditions" to get same pipeline to algos as conditions

    signal muon_bx_p2, muon_bx_p1, muon_bx_0, muon_bx_m1, muon_bx_m2 : muon_objects_array(0 to NR_MUON_OBJECTS-1);
    signal eg_bx_p2, eg_bx_p1, eg_bx_0, eg_bx_m1, eg_bx_m2 : calo_objects_array(0 to NR_EG_OBJECTS-1);
    signal jet_bx_p2, jet_bx_p1, jet_bx_0, jet_bx_m1, jet_bx_m2 : calo_objects_array(0 to NR_JET_OBJECTS-1);
    signal tau_bx_p2, tau_bx_p1, tau_bx_0, tau_bx_m1, tau_bx_m2 : calo_objects_array(0 to NR_TAU_OBJECTS-1);
    signal ett_bx_p2, ett_bx_p1, ett_bx_0, ett_bx_m1, ett_bx_m2 : std_logic_vector(MAX_ESUMS_BITS-1 downto 0);
-- HB 2015-04-28: changed for "htt" - object type from TME [string(1 to 3)] in esums_conditions.vhd
    signal htt_bx_p2, htt_bx_p1, htt_bx_0, htt_bx_m1, htt_bx_m2 : std_logic_vector(MAX_ESUMS_BITS-1 downto 0);
    signal etm_bx_p2, etm_bx_p1, etm_bx_0, etm_bx_m1, etm_bx_m2 : std_logic_vector(MAX_ESUMS_BITS-1 downto 0);
    signal htm_bx_p2, htm_bx_p1, htm_bx_0, htm_bx_m1, htm_bx_m2 : std_logic_vector(MAX_ESUMS_BITS-1 downto 0);
    signal ext_cond_bx_p2, ext_cond_bx_p1, ext_cond_bx_0, ext_cond_bx_m1, ext_cond_bx_m2 : std_logic_vector(NR_EXTERNAL_CONDITIONS-1 downto 0);

    signal ext_cond_bx_p2_pipe, ext_cond_bx_p1_pipe, ext_cond_bx_0_pipe, ext_cond_bx_m1_pipe, ext_cond_bx_m2_pipe : std_logic_vector(NR_EXTERNAL_CONDITIONS-1 downto 0);

    signal algo : std_logic_vector(NR_ALGOS-1 downto 0) := (others => '0');

-- ==== Inserted by TME - begin =============================================================================================================
-- HB 2015-08-10: generated without TME for tests

-- Signal definition for differences for correlation conditions

-- inputs for et LUT for invariant mass
    signal eg_et_vector_bx_0: diff_inputs_array(0 to NR_EG_OBJECTS-1) := (others => (others => '0'));
    signal tau_et_vector_bx_0: diff_inputs_array(0 to NR_TAU_OBJECTS-1) := (others => (others => '0'));
    signal muon_pt_vector_bx_0: diff_inputs_array(0 to NR_MUON_OBJECTS-1) := (others => (others => '0'));
-- inputs for eta and phi differences
    signal eg_eta_integer_bx_0: diff_integer_inputs_array(0 to NR_EG_OBJECTS-1) := (others => 0);
    signal eg_phi_integer_bx_0: diff_integer_inputs_array(0 to NR_EG_OBJECTS-1) := (others => 0);
    signal tau_eta_integer_bx_0: diff_integer_inputs_array(0 to NR_TAU_OBJECTS-1) := (others => 0);
    signal tau_phi_integer_bx_0: diff_integer_inputs_array(0 to NR_TAU_OBJECTS-1) := (others => 0);
    signal etm_phi_integer_bx_0: diff_integer_inputs_array(0 to NR_ETM_OBJECTS-1) := (others => 0);
    signal htm_phi_integer_bx_0: diff_integer_inputs_array(0 to NR_HTM_OBJECTS-1) := (others => 0);
    signal muon_eta_integer_bx_0: diff_integer_inputs_array(0 to NR_MUON_OBJECTS-1) := (others => 0);
    signal muon_phi_integer_bx_0: diff_integer_inputs_array(0 to NR_MUON_OBJECTS-1) := (others => 0);
    signal eg_eta_conv_2_muon_eta_integer_bx_0: diff_integer_inputs_array(0 to NR_EG_OBJECTS-1) := (others => 0);
    signal eg_phi_conv_2_muon_phi_integer_bx_0: diff_integer_inputs_array(0 to NR_EG_OBJECTS-1) := (others => 0);
    signal htm_phi_conv_2_muon_phi_integer_bx_0: diff_integer_inputs_array(0 to NR_HTM_OBJECTS-1) := (others => 0);
-- outputs of eta and phi differences and inputs for cosh_deta, cos_dphi, deta and dphi LUTs
    signal diff_eg_eg_bx_0_bx_0_eta_integer: dim2_max_eta_range_array(0 to NR_EG_OBJECTS-1, 0 to NR_EG_OBJECTS-1) := (others => (others => 0));
    signal diff_eg_eg_bx_0_bx_0_phi_integer: dim2_max_phi_range_array(0 to NR_EG_OBJECTS-1, 0 to NR_EG_OBJECTS-1) := (others => (others => 0));
    signal diff_eg_tau_bx_0_bx_0_eta_integer: dim2_max_eta_range_array(0 to NR_EG_OBJECTS-1, 0 to NR_TAU_OBJECTS-1) := (others => (others => 0));
    signal diff_eg_tau_bx_0_bx_0_phi_integer: dim2_max_phi_range_array(0 to NR_EG_OBJECTS-1, 0 to NR_TAU_OBJECTS-1) := (others => (others => 0));
    signal diff_muon_muon_bx_0_bx_0_eta_integer: dim2_max_eta_range_array(0 to NR_MUON_OBJECTS-1, 0 to NR_MUON_OBJECTS-1) := (others => (others => 0));
    signal diff_muon_muon_bx_0_bx_0_phi_integer: dim2_max_phi_range_array(0 to NR_MUON_OBJECTS-1, 0 to NR_MUON_OBJECTS-1) := (others => (others => 0));
    signal diff_eg_muon_bx_0_bx_0_eta_integer: dim2_max_eta_range_array(0 to NR_EG_OBJECTS-1, 0 to NR_MUON_OBJECTS-1) := (others => (others => 0));
    signal diff_eg_muon_bx_0_bx_0_phi_integer: dim2_max_phi_range_array(0 to NR_EG_OBJECTS-1, 0 to NR_MUON_OBJECTS-1) := (others => (others => 0));
    signal diff_eg_etm_bx_0_bx_0_phi_integer: dim2_max_phi_range_array(0 to NR_EG_OBJECTS-1, 0 to NR_ETM_OBJECTS-1) := (others => (others => 0));
    signal diff_muon_htm_bx_0_bx_0_phi_integer: dim2_max_phi_range_array(0 to NR_MUON_OBJECTS-1, 0 to NR_HTM_OBJECTS-1) := (others => (others => 0));
-- outputs of calo_deta, calo_dphi, cosh_deta and cos_dphi LUTs, inputs for deta, dphi, dr and invariant mass calculations
    signal eg_eg_bx_0_bx_0_cosh_deta_vector : calo_cosh_cos_vector_array(0 to NR_EG_OBJECTS-1, 0 to NR_EG_OBJECTS-1) := (others => (others => (others => '0')));
    signal eg_eg_bx_0_bx_0_cos_dphi_vector : calo_cosh_cos_vector_array(0 to NR_EG_OBJECTS-1, 0 to NR_EG_OBJECTS-1) := (others => (others => (others => '0')));
    signal eg_tau_bx_0_bx_0_cosh_deta_vector : calo_cosh_cos_vector_array(0 to NR_EG_OBJECTS-1, 0 to NR_TAU_OBJECTS-1) := (others => (others => (others => '0')));
    signal eg_tau_bx_0_bx_0_cos_dphi_vector : calo_cosh_cos_vector_array(0 to NR_EG_OBJECTS-1, 0 to NR_TAU_OBJECTS-1) := (others => (others => (others => '0')));
    signal muon_muon_bx_0_bx_0_cosh_deta_vector : muon_cosh_cos_vector_array(0 to NR_MUON_OBJECTS-1, 0 to NR_MUON_OBJECTS-1) := (others => (others => (others => '0')));
    signal muon_muon_bx_0_bx_0_cos_dphi_vector : muon_cosh_cos_vector_array(0 to NR_MUON_OBJECTS-1, 0 to NR_MUON_OBJECTS-1) := (others => (others => (others => '0')));
    signal eg_muon_bx_0_bx_0_cosh_deta_vector : calo_muon_cosh_cos_vector_array(0 to NR_EG_OBJECTS-1, 0 to NR_MUON_OBJECTS-1) := (others => (others => (others => '0')));
    signal eg_muon_bx_0_bx_0_cos_dphi_vector : calo_muon_cosh_cos_vector_array(0 to NR_EG_OBJECTS-1, 0 to NR_MUON_OBJECTS-1) := (others => (others => (others => '0')));
    signal diff_eg_eg_bx_0_bx_0_eta_vector: deta_dphi_vector_array(0 to NR_EG_OBJECTS-1, 0 to NR_EG_OBJECTS-1) := (others => (others => (others => '0')));
    signal diff_eg_eg_bx_0_bx_0_phi_vector: deta_dphi_vector_array(0 to NR_EG_OBJECTS-1, 0 to NR_EG_OBJECTS-1) := (others => (others => (others => '0')));
    signal diff_eg_tau_bx_0_bx_0_eta_vector: deta_dphi_vector_array(0 to NR_EG_OBJECTS-1, 0 to NR_TAU_OBJECTS-1) := (others => (others => (others => '0')));
    signal diff_eg_tau_bx_0_bx_0_phi_vector: deta_dphi_vector_array(0 to NR_EG_OBJECTS-1, 0 to NR_TAU_OBJECTS-1) := (others => (others => (others => '0')));
    signal diff_muon_muon_bx_0_bx_0_eta_vector: deta_dphi_vector_array(0 to NR_MUON_OBJECTS-1, 0 to NR_MUON_OBJECTS-1) := (others => (others => (others => '0')));
    signal diff_muon_muon_bx_0_bx_0_phi_vector: deta_dphi_vector_array(0 to NR_MUON_OBJECTS-1, 0 to NR_MUON_OBJECTS-1) := (others => (others => (others => '0')));
    signal diff_eg_muon_bx_0_bx_0_eta_vector: deta_dphi_vector_array(0 to NR_EG_OBJECTS-1, 0 to NR_MUON_OBJECTS-1) := (others => (others => (others => '0')));
    signal diff_eg_muon_bx_0_bx_0_phi_vector: deta_dphi_vector_array(0 to NR_EG_OBJECTS-1, 0 to NR_MUON_OBJECTS-1) := (others => (others => (others => '0')));
    signal diff_eg_etm_bx_0_bx_0_phi_vector: deta_dphi_vector_array(0 to NR_EG_OBJECTS-1, 0 to NR_ETM_OBJECTS-1) := (others => (others => (others => '0')));
    signal diff_muon_htm_bx_0_bx_0_phi_vector: deta_dphi_vector_array(0 to NR_MUON_OBJECTS-1, 0 to NR_HTM_OBJECTS-1) := (others => (others => (others => '0')));

-- Signal definition for muon charge correlation (only once for all muon conditions) - written by TME on the base of templates
    signal ls_charcorr_double_bx_0_bx_0, os_charcorr_double_bx_0_bx_0 : muon_charcorr_double_array;
    signal ls_charcorr_triple_bx_0_bx_0, os_charcorr_triple_bx_0_bx_0 : muon_charcorr_triple_array;
    signal ls_charcorr_quad_bx_0_bx_0, os_charcorr_quad_bx_0_bx_0 : muon_charcorr_quad_array;

-- Signal definition for conditions names
    signal EG_16_TAU_8_DR : std_logic;
    signal EG_8_TAU_16_DETA_DPHI : std_logic;
    signal EG_24_EG_32_DETA_DPHI : std_logic;
    signal EG_24_EG_8P5_INV_MASS : std_logic;
    signal MUON_24_MUON_8P5_INV_MASS : std_logic;
    signal EG_24_MUON_8P5_INV_MASS : std_logic;
    signal EG_24_ETM_100_DETA_DPHI : std_logic;
    signal MUON_8P5_HTM_100_DETA_DPHI : std_logic;

-- Signal definition for algorithms names
    signal L1_EG_16_TAU_8_DR : std_logic;
    signal L1_EG_8_TAU_16_DETA_DPHI : std_logic;
    signal L1_EG_24_EG_32_DETA_DPHI : std_logic;
    signal L1_EG_24_EG_8P5_INV_MASS : std_logic;
    signal L1_MUON_24_MUON_8P5_INV_MASS : std_logic;
    signal L1_EG_24_MUON_8P5_INV_MASS : std_logic;
    signal L1_EG_24_ETM_100_DETA_DPHI : std_logic;
    signal L1_MUON_8P5_HTM_100_DETA_DPHI : std_logic;

-- ==== Inserted by TME - end ===============================================================================================================

begin

p_m_2_bx_pipeline_i: entity work.p_m_2_bx_pipeline
    port map(
        lhc_clk,
        muon_data, muon_bx_p2, muon_bx_p1, muon_bx_0, muon_bx_m1, muon_bx_m2,
        eg_data, eg_bx_p2, eg_bx_p1, eg_bx_0, eg_bx_m1, eg_bx_m2,
        jet_data, jet_bx_p2, jet_bx_p1, jet_bx_0, jet_bx_m1, jet_bx_m2,
        tau_data, tau_bx_p2, tau_bx_p1, tau_bx_0, tau_bx_m1, tau_bx_m2,
        ett_data, ett_bx_p2, ett_bx_p1, ett_bx_0, ett_bx_m1, ett_bx_m2,
        ht_data, htt_bx_p2, htt_bx_p1, htt_bx_0, htt_bx_m1, htt_bx_m2,
        etm_data, etm_bx_p2, etm_bx_p1, etm_bx_0, etm_bx_m1, etm_bx_m2,
        htm_data, htm_bx_p2, htm_bx_p1, htm_bx_0, htm_bx_m1, htm_bx_m2,
        external_conditions, ext_cond_bx_p2, ext_cond_bx_p1, ext_cond_bx_0, ext_cond_bx_m1, ext_cond_bx_m2
    );

-- Parameterized pipeline stages for External conditions, actually 2 stages (fixed) in conditions, see external_conditions_pipeline_stages
ext_cond_pipe_p: process(lhc_clk, ext_cond_bx_p2, ext_cond_bx_p1, ext_cond_bx_0, ext_cond_bx_m1, ext_cond_bx_m2)
    type ext_cond_pipe_array is array (0 to external_conditions_pipeline_stages+1) of std_logic_vector(NR_EXTERNAL_CONDITIONS-1 downto 0);
    variable ext_cond_bx_p2_pipe_temp : ext_cond_pipe_array := (others => (others => '0'));
    variable ext_cond_bx_p1_pipe_temp : ext_cond_pipe_array := (others => (others => '0'));
    variable ext_cond_bx_0_pipe_temp : ext_cond_pipe_array := (others => (others => '0'));
    variable ext_cond_bx_m1_pipe_temp : ext_cond_pipe_array := (others => (others => '0'));
    variable ext_cond_bx_m2_pipe_temp : ext_cond_pipe_array := (others => (others => '0'));
    begin
        ext_cond_bx_p2_pipe_temp(external_conditions_pipeline_stages+1) := ext_cond_bx_p2;
        ext_cond_bx_p1_pipe_temp(external_conditions_pipeline_stages+1) := ext_cond_bx_p1;
        ext_cond_bx_0_pipe_temp(external_conditions_pipeline_stages+1) := ext_cond_bx_0;
        ext_cond_bx_m1_pipe_temp(external_conditions_pipeline_stages+1) := ext_cond_bx_m1;
        ext_cond_bx_m2_pipe_temp(external_conditions_pipeline_stages+1) := ext_cond_bx_m2;
        if (external_conditions_pipeline_stages > 0) then 
            if (lhc_clk'event and (lhc_clk = '1') ) then
                ext_cond_bx_p2_pipe_temp(0 to external_conditions_pipeline_stages) := ext_cond_bx_p2_pipe_temp(1 to external_conditions_pipeline_stages+1);
                ext_cond_bx_p1_pipe_temp(0 to external_conditions_pipeline_stages) := ext_cond_bx_p1_pipe_temp(1 to external_conditions_pipeline_stages+1);
                ext_cond_bx_0_pipe_temp(0 to external_conditions_pipeline_stages) := ext_cond_bx_0_pipe_temp(1 to external_conditions_pipeline_stages+1);
                ext_cond_bx_m1_pipe_temp(0 to external_conditions_pipeline_stages) := ext_cond_bx_m1_pipe_temp(1 to external_conditions_pipeline_stages+1);
                ext_cond_bx_m2_pipe_temp(0 to external_conditions_pipeline_stages) := ext_cond_bx_m2_pipe_temp(1 to external_conditions_pipeline_stages+1);
            end if;
        end if;
        ext_cond_bx_p2_pipe <= ext_cond_bx_p2_pipe_temp(1); -- used pipe_temp(1) instead of pipe_temp(0), to prevent warnings in compilation
        ext_cond_bx_p1_pipe <= ext_cond_bx_p1_pipe_temp(1); -- used pipe_temp(1) instead of pipe_temp(0), to prevent warnings in compilation
        ext_cond_bx_0_pipe <= ext_cond_bx_0_pipe_temp(1); -- used pipe_temp(1) instead of pipe_temp(0), to prevent warnings in compilation
        ext_cond_bx_m1_pipe <= ext_cond_bx_m1_pipe_temp(1); -- used pipe_temp(1) instead of pipe_temp(0), to prevent warnings in compilation
        ext_cond_bx_m2_pipe <= ext_cond_bx_m2_pipe_temp(1); -- used pipe_temp(1) instead of pipe_temp(0), to prevent warnings in compilation
end process;

-- ==== Inserted by TME - begin =============================================================================================================

-- Instantiations of eta and phi conversions, only instantiated for correlation conditions with unequal eta/phi resolutions - written by TME on the base of templates

-- Instantiations of differences calculation for correlation conditions - written by TME on the base of templates
-- FUTURE : this feature needs to be implemented in menu compiler --

-- Instantiations of muon charge correlations (only once for all muon conditions) - written by TME on the base of templates

-- HB 2015-08-17: generated without TME for tests

-- Instantiations of calo objects parameter conversions
eg_data_bx_0_l: for i in 0 to NR_EG_OBJECTS-1 generate
    eg_et_vector_bx_0(i)(EG_PT_VECTOR_WIDTH-1 downto 0) <= CONV_STD_LOGIC_VECTOR(EG_PT_LUT(CONV_INTEGER(eg_bx_0(i)(D_S_I_EG_V2.et_high downto D_S_I_EG_V2.et_low))), EG_PT_VECTOR_WIDTH);
    eg_eta_integer_bx_0(i) <= CONV_INTEGER(signed(eg_bx_0(i)(D_S_I_EG_V2.eta_high downto D_S_I_EG_V2.eta_low)));
    eg_phi_integer_bx_0(i) <= CONV_INTEGER(eg_bx_0(i)(D_S_I_EG_V2.phi_high downto D_S_I_EG_V2.phi_low));
    eg_eta_conv_2_muon_eta_integer_bx_0(i) <= EG_ETA_CONV_2_MUON_ETA_LUT(CONV_INTEGER(eg_bx_0(i)(D_S_I_EG_V2.eta_high downto D_S_I_EG_V2.eta_low)));
    eg_phi_conv_2_muon_phi_integer_bx_0(i) <= EG_PHI_CONV_2_MUON_PHI_LUT(CONV_INTEGER(eg_bx_0(i)(D_S_I_EG_V2.phi_high downto D_S_I_EG_V2.phi_low)));
end generate;

tau_data_bx_0_l: for i in 0 to NR_TAU_OBJECTS-1 generate
    tau_et_vector_bx_0(i)(TAU_PT_VECTOR_WIDTH-1 downto 0) <= CONV_STD_LOGIC_VECTOR(TAU_PT_LUT(CONV_INTEGER(tau_bx_0(i)(D_S_I_TAU_V2.et_high downto D_S_I_TAU_V2.et_low))), TAU_PT_VECTOR_WIDTH);
    tau_eta_integer_bx_0(i) <= CONV_INTEGER(signed(tau_bx_0(i)(D_S_I_TAU_V2.eta_high downto D_S_I_TAU_V2.eta_low)));
    tau_phi_integer_bx_0(i) <= CONV_INTEGER(tau_bx_0(i)(D_S_I_TAU_V2.phi_high downto D_S_I_TAU_V2.phi_low));
end generate;

-- Instantiations of differences calculation for calo correlation conditions
diff_eg_tau_eta_bx_0_bx_0_i: entity work.sub_eta_integer_obj_vs_obj
    generic map(NR_EG_OBJECTS, NR_TAU_OBJECTS)
    port map(eg_eta_integer_bx_0, tau_eta_integer_bx_0, diff_eg_tau_bx_0_bx_0_eta_integer);      

diff_eg_tau_phi_bx_0_bx_0_i: entity work.sub_phi_integer_obj_vs_obj
    generic map(NR_EG_OBJECTS, NR_TAU_OBJECTS, CALO_PHI_HALF_RANGE_BINS)
    port map(eg_phi_integer_bx_0, tau_phi_integer_bx_0, diff_eg_tau_bx_0_bx_0_phi_integer);      

diff_eg_eg_eta_bx_0_bx_0_i: entity work.sub_eta_integer_obj_vs_obj
    generic map(NR_EG_OBJECTS, NR_EG_OBJECTS)
    port map(eg_eta_integer_bx_0, eg_eta_integer_bx_0, diff_eg_eg_bx_0_bx_0_eta_integer);      

diff_eg_eg_phi_bx_0_bx_0_i: entity work.sub_phi_integer_obj_vs_obj
    generic map(NR_EG_OBJECTS, NR_EG_OBJECTS, CALO_PHI_HALF_RANGE_BINS)
    port map(eg_phi_integer_bx_0, eg_phi_integer_bx_0, diff_eg_eg_bx_0_bx_0_phi_integer);      

eg_tau_bx_0_bx_0_l1: for i in 0 to NR_EG_OBJECTS-1 generate
    eg_tau_bx_0_bx_0_l2: for j in 0 to NR_TAU_OBJECTS-1 generate
	diff_eg_tau_bx_0_bx_0_eta_vector(i,j) <= CONV_STD_LOGIC_VECTOR(EG_TAU_DIFF_ETA_LUT(diff_eg_tau_bx_0_bx_0_eta_integer(i,j)),DETA_DPHI_VECTOR_WIDTH_ALL);
	diff_eg_tau_bx_0_bx_0_phi_vector(i,j) <= CONV_STD_LOGIC_VECTOR(EG_TAU_DIFF_PHI_LUT(diff_eg_tau_bx_0_bx_0_phi_integer(i,j)),DETA_DPHI_VECTOR_WIDTH_ALL);
    end generate eg_tau_bx_0_bx_0_l2;
end generate eg_tau_bx_0_bx_0_l1;

eg_eg_bx_0_bx_0_l1: for i in 0 to NR_EG_OBJECTS-1 generate
    eg_eg_bx_0_bx_0_l2: for j in 0 to NR_EG_OBJECTS-1 generate
	diff_eg_eg_bx_0_bx_0_eta_vector(i,j) <= CONV_STD_LOGIC_VECTOR(EG_EG_DIFF_ETA_LUT(diff_eg_eg_bx_0_bx_0_eta_integer(i,j)),DETA_DPHI_VECTOR_WIDTH_ALL);
	diff_eg_eg_bx_0_bx_0_phi_vector(i,j) <= CONV_STD_LOGIC_VECTOR(EG_EG_DIFF_PHI_LUT(diff_eg_eg_bx_0_bx_0_phi_integer(i,j)),DETA_DPHI_VECTOR_WIDTH_ALL);
    end generate eg_eg_bx_0_bx_0_l2;
end generate eg_eg_bx_0_bx_0_l1;

-- Instantiations of cosh_deta and cos_dphi luts for calos 
eg_eg_bx_0_bx_0_cosh_cos_l1: for i in 0 to NR_EG_OBJECTS-1 generate
    eg_eg_bx_0_bx_0_cosh_cos_l2: for j in 0 to NR_EG_OBJECTS-1 generate
	eg_eg_bx_0_bx_0_cosh_deta_vector(i,j) <= CONV_STD_LOGIC_VECTOR(EG_EG_COSH_DETA_LUT(diff_eg_eg_bx_0_bx_0_eta_integer(i,j)),EG_EG_COSH_COS_VECTOR_WIDTH);
	eg_eg_bx_0_bx_0_cos_dphi_vector(i,j) <= CONV_STD_LOGIC_VECTOR(EG_EG_COS_DPHI_LUT(diff_eg_eg_bx_0_bx_0_phi_integer(i,j)),EG_EG_COSH_COS_VECTOR_WIDTH);
    end generate eg_eg_bx_0_bx_0_cosh_cos_l2;
end generate eg_eg_bx_0_bx_0_cosh_cos_l1;

-- Instantiations of muon charge correlations (only once for all muon conditions in certain bx) 
muon_charge_correlations_bx_0_bx_0_i: entity work.muon_charge_correlations
    port map(muon_bx_0, muon_bx_0,
        ls_charcorr_double_bx_0_bx_0, os_charcorr_double_bx_0_bx_0,
        ls_charcorr_triple_bx_0_bx_0, os_charcorr_triple_bx_0_bx_0,
        ls_charcorr_quad_bx_0_bx_0, os_charcorr_quad_bx_0_bx_0);

-- Instantiations of muon objects parameter conversions
muon_data_bx_0_l: for i in 0 to NR_MUON_OBJECTS-1 generate
    muon_pt_vector_bx_0(i)(MUON_PT_VECTOR_WIDTH-1 downto 0) <= CONV_STD_LOGIC_VECTOR(MUON_PT_LUT(CONV_INTEGER(muon_bx_0(i)(D_S_I_MUON_V2.pt_high downto D_S_I_MUON_V2.pt_low))), MUON_PT_VECTOR_WIDTH);
    muon_eta_integer_bx_0(i) <= CONV_INTEGER(signed(muon_bx_0(i)(D_S_I_MUON_V2.eta_high downto D_S_I_MUON_V2.eta_low)));
    muon_phi_integer_bx_0(i) <= CONV_INTEGER(muon_bx_0(i)(D_S_I_MUON_V2.phi_high downto D_S_I_MUON_V2.phi_low));
end generate;

-- Instantiations of differences calculation for muon correlation conditions
diff_muon_muon_eta_bx_0_bx_0_i: entity work.sub_eta_integer_obj_vs_obj
    generic map(NR_MUON_OBJECTS, NR_MUON_OBJECTS)
    port map(muon_eta_integer_bx_0, muon_eta_integer_bx_0, diff_muon_muon_bx_0_bx_0_eta_integer);      

diff_muon_muon_phi_bx_0_bx_0_i: entity work.sub_phi_integer_obj_vs_obj
    generic map(NR_MUON_OBJECTS, NR_MUON_OBJECTS, MUON_PHI_HALF_RANGE_BINS)
    port map(muon_phi_integer_bx_0, muon_phi_integer_bx_0, diff_muon_muon_bx_0_bx_0_phi_integer);      
    
muon_muon_bx_0_bx_0_l1: for i in 0 to NR_MUON_OBJECTS-1 generate
    muon_muon_bx_0_bx_0_l2: for j in 0 to NR_MUON_OBJECTS-1 generate
	diff_muon_muon_bx_0_bx_0_eta_vector(i,j) <= CONV_STD_LOGIC_VECTOR(MUON_MUON_DIFF_ETA_LUT(diff_muon_muon_bx_0_bx_0_eta_integer(i,j)),DETA_DPHI_VECTOR_WIDTH_ALL);
	diff_muon_muon_bx_0_bx_0_phi_vector(i,j) <= CONV_STD_LOGIC_VECTOR(MUON_MUON_DIFF_PHI_LUT(diff_muon_muon_bx_0_bx_0_phi_integer(i,j)),DETA_DPHI_VECTOR_WIDTH_ALL);
    end generate muon_muon_bx_0_bx_0_l2;
end generate muon_muon_bx_0_bx_0_l1;

-- Instantiations of cosh_deta and cos_dphi luts for muon 
muon_muon_bx_0_bx_0_cosh_cos_l1: for i in 0 to NR_MUON_OBJECTS-1 generate
    muon_muon_bx_0_bx_0_cosh_cos_l2: for j in 0 to NR_MUON_OBJECTS-1 generate
	muon_muon_bx_0_bx_0_cosh_deta_vector(i,j) <= CONV_STD_LOGIC_VECTOR(MUON_MUON_COSH_DETA_LUT(diff_muon_muon_bx_0_bx_0_eta_integer(i,j)),MUON_MUON_COSH_COS_VECTOR_WIDTH);
	muon_muon_bx_0_bx_0_cos_dphi_vector(i,j) <= CONV_STD_LOGIC_VECTOR(MUON_MUON_COS_DPHI_LUT(diff_muon_muon_bx_0_bx_0_phi_integer(i,j)),MUON_MUON_COSH_COS_VECTOR_WIDTH);
    end generate muon_muon_bx_0_bx_0_cosh_cos_l2;
end generate muon_muon_bx_0_bx_0_cosh_cos_l1;

-- Instantiations of differences calculation for muon correlation conditions
diff_eg_muon_eta_bx_0_bx_0_i: entity work.sub_eta_integer_obj_vs_obj
    generic map(NR_EG_OBJECTS, NR_MUON_OBJECTS)
    port map(eg_eta_conv_2_muon_eta_integer_bx_0, muon_eta_integer_bx_0, diff_eg_muon_bx_0_bx_0_eta_integer);      

diff_eg_muon_phi_bx_0_bx_0_i: entity work.sub_phi_integer_obj_vs_obj
    generic map(NR_EG_OBJECTS, NR_MUON_OBJECTS, MUON_PHI_HALF_RANGE_BINS)
    port map(eg_phi_conv_2_muon_phi_integer_bx_0, muon_phi_integer_bx_0, diff_eg_muon_bx_0_bx_0_phi_integer);      
    
-- Instantiations of deta and dphi luts for calos 
eg_muon_bx_0_bx_0_l1: for i in 0 to NR_EG_OBJECTS-1 generate
    eg_muon_bx_0_bx_0_l2: for j in 0 to NR_MUON_OBJECTS-1 generate
	diff_eg_muon_bx_0_bx_0_eta_vector(i,j) <= CONV_STD_LOGIC_VECTOR(EG_MUON_DIFF_ETA_LUT(diff_eg_muon_bx_0_bx_0_eta_integer(i,j)),DETA_DPHI_VECTOR_WIDTH_ALL);
	diff_eg_muon_bx_0_bx_0_phi_vector(i,j) <= CONV_STD_LOGIC_VECTOR(EG_MUON_DIFF_PHI_LUT(diff_eg_muon_bx_0_bx_0_phi_integer(i,j)),DETA_DPHI_VECTOR_WIDTH_ALL);
    end generate eg_muon_bx_0_bx_0_l2;
end generate eg_muon_bx_0_bx_0_l1;

-- Instantiations of cosh_deta and cos_dphi luts for muon 
eg_muon_bx_0_bx_0_cosh_cos_l1: for i in 0 to NR_EG_OBJECTS-1 generate
    eg_muon_bx_0_bx_0_cosh_cos_l2: for j in 0 to NR_MUON_OBJECTS-1 generate
	eg_muon_bx_0_bx_0_cosh_deta_vector(i,j) <= CONV_STD_LOGIC_VECTOR(EG_MUON_COSH_DETA_LUT(diff_eg_muon_bx_0_bx_0_eta_integer(i,j)), EG_MUON_COSH_COS_VECTOR_WIDTH);
	eg_muon_bx_0_bx_0_cos_dphi_vector(i,j) <= CONV_STD_LOGIC_VECTOR(EG_MUON_COS_DPHI_LUT(diff_eg_muon_bx_0_bx_0_phi_integer(i,j)), EG_MUON_COSH_COS_VECTOR_WIDTH);
    end generate eg_muon_bx_0_bx_0_cosh_cos_l2;
end generate eg_muon_bx_0_bx_0_cosh_cos_l1;

etm_phi_integer_bx_0(0) <= CONV_INTEGER(etm_bx_0(D_S_I_ETM.phi_high downto D_S_I_ETM.phi_low));

diff_eg_etm_phi_bx_0_bx_0_i: entity work.sub_phi_integer_obj_vs_obj
    generic map(NR_EG_OBJECTS, 1, CALO_PHI_HALF_RANGE_BINS)
    port map(eg_phi_integer_bx_0, etm_phi_integer_bx_0, diff_eg_etm_bx_0_bx_0_phi_integer);      

eg_etm_bx_0_bx_0_l1: for i in 0 to NR_EG_OBJECTS-1 generate
    eg_etm_bx_0_bx_0_l2: for j in 0 to NR_ETM_OBJECTS-1 generate
	diff_eg_etm_bx_0_bx_0_phi_vector(i,j) <= CONV_STD_LOGIC_VECTOR(EG_ETM_DIFF_PHI_LUT(diff_eg_etm_bx_0_bx_0_phi_integer(i,j)),DETA_DPHI_VECTOR_WIDTH_ALL);
    end generate eg_etm_bx_0_bx_0_l2;
end generate eg_etm_bx_0_bx_0_l1;

htm_phi_conv_2_muon_phi_integer_bx_0(0) <= HTM_PHI_CONV_2_MUON_PHI_LUT(CONV_INTEGER(htm_bx_0(D_S_I_HTM.phi_high downto D_S_I_HTM.phi_low)));

diff_muon_htm_phi_bx_0_bx_0_i: entity work.sub_phi_integer_obj_vs_obj
    generic map(NR_MUON_OBJECTS, 1, MUON_PHI_HALF_RANGE_BINS)
    port map(muon_phi_integer_bx_0, htm_phi_conv_2_muon_phi_integer_bx_0, diff_muon_htm_bx_0_bx_0_phi_integer);      

muon_htm_bx_0_bx_0_l1: for i in 0 to NR_MUON_OBJECTS-1 generate
    muon_htm_bx_0_bx_0_l2: for j in 0 to NR_HTM_OBJECTS-1 generate
	diff_muon_htm_bx_0_bx_0_phi_vector(i,j) <= CONV_STD_LOGIC_VECTOR(MUON_HTM_DIFF_PHI_LUT(diff_muon_htm_bx_0_bx_0_phi_integer(i,j)),DETA_DPHI_VECTOR_WIDTH_ALL);
    end generate muon_htm_bx_0_bx_0_l2;
end generate muon_htm_bx_0_bx_0_l1;

-- =========================================================================================================================================
 
EG_16_TAU_8_DR_i: entity work.calo_calo_correlation_condition
     generic map(true,
        false, false, true, false, 
        NR_EG_OBJECTS, true, EG_TYPE,
        X"0020", 
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        X"F",
        NR_TAU_OBJECTS, true, TAU_TYPE,
        X"0010", 
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        X"F",
        0.000, 0.000, 0.000, 0.000,
        8113.8, 8113.6, DETA_DPHI_VECTOR_WIDTH_ALL, DETA_DPHI_PRECISION_ALL,
--         0.0, 0.0, INV_MASS_LIMITS_PRECISION_ALL, CALO_INV_MASS_ET_PRECISION, EG_PT_VECTOR_WIDTH, TAU_PT_VECTOR_WIDTH, CALO_INV_MASS_COSH_COS_PRECISION, CALO_COSH_COS_VECTOR_WIDTH
        0.0, 0.0, INV_MASS_LIMITS_PRECISION_ALL, EG_PT_VECTOR_WIDTH, TAU_PT_VECTOR_WIDTH, CALO_INV_MASS_COSH_COS_PRECISION, CALO_COSH_COS_VECTOR_WIDTH
    )
    port map(
        lhc_clk, eg_bx_0, tau_bx_0, diff_eg_tau_bx_0_bx_0_eta_vector, diff_eg_tau_bx_0_bx_0_phi_vector,
        eg_et_vector_bx_0, tau_et_vector_bx_0, eg_tau_bx_0_bx_0_cosh_deta_vector, eg_tau_bx_0_bx_0_cos_dphi_vector,        
	EG_16_TAU_8_DR
    );

EG_8_TAU_16_DETA_DPHI_i: entity work.calo_calo_correlation_condition
     generic map(true,
        true, true, false, false, 
        NR_EG_OBJECTS, true, EG_TYPE,
        X"0010", 
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        X"F",
        NR_TAU_OBJECTS, true, TAU_TYPE,
        X"0020", 
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        X"F",
        2.827, 2.827, 0.349, 0.349,
        0.0, 0.0, DETA_DPHI_VECTOR_WIDTH_ALL, DETA_DPHI_PRECISION_ALL,
        0.0, 0.0, INV_MASS_LIMITS_PRECISION_ALL, EG_PT_VECTOR_WIDTH, TAU_PT_VECTOR_WIDTH, CALO_INV_MASS_COSH_COS_PRECISION, CALO_COSH_COS_VECTOR_WIDTH
    )
    port map(
        lhc_clk, eg_bx_0, tau_bx_0, diff_eg_tau_bx_0_bx_0_eta_vector, diff_eg_tau_bx_0_bx_0_phi_vector,
        eg_et_vector_bx_0, tau_et_vector_bx_0, eg_tau_bx_0_bx_0_cosh_deta_vector, eg_tau_bx_0_bx_0_cos_dphi_vector,        
	EG_8_TAU_16_DETA_DPHI
    );

EG_24_EG_32_DETA_DPHI_i: entity work.calo_calo_correlation_condition
     generic map(true,
        true, true, false, false,
        NR_EG_OBJECTS, true, EG_TYPE,
        X"0030", 
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        X"F",
        NR_EG_OBJECTS, true, EG_TYPE,
        X"0040", 
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        X"F",
        3.0, 2.6, 0.4, 0.3,
        0.0, 0.0, DETA_DPHI_VECTOR_WIDTH_ALL, DETA_DPHI_PRECISION_ALL,
        0.0, 0.0, INV_MASS_LIMITS_PRECISION_ALL, EG_PT_VECTOR_WIDTH, EG_PT_VECTOR_WIDTH, CALO_INV_MASS_COSH_COS_PRECISION, CALO_COSH_COS_VECTOR_WIDTH
    )
    port map(
        lhc_clk, eg_bx_0, eg_bx_0, diff_eg_eg_bx_0_bx_0_eta_vector, diff_eg_eg_bx_0_bx_0_phi_vector,
        eg_et_vector_bx_0, eg_et_vector_bx_0, eg_eg_bx_0_bx_0_cosh_deta_vector, eg_eg_bx_0_bx_0_cos_dphi_vector,        
	EG_24_EG_32_DETA_DPHI
    );

EG_24_EG_8P5_INV_MASS_i: entity work.calo_calo_correlation_condition
     generic map(true,
        false, false, false, true,
        NR_EG_OBJECTS, true, EG_TYPE,
        X"0030", 
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        X"F",
        NR_EG_OBJECTS, true, EG_TYPE,
        X"0011", 
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        X"F",
        0.000, 0.000, 0.000, 0.000,
        0.0, 0.0, DETA_DPHI_VECTOR_WIDTH_ALL, DETA_DPHI_PRECISION_ALL,
        13128.5, 13128.3, INV_MASS_LIMITS_PRECISION_ALL, EG_PT_VECTOR_WIDTH, EG_PT_VECTOR_WIDTH, CALO_INV_MASS_COSH_COS_PRECISION, CALO_COSH_COS_VECTOR_WIDTH
    )
    port map(
        lhc_clk, eg_bx_0, eg_bx_0, diff_eg_eg_bx_0_bx_0_eta_vector, diff_eg_eg_bx_0_bx_0_phi_vector, 
        eg_et_vector_bx_0, eg_et_vector_bx_0, eg_eg_bx_0_bx_0_cosh_deta_vector, eg_eg_bx_0_bx_0_cos_dphi_vector,
        EG_24_EG_8P5_INV_MASS
    );

MUON_24_MUON_8P5_INV_MASS_i: entity work.muon_muon_correlation_condition
     generic map(true,
        false, false, false, true, 
        true, X"0030", 
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        "ign", X"FFFF", X"F", 
        true, X"0011", 
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        "ign", X"FFFF", X"F",         
        "ig",
        0.000, 0.000, 0.000, 0.000,
        0.0, 0.0, DETA_DPHI_VECTOR_WIDTH_ALL, DETA_DPHI_PRECISION_ALL,
        172.1, 171.1, INV_MASS_LIMITS_PRECISION_ALL, MUON_PT_VECTOR_WIDTH, MUON_INV_MASS_COSH_COS_PRECISION, MUON_MUON_COSH_COS_VECTOR_WIDTH
    )
    port map(
        lhc_clk, muon_bx_0, muon_bx_0, ls_charcorr_double_bx_0_bx_0, os_charcorr_double_bx_0_bx_0, diff_muon_muon_bx_0_bx_0_eta_vector, diff_muon_muon_bx_0_bx_0_eta_vector,
        muon_pt_vector_bx_0, muon_pt_vector_bx_0, muon_muon_bx_0_bx_0_cosh_deta_vector, muon_muon_bx_0_bx_0_cos_dphi_vector, MUON_24_MUON_8P5_INV_MASS
    );

EG_24_MUON_8P5_INV_MASS_i: entity work.calo_muon_correlation_condition
     generic map(
        false, false, false, true, 
        NR_EG_OBJECTS, true, EG_TYPE,
        X"0030", 
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        X"F",
        true, X"0011", 
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        "ign", X"FFFF", X"F",         
        0.000, 0.000, 0.000, 0.000,
        0.0, 0.0, DETA_DPHI_VECTOR_WIDTH_ALL, DETA_DPHI_PRECISION_ALL,
        165.5, 165.4, INV_MASS_LIMITS_PRECISION_ALL, EG_PT_VECTOR_WIDTH, MUON_PT_VECTOR_WIDTH, CALO_MUON_INV_MASS_COSH_COS_PRECISION, EG_MUON_COSH_COS_VECTOR_WIDTH
    )
    port map(
        lhc_clk, eg_bx_0, muon_bx_0, diff_eg_muon_bx_0_bx_0_eta_vector, diff_eg_muon_bx_0_bx_0_phi_vector,
        eg_et_vector_bx_0, muon_pt_vector_bx_0, eg_muon_bx_0_bx_0_cosh_deta_vector, eg_muon_bx_0_bx_0_cos_dphi_vector, EG_24_MUON_8P5_INV_MASS
    );

EG_24_ETM_100_DETA_DPHI_i: entity work.calo_esums_correlation_condition
     generic map(
        NR_EG_OBJECTS, true, EG_TYPE,
        X"0030", 
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        X"F",
        true, ETM_TYPE, X"00C8", 
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        0.349, 0.349, DETA_DPHI_VECTOR_WIDTH_ALL, DETA_DPHI_PRECISION_ALL
    )
    port map(
        lhc_clk, eg_bx_0, etm_bx_0, diff_eg_etm_bx_0_bx_0_phi_vector,
        EG_24_ETM_100_DETA_DPHI
    );

MUON_8P5_HTM_100_DETA_DPHI_i: entity work.muon_esums_correlation_condition
     generic map(
        true, X"0011", 
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        "ign", X"FFFF", X"F", 
        true, HTM_TYPE, X"00C8", 
        true, X"0000", X"0000",
        true, X"0000", X"0000",
        0.109, 0.109, DETA_DPHI_VECTOR_WIDTH_ALL, DETA_DPHI_PRECISION_ALL
    )
    port map(
        lhc_clk, muon_bx_0, htm_bx_0, diff_muon_htm_bx_0_bx_0_phi_vector,
        MUON_8P5_HTM_100_DETA_DPHI
    );

-- Instantiations of algorithms - written by TME on the base of templates
L1_EG_16_TAU_8_DR <= EG_16_TAU_8_DR;
algo(0) <= L1_EG_16_TAU_8_DR;

L1_EG_8_TAU_16_DETA_DPHI <= EG_8_TAU_16_DETA_DPHI;
algo(1) <= L1_EG_8_TAU_16_DETA_DPHI;

L1_EG_24_EG_32_DETA_DPHI <= EG_24_EG_32_DETA_DPHI;
algo(2) <= L1_EG_24_EG_32_DETA_DPHI;

L1_EG_24_EG_8P5_INV_MASS <= EG_24_EG_8P5_INV_MASS;
algo(3) <= L1_EG_24_EG_8P5_INV_MASS;

L1_MUON_24_MUON_8P5_INV_MASS <= MUON_24_MUON_8P5_INV_MASS;
algo(4) <= L1_MUON_24_MUON_8P5_INV_MASS;

L1_EG_24_MUON_8P5_INV_MASS <= EG_24_MUON_8P5_INV_MASS;
algo(5) <= L1_EG_24_MUON_8P5_INV_MASS;

L1_EG_24_ETM_100_DETA_DPHI <= EG_24_ETM_100_DETA_DPHI;
algo(6) <= L1_EG_24_ETM_100_DETA_DPHI;

L1_MUON_8P5_HTM_100_DETA_DPHI <= MUON_8P5_HTM_100_DETA_DPHI;
algo(7) <= L1_MUON_8P5_HTM_100_DETA_DPHI;

-- ==== Inserted by TME - end ===============================================================================================================

-- One pipeline stages for algorithms
algo_pipeline_p: process(lhc_clk, algo)
    begin
    if (lhc_clk'event and lhc_clk = '1') then
        algo_o <= algo;
    end if;
end process;

end architecture rtl;
