--------------------------------------------------------------------------------
-- Synthesizer : ISE 14.6
-- Platform    : Linux Ubuntu 10.04
-- Targets     : Synthese
--------------------------------------------------------------------------------
-- This work is held in copyright as an unpublished work by HEPHY (Institute
-- of High Energy Physics) All rights reserved.  This work may not be used
-- except by authorized licensees of HEPHY. This work is the
-- confidential information of HEPHY.
--------------------------------------------------------------------------------
-- $HeadURL: svn://heros.hephy.at/GlobalTriggerUpgrade/l1tm/L1Menu_CaloMuonCorrelation_2015_hb_test/vhdl/module_0/src/algo_mapping_rop.vhd $
-- $Date: 2015-08-14 11:00:09 +0200 (Fre, 14 Aug 2015) $
-- $Author: bergauer $
-- $Revision: 4149 $
--------------------------------------------------------------------------------

-- Desription:
-- Mapping of algo indexes for ROP

-- HB 2015-08-10: generated without TME for calo-muon-correlation tests

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

use work.gtl_pkg.ALL;
use work.gt_mp7_core_pkg.all;

entity algo_mapping_rop is
    port(
        lhc_clk : in std_logic;
        algo_before_prescaler : in std_logic_vector(NR_ALGOS-1 downto 0);
        algo_after_prescaler : in std_logic_vector(NR_ALGOS-1 downto 0);
        algo_after_finor_mask : in std_logic_vector(NR_ALGOS-1 downto 0);
        algo_before_prescaler_rop : out std_logic_vector(MAX_NR_ALGOS-1 downto 0);
        algo_after_prescaler_rop : out std_logic_vector(MAX_NR_ALGOS-1 downto 0);
        algo_after_finor_mask_rop : out std_logic_vector(MAX_NR_ALGOS-1 downto 0)
    );
end algo_mapping_rop;

architecture rtl of algo_mapping_rop is
    signal a_b_p: std_logic_vector(NR_ALGOS-1 downto 0);
    signal a_a_p: std_logic_vector(NR_ALGOS-1 downto 0);
    signal a_a_f: std_logic_vector(NR_ALGOS-1 downto 0);
    signal algo_before_prescaler_rop_int: std_logic_vector(MAX_NR_ALGOS-1 downto 0);
    signal algo_after_prescaler_rop_int: std_logic_vector(MAX_NR_ALGOS-1 downto 0);
    signal algo_after_finor_mask_rop_int: std_logic_vector(MAX_NR_ALGOS-1 downto 0);
begin

a_b_p <= algo_before_prescaler;
a_a_p <= algo_after_prescaler;
a_a_f <= algo_after_finor_mask;

-- ==== Inserted by TME - begin =============================================================================================================

algo_before_prescaler_rop_int <= (
0 => a_b_p(0),
1 => a_b_p(1),
2 => a_b_p(2),
3 => a_b_p(3),
4 => a_b_p(4),
5 => a_b_p(5),
6 => a_b_p(6),
7 => a_b_p(7),
 others => '0');

algo_after_prescaler_rop_int <= (
0 => a_a_p(0),
1 => a_a_p(1),
2 => a_a_p(2),
3 => a_a_p(3),
4 => a_a_p(4),
5 => a_a_p(5),
6 => a_a_p(6),
7 => a_a_p(7),
 others => '0');

algo_after_finor_mask_rop_int <= (
0 => a_a_f(0),
1 => a_a_f(1),
2 => a_a_f(2),
3 => a_a_f(3),
4 => a_a_f(4),
5 => a_a_f(5),
6 => a_a_f(6),
7 => a_a_f(7),
 others => '0');

-- ==== Inserted by TME - end ===============================================================================================================

algo_2_rop_p: process(lhc_clk, algo_before_prescaler_rop_int, algo_after_prescaler_rop_int, algo_after_finor_mask_rop_int)
    begin
    if lhc_clk'event and lhc_clk = '1' then
        algo_before_prescaler_rop <= algo_before_prescaler_rop_int;
        algo_after_prescaler_rop <= algo_after_prescaler_rop_int;
        algo_after_finor_mask_rop <= algo_after_finor_mask_rop_int;
    end if;
end process;

end architecture rtl;

